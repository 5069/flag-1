# FLAG plot for BSM
# Goes with first version of FLAG-3; 14/12/2015
# run with "python BSM_B4.py"
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm B_4$"			# plot title
plotnamestring	= "BSM_B4"			# filename for plot
plotxticks	= ([[0.7,0.9]])# locations for x-ticks
nminorticks	= 10			# number of intermediate minor ticks
					# 0 if none
yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.44,10.0,"$N_f=2+1$"],
#		   [+0.44,2.5,"$N_f=2$"]
		   [0.0,11.5," "],
		   [0.0,5.3," "],
		   [0.0,0.6," "]
		    ]
xlimits		= [0.60,1.10]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.5					# x-position for the data labels
#tpos = 0.87					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.78,0.045,0.045,0.02,0.02,  "ETM 15"	,['s','g','g',0,tpos]]
]
dat2p1=[
[0.69,	0.07,0.07,0.01,0.01,  "RBC/UKQCD 12E"	,['s','w','r',0,tpos]],
[0.981,	0.061,0.061,0.003,0.003,  "SWME 14C"   	,['s','l','g',0,tpos]],
[0.981,	0.062,0.062,0.003,0.003,  "SWME 15A"   	,['s','g','g',0,tpos]],
[0.920,	0.020,0.020,0.012,0.012,  "RBC/UKQCD 16"   	,['s','g','g',0,tpos]]
]
dat2=[                          
[0.76,0.03,0.03,0.02,0.02, "ETM 12D" 		,['s','g','g',0,tpos]] 
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[0.76,0.03,0.03,0.00,0.000,"our estimate for $N_f=2$",['s','k','k',0,tpos],0],
[0.926,0.019,0.019,0.00,0.000," ",['s','k','k',0,tpos],0],
[0.78,0.045,0.045,0.00,0.000,"our estimate for $N_f=2+1+1$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


