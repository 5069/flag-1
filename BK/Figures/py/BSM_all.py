# FLAG plot for BSM
# Goes with first version of FLAG-3; 14/12/2015
# run with "python BSM_B2.py"
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm B_2$"			# plot title
plotnamestring	= "BSM_all"			# filename for plot
plotxticks	= ([[0.40,0.50]])# locations for x-ticks
nminorticks	= 10			# number of intermediate minor ticks
					# 0 if none
yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.44,10.0,"$N_f=2+1$"],
#		   [+0.44,2.5,"$N_f=2$"]
                  [+0.30,11.5,"$\\rm N_f=2+1+1$"],
                   [0.30,5.0,"$\\rm N_f=2+1$"],
                   [0.30,0.5,"$\\rm N_f=2$"]		    ]
xlimits		= [0.35,0.60]			# plot's xlimits
logo		= 'none'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.1					# x-position for the data labels
#tpos = 0.87					# x-position for the data labels
MULTIPLE = 1	
SHIFT = -0.1
FS=10
SIZE=[12*2.54,6*2.54]
REMOVEWHITESPACE=1
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.46,0.032,0.032,0.01,0.01,  ""	,['s','g','g',0,tpos]]
]
dat2p1=[
[0.43,	0.05,0.05,0.01,0.01,  ""	,['s','w','r',0,tpos]],
[0.525,	0.023,0.023,0.001,0.001,  ""   	,['s','l','g',0,tpos]],
[0.525,	0.023,0.023,0.001,0.001,  ""   	,['s','g','g',0,tpos]],
[0.488,	0.018,0.018,0.007,0.007,  ""   	,['s','g','g',0,tpos]]
]
dat2=[                          
[0.47,0.022,0.022,0.01,0.01, "" 		,['s','g','g',0,tpos]] 
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[0.47,0.022,0.022,0.00,0.000,"",['s','k','k',0,tpos],0],
[0.502,0.014,0.014,0.00,0.000,"",['s','k','k',0,tpos],0],
[0.46,0.032,0.032,0.00,0.000,"",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 

# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
#ax=fig.add_subplot(1,4,1,aspect=1.01)
ax = plt.subplot2grid((1,5), (0, 0), colspan=1)
#ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
execfile('FLAGplot.py')

# R.H.S. PLOT
# layout specifications
titlestring	= "$\\rm B_3$"			# plot title
plotxticks	= ([[0.65,0.85]])# locations for x-ticks
nminorticks	= 10			# number of intermediate minor ticks
					# 0 if none
yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.44,10.0,"$N_f=2+1$"],
#		   [+0.44,2.5,"$N_f=2$"]
		   [0.0,11.5," "],
		   [0.0,5.3," "],
		   [0.0,0.6," "]
		    ]
xlimits		= [0.60,0.95]			# plot's xlimits
logo		= 'none'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.1					# x-position for the data labels
#tpos = 0.87					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.79,0.054,0.054,0.02,0.02,  ""	,['s','g','g',0,tpos]]
]
dat2p1=[
[0.75,	0.092,0.092,0.02,0.02,  ""	,['s','w','r',0,tpos]],
[0.774,	0.064,0.064,0.006,0.006,  ""   	,['s','l','g',0,tpos]],
[0.773,	0.035,0.035,0.006,0.006,  ""   	,['s','g','g',0,tpos]],
[0.743,	0.066,0.066,0.014,0.014,  ""   	,['s','g','g',0,tpos]]
]
dat2=[                          
[0.78,0.045,0.045,0.04,0.04, "" 		,['s','g','g',0,tpos]] 
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[0.78,0.045,0.045,0.00,0.000,"",['s','k','k',0,tpos],0],
[0.766,0.032,0.032,0.00,0.000,"",['s','k','k',0,tpos],0],
[0.79,0.054,0.054,0.00,0.000,"",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]
#ax=fig.add_subplot(1,4,2)
ax = plt.subplot2grid((1,5), (0, 1), colspan=1)

ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
execfile('FLAGplot.py')


titlestring	= "$\\rm B_4$"			# plot title
plotxticks	= ([[0.7,0.9]])# locations for x-ticks
nminorticks	= 10			# number of intermediate minor ticks
					# 0 if none
yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.44,10.0,"$N_f=2+1$"],
#		   [+0.44,2.5,"$N_f=2$"]
		   [0.0,11.5," "],
		   [0.0,5.3," "],
		   [0.0,0.6," "]
		    ]
xlimits		= [0.60,1.10]			# plot's xlimits
logo		= 'none'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.5					# x-position for the data labels
#tpos = 0.87					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.78,0.045,0.045,0.02,0.02,  ""	,['s','g','g',0,tpos]]
]
dat2p1=[
[0.69,	0.07,0.07,0.01,0.01,  ""	,['s','w','r',0,tpos]],
[0.981,	0.061,0.061,0.003,0.003,  ""   	,['s','l','g',0,tpos]],
[0.981,	0.062,0.062,0.003,0.003,  ""   	,['s','g','g',0,tpos]],
[0.920,	0.020,0.020,0.012,0.012,  ""   	,['s','g','g',0,tpos]]
]
dat2=[                          
[0.76,0.03,0.03,0.02,0.02, "" 		,['s','g','g',0,tpos]] 
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[0.76,0.03,0.03,0.00,0.000,"",['s','k','k',0,tpos],0],
[0.926,0.019,0.019,0.00,0.000,"",['s','k','k',0,tpos],0],
[0.78,0.045,0.045,0.00,0.000,"",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]

#ax=fig.add_subplot(1,4,3)
ax = plt.subplot2grid((1,5), (0, 2), colspan=1)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
execfile('FLAGplot.py')


# layout specifications
titlestring	= "$\\hspace{-3}\\rm B_5$"			# plot title
#plotnamestring	= "BK"			# filename for plot
plotxticks	= ([[0.40,0.60,0.80]])# locations for x-ticks
nminorticks	= 4			# number of intermediate minor ticks
					# 0 if none
yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.44,10.0,"$N_f=2+1$"],
#		   [+0.44,2.5,"$N_f=2$"]
		   [+0.60,24.3," "],
		   [+0.60,14.3," "],
		   [+0.60,2.5," "]
		    ]
xlimits		= [0.30,1.6]			# plot's xlimits
#logo		= 'upper right'			# either of 'upper right'
logo		= 'bespoke'			# either of 'upper right'
LOGOxpos	=0.777
LOGOypos	=0.865
LOGOPxpos	=.0
LOGOPypos	=+250
#logo		= 'none'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
#tpos = 1.1					# x-position for the data labels
tpos = 0.9					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.49,0.04,0.04,0.03,0.03,  "ETM 15"	,['s','g','g',0,tpos]]
]
dat2p1=[
[0.47,	0.06,0.06,0.01,0.01,  "RBC/UKQCD 12E"	,['s','w','r',0,tpos]],
[0.748,	0.080,0.080,0.009,0.009,  "SWME 14C"   	,['s','l','g',0,tpos]],
[0.751,	0.068,0.068,0.007,0.007,  "SWME 15A"   	,['s','g','g',0,tpos]],
[0.707,	0.045,0.045,0.008,0.008,  "RBC/UKQCD 16"   	,['s','g','g',0,tpos]]
]
dat2=[                          
[0.58,0.028,0.028,0.02,0.02, "ETM 12D" 		,['s','g','g',0,tpos]] 
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[0.58,0.028,0.028,0.00,0.000,"our average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
[0.720,0.038,0.038,0.00,0.000,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[0.49,0.042,0.042,0.00,0.000,"our average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]

#ax=fig.add_subplot(1,4,4)
ax = plt.subplot2grid((1,5), (0, 3), colspan=2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
execfile('FLAGplot.py')

