tar -cvf FLAG_arXivSubmission.tar \
FLAG.bib \
FLAG_master.tex \
summarytable.tex \
acknowledgment.tex \
defs.tex \
introduction.tex \
Tables/Summarytable*.tex \
quality_criteria.tex \
qmass/*.tex \
qmass/Figures/*.eps \
Vudus/*.tex \
Vudus/Figures/*.eps \
LEC/*.tex \
LEC/Figures/*.eps \
BK/*.tex \
BK/Figures/*.eps \
HQ/*.tex \
HQ/macros_static.sty \
HQ/HQSubsections/*.tex \
HQ/AppendixTables/*.tex \
HQ/Figures/*.eps \
Alpha_s/*.tex \
Alpha_s/Figures/R4.eps \
Alpha_s/Figures/R4Flag3.eps \
Alpha_s/Figures/r0LamMSbar.eps \
Alpha_s/Figures/alphasMSbarZ.eps \
Glossary/gloss_app.tex \
JHEP.bst


