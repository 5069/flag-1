import re
import os
import numpy as np
import string
import spires
# This script generates individual bibtex files for results entering FLAG averages. These files are published together with the
# corresponding FLAG plots and summary table on the FLAG website
# 12.2020 juettner@soton.ac.uk


# LATEX TABLE HEADER/FOOTER
sheader="\\begin{sidewaystable}[h]\n\
\\vspace{-1cm}\n\
\\centering\n\
\\begin{tabular}{|l|l||l|l||l|l||l|l||l|}\n\
\hline\n\
Quantity \\rule[-0.2cm]{0cm}{0.6cm}    & \\hspace{-1.5mm}Sect.\\hspace{-2mm} &$N_f=2+1+1$ & Refs. &  $N_f=2+1$ & Refs. &$N_f=2$ &Refs.\\\\ \n\
\\hline\n\
\\hline"
sheader2="\\documentclass[10pt]{article}\n\
\\usepackage[hscale=0.7,vscale=0.8]{geometry}\n\
\\usepackage{rotating}\n\
\\usepackage{color}\n\
\\usepackage{amssymb}\n\
\\input{../defs.tex}\n\
\\begin{document}\n"
sfooter="\end{tabular}\end{sidewaystable}"

class FLAGresult:
    # CLASS hodling results
    def __init__(self, tag, quantity,reference,Nf,result,section,units ):
        self.tag = tag
        self.quantity = quantity
        self.reference = reference
        self.Nf = Nf
        self.result = result
        self.section = section
        self.units = units

    def Nresults(self):
        if re.findall('cite',self.reference):
         self.res = re.split('{',self.reference)[1]
         self.res = re.split('}',self.res)[0]
         return len(re.split(',',self.res))
 	else:
	 return 'can\'t count Nresults -- no cite'
    
    def printall(self):
        print self.tag
        print self.quantity
        print self.reference 
        print self.Nf
        print self.result
 


def getline(line):
 # reads a line from the macro block now accompanying every FLAG result 
 splitline=re.split('&',line)
 tag=splitline[0]
 tag=re.split('%',tag)[1].replace("\t","").replace(" ","")
 data=[]
 for i in splitline[1:]:
  if (re.match('END\n',i)) or (re.match('END\r\n',i)):
   break
  dum=i.replace(" ","")
  data.append(dum.replace("\t",""))
 return tag,data

def removeNf(dat):
 # the observables in the LEC section contain an explicit N_f which I would like to remove
 dat=re.sub(r"\\big\|_\{(.*?)\}","",dat)
 return dat

def format_error(res): # make sure all results are displayed with the same
		    # style for the error (if only mantissa, remove coma)
 dum = re.split("\(",res)
 serr=''
 err=0.
 for s in dum[1:]:
  ss = re.sub("\)","",s)
  fs = float(ss)
  if (fs<1.):
   serr+="("+str(int(ss[ss.find(".")+1:]))+")"
  else:
   serr+="("+ss+")"
 return dum[0]+serr

def getresult(line):
 # reads a line from the results block (usually the equation containing the result) and
 # extracts the part contained between the delimiters FLAGAVBEGIN and FLAGAVEND
 # the text between the delimiters is expected to be of the form
 # 'variable name' = 'result'
 res=[] #/{(.*?)}/
 if ("|_{\Nf=2" in line):
   line=	 removeNf(line)
 splitline=re.split('FLAGAV',line.replace("&",""))
 if(len(splitline)>1):
  for i in range(len(splitline)):
   if re.match('BEGIN',splitline[i]):
	 dummy=re.split('BEGIN',splitline[i])[1][:-1]
	 #print dummy
	 if not re.search('alpha',dummy):
	  dummy=dummy.replace("\\al","")
	 dummy=dummy.replace("\\!","")
	 res.append(re.split('=',dummy))
	 for j in range(len(res[0])): 
          #res[0][j]=res[0][j].replace(" ","")	      
 	  if re.match("\(",res[0][j]):          # some dimensionful results are quoted like (###\pm#)MeV
	   dummy =  re.sub(r"^\(","",res[0][j]) # the following gets rid of the brackets
	   res[0][j]=dummy.rsplit(')')[-2]
	  if j==1:
 	   if re.search("\\pm",res[0][j]):
 	    res[0][j]=res[0][j].replace("\\;","") # special case where line ends with \;
	    res[0][j]=res[0][j].replace(",","")  # special case where line ends with ,
	    dummy = re.sub(r"\\pm","(",res[0][j])# some results provide error with \pm - this
	    res[0][j]=dummy+")"                  # rewrites results with errors in brackets
	  res[0][j]=re.sub("\~","",res[0][j])
  #print res[0][1],
  res[0][1]=format_error(res[0][1])
 return res

def findtag(allres,tag):
 # searches for a result with a given tag
 found=[]
 for i in range(len(allres)):
  if (allres[i].tag==tag):
    found.append(allres[i])
 return found

def make_table_line(outfilehandle,allres,tag):
 # generate a data line for the results table for a given tag
 current = findtag(allres,tag)
 s_2=''
 s_2p1=''
 s_2p1p1=''
 s5=''
 b2=0
 b2p1=0
 b2p1p1=0
 b5=0
 sdummy="&&"
 for i in range(len(current)):
  sN = str( current[i].Nresults())
  s="$"+current[i].result+"$&"+current[i].reference+"&"
  if current[i].Nf=='2':
   s2=s
   b2=1
  elif current[i].Nf=='2+1':
   s2p1=s
   b2p1=1
  elif current[i].Nf=='2+1+1':
   s2p1p1=s
   b2p1p1=1
  elif current[i].Nf=='5': # for alpha_s
   s="\\multicolumn{3}{c|}{$"+current[i].result+"$}&"+current[i].reference+"&"
   s5=s
   b5=1
  s="$"+current[i].quantity+"$"+current[i].units.replace("'","")+"&"+current[i].section+"}&"
  if b2p1p1:
   s+=s2p1p1
  else:
   s+=sdummy
  if b2p1:
   s+=s2p1
  else:
   s+=sdummy
  if b2:
   s+=s2[:-1]
  else:
   s+=sdummy[:-1]
  if (b2==0) & (b2p1==0) & (b2p1p1==0):
   s="$"+current[i].quantity+"$"+current[i].units.replace("'","")+"&"+current[i].section+"}&"
   s+=s5+"&"
 s+="\\\\[1mm]\n"
 #outfilehandle.write(s)


def parse_file(filename):
 # parses a FLAG section file for macros and results
 with open(filename) as f:
    FORMULA=[]
    itson=0
    itsonF=0
    allres=[]
    for line in f:
	    if(re.search("\label{sec:",line)):
             current=re.split('label{sec:',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{sec:'+currentsec
            elif(re.search("\label{s:",line)):
             current=re.split('label{s:',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{s:'+currentsec
            elif(re.search("\label{sec_",line)):
             current=re.split('label{sec_',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{sec_'+currentsec
            elif(re.search("\label{subsec",line)):
             current=re.split('label{subsec',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{subsec'+currentsec
	    if(re.match("%FLAGRESULTFORMULA BEGIN",line)):
		    results=[]
		    itsonF=1
	    elif(re.match("%FLAGRESULTFORMULA END",line)):
		    itsonF=0
		    for i in range(len(dict0['TAG'])):
	             unit=(dict0['UNITS'][i] if (dict0['UNITS'][i]!='1') else '')
		     #print dict0['TAG'][i],results[i][0][0],dict0['REFS'][i], dict0['FLAVOURs'][i],results[i][0][1],currentsec, unit
		     #print i,dict0['TAG'][i],results[i][0][0],dict0['REFS'][i],results[i][0][1]
                     allres.append(FLAGresult(
			dict0['TAG'][i],results[i][0][0],dict0['REFS'][i],
			dict0['FLAVOURs'][i],results[i][0][1],currentsec, unit))
	    elif(re.match("%FLAGRESULT BEGIN",line)):
    	            dict0={}
		    itson=1
            elif(re.match("%FLAGRESULT END",line)):
                    itson=0
            elif(itson==1):
             tag,items = getline(line)
             dict0[tag]=items
            elif(itsonF==1):
             FORMULA.append(line)
	     dummy = getresult(line)
	     if dummy:
 	      results.append(dummy)
 return allres,FORMULA

def make_ref_latex(FF):
 # this routine generates a latex file containing all 
 # results but in latex code - this is kind of outdated now 
 # that I have moved to explicitly extracting the results from 
 # the equation blocks in the latex source rather than from a
 # hand edited macro-block (error prone)
 f=open('allFLAGresults.tex','w')
 f.write("\\documentclass[10pt]{article}\n")
 f.write("\\usepackage[hscale=0.7,vscale=0.8]{geometry}\n")
 f.write("\\usepackage{rotating}\n")
 f.write("\\usepackage{color}\n")
 f.write("\\input{../defs.tex}\n")
 f.write("\\begin{document}\n")
 for i in range(len(FF)):
  for j in FF[i]:
    f.write(j)
 f.write("\\end{document}\n")
 f.close()
def get_collaborationtag(arXivref):
	itson=0
	for line in open('../FLAG.bib','r'):
		if itson==1:
   		  collaborationtaglist =  re.findall(r"\[([^}]+)\]", line)
		  if collaborationtaglist:
		   return collaborationtaglist[0]
	          else:
		   return 'none'
		if re.findall(arXivref,line):
			itson=1
			
def get_all_in_file(filename):
 # parses file and does some post-processing (like determining unique tags) 
 allres,FORMULA=parse_file(filename)
 alltags=[]
 for i in range(len(allres)):
  alltags.append(allres[i].tag)
  if re.findall('cite',allres[i].reference):
   print allres[i].reference
   arXivref =  re.split(',',re.findall(r"\{([^}]+)\}", allres[i].reference)[0])
   # check if plot in subdirectory. If yes, remove substructure
   bibtexfilename=re.sub('/',"_over_",allres[i].tag)+'.bib'
   h=open('../web/'+bibtexfilename,'a')
   h.write('%%%% Nf='+allres[i].Nf+'\n')
   for j in range(len(arXivref)):
    collaboration_tag=get_collaborationtag(arXivref[j])
    h.write('%%%% FLAG collaboration tag: '+collaboration_tag+'\n')
    rType,ref=spires.findRefType(arXivref[j])
    h.write(spires.getBiBTeX(ref,rType))
    h.write('\n')
   h.close()

 Aalltags = np.array(alltags)
 _, idx = np.unique(Aalltags, return_index=True) # unique alone would change order
 uniquetags = Aalltags[np.sort(idx)]
 #for i in uniquetags:
 # make_table_line(outfilehandle,allres,i)
 #return FORMULA

################################################################################
## START ACTUAL WORK OF COMPILING ALL RESULTS ##################################
################################################################################
# TABLE THREE
################################################################################
# remove old bibtex files
os.system('rm ../web/*.bib')
filenames=["../NME/NME.tex"]
FF=[]
for i in filenames:
 FF.append(get_all_in_file(i))
################################################################################
# TABLE TWO
################################################################################
# remove old bibtex files
os.system('rm ../web/*.bib')
filenames=["../HQ/HQSubsections/DL.tex",
	   "../HQ/HQSubsections/DSL.tex",
	   "../HQ/HQSubsections/BL.tex",
	   "../HQ/HQSubsections/DCKM.tex",
	   "../HQ/HQSubsections/BB.tex"]
#	   "../Alpha_s/s9.tex"]
FF=[]
for i in filenames:
 FF.append(get_all_in_file(i))
get_all_in_file("../Alpha_s/s10.tex")

################################################################################
# TABLE ONE
################################################################################
filenames=["../qmass/qmass_mudms.tex",
	   "../qmass/qmass_mc.tex",
	   "../qmass/qmass_mb.tex",
	   "../Vudus/VudVus.tex",
	   "../LEC/LECs.tex",
	   "../BK/BK.tex"]

FF=[]
for i in filenames:
 FF.append(get_all_in_file(i))
