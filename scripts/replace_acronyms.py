import os, re,fnmatch,shutil
# with FLAG-3 we have asked the WGs to use acronyms of the form 'FNAL/MILC 15 ' where 
# label is the collaboration name and the 2nd part is the unique label that identifies
# the bibtex entry.
# The following script parses the whole FLAG-draft and replaces all the labels according
# to the mapping defined in 'acronym_list_forscritp.txt'
# AJ 2016 juettner@soton.ac.uk

def find(pattern, path):
    """
     find(pattern, path)
     make list of all tex and python files
    """
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def findnth(source, target, n):
    """
     findnth(source, target, n)
     find nth occurence of target
    """
    num = 0
    start = -1
    while num < n:
        start = source.find(target, start+1)
        if start == -1: return -1
        num += 1
    return start

def replacenth(source, old, new, n):
    """
     replacenth(source, old, new, n)
     replace nth occurence of old
    """
    p = findnth(source, old, n)
    if n == -1: return source
    return source[:p] + new + source[p+len(old):]

def process_line(line,pattern,new):
 """
  process_line(line,pattern,new):
  parse line and find occurences of pattern and replace it with new
 """
 # check if pattern in line
 if pattern in line:
  # check if pattern part of cite command
  all = re.findall(r"\\cite\{.*"+pattern+".*\}|\\citep\{.*"+pattern+".*\}|"+pattern, line) # match 
  incite = re.findall(r"\\cite\{.*"+pattern+".*\}|\\citep\{.*"+pattern+".*\}", line) # match 
  #all = re.findall(r""+pattern+"", line)
  Nincite=len(incite) 
  Nall=len(all) 
  #print Nall,Nincite,all,incite
  Nnotincite=Nall-Nincite
  #print Nnotincite,Nincite, Nall,all,line
  cnt=1
  res=line
  if Nnotincite>0:
   print "OLD: ",re.sub("\\n","",res)
   for i in all:
    if "cite" not in i: 
     res=replacenth(res,pattern,new,cnt) # counter must be one since I do recursion
    else:
     cnt+=1
   print "NEW: ",res
 else: # do nothing if no match 
  res=line
 return res 
    


def process_acronym_file(filename):
 """
  process_acronym_file(filename)
  parser for acronym file (in base directory of FLAG project called acronym_list_forscript.txt)
 """
 res=[]
 f=open(filename,'r')
 for line in f:
  if ((not re.match(r'#',line)) and (not re.match(r' ',line)) and (not re.match(r'\n',line))): 
   dum=re.split('&',line)
   if len(re.sub(' ','',dum[3]))>1:
    newID=re.sub("\\n","",dum[3]) # remove trailing newline
    collab=re.sub("\\t","",dum[0]) # remove trailing tab
    ss = [i for i in re.split(' ',dum[0]) if ((i!='') and (i!='\t'))]
    #print re.split(' ',dum[0]),re.split(' ',dum[0])[-1],ss
    arXivID=re.sub("\\t","",ss[-1]) # remove trailing tab
    res.append([arXivID,re.sub("\\t","",newID)])
 f.close()
 return res



# DO THE JOB:
# process acronym replacement list
replacementlist = process_acronym_file('../acronym_list_forscript.txt')
# make list of tex and python files in the project
all_py_files  =find('*.py', '../')
all_tex_files =find('*.tex','../')

# do replacements in FLAG.bib (mostly in 'author' line)
i='../FLAG.bib'
g=open('dummy','w+')
f=open(i,'r')
for line in f:
 localcopy=line
 for j in replacementlist:
  if re.findall('author',line):
   localcopy=process_line(localcopy,j[0],j[1])
 #print localcopy[:-1]
 g.write(localcopy)
f.close()
g.close()
os.remove(i)
shutil.move('dummy',i)


# do replacements in all .py (plots) and .tex files
# note that you will still need to recompile all plots!
for files in [all_py_files,all_tex_files]:
 for i in files:
  g=open('dummy','w+')
  f=open(i,'r+')
  for line in f:
   localcopy=line
   for j in replacementlist:
    localcopy=process_line(localcopy,j[0],j[1])
   #print localcopy[:-1]
   g.write(localcopy)
  f.close()
  g.close()
  os.remove(i)
  shutil.move('dummy',i)

