import numpy as np
import spires
import re

path='../FLAG.bib'

for line in open(path, 'r'):
   match_eprint = re.compile(r'eprint')
   if match_eprint.findall(line):
    eprint=re.findall('\".*',line)[0][1:-2]
    rType, ref = spires.findRefType(eprint)
    print spires.getBiBTeX(ref,rType)
