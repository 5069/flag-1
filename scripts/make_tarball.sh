tar -cvf FLAG4_Review.tar \
FLAG_master.bbl \
*.tex \
Tables/*.tex \
Glossary/*.tex \
HQ/*.tex \
HQ/AppendixTables/*.tex \
HQ/HQSubsections/*.tex \
HQ/Figures/*.eps \
HQ/macros_static.sty \
LEC/*.tex \
LEC/Figures/*.eps \
Vudus/*.tex \
Vudus/Figures/*.eps \
qmass/*.tex \
qmass/Figures/*.eps \
BK/*.tex \
BK/Figures/*.eps \
Alpha_s/*.tex \
Alpha_s/Figures/r0LamMSbar.eps Alpha_s/Figures/alphasMSbarZ.eps Alpha_s/Figures/scaledep_alpha_R4.eps Alpha_s/Figures/Rm0p3R6m0p5current2pt.eps \
NME/*.tex \
NME/Tables/*.tex \
NME/Figures/plots/*.eps \
NME/Figures/disconnected.eps \
NME/Figures/2-pt_with_t.eps \
NME/Figures/connected_with_t.eps 

#HQ/Figures/*.eps \
#LEC/Figures/*.eps \
#Vudus/Figures/*.eps \
#qmass/Figures/*.eps \
#BK/Figures/*.eps \
#Alpha_s/Figures/*.eps \
#NME/Figures/plots/*.eps \

#hypernat.sty 
#JHEP_FLAG.bst

