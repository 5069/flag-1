List of pending issues:


* [arXiv:1312.1228] -> should become FNAL/MILC 13YYY

* FNAL/MILC 12 [145] on pg.45 has acronym [MILC 12] in the references, should be FNAL/MILC 12XX. This is wrong already in FLAG2. 

* []     [arXiv:1503.02769]	  Bazavov:2015yea	-> MILC 15yea
	  -> occurs in references but is not cited in the text?	  

* []	  [arXiv:1410.8374]	  Bruno:2014ufa	      	-> ALPHA 14ufa  
   	  -> not referenced by acronym

* screen figures in submitted sections and make them look nice
  (pheno results blue empty, PDG filled triangle, ...)
  28.10.2015 - did this for Vudus section, alpha_s section py files seem to be outdated and I wrote to Rainer et al.
               for LEC section I don't have the python files

* compile list with criteria agreed upon

* write script for automatic updating of FLAG.bib file that doesn't touch title and author list
  28.10.2015 - first step taken, I now have a script that retrieves the most recent version of ALL
               entries in the FLAG.bib file; still thinking about how to automatise updating process

* there are several occurences of the PDG in the bibliography

* find and fix bibtex entry 
@Article{DeTarHeller,
     author    = "DeTar, C. and Heller, Urs M.",
     title     = "In preparation",
     year      = "2009"
}

* I converted the plot alpha_S for R_4 in the alpha_s section into a python file and added a FLAG 2015 logo to it - Rainer wrote back saying that they deliberately did not attach a 2015 logo to this plot in order to indicate that it hasn't changed with respect to FLAG2. For now I have left the 2015 logo - EB needs to decide what to do

* AJ: Matlman:2008bx = Matlman 08?
