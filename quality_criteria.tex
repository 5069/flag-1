\section{Quality criteria, averaging and error estimation}
\label{sec:qualcrit}

The essential characteristics of our approach to the problem of rating
and averaging lattice quantities 
have been outlined in our first publication~\cite{Colangelo:2010et}. 
Our aim is to help the reader
assess the reliability of a particular lattice result without
necessarily studying the original article in depth. This is a delicate
issue, since the ratings may make things appear 
simpler than they are. Nevertheless,
it safeguards against the common practice of using lattice results, and
drawing physics conclusions from them, without a critical assessment
of the quality of the various calculations. We believe that, despite
the risks, it is important to provide some compact information about
the quality of a calculation. We stress, however, the importance of the
accompanying detailed discussion of the results presented in the various
sections of the present review.
 
\subsection{Systematic errors and colour code}
\label{sec:color-code}

The major sources of systematic error are common to most lattice
calculations. These include, as discussed in detail below,
the chiral, continuum, and infinite-volume extrapolations.
To each such source of error for which
systematic improvement is possible we
assign one of three coloured symbols: green
star, unfilled green circle
(which replaced in Ref.~\cite{Aoki:2013ldr}
the amber disk used in the original FLAG review~\cite{Colangelo:2010et})
or red square.
These correspond to the following ratings: 
\begin{itemize}[noitemsep,nolistsep] 
\item[\good] the parameter values and ranges used 
to generate the data sets allow for a satisfactory control of the systematic uncertainties;
\item[\soso] the parameter values and ranges used to generate
the data sets allow for a reasonable attempt at estimating systematic uncertainties, which
however could be improved;
\item[\bad] the parameter values and ranges used to generate
the data sets are unlikely to allow for a reasonable control of systematic uncertainties.
\end{itemize}
The appearance of a red tag, even in a
single source of systematic error of a given lattice result,
disqualifies it from inclusion in the global average.

Note that in the first two editions~\cite{Colangelo:2010et,Aoki:2013ldr},
FLAG used the three symbols in order to rate the reliability of the systematic errors 
attributed to a given result by the paper's authors.
Starting with the previous edition~\cite{Aoki:2016frl} the meaning of the 
symbols has changed slightly---they now rate the quality of a particular simulation, 
based on the values and range of the chosen parameters,
and its aptness to obtain well-controlled systematic uncertainties. 
They do not rate the quality of the analysis performed by the authors 
of the publication. The latter question is
deferred to the relevant sections of the present review, 
which contain detailed discussions of 
the results contributing (or not) to each FLAG average or estimate. 

For most quantities the colour-coding system refers to the following  
sources of systematic errors: (i) chiral extrapolation; 
(ii) continuum extrapolation; (iii) finite volume. 
As we will see below, renormalization is another source of systematic
uncertainties in several quantities. This we also classify using the 
three coloured symbols listed above, but now with
a different rationale:  they express how reliably these quantities are 
renormalized, from a field-theoretic point of view
(namely, nonperturbatively, or with 2-loop or 1-loop perturbation theory).

Given the sophisticated status that the field has attained,
several aspects, besides those rated by the coloured symbols,
need to be evaluated before one can conclude
whether a particular analysis leads to results that should be included in an
average or estimate. Some of these aspects are not so easily expressible
in terms of an adjustable parameter such as the lattice spacing, the pion mass
or the volume. As a result of such considerations,
it sometimes occurs, albeit rarely, that a given
result does not contribute to the FLAG average or estimate, 
despite not carrying any red tags.
This happens, for instance, whenever aspects of the analysis appear 
to be incomplete 
(e.g., an incomplete error budget), so that the presence
of inadequately controlled systematic effects cannot be excluded. 
This mostly refers to results with a statistical error only, or results
in which the quoted error budget obviously fails to account 
for an important contribution.

Of course, any colour coding has to be treated with caution; we emphasize
that the criteria are subjective and evolving. Sometimes, a single
source of systematic error dominates the systematic uncertainty and it
is more important to reduce this uncertainty than to aim for green
stars for other sources of error. In spite of these caveats, we hope
that our attempt to introduce quality measures for lattice simulations
will prove to be a useful guide. In addition, we would like to
stress that the agreement of lattice results obtained using
different actions and procedures provides further validation.

\subsubsection{Systematic effects and rating criteria}
\label{sec:Criteria}

The precise criteria used in determining the colour coding are
unavoidably time-dependent; as lattice calculations become more
accurate, the standards against which they are measured become
tighter. For this reason FLAG reassesses criteria with each edition and as a result
some of the quality criteria (the one on chiral extrapolation for instance) have been tightened up   
over time~\cite{Colangelo:2010et,Aoki:2013ldr,Aoki:2016frl}.

In the following, we present the rating criteria used in the current report. 
While these criteria apply to most quantities without modification
there are cases where they need to be amended or additional criteria need to be defined. 
For instance, when discussing
results obtained in the $\epsilon$-regime of chiral perturbation theory in Sec.~\ref{sec:LECs}
the finite volume criterion listed below for the $p$-regime is no longer appropriate.\footnote{We refer to Sec.~\ref{sec:chPT} and Appendix \ref{sec_ChiPT} in the Glossary for an explanation of the various regimes of chiral perturbation theory.} Similarly, the discussion
of the strong coupling constant in Sec.~\ref{sec:alpha_s} requires tailored criteria
for renormalization, perturbative behaviour, and continuum extrapolation. In such cases,
the modified criteria are discussed in the respective sections. Apart from only a few exceptions the 
following colour code applies in the tables:


\begin{itemize}
\item Chiral extrapolation:
\begin{itemize}[noitemsep,nolistsep] 
	\item[\good] $M_{\pi,\mathrm{min}}< 200$ MeV, with three or more pion masses used in the extrapolation \\
	\underline{or} two values of $M_\pi$ with one lying within 10 MeV of 135MeV (the physical neutral pion mass) and the other one below 200 MeV  
	\item[\soso]  200 MeV $\le M_{\pi,{\mathrm{min}}} \le$ 400 MeV, with three or more pion masses used in the extrapolation \\\underline{or} two values of $M_\pi$ with $M_{\pi,{\mathrm{min}}}<$ 200 MeV \\\underline{or} a single value of $M_\pi$, lying within 10 MeV of 135 MeV (the physical neutral pion mass)
	\item[\bad] otherwise  % 400 MeV $ < M_{\pi,\mathrm{min}}$ 
	\end{itemize}
This criterion has changed with respect to the previous edition~\cite{Aoki:2016frl}.
\item 
Continuum extrapolation:
\begin{itemize}[noitemsep,nolistsep] 
	\item[\good] at least three lattice spacings \underline{and} at least two points below 0.1 fm \underline{and} a range of lattice spacings satisfying $[a_{\mathrm{max}}/a_{\mathrm{min}}]^2 \geq 2$
	\item[\soso] at least two lattice spacings \underline{and} at least one point below 0.1 fm 
	\underline{and} a range of lattice spacings 
	satisfying $[a_{\mathrm{max}}/a_{\mathrm{min}}]^2 \geq 1.4$
	\item[\bad] otherwise
\end{itemize}
It is assumed that the lattice action is $\cO(a)$-improved (i.e., the
discretization errors vanish quadratically with the lattice spacing);
otherwise this will be explicitly mentioned. For
unimproved actions an additional lattice spacing is required.
This condition is unchanged from Ref.~\cite{Aoki:2016frl}.
\item 
Finite-volume effects:\\ 
The finite-volume colour code used for a result is 
chosen to be the worse of the QCD and the QED codes, as described below. If only QCD is used the QED colour code is ignored.

\emph{-- For QCD:}
\begin{itemize}[noitemsep,nolistsep] 
	\item[\good] $[M_{\pi,\mathrm{min}} / M_{\pi,\mathrm{fid}}]^2 \exp\{4-M_{\pi,\mathrm{min}}[L(M_{\pi,\mathrm{min}})]_{\mathrm{max}}\} < 1$,
	\underline{or} at least three volumes
	\item[\soso] $[M_{\pi,\mathrm{min}} / M_{\pi,\mathrm{fid}}]^2 \exp\{3-M_{\pi,\mathrm{min}}[L(M_{\pi,\mathrm{min}})]_{\mathrm{max}}\} < 1$,
	\underline{or} at least two volumes
	\item[\bad]  otherwise 
\end{itemize}
where we have introduced $[L(M_{\pi,\mathrm{min}})]_{\mathrm{max}}$, which is the maximum box size used in 
the simulations performed at the smallest pion mass $M_{\pi,{\rm min}}$, as well as a fiducial pion mass 
$M_{\pi,{\rm fid}}$, which we set to 200
MeV (the cutoff value for a green star in the chiral extrapolation). 
It is assumed here that calculations are in the $p$-regime of chiral perturbation
theory, and that all volumes used exceed 2~fm. 
This condition has been improved between the second~\cite{Aoki:2013ldr} and the third~\cite{Aoki:2016frl}
edition of the FLAG review but remains unchanged since.
The rationale for this condition is as follows.
Finite volume effects contain the universal factor $\exp\{- L~M_\pi\}$,
and if this were the only contribution a criterion based on
the values of $M_{\pi,\textrm{min}} L$ would be appropriate. 
This is what we used in Ref.~\cite{Aoki:2013ldr} 
(with $M_{\pi,\textrm{min}} L>4$ for \good \,
and $M_{\pi,\textrm{min}} L>3$ for \soso).
However, as pion masses decrease, one must also account for
the weakening of the pion couplings. In particular,
1-loop chiral perturbation theory~\cite{Colangelo:2005gd} 
reveals a behaviour proportional to
$M_\pi^2 \exp\{- L~M_\pi\}$. 
Our new condition includes this weakening of the coupling, 
and ensures, for example, that simulations with
$M_{\pi,\mathrm{min}} = 135~{\rm MeV}$ and $L~M_{\pi,\mathrm{min}} =
3.2$ are rated equivalently to those with $M_{\pi,\mathrm{min}} = 200~{\rm MeV}$
and $L~M_{\pi,\mathrm{min}} = 4$.

\emph{-- For QED (where applicable):}
\begin{itemize}[noitemsep,nolistsep]
	\item[\good]$1/([M_{\pi,\mathrm{min}}L(M_{\pi,\mathrm{min}})]_{\mathrm{max}})^{n_{\mathrm{min}}}<0.02$,
		\underline{or} at least four volumes
	\item[\soso] $1/([M_{\pi,\mathrm{min}}L(M_{\pi,\mathrm{min}})]_{\mathrm{max}})^{n_{\mathrm{min}}}<0.04$,
		\underline{or} at least three volumes
	\item[\bad]  otherwise 
\end{itemize}
Because of the infrared-singular structure of QED, electromagnetic finite-volume effects decay only like a power of the inverse spatial extent. In several cases like mass splittings~\cite{Borsanyi:2014jba,Davoudi:2014qua} or leptonic decays~\cite{Lubicz:2016xro}, the leading corrections are known to be universal, i.e., independent of the structure of the involved hadrons. In such cases, the leading universal effects can be directly subtracted exactly from the lattice data. We denote $n_{\mathrm{min}}$ the smallest power of $\frac{1}{L}$ at which such a subtraction cannot be done. In the widely used finite-volume formulation $\mathrm{QED}_L$, one always has $n_{\mathrm{min}}\leq 3$ due to the nonlocality of the theory~\cite{Davoudi:2018qpl}.
While the QCD criteria have not changed with respect to Ref.~\cite{Aoki:2016frl} the QED criteria are new. They are used here only in Sec.~\ref{sec:qmass}.
\item Isospin breaking effects (where applicable):
\begin{itemize}[noitemsep,nolistsep]
	\item[\good] all leading isospin breaking effects are included in the lattice calculation
	\item[\soso] isospin breaking effects are included using the electro-quenched approximation
	\item[\bad]otherwise
\end{itemize}
This criterion is used for quantities which are breaking isospin symmetry or which can be determined at the sub-percent accuracy where isospin breaking effects, if not included, are expected to be the dominant source of uncertainty. In the current edition, this criterion is only used for the up and down quark masses, and related quantities ($\epsilon$, $Q^2$ and $R^2$).
The criteria for isospin breaking effects feature for the first time in the FLAG review.
\item Renormalization (where applicable):
\begin{itemize}[noitemsep,nolistsep]
	\item[\good]  nonperturbative
	\item[\soso]  1-loop perturbation theory or higher  with a reasonable estimate of truncation errors
	\item[\bad]  otherwise 
\end{itemize}	
In Ref.~\cite{Colangelo:2010et}, we assigned a red square to all
results which were renormalized at 1-loop in perturbation theory. In 
Ref.~\cite{Aoki:2013ldr}, we decided that this was too restrictive, since 
the error arising from renormalization constants, calculated in perturbation theory at
1-loop, is often estimated conservatively and reliably. We did not change these criteria since.

\item Renormalization Group (RG) running (where applicable): \\ 
For scale-dependent quantities, such as quark masses or $B_K$, it is
essential that contact with continuum perturbation theory can be established.
Various different methods are used for this purpose
(cf.~Appendix \ref{sec_match}): Regularization-independent Momentum
Subtraction (RI/MOM), the Schr\"odinger functional, and direct comparison with
(resummed) perturbation theory. Irrespective of the particular method used,
the uncertainty associated with the choice of intermediate
renormalization scales in the construction of physical observables
must be brought under control. This is best achieved by performing
comparisons between nonperturbative and perturbative running over a
reasonably broad range of scales. These comparisons were initially
only made in the Schr\"odinger functional approach, but are now
also being performed in RI/MOM schemes.  We mark the data for which
information about nonperturbative running checks is available and
give some details, but do not attempt to translate this into a
colour code. 
\end{itemize}

The pion mass plays an important role in the criteria relevant for
chiral extrapolation and finite volume.  For some of the
regularizations used, however, it is not a trivial matter to identify
this mass. 
%
In the case of twisted-mass fermions, discretization
effects give rise to a mass difference between charged and neutral
pions even when the up- and down-quark masses are equal: the charged pion
is found to be the heavier of the two for twisted-mass Wilson fermions
(cf.~Ref.~\cite{Boucaud:2007uk}).
In early works, typically
referring to $N_f=2$ simulations (e.g., Refs.~\cite{Boucaud:2007uk}
and~\cite{Baron:2009wt}), chiral extrapolations are based on chiral
perturbation theory formulae which do not take these regularization
effects into account. After the importance of accounting for isospin
breaking when doing chiral fits was shown in Ref.~\cite{Bar:2010jk},
later works, typically referring to $N_f=2+1+1$ simulations, have taken
these effects into account~\cite{Carrasco:2014cwa}.
We use $M_{\pi^\pm}$ for $M_{\pi,\mathrm{min}}$
in the chiral-extrapolation rating criterion. On the
other hand, 
we identify $M_{\pi,\mathrm{min}}$ with
the root mean square (RMS) of $M_{\pi^+}$,
$M_{\pi^-}$ and $M_{\pi^0}$ in the finite-volume rating criterion.\footnote{
This is a change from FLAG 13, where we used
the charged pion mass when evaluating both chiral-extrapolation
and finite-volume effects.}

In the case of staggered fermions,
discretization effects give rise to several light states with the
quantum numbers of the pion.\footnote{
We refer the interested reader to a number of good reviews on the
subject~\cite{Durr:2005ax,Sharpe:2006re,Kronfeld:2007ek,Golterman:2008gt,Bazavov:2009bb}.}
The mass splitting among these ``taste'' partners represents a
discretization effect of $\cO(a^2)$, which can be significant at large
lattice spacings but shrinks as the spacing is reduced. In the
discussion of the results obtained with staggered quarks given in the
following sections, we assume that these artifacts are under
control. We conservatively identify $M_{\pi,\mathrm{min}}$ with the root mean
square (RMS) average of the masses of all the taste partners, 
both for chiral-extrapolation and finite-volume criteria.\footnote{
In FLAG 13, the RMS value
was used in the chiral-extrapolation criteria throughout the paper. 
For the finite-volume rating, however,
$M_{\pi,\mathrm{min}}$ was identified with the RMS value only in
Secs.~\ref{sec:vusvud} and \ref{sec:BK}, while in Secs.~\ref{sec:qmass},
\ref{sec:LECs}, \ref{sec:DDecays} and \ref{sec:BDecays} it
was identified with the mass of the lightest pseudoscalar state.}

In some of the simulations, the fermion formulations employed for the
valence quarks are different from those used for the sea quarks. Even
when the fermion formulations are the same, there are cases where the
sea and valence quark masses differ. In such cases, we use the smaller
of the valence-valence and valence-sea $M_{\pi_{\rm min}}$ values in the
finite-volume criteria, since either of these channels may give the leading
contribution depending on the quantity of interest at the one-loop
level of chiral perturbation theory. For the chiral-extrapolation
criteria, on the other hand, we use the unitary point, where the sea and
valence quark masses are the same, to define $M_{\pi_{\rm min}}$.

The strong coupling $\alpha_s$ is computed in lattice QCD with methods
differing substantially
from those used in the calculations of the other quantities 
discussed in this review. Therefore, we have established separate criteria for
$\alpha_s$ results, which will be discussed in Sec.~\ref{s:crit}.

In the new section on nuclear matrix elements, Sec.~\ref{sec:NME},
an additional criterion has been introduced.
This concerns the level of control over contamination from excited states,
which is a more challenging issue for nucleons than for mesons. 
In addition, the chiral-extrapolation criterion in
this section is somewhat stricter than that given above.

\subsubsection{Heavy-quark actions}
\label{sec:HQCriteria}

For the $b$ quark,
the discretization of the
heavy-quark action follows a very different approach from that used for light
flavours. There are several different methods for
treating heavy quarks on the lattice, each with its own issues and
considerations.  Most of these methods use
Effective Field Theory (EFT) at some point in the computation, either
via direct simulation of the EFT, or by using EFT
as a tool to estimate the size of cutoff errors, 
or by using EFT to extrapolate from the simulated
lattice quark masses up to the physical $b$-quark mass. 
Because of the use of an EFT, truncation errors must be
considered together with discretization errors. 

The charm quark lies at an intermediate point between the heavy
and light quarks. In our earlier reviews, the calculations
involving charm quarks often treated it using one of the approaches adopted
for the $b$ quark. Since the last report~\cite{Aoki:2016frl}, however, we found
more recent calculations to simulate the charm quark using light-quark actions.
This has become possible thanks to the increasing availability of
dynamical gauge field ensembles with fine lattice spacings.
But clearly, when charm quarks are treated relativistically, discretization
errors are more severe than those of the corresponding light-quark quantities.

In order to address these complications, we add a new heavy-quark
treatment category to the rating system. The purpose of this
criterion is to provide a guideline for the level of action and
operator improvement needed in each approach to make reliable
calculations possible, in principle. 

A description of the different approaches to treating heavy quarks on
the lattice is given in Appendix~\ref{app:HQactions}, including a
discussion of the associated discretization, truncation, and matching
errors.  For truncation errors we use HQET power counting throughout,
since this review is focused on heavy-quark quantities involving $B$
and $D$ mesons rather than bottomonium or charmonium quantities.  
Here we describe the criteria for how each approach
must be implemented in order to receive an acceptable (\okay) rating
for both the heavy-quark actions and the weak operators.  Heavy-quark
implementations without the level of improvement described below are
rated not acceptable (\bad). The matching is evaluated together with
renormalization, using the renormalization criteria described in
Sec.~\ref{sec:Criteria}.  We emphasize that the heavy-quark
implementations rated as acceptable and described below have been
validated in a variety of ways, such as via phenomenological agreement
with experimental measurements, consistency between independent
lattice calculations, and numerical studies of truncation errors.
These tests are summarized in Sec.~\ref{sec:BDecays}.  \smallskip
\\ {\it Relativistic heavy-quark actions:} \\
\noindent 
\okay \hspace{0.2cm}   at least tree-level $\cO(a)$ improved action and 
weak operators  \\
This is similar to the requirements for light-quark actions. All
current implementations of relativistic heavy-quark actions satisfy
this criterion. \smallskip \\
{\it NRQCD:} \\
\noindent 
\okay \hspace{0.2cm}   tree-level matched through $\cO(1/m_h)$ 
and improved through $\cO(a^2)$ \\
The current implementations of NRQCD satisfy this criterion, and also
include tree-level corrections of $\cO(1/m_h^2)$ in the action. 
\smallskip \\
{\it HQET: }\\
\noindent 
\okay \hspace{0.2cm}  tree-level  matched through $\cO(1/m_h)$ 
with discretization errors starting at $\cO(a^2)$ \\
The current implementation of HQET by the ALPHA collaboration
satisfies this criterion, since both action and weak operators are
matched nonperturbatively through $\cO(1/m_h)$.  Calculations that
exclusively use a static-limit action do not satisfy this criterion,
since the static-limit action, by definition, does not include $1/m_h$
terms.  We therefore include static computations in our final estimates only if truncation errors (in $1/m_h$)  are discussed and included in the systematic uncertainties.\smallskip \\
{\it Light-quark actions for heavy quarks:}  \\
\noindent 
\okay \hspace{0.2cm}  discretization errors starting at $\cO(a^2)$ or higher \\
This applies to calculations that use the tmWilson action, a
nonperturbatively improved Wilson action, domain wall fermions or the HISQ action for charm-quark 
quantities. It also applies to calculations that use these light
quark actions in the charm region and above together with either the
static limit or with an HQET-inspired extrapolation to obtain results
at the physical $b$-quark mass. In these cases, the continuum-extrapolation criteria described earlier 
must be applied to the entire range of heavy-quark masses used in 
the calculation.

\subsubsection{Conventions for the figures}
\label{sec:figurecolours}

For a coherent assessment of the present situation, the quality of the
data plays a key role, but the colour coding cannot be carried over to
the figures. On the other hand, simply showing all data on equal
footing might give the misleading impression that the overall
consistency of the information available on the lattice is
questionable. Therefore, in the figures we indicate the quality of the data
in a rudimentary way, using the following symbols:
\begin{itemize}[noitemsep,nolistsep]
	\item[\raisebox{0.3mm}{\hspace{0.65mm}{\color{darkgreen}$\blacksquare$}}] corresponds to results included in the average or estimate (i.e., results that contribute to the black square below);
	\item[\raisebox{0.3mm}{\hspace{0.65mm}{\color{lightgreen}$\blacksquare$\hspace{-0.3cm}\color{darkgreen}$\square$}}] corresponds to results that are not included in the average but pass all quality criteria;
	\item[\raisebox{0.3mm}{\hspace{0.65mm}{\color{red}$\square$}}] corresponds to all other results;
	\item[\raisebox{0.3mm}{\hspace{0.65mm}{\color{black}$\blacksquare$}}]corresponds to FLAG averages or estimates; they are also highlighted by a gray vertical band.
\end{itemize} 
The reason for not including a given result in
the average is not always the same: the result may fail one of the
quality criteria; the paper may be unpublished; 
it may be superseded by newer results;
or it may not offer a complete error budget. 

Symbols other than squares are
used to distinguish results with specific properties and are always
explained in the caption.\footnote{%
%
For example, for quark-mass results we
distinguish between perturbative and nonperturbative renormalization, 
for low-energy constants we distinguish between the $p$- and $\epsilon$-regimes, 
and for heavy-flavour results we distinguish between
those from leptonic and semi-leptonic decays.}

Often, nonlattice data are also shown in the figures for comparison. 
For these we use the following symbols:
\begin{itemize}[noitemsep,nolistsep]
	\item[\raisebox{0.15mm}{\hspace{0.65mm}\color{blue}\Large\textbullet}]
	corresponds to nonlattice results;
	\item[\raisebox{0.35mm}{\hspace{0.65mm}{\color{black}$\blacktriangle$}}] corresponds to Particle Data Group (PDG) results.
\end{itemize}
\subsection{Averages and estimates}\label{sec:averages}

%%
FLAG results of a given quantity are denoted either as {\it averages} or as {\it estimates}. Here we clarify this distinction. To start with, both {\it averages} and {\it estimates} are based on results without any red tags in their colour coding. For many observables there are enough independent lattice calculations of good quality, with all sources of error (not merely those related to the colour-coded criteria), as analyzed in the original papers, appearing to be under control. In such cases, it makes sense to average these results and propose such an {\it average} as the best current lattice number. The averaging procedure applied to this data and the way the error is obtained is explained in detail in Sec.~\ref{sec:error_analysis}. In those cases where only a sole result passes our rating criteria (colour coding), we refer to it as our FLAG {\it average}, provided it also displays adequate control of all other sources of systematic uncertainty.

On the other hand, there are some cases in which this procedure leads to a result that, in our opinion, does not cover all uncertainties. Systematic  errors are by their nature often subjective and difficult to estimate, and may thus end up being underestimated in one or more results that receive green symbols for all explicitly tabulated criteria.   
Adopting a conservative policy, in these cases we opt for an {\it estimate} (or a range), which we consider as a fair assessment of the knowledge acquired on the lattice at present. This {\it estimate} is not obtained with a prescribed mathematical procedure, but reflects what we consider the best possible analysis of the available information. The hope is that this will encourage more detailed investigations by the lattice community.

There are two other important criteria that also play a role in this
respect, but that cannot be colour coded, because a systematic
improvement is not possible. These are: {\em i)} the publication
status, and {\em ii)} the number of sea-quark flavours $\Nf$. As far as the
former criterion is concerned, we adopt the following policy: we
average only results that have been published in peer-reviewed
journals, i.e., they have been endorsed by referee(s). The only
exception to this rule consists in straightforward updates of previously
published results, typically presented in conference proceedings. Such
updates, which supersede the corresponding results in the published
papers, are included in the averages. 
Note that updates of earlier results rely, at least partially, on the
same gauge-field-configuration ensembles. For this reason, we do not
average updates with earlier results. 
Nevertheless, all results are
listed in the tables,\footnote{%
%
Whenever figures turn out to be overcrowded,
older, superseded results are omitted. However, all the most recent results
from each collaboration are displayed.}
%
and their publication status is identified by the following
symbols:
\begin{itemize}
\item Publication status:\\
\gA  \hspace{0.2cm}published or plain update of published results\\
\oP  \hspace{0.2cm}preprint\\ 
\rC  \hspace{0.2cm}conference contribution
\end{itemize}
In the present edition, the
publication status on the {\bf 30th of September 2018} is relevant.
If the paper appeared in print after that date, this is accounted for in the
bibliography, but does not affect the averages.\footnote{%
%
As noted above in footnote 1, three exceptions to this deadline were made.}
%

As noted above,
in this review we present results from simulations with $N_f=2$,
$N_f=2+1$ and $N_f=2+1+1$ (except for $ r_0 \Lambda_\msbar$ where we
also give the $N_f=0$ result). We are not aware of an {\em a priori} way
to quantitatively estimate the difference between results produced in
simulations with a different number of dynamical quarks. We therefore
average results at fixed $\Nf$ separately; averages of calculations
with different $\Nf$ are not  provided.

To date, no significant differences between results with different
values of $N_f$ have been observed in the quantities 
listed in Tabs.~\ref{tab:summary1}, \ref{tab:summary2}, and \ref{tab:summary3}.
In the future, as the accuracy
and the control over systematic effects in lattice calculations 
increases, it will hopefully be possible to see a difference between results
from simulations with $\Nf = 2$ and $\Nf = 2 + 1$, 
and thus determine the size of the
Zweig-rule violations related to strange-quark loops. This is a very
interesting issue {\em per se}, and one which can be quantitatively 
addressed only with lattice calculations.

The question of differences between results with $\Nf=2+1$ and
$\Nf=2+1+1$ is more subtle.
The dominant effect of including the charm sea quark is to
shift the lattice scale, an effect that is accounted for by
fixing this scale nonperturbatively using physical quantities.
For most of the quantities discussed in this review, it is 
expected that residual effects are small in the continuum limit,
suppressed by $\alpha_s(m_c)$ and powers of $\Lambda^2/m_c^2$.
Here $\Lambda$ is a hadronic scale that can only be
roughly estimated and depends on the process under consideration.
Note that the $\Lambda^2/m_c^2$ effects have been addressed 
in~Refs.~\cite{Bruno:2014ufa,Athenodorou:2018wpk}.
Assuming that such effects are small, it might be reasonable to
average the results from $\Nf=2+1$ and $\Nf=2+1+1$ simulations.



\subsection{Averaging procedure and error analysis}
\label{sec:error_analysis}

In the present report, we repeatedly average results
obtained by different collaborations, and estimate the error on the resulting
averages. 
Here we provide details on how averages are obtained.

\subsubsection{Averaging --- generic case}
We follow the procedure of the previous  two editions~\cite{Aoki:2013ldr,Aoki:2016frl},
which we describe here in full detail.

One of the problems arising when forming averages is that not all
of the data sets are independent.
In particular, the same gauge-field configurations,
produced with a given fermion discretization, are often used by
different research teams with different valence-quark lattice actions,
obtaining results that are not really independent.  
Our averaging procedure takes such correlations into account. 

Consider a given measurable quantity $Q$, measured by $M$ distinct,
not necessarily uncorrelated, numerical experiments (simulations). The result
of each of these measurement is expressed as
\begin{equation}
Q_i \,\, = \,\, x_i \, \pm \, \sigma^{(1)}_i \pm \, \sigma^{(2)}_i \pm \cdots
\pm \, \sigma^{(E)}_i  \,\,\, ,
\label{eq:resultQi}
\end{equation}
where $x_i$ is the value obtained by the $i^{\rm th}$ experiment
($i = 1, \cdots , M$) and $\sigma^{(k)}_i$ (for $k = 1, \cdots , E$) 
are the various errors.
Typically $\sigma^{(1)}_i$ stands for the statistical error 
and $\sigma^{(\alpha)}_i$ ($\alpha \ge 2$) are the different
systematic errors from various sources. 
For each individual result, we estimate the total
error $\sigma_i $ by adding statistical and systematic errors in quadrature:
\begin{eqnarray}
Q_i \,\, &=& \,\, x_i \, \pm \, \sigma_i \,\,\, ,
\nonumber \\
\sigma_i \,\, &\equiv& \,\, \sqrt{\sum_{\alpha=1}^E \Big [\sigma^{(\alpha)}_i \Big ]^2} \,\,\, .
\label{eq:av-err-Qi}
\end{eqnarray}
With the weight factor of each total error estimated in standard fashion,
\begin{equation}
\omega_i \,\, = \,\, \dfrac{\sigma_i^{-2}}{\sum_{i=1}^M \sigma_i^{-2}} \,\,\, ,
\label{eq:weighti}
\end{equation}
the central value of the average over all simulations is given by
\begin{eqnarray}
x_{\rm av} \,\, &=& \,\, \sum_{i=1}^M x_i\, \omega_i \,\, . 
\end{eqnarray}
The above central value corresponds to a $\chi_{\rm min}^2$ weighted
average, evaluated by adding statistical and systematic errors in quadrature.
If the fit is not of good quality ($\chi_{\rm min}^2/{\rm dof} > 1$),
the statistical and systematic error bars are stretched by a factor
$S = \sqrt{\chi^2/{\rm dof}}$.

Next, we examine error budgets for
individual calculations and look for potentially correlated
uncertainties. Specific problems encountered in connection with
correlations between different data sets are described in the text
that accompanies the averaging.
If there is reason to believe that a source of error is correlated
between two calculations, a $100\%$ correlation is assumed.
The correlation matrix $C_{ij}$ for the set of correlated lattice results is
estimated by a prescription due to Schmelling~\cite{Schmelling:1994pz}.
This consists in defining
\begin{equation}
\sigma_{i;j} \,\, = \,\, \sqrt{{\sum_{\alpha}}^\prime \Big[ \sigma_i^{(\alpha)} \Big]^2 } \,\,\, ,
\label{eq:sigmaij}
\end{equation}
with $\sum_{\alpha}^\prime$ running only over those errors of $x_i$ that
are correlated with the corresponding errors of the measurement $x_j$. 
This expresses the part of the uncertainty in $x_i$
that is correlated with the uncertainty in $x_j$. 
If no such correlations are known to exist, then
we take $\sigma_{i;j} =0$. 
The diagonal and off-diagonal elements of the correlation
matrix are then taken to be
\begin{eqnarray}
C_{ii} \,\,&=& \,\, \sigma_i^2 \qquad \qquad (i = 1, \cdots , M) \,\,\, ,
\nonumber \\
C_{ij} \,\,&=& \,\, \sigma_{i;j} \, \sigma_{j;i} \qquad \qquad (i \neq j) \,\,\, .
\label{eq:Ciiij}
\end{eqnarray}
Finally, the error of the average is estimated by
\begin{equation}
\sigma^2_{\rm av} \,\, = \,\, \sum_{i=1}^M \sum_{j=1}^M \omega_i \,\omega_j \,C_{ij}\,\,,
\label{eq:sigma2av}
\end{equation}
and the FLAG average is
\begin{equation}
Q_{\rm av} \,\, = \,\, x_{\rm av} \, \pm \, \sigma_{\rm av} \,\,\, .
\end{equation}
% nested averaging
\input{HQ/HQSubsections/nested_average.tex}
