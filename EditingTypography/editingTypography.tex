\documentclass[a4paper,color]{article}
\usepackage{a4wide,xcolor,multicol,textcomp,url}

\newcommand{\orange}[1]{\textcolor{orange}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\magenta}[1]{\textcolor{magenta}{#1}}

\usepackage[colorlinks=true,a4paper=true,backref=false, linktocpage=true,
citecolor=blue,urlcolor=blue,linkcolor=blue,pdfpagemode=UseOutlines]{hyperref}

\newcommand{\msbar}{{\overline{{\rm MS}}}}
\newcommand{\latextilde}{{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}}

\begin{document}   
\pagestyle{empty}    
\vspace{-1.0truecm}   


\centerline{\LARGE{\bf FLAG5: Guidelines for editing, style and typography}}
\vskip 2cm   

\centering
\today

\vskip 2cm 
\abstract{ 
  We collect a set of rules and guidelines for preparing and editing the FLAG sections, including the ones concerning style and typography.
}
\vskip 2cm
%\begin{flushleft}   
%\date
% \centering
% \today
%\end{flushleft}   
%\vskip 0.5 cm   
%\vfill\eject   
   
\pagestyle{empty}
%\clearpage   
\setcounter{page}{1}   
\pagestyle{plain}   
%\newpage   
\pagestyle{plain} \setcounter{page}{1}   

\section{FLAG git repository}
The central git repository is hosted at bitbucket.org:5069/flag.git (note that the repository used for FLAG-3 is out-of-date and no longer maintained). Compared to our old gitserver bitbucket offers many state-of-the-art collaboration features that makes life for the WGs and the EB easier.

In order to get started at least one WG member will get read access to
the main repository. To this end please send your bitbucket user ID to
\href{mailto:juettner@soton.ac.uk}{\tt juettner@soton.ac.uk} such that
the account can be affiliated with the main repository (new bitbucket
accounts can be registered under
\href{https://bitbucket.org/account/signup/}{\tt https://bitbucket.org/account/signup/}).

We follow the ‘forking workflow’ explained also
\href{https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow}{here}.

Forking means that a user generates a copy of the FLAG repository in his bitbucket work space. The user can pull and push from this repository without touching the main FLAG repository. 

Under Settings$\rightarrow$User and group access please add Andreas Juettner (5069) and Urs Wenger (uwenger) with read access.

Once a WG wishes to add their updated section to the main repository they will bring their forked repository up-to-date with respect to the main repository and then issue a ‘pull request’. The EB will receive a notification, revise the suggested changes and eventually merge the WG contribution into the main branch. 

\section{Tags for averages}
We have automatised the process of compiling the table with
  all FLAG averages in the introduction of the FLAG review. This guarantees that the
latest modifications in individual sections are up-to-date also in the
summary table.

For this to work we have defined some macros that we ask you to use when presenting a FLAG average in the text. Here
is an example:
\begin{verbatim}
%FLAGRESULT BEGIN
% TAG      & f+0    & f+0&f+0 &END
% REFS     & \cite{Bazavov:2013maa,Carrasco:2016kpy} &\cite{Bazavov:2012cd,Boyle:2015hfa} 
                         &\cite{Lubicz:2009ht} &END
% UNITS    & 1 & 1 & 1 &END
% NUMRESULTS & 2 & 2 & 1 &END
% FLAVOURs & 2+1+1 & 2+1 & 2 &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
  &\label{eq:fplus_direct_2p1p1}
  \mbox{direct},\,\Nf=2+1+1:&\FLAGAVBEGIN f_+(0) &= 0.9706(27)\FLAGAVEND  
                       &&\Ref~\mbox{\cite{Bazavov:2013maa,Carrasco:2016kpy}},\\
  &\label{eq:fplus_direct_2p1}
  \mbox{direct},\,\Nf=2+1:  &\FLAGAVBEGIN f_+(0) &= 0.9677(27) \FLAGAVEND     
                       &&\Refs~\mbox{\cite{Bazavov:2012cd,Boyle:2015hfa}},   \\
  &\label{eq:fplus_direct_2}
  \mbox{direct},\,\Nf=2:    &\FLAGAVBEGIN f_+(0) &= 0.9560(57)(62)\FLAGAVEND  
                       &&\Ref~\mbox{\cite{Lubicz:2009ht}},
\end{align}
%FLAGRESULTFORMULA END\end{verbatim}
Note that all the lines between \verb|%FLAGRESULT BEGIN| and \verb|%FLAGRESULTFORMULA END| are relevant since they
will be parsed by the script that generates the summary table. 

Note 
that the tags are also used to generate the bib-files which can be
downloaded from the webpage. They contain all the references 
contributing to an average  in order to make referencing easy for the
end user.

\section{Citations and acronyms}
Any new references can be included in the bib-file on github, or
submitted in a separate file including only the new bibtex entries.\\\vspace*{0.25cm}


Do not invent new
reference names, but use the proper reference, e.g., {\tt \textbackslash cite\{McNeile:2010ji\}} as given in the bibtex entry \\

{\tt @Article\{McNeile:2010ji,\\...\\
\} }\\\vspace*{0.25cm}


In the text use the collaboration name and the bibtex-appendage as the
temporary acronym, such as HPQCD 10ji for McNeile:2010ji. The proper acronyms, such as HPQCD~10C, will be globally assigned by the EB once the whole document is put together.




\section{Abbreviations}
Abbreviations should start with a capital letter. They should not be used as the first word in a sentence.

\begin{itemize}
\item Sec.\latextilde \quad when referring to a specific section
%, except when first word of sentence
\item Fig.\latextilde \quad ditto
\item Eq.\latextilde \quad \,ditto
\item Tab.\latextilde \,\,\, ditto
\item Ref.\latextilde \quad used in cases such ``As FLAG discussed in Ref.~[56]" but not in constructions like ``This point was made previously [85]."
\item Figs.\latextilde, Eqs.\latextilde, Tabs.\latextilde, Refs.\latextilde \quad in case of plural.
\end{itemize}

\noindent
In contrast, we use Appendix as a full word.

\vspace*{0.25cm}
\noindent
Abbreviations such as
etc., e.g., i.e., viz.~are lower case and not italicized. 
%However, if one writes out entire non-English words, they should be italicized, e.g., {\em a priori}.

\vspace*{0.25cm}
\noindent
We use dof instead of d.o.f.


\section{Mathematical symbols and numbers}
No sentence should start with a 
number (unless spelled as a word), or with a math symbol, like $F_\pi$.   (So, for
instance, ``The pion decay constant has been computed ..."  or ``The pion decay 
constant $F_\pi$ has been computed ..." instead of ``$F_\pi$ has been computed...") \\

All integer numbers less than 10 should be spelled out in the middle of a sentence.


\section{Commas}
etc., e.g., i.e., viz.: should always be preceded and followed by
commas. (Note that this has not been followed by FLAG in the past.) The same applies to words and expressions such as ``namely", ``for example", ``that is", and ``respectively".

% Section III.A.2 of the Physical Review Style Guide gives additional information about what to do about commas with

The last comma in the previous sentence (before and) is known as the serial comma.  It should be used in a list of multiple
items according to the Physical Review Style Guide.


We suggest to use commas after prepositional (or
other) phrases that begin a sentence.  For example, we put a comma
after:
``In Sec.~3, we detail ..."

PR would say not to use a comma around a variable just after the phrase that defines it.  For example, ``The melting temperature $T_m$ is determined,..."
not ``The melting temperature, $T_m$, is determined..."


%\section{Italization}



\section{Foreign language and loan words}
We suggest that the spelling of words borrowed from a non-English
language is taken over from the original language, including the
pluralization. They should be lower case. So we use for example ``ansatz"
and ``ans\"atze", or ``plateau" and ``plateaux", or ``formulae". \\

The word ``data" is used in singular form, such as in ``Experimental data is progressively more precise".



\section{Dashes}
There are four different types of dashes:
\begin{itemize}
\item The minus sign ($-2$) (\LaTeX: \$-\$).
\item The hyphen connecting compounds (Kaluza-Klein, type-II) (\LaTeX: {-}).
\item The en-dash describes a range (1-–10) (\LaTeX: {-}\,{-}). 
\item The em-dash--—used for clauses--—should not be surrounded by spaces (\LaTeX: {-}\,{-}\,{-}). Could also be a
parenthetical 
phrase. 
\end{itemize}

\section{Parentheses and capitals}
 Parentheses in the text should in general be round (like this),
and should not contain extra spaces: [ this is wrong ]. 
Square
brackets enclose a phrase that already contains parentheses, as in
``[as in Eq.~(35)]".

Capitals should be used only after full stops and not after colon,
semicolon or parenthesis. (Unless an entire sentence appears with
parentheses.) Such as here.

\section{``That" vs. ``Which"}

These two are often used interchangeably, but style guides prefer a distinction
exemplified by the following two sentences:
\begin{quote}
The office, which has two lunchrooms, is located in Cincinnati.\\
The office that has two lunchrooms is located in Cincinnati.
\end{quote}
``Which" is used for a descriptive subclause, enclosed in commas,
that can be removed without changing the meaning of the outer sentence.
``That" is an essential part of the meaning of the entire sentence.
%We can decide whether we want to enforce this. It may be a fool's errand!


\section{Footnotes}
Footnotes should always be placed after the punctuation mark.\footnote{Like this.}
They should begin with a capital letter and end with a full stop.

\section{Hyphenation conventions}
A hyphen should be used when two words make a single compound adjective. Example: ``lattice-QCD" should be used as an adjective (e.g., ``this is a lattice-QCD calculation") whereas ``lattice QCD" as a noun (``the calculation is done in lattice QCD").
\\\vspace*{0.25cm}

\noindent
No hyphen is used when a word is negated, such as in ``nonzero" or ``nonperturbative".
\\\vspace*{0.25cm}


\noindent
Examples:
\begin{multicols}{3}
\begin{itemize}
\item lattice result			
\item lattice determination		
\item lattice simulation		
\item lattice-QCD calculation		
\item lattice data			
\item lattice spacing			
\item lattice renormalization		
\item lattice regularized		
\item renormalized			
\item lattice formalism		
\item lattice renormalization factors	
\item lattice estimate		
\item CP violation
\item $W$ boson

\item $B$ meson
\item $D$ meson
\item $D$-meson decay constant 
\item $b$ hadron
\item flavour-changing neutral current
\item Heavy-Quark Effective Theory
\item nonrelativistic
\item Relativistic Heavy-Quark Action
\item heavy-quark physics
\item static heavy-quark limit 
\item four-flavour averages
\item four-fermion operator
\item four-quark operator
\item 1-loop, 2-loop, ... \\(not one-loop, ...)
\item charged-meson decay constant
\item $\beta$-function
\item short-distance QCD correction factor
\item $B$-factory experiment
\item $SU(3)$-breaking ratio
\item isospin averaged		
\item gauge average			
\item taste partner			
\item taste-Goldstone-pion		
\item $\msbar$ scheme			
\item mid nineties			
\item twisted mass			
\item isospin breaking		     				
\item colour code    			
\item FLAG average			
\item low energy			
\item quark flavours			
\item reanalysis			
\item $\beta$ decay			
\item also $\pi$-, $\tau$-, ...
\item flavour-phenomenology		
\item nonsinglet			
\item nonperturbative			
\item 1-loop				
\item light-quark			
\item heavy-quark			
\item state-of-the art	
\item pseudoscalar	
\item subsection			
\item QCD matrix element		
\item semileptonic			
\item semiinclusive			
\item nonstrange			
\item nonexotic			
\item SU(2) isospin-breaking		 
\item isospin				     			
\item nonperturbative			
\item and other non...
\item reanalysis	
\item chiral symmetry breaking
\item up-down-quark mass
\item three-flavour QCD
\item four-flavour calculations
\item pion decay constant
\item input quark mass
\item short-distance calculation
\item $b$ quark (as a noun)
\item isospin-breaking effects
\item symmetry-breaking effect
\item elm. self-energy
\item isospin-averaged up-quark mass
\item isospin-breaking corrections/effects
\item isospin-breaking quark-mass ratio
\item low energy pion physics
\item quark-mass ratio
\item post-2010 lattice results
\item charge conjugation invariance
\item form factor
\item pion-mass splitting

\item $B$-factory experiment
\item $SU(3)$-breaking ratio
\item $z$-parameterization, $z$-expansion
\item $w$-dependence
\item well-known tension
\item sea-quark mass
\item domain-wall fermion
\item step-scaling	
\item scale-setting error
\item final-state meson	
\item data set
\end{itemize}
\end{multicols}


\section{Spelling}
This concerns GB vs.~US spelling, that is, -ise/-ize vs. -ize. The Oxford dictionary recommends:

\begin{multicols}{3}
\begin{itemize}
\item optimize
\item renormalize
\item renormalization
\item discretization
\item  digitize
\item  itemize
\item  parameterize\\ (not parametrize)
\item  summarize
\item  minimize
\item  stabilize
\item  emphasize
\item  regularize
\item  realize 
\item  quantize
\item  factorize 
\item  normalize
\item  generalize
\item  sizeable\\ (sizable is ranked as American)
\end{itemize}
\end{multicols}


\end{document}
