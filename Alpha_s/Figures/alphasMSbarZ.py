# FLAG plot for alpha_s^3(M_z)
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= r"$\rm \alpha_s$"                               # plot title
#titlestring	= r"$\rm \alpha_s$ wt subavs+smallrange 180709"   # plot title

plotnamestring	= "alphasMSbarZ" # filename for plot

plotxticks	= ([[0.1100, 0.1150, 0.1200, 0.1250]]) # locations for x-ticks
#plotxticks	= ([[0.1000, 0.1050, 0.1100, 0.1150, 0.1200, 0.1250]]) # locations for x-ticks
#plotxticks	= ([[0.1125, 0.115, 0.1175, 0.12, 0.1225]]) # locations for x-ticks
#plotxticks	= ([[1.14, 1.18,  1.22,  1.26]])# locations for x-ticks

yaxisstrings	= [				# x,y-location for y-strings
		   [+0.0990,-0.50,"$\\rm Eig$"],
		   [+0.0990,3.0,"$\\rm Vert$"],
		   [+0.0990,8.0,"$\\rm Curr$"],
		   [+0.0990,13.5,"$\\rm Wloop$"],
		   [+0.0990,17.25,"$\\rm Vpol$"],
		   [+0.0990,20.5,"$\\rm Pot$"],
		   [+0.0990,24.5,"$\\rm Step$"]
		    ]

#yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.0990,7.0,"$\\rm N_f=3$"],
#		   [+0.0990,17.0,"$\\rm N_f=4$"]
#		    ]

#yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.0990,3.5,"$\\rm N_f=0,2$"],
#		   [+0.0990,15.5,"$\\rm N_f=3$"],
#		   [+0.0990,25.5,"$\\rm N_f=4$"]
#		    ]

xlimits		= [0.1100,0.1300]   		# plot's xlimits
#xlimits        = [0.0975,0.1375]               # plot's xlimits
#xlimits	= [0.1125,0.1275]   		# plot's xlimits
#xlimits        = [1.119,1.4]	        	# plot's xlimits

logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
PRELIM = 0                                      # 0 nothing
                                                # 1 large diagonal PRELIMINARY watermark
                                                # 2 smaller PRELIMINARY watermark after FLAG logo

tpos = 0.1235					# x-position for the data labels
#tpos = 1.28					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green, l=light green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak


# +- statistical then +- total error

datstep=[
[0.1180, 0.0030, 0.0030, 0.0030, 0.0030,  "PACS-CS 09A"	,['s','g','g',0,tpos]]  ,
[0.11852,0.00084,0.00084,0.00084,0.00084, "ALPHA 17"	,['s','g','g',0,tpos]]  ]

datpot=[
[0.1156, 0.0021, 0.0022, 0.0021, 0.0022,  "Bazavov 12"	,['s','l','g',0,tpos]]  ,
[0.1166, 0.0012, 0.0008, 0.0012, 0.0008,  "Bazavov 14"	,['s','g','g',0,tpos]]  ,
[0.1179, 0.0007, 0.0007, 0.0015, 0.0015,  "Takaura 18"  ,['s','w','r',0,tpos]]  ]

datvacpol=[
[0.1118, 0.0003, 0.0003, 0.0016, 0.0017,  "JLQCD 10"	,['s','w','r',0,tpos]]  ,
[0.1181, 0.0027, 0.0027, 0.0028, 0.0035,  "Hudspith 18" ,['s','w','r',0,tpos]]  ]

datwilloop=[
[0.1170, 0.0012, 0.0012, 0.0012, 0.0012,  "HPQCD 05A"	,['s','l','g',0,tpos]]  ,
[0.1183, 0.0008, 0.0008, 0.0008, 0.0008,  "HPQCD 08A"	,['s','l','g',0,tpos]]  ,
[0.1192, 0.0011, 0.0011, 0.0011, 0.0011,  "Maltman 08"	,['s','g','g',0,tpos]]  ,
[0.1184, 0.0006, 0.0006, 0.0006, 0.0006,  "HPQCD 10"	,['s','g','g',0,tpos]]  ]

datcurrent=[
[0.1174, 0.0012, 0.0012, 0.0012, 0.0012,  "HPQCD 08B"	,['s','w','r',0,tpos]]  ,
[0.1183, 0.0007, 0.0007, 0.0007, 0.0007,  "HPQCD 10"	,['s','g','g',0,tpos]]  ,
[0.11822,0.00074,0.00074,0.00074,0.00074, "HPQCD 14A"	,['s','g','g',0,tpos]]  ,
[0.1177, 0.0026, 0.0026, 0.0026, 0.0026,  "JLQCD 16"	,['s','g','g',0,tpos]]  ,
[0.11622,0.00084,0.00084,0.00084,0.00084, "Maezawa 16"	,['s','w','r',0,tpos]]  ]

datvert=[
[0.1198, 0.0009, 0.0009, 0.0011, 0.0011,  "ETM 11D"	,['s','w','r',0,tpos]]  ,
[0.1200, 0.0014, 0.0014, 0.0014, 0.0014,  "ETM 12C"	,['s','w','r',0,tpos]]  ,
[0.1196, 0.0009, 0.0009, 0.0011, 0.0011,  "ETM 13D"	,['s','w','r',0,tpos]]  ]

dateigen=[
[0.1226, 0.0036, 0.0036, 0.0036, 0.0036,  "                    Nakayama 18",['s','w','r',0,tpos]] ]


datpdg=[
 [0.11823,0.00081,0.00081,0.00081,0.00081, "FLAG estimate"                      ,['s','k','k',0,tpos]]  ,
 [0.1174, 0.0016, 0.0016, 0.0016, 0.0016,  "PDG 18 nonlattice average"     	,['^','k','k',0,tpos]]  ,
 [0.1192, 0.0018, 0.0018, 0.0018, 0.0018,  "PDG 18 Tau-decay"                   ,['^','k','k',0,tpos]]  ,
 [0.1156, 0.0021, 0.0021, 0.0021, 0.0021,  "PDG 18 DIS"                         ,['^','k','k',0,tpos]]  ,
 [0.1169, 0.0034, 0.0034, 0.0034, 0.0034,  "PDG 18 Jets"                        ,['^','k','k',0,tpos]]  ,
 [0.1151, 0.0028, 0.0028, 0.0028, 0.0028,  "PDG 18 Hadron colliders"            ,['^','k','k',0,tpos]]  ,
 [0.1196, 0.0030, 0.0030, 0.0030, 0.0030,  "PDG 18 EW precision fits"           ,['^','k','k',0,tpos]]  ]

#datpdg=[
#[0.1174, 0.0016, 0.0016, 0.0016, 0.0016,  "PDG 18 nonlattice average"     	,['^','k','k',0,tpos]]  ]

#[0.1175, 0.0017, 0.0017, 0.0017, 0.0017,  "PDG nonlattice average"     	,['^','k','k',0,tpos]]  ]
#[0.1183, 0.0012, 0.0012, 0.0012, 0.0012,  "PDG nonlattice average"     	,['^','b','b',0,tpos]]  ]
#[0.1184, 0.0007, 0.0007, 0.0007, 0.0007,  "PDG"     	,['^','k','k',0,tpos]]  ]


#datnf5=[
#[0.1170, 0.0012, 0.0012, 0.0012, 0.0012,  "HPQCD 05A"	,['s','l','g',0,tpos]]  ,
#[0.1183, 0.0008, 0.0008, 0.0008, 0.0008,  "HPQCD 08A"	,['s','l','g',0,tpos]]  ,
#[0.1174, 0.0012, 0.0012, 0.0012, 0.0012,  "HPQCD 08B"	,['s','w','r',0,tpos]]  ,
#[0.1192, 0.0011, 0.0011, 0.0011, 0.0011,  "Maltman 08"	,['s','g','g',0,tpos]]  ,
#[0.1180, 0.0030, 0.0030, 0.0030, 0.0030,  "PACS-CS 09A"	,['s','g','g',0,tpos]]  ,
#[0.1184, 0.0006, 0.0006, 0.0006, 0.0006,  "HPQCD 10"	,['s','g','g',0,tpos]]  ,
#[0.1183, 0.0007, 0.0007, 0.0007, 0.0007,  "HPQCD 10"	,['s','g','g',0,tpos]]  ,
#[0.1118, 0.0003, 0.0003, 0.0016, 0.0017,  "JLQCD 10"	,['s','w','r',0,tpos]]  ,
#[0.1156, 0.0021, 0.0022, 0.0021, 0.0022,  "Bazavov 12"	,['s','l','g',0,tpos]]  ,
#[0.1166, 0.0012, 0.0008, 0.0012, 0.0008,  "Bazavov 14"	,['s','g','g',0,tpos]]  ,
#[0.1177, 0.0026, 0.0026, 0.0026, 0.0026,  "JLQCD 16"	,['s','g','g',0,tpos]]  ,
#[0.11622,0.00084,0.00084,0.00084,0.00084, "Maezawa 16"	,['s','w','r',0,tpos]]  ,
#[0.11852,0.00084,0.00084,0.00084,0.00084, "ALPHA 17"	,['s','g','g',0,tpos]]  ,
#[0.1226, 0.0036, 0.0036, 0.0036, 0.0036,  "Nakayama 18 ??",['s','w','k',0,tpos]],
#[0.1181, 0.0027, 0.0027, 0.0028, 0.0035,  "Hudspith 18 ??" ,['s','w','r',0,tpos]]  ]

#dat02nf5=[
#[0.106,  0.004,  0.004,  0.004,   0.004,  "El-Khadra 92" ,['s','w','r',0,tpos]]  ,
#[0.108,  0.005,  0.005,  0.006,   0.006,  "Aoki 94"      ,['s','w','r',0,tpos]]  ,
#[0.115,  0.002,  0.002,  0.002,   0.002,  "Davies 94"    ,['s','w','r',0,tpos]]  ,
#[0.107,  0.005,  0.005,  0.005,   0.005,  "Wingate 95"   ,['s','w','r',0,tpos]]  ,
#[0.1118, 0.0017, 0.0017, 0.0017,  0.0017, "SESAM 99"     ,['s','w','r',0,tpos]]  ,
#[0.113,  0.003,  0.003,  0.005,   0.005,  "Boucaud 01B"  ,['s','w','r',0,tpos]]  ,
#[0.112,  0.001,  0.001,  0.003,   0.003,  "QCDSF/UKQCD 05" ,['s','w','r',0,tpos]]  ]

#dat2p1p1nf5=[
##[0.0000, 0.0000, 0.0000, 0.0000, 0.0000,  " "           ,['s','w','r',0,tpos]]  ,
#[0.1198, 0.0009, 0.0009, 0.0010, 0.0011,  "ETM 11D"	,['s','w','r',0,tpos]]  ,
#[0.1200, 0.0014, 0.0014, 0.0014, 0.0014,  "ETM 12C"	,['s','w','r',0,tpos]]  ,
#[0.1196, 0.0009, 0.0009, 0.0011, 0.0011,  "ETM 13D"	,['s','w','r',0,tpos]]  ,
#[0.11822,0.00074,0.00074,0.00074,0.00074, "HPQCD 14A"	,['s','g','g',0,tpos]]  ]


# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
#
# [np.nan,0.0000,0.0000,0.0000,0.0000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
#

 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0],    # eigenvalues

 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0],    # vertex

 [0.11824,0.0015,0.0015,0.0015,0.0015,"dontshow",['','k','k',0,tpos],0],                    # currents range
# [0.11824,0.0011,0.0011,0.0011,0.0011,"dontshow",['','k','k',0,tpos],0],                   # currents range
# [0.1182,0.0000,0.0000,0.0000,0.0000,"dontshow",['','k','k',0,tpos],0],                    # currents range
# [0.1182,0.0005,0.0005,0.0005,0.0005,"dontshow",['','k','k',0,tpos],0],                    # currents wt av+err

 [0.11858,0.00120,0.00120,0.00120,0.00120,"dontshow",['','k','k',0,tpos],0],                # wil range
# [0.1186,0.0005,0.0005,0.0005,0.0005,"dontshow",['','k','k',0,tpos],0],                    # wil wt av+err

 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0],    # vac pol

 [0.1166,0.0016,0.0016,0.0016,0.0016,"dontshow",['','k','k',0,tpos],0],                     # pot range
# [0.1166,0.0000,0.0000,0.0000,0.0000,"dontshow",['','k','k',0,tpos],0],                    # pot range
# [0.1166,0.0012,0.0008,0.0012,0.0008,"dontshow",['','k','k',0,tpos],0],                    # pot asym av
# [0.1166,0.0010,0.0010,0.0010,0.0010,"dontshow",['','k','k',0,tpos],0],                    # pot sym av

 [0.11848,0.00081,0.00081,0.00081,0.00081,"dontshow",['','k','k',0,tpos],0],                # step range
# [0.11852,0.00084,0.00084,0.00084,0.00084,"dontshow",['','k','k',0,tpos],0],               # step range ALPHA17
# [0.1185,0.0000,0.0000,0.0000,0.0000,"dontshow",['','k','k',0,tpos],0],                    # step range
# [0.1185,0.0008,0.0008,0.0008,0.0008,"dontshow",['','k','k',0,tpos],0],                    # step wt av+err

 [0.11806,0.00072,0.00072,0.00072,0.00072,"FLAG + PDG",  ['s','k','k',0,tpos],0]
# [0.11823,0.00081,0.00081,0.00081,0.00081,"FLAG estimate",  ['s','k','k',0,tpos],0]
 
# [0.11824,0.00081,0.00081,0.00081,0.00081,"smallest range",  ['s','k','k',0,tpos],0]
# [0.11824,0.0011,0.0011,0.0011,0.0011,"average range",  ['s','k','k',0,tpos],0]

# [0.11824,0.0004,0.0004,0.0004,0.0004,"naive errors",  ['s','k','k',0,tpos],0]
# [0.1182,0.0012,0.0012,0.0012,0.0012,"FLAG16 estimate",  ['s','k','k',0,tpos],0]

# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
# [0.1182,0.0005,0.0005,0.0005,0.0005,"dontshow",['','k','k',0,tpos],0], 
# [0.1186,0.0005,0.0005,0.0005,0.0005,"dontshow",['','k','k',0,tpos],0], 
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
# [0.1166,0.0012,0.0008,0.0012,0.0008,"dontshow",['','k','k',0,tpos],0], 
## [0.1166,0.0010,0.0010,0.0010,0.0010,"dontshow",['','k','k',0,tpos],0], 
# [0.1185,0.0008,0.0008,0.0008,0.0008,"dontshow",['','k','k',0,tpos],0], 
# [0.1182,0.0012,0.0012,0.0012,0.0012,"FLAG16 estimate",  ['s','k','k',0,tpos],0]

# [0.1182,0.0012,0.0012,0.0012,0.0012,"dontshow",['','k','k',0,tpos],0], 
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0],
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0],
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0] 
#
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0]
# [0.1198 ,0.0009,0.0005,0.0009,0.00005,"our estimate for $N_f=2+1+1$, ghost-gluon v." ,['s','w','k',0,tpos],1],
# [0.1181, 0.0014,0.0012,0.0014,0.0012,"our estimate for $N_f=2+1$, vac. pol.", ['s','w','k',0,tpos],1],
# [0.12047, 0.00081,0.00180,0.00081,0.00180, "our estimate for $N_f=2+1$, SF.", ['s','w','k',0,tpos],1],
# [0.11785, 0.0007,0.0007,0.0007,0.0007, "our estimate for $N_f=2+1$, JJ.", ['s','w','k',0,tpos],1],
#[0.1184, 0.0007,0.0007,0.0007,0.0007, "PDG",                ['s','w','k',0,tpos],1],
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0] 
]

# The follwing list should contain the list names of the previous DATA-blocks
#datasets=[dat02nf5,datnf5,dat2p1p1nf5,datpdg]
#datasets=[datnf5,dat2p1p1nf5,datpdg]
#datasets=[datstep,datpot,datvacpol,datwilloop,datcurrent,datvert,dateigen,datpdg]
datasets=[dateigen,datvert,datcurrent,datwilloop,datvacpol,datpot,datstep,datpdg]



################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


