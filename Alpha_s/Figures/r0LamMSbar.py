# FLAG plot for r0LamMSbar
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np

################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= r"$\rm r_0\Lambda$"                   # plot title
#titlestring	= "$f_K/f_\pi$"			        # plot title

plotnamestring	= "r0LamMSbar"                   # filename for plot

plotxticks	= ([[0.60, 0.70, 0.80, 0.90]])          # locations for x-ticks

yaxisstrings	= [                                     # x,y-location for y-strings
		   [+0.45,56.5,"$\\rm N_f=4$"],
		   [+0.45,44.5,"$\\rm N_f=3$"],
		   [+0.45,29.0,"$\\rm N_f=2$"],
		   [+0.45,12.5,"$\\rm N_f=0$"]
		    ]

xlimits		= [0.40,1.60]   		# plot's xlimits

logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'

PRELIM = 0                                      # 0 nothing
                                                # 1 large diagonal PRELIMINARY watermark
                                                # 2 smaller PRELIMINARY watermark after FLAG logo
tpos = 1.15				        # x-position for the data labels
#tpos = 1.10				        # x-position for the data labels

FS=9                                            # something to do with size of plot
MS=7
SIZE=[20.32,20.32]

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green, l=light green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

dat2nf0=[
[0.560, 0.024, 0.024, 0.024, 0.024,  "El-Khadra 92",   ['s','w','r',0,tpos]]  ,
[0.686, 0.054, 0.054, 0.054, 0.054,  "UKQCD 92",       ['s','w','r',0,tpos]]  ,
[0.661, 0.027, 0.027, 0.027, 0.027,  "Bali 92",        ['s','w','r',0,tpos]]  ,
[0.590, 0.060, 0.060, 0.060, 0.060,  "Luscher 93",     ['s','l','g',0,tpos]]  ,
[0.91,  0.13,  0.13,  0.13,  0.13,   "Alles 96",       ['s','w','r',0,tpos]]  ,
[0.602, 0.048, 0.048, 0.048, 0.048,  "ALPHA 98",       ['s','g','g',0,tpos]]  ,
[0.79,  0.01,  0.01,  0.01,  0.01,   "Boucaud 98A",    ['s','w','r',0,tpos]]  ,
[0.78,  0.01,  0.01,  0.04,  0.04,   "Boucaud 98B",    ['s','w','r',0,tpos]]  ,
[0.84,  0.04,  0.04,  0.05,  0.06,   "Becirevic 99B",  ['s','w','r',0,tpos]]  ,
[0.63,  0.01,  0.01,  0.01,  0.03,   "Boucaud 00A",    ['s','l','g',0,tpos]]  ,
[0.62,  0.07,  0.07,  0.07,  0.07,   "Boucaud 01A",    ['s','l','g',0,tpos]]  ,
[0.69,  0.05,  0.05,  0.05,  0.05,   "Soto 01",        ['s','l','g',0,tpos]]  ,
[0.614, 0.002, 0.002, 0.006, 0.006,  "QCDSF/UKQCD 05", ['s','g','g',0,tpos]]  ,
[0.85,  0.09,  0.09,  0.09,  0.09,   "Boucaud 05",     ['s','w','r',0,tpos]]  ,
[0.59,  0.01,  0.01,  0.022, 0.014,  "Boucaud 08",     ['s','w','r',0,tpos]]  ,
[0.637, 0.032, 0.030, 0.032, 0.030,  "Brambilla 10",   ['s','g','g',0,tpos]]  ,
[0.62,  0.01,  0.01,  0.01,  0.01,   "Sternbeck 10",   ['s','w','r',0,tpos]]  ,
[0.621, 0.011, 0.011, 0.011, 0.011,  "Kitazawa 16",     ['s','g','g',0,tpos]]  ,
#[0.618, 0.011, 0.011, 0.011, 0.011,  "FlowQCD 15",     ['s','l','g',0,tpos]]  ,
[0.606, 0.009, 0.009, 0.032, 0.010,  "Ishikawa 17",    ['s','g','g',0,tpos]]  ,  # note: JHEP result
[0.590, 0.016, 0.016, 0.016, 0.016,  "Husung 17",      ['s','l','g',0,tpos]]  ]

dat2nf2=[
[0.669, 0.069, 0.069, 0.069, 0.069,  "Boucaud 01B",    ['s','w','r',0,tpos]]  ,
[0.62,  0.02,  0.02,  0.03,  0.03,   "ALPHA 04",       ['s','w','r',0,tpos]]  ,
[0.617, 0.040, 0.040, 0.045, 0.045,  "QCDSF/UKQCD 05", ['s','w','r',0,tpos]]  ,
[0.581, 0.022, 0.022, 0.046, 0.022,  "JLQCD/TWQCD 08C",['s','w','r',0,tpos]]  ,
[0.72,  0.05,  0.05,  0.05,  0.05,   "ETM 10F",        ['s','g','g',0,tpos]]  ,
[0.60,  0.03,  0.03,  0.04,  0.04,   "Sternbeck 10",   ['s','w','r',0,tpos]]  ,
[0.658, 0.055, 0.055, 0.055, 0.055,  "ETM 11C",        ['s','g','g',0,tpos]]  ,
[0.789, 0.052, 0.052, 0.052, 0.052,  "ALPHA 12",       ['s','g','g',0,tpos]]  ,
[0.692, 0.031, 0.031, 0.031, 0.031,  "Karbstein 14",   ['s','l','g',0,tpos]]  ,
[0.643, 0.034, 0.034, 0.034, 0.034,  "Karbstein 18",   ['s','g','g',0,tpos]]  ]

dat2nf2p1=[
[0.763, 0.042, 0.042, 0.042, 0.042,  "HPQCD 05A",      ['s','l','g',0,tpos]]  ,
[0.809, 0.029, 0.029, 0.029, 0.029,  "HPQCD 08A",      ['s','l','g',0,tpos]]  ,
[0.777, 0.042, 0.042, 0.042, 0.042,  "HPQCD 08B",      ['s','w','r',0,tpos]]  ,
[0.841, 0.040, 0.040, 0.040, 0.040,  "Maltman 08",     ['s','g','g',0,tpos]]  ,
[0.824, 0.141, 0.141, 0.141, 0.141,  "PACS-CS 09",     ['s','g','g',0,tpos]]  ,
[0.812, 0.022, 0.022, 0.022, 0.022,  "HPQCD 10",       ['s','g','g',0,tpos]]  ,
[0.809, 0.025, 0.025, 0.025, 0.025,  "HPQCD 10",       ['s','g','g',0,tpos]]  ,
[0.591, 0.012, 0.012, 0.012, 0.012,  "JLQCD 10",       ['s','w','r',0,tpos]]  ,
[0.70,  0.07,  0.07,  0.07,  0.07,   "Bazavov 12",     ['s','l','g',0,tpos]]  ,
[0.746, 0.027, 0.042, 0.027, 0.042,  "Bazavov 14",     ['s','g','g',0,tpos]]  ,
[0.792, 0.089, 0.089, 0.089, 0.089,  "JLQCD 16",       ['s','g','g',0,tpos]]  ,
[0.739, 0.024, 0.024, 0.024, 0.024,  "Maezawa 16",     ['s','w','r',0,tpos]]  ,
[0.816, 0.029, 0.029, 0.029, 0.029,  "ALPHA 17",       ['s','g','g',0,tpos]]  ,
[0.978, 0.144, 0.144, 0.144, 0.144,  "Nakayama 18",    ['s','w','r',0,tpos]]  ,
[0.806, 0.096, 0.096, 0.096, 0.096,  "Hudspith 18",    ['s','w','r',0,tpos]]  ,
[0.799, 0.051, 0.051, 0.051, 0.051,  "Takaura 18",     ['s','w','r',0,tpos]]  ]

# "Bazavov 14",     ['s','g','g',0,tpos]]  ]  # r0/r1 = 1.508

dat2nf2p1p1=[
[0.756, 0.031, 0.031, 0.042, 0.042,  "ETM 11D",        ['s','w','r',0,tpos]]  ,
[0.775, 0.041, 0.041, 0.041, 0.041,  "ETM 12C",        ['s','w','r',0,tpos]]  ,
[0.752, 0.038, 0.038, 0.045, 0.045,  "ETM 13D",        ['s','w','r',0,tpos]]  ,
[0.703, 0.026, 0.026, 0.026, 0.026,  "HPQCD 14A",      ['s','g','g',0,tpos]]  ]


# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
#
# FLAG 18:
 [0.615, 0.018, 0.018, 0.018, 0.018, "FLAG estimate for $\\rm N_f=0$", ['s','k','k',0,tpos],1],
 [0.79,  0.15,  0.05,  0.15,  0.05,  "FLAG estimate for $\\rm N_f=2$", ['s','k','k',0,tpos],1],
 [0.806, 0.029, 0.029, 0.029, 0.029, "FLAG estimate for $\\rm N_f=3$", ['s','k','k',0,tpos],1],
 [0.703, 0.026, 0.026, 0.026, 0.026, "FLAG average for $\\rm N_f=4$", ['s','k','k',0,tpos],1],
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$", ['s','w','k',0,tpos],0]
#
# FLAG 16:
# [0.62, 0.02, 0.02, 0.02, 0.02, "PREVIOUS FLAG 16 estimate for $\\rm N_f=0$", ['s','k','k',0,tpos],1],
# [0.79, 0.13, 0.05, 0.13, 0.05, "FLAG 16 estimate for $\\rm N_f=2$", ['s','k','k',0,tpos],1],
# [0.80, 0.05, 0.05, 0.05, 0.05, "PREVIOUS FLAG 16 estimate for $\\rm N_f=3$", ['s','k','k',0,tpos],1],
# [0.703, 0.026, 0.026, 0.026, 0.026, "FLAG 16 average for $\\rm N_f=4$", ['s','k','k',0,tpos],1],
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$", ['s','w','k',0,tpos],0]
#
# [0.614, 0.006, 0.006, 0.006, 0.006, "naive FLAG estimate for $\\rm N_f=0$", ['s','k','k',0,tpos],1],
# [0.79, 0.13, 0.05, 0.13, 0.05, "FLAG estimate for $\\rm N_f=2$", ['s','k','k',0,tpos],1],
# [0.804, 0.012, 0.012, 0.012, 0.012, "naive FLAG estimate for $\\rm N_f=3$", ['s','k','k',0,tpos],1],
# [0.703, 0.026, 0.026, 0.026, 0.026, "FLAG average for $\\rm N_f=4$", ['s','k','k',0,tpos],1],
#
]


# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2nf0,dat2nf2,dat2nf2p1,dat2nf2p1p1]



################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


