# FLAG plot for alpha_s^3(M_z)
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= r"$R$"                       # plot title
#titlestring	= "$f_K/f_\pi$"		        # plot title

plotnamestring	= "Rm0p3R6m0p5current2pt"     # filename for plot
#plotnamestring	= "Rm0p3R6m0p5current2pt_190110"     # filename for plot

plotxticks	= ([[0.9500, 1.000, 1.0500, 1.1000]]) # locations for x-ticks
#plotxticks	= ([[1.000, 1.2000, 1.4000, 1.6000]]) # locations for x-ticks

yaxisstrings	= [				# x,y-location for y-strings
		   [+1.0000,1.25,"$\\rm R_8/R_{10}$"],
		   [+1.0000,5.00,"$\\rm R_6/R_8$"],
		   [+1.0000,9.5,"$\\rm R_{10}-0.3$"],
		   [+1.0000,13.5,"$\\rm R_8-0.3$"],
		   [+1.0000,17.5,"$\\rm R_6-0.5$"],
		   [+1.0000,21.5,"$\\rm R_4-0.3$"]
		    ]

#yaxisstrings	= [				# x,y-location for y-strings
#		   [+1.0000,1.25,"$\\rm R_8/R_{10}$"],
#		   [+1.0000,5.25,"$\\rm R_6/R_8$"],
#		   [+1.0000,10.0,"$\\rm R_{10}-0.3$"],
#		   [+1.0000,15.0,"$\\rm R_8-0.3$"],
#		   [+1.0000,20.0,"$\\rm R_6-0.5$"],
#		   [+1.0000,25.0,"$\\rm R_4-0.3$"]
#		    ]

#yaxisstrings	= [				# x,y-location for y-strings
#		   [+1.0000,1.25,"$\\rm R_8/R_{10}$"],
#		   [+1.0000,5.25,"$\\rm R_6/R_8$"],
#		   [+1.0000,9.0,"$\\rm R_{10}$"],
#		   [+1.0000,14.0,"$\\rm R_8$"],
#		   [+1.0000,19.0,"$\\rm R_6$"],
#		   [+1.0000,23.5,"$\\rm R_4$"]
#		    ]

xlimits         = [0.9500,1.1650]   		# plot's xlimits
#xlimits	= [0.9500,1.4000]   		# plot's xlimits
#xlimits         = [1.0000,1.7500]   		# plot's xlimits

logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'

PRELIM = 0                                      # 0 nothing
                                                # 1 large diagonal PRELIMINARY watermark
                                                # 2 smaller PRELIMINARY watermark after FLAG logo

tpos = 1.1250					# x-position for the data labels
#tpos = 1.600					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green, l=light green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak


# +- statistical then +- total error

# for clarity subtract 0.3 from R4, R8, R10 and 0.5 from R6

datR8oR10=[
[1.049,  0.002,  0.002,  0.002,  0.002,   "HPQCD 08"   	,['o','l','b',0,tpos]]  ,
[1.0495, 0.0007, 0.0007, 0.0007, 0.0007,  "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
[1.0481, 0.0009, 0.0009, 0.0009, 0.0009,  "JLQCD 16"    ,['o','l','b',0,tpos]]  ]

datR6oR8=[
[1.113,  0.002,  0.002,  0.002,  0.002,   "HPQCD 08"   	,['o','l','b',0,tpos]]  ,
[1.114,  0.002,  0.002,  0.002,  0.002,   "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
[1.111,  0.002,  0.002,  0.002,  0.002,   "JLQCD 16"    ,['o','l','b',0,tpos]]  ]

datR10=[
[1.004,  0.009,  0.009,  0.009,  0.009,   "HPQCD 08"   	,['o','l','b',0,tpos]]  ,
#[1.007,  0.002,  0.002,  0.002,  0.002,   "HPQCD 10"   	,['o','l','b',0,tpos]]  ,
[1.002,  0.008,  0.008,  0.008,  0.008,   "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
[0.997,  0.004,  0.004,  0.004,  0.004,   "JLQCD 16"    ,['o','l','b',0,tpos]]  ]
#datR10=[
#[1.304,  0.009,  0.009,  0.009,  0.009,   "HPQCD 08"  	 ,['o','l','b',0,tpos]]  ,
#[1.307,  0.002,  0.002,  0.002,  0.002,   "HPQCD 10"  	 ,['o','l','b',0,tpos]]  ,
#[1.302,  0.008,  0.008,  0.008,  0.008,   "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
#[1.297,  0.004,  0.004,  0.004,  0.004,   "JLQCD 16"    ,['o','l','b',0,tpos]]  ]

datR8=[
[1.070,  0.010,  0.010,  0.010,  0.010,   "HPQCD 08"   	,['o','l','b',0,tpos]]  ,
#[1.075,  0.003,  0.003,  0.003,  0.003,   "HPQCD 10"   	,['o','l','b',0,tpos]]  ,
[1.067,  0.008,  0.008,  0.008,  0.008,   "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
[1.059,  0.004,  0.004,  0.004,  0.004,   "JLQCD 16"    ,['o','l','b',0,tpos]]  ]
#datR8=[
#[1.370,  0.010,  0.010,  0.010,  0.010,   "HPQCD 08"    ,['o','l','b',0,tpos]]  ,
#[1.375,  0.003,  0.003,  0.003,  0.003,   "HPQCD 10"    ,['o','l','b',0,tpos]]  ,
#[1.367,  0.008,  0.008,  0.008,  0.008,   "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
#[1.359,  0.004,  0.004,  0.004,  0.004,   "JLQCD 16"    ,['o','l','b',0,tpos]]  ]

datR6=[
[1.028,  0.011,  0.011,  0.011,  0.011,   "HPQCD 08"   	,['o','l','b',0,tpos]]  ,
#[1.031,  0.004,  0.004,  0.004,  0.004,   "HPQCD 10"   	,['o','l','b',0,tpos]]  ,
[1.020,  0.004,  0.004,  0.004,  0.004,   "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
[1.009,  0.007,  0.007,  0.007,  0.007,   "JLQCD 16"    ,['o','l','b',0,tpos]]  ]
#datR6=[
#[1.528,  0.011,  0.011,  0.011,  0.011,   "HPQCD 08"    ,['o','l','b',0,tpos]]  ,
#[1.531,  0.004,  0.004,  0.004,  0.004,   "HPQCD 10"    ,['o','l','b',0,tpos]]  ,
#[1.520,  0.004,  0.004,  0.004,  0.004,   "Maezawa 16"  ,['o','l','b',0,tpos]]  ,
#[1.509,  0.007,  0.007,  0.007,  0.007,   "JLQCD 16"    ,['o','l','b',0,tpos]]  ]

datR4=[
[0.981,  0.005,  0.005,  0.005,  0.005,   "HPQCD 08"   	,['o','l','b',0,tpos]]  ,
#[0.982,  0.004,  0.004,  0.004,  0.004,   "HPQCD 10"   	,['o','l','b',0,tpos]]  ,
[0.9743, 0.0065, 0.0065, 0.0065, 0.0065,  "Maezawa 16"  ,['o','l','b',0,tpos]]  ]
#datR4=[
#[1.281,  0.005,  0.005,  0.005,  0.005,   "HPQCD 08"    ,['o','l','b',0,tpos]]  ,
#[1.282,  0.004,  0.004,  0.004,  0.004,   "HPQCD 10"    ,['o','l','b',0,tpos]]  ,
#[1.2743, 0.0065, 0.0065, 0.0065, 0.0065,  "Maezawa 16"  ,['o','l','b',0,tpos]]  ]


# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
#
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','w','k',0,tpos],0], 
# [0.1182,0.0012,0.0012,0.0012,0.0012,"dontshow",['','k','k',0,tpos],0], 
# [0.1182,0.0012,0.0012,0.0012,0.0012,"dontshow",['','k','k',0,tpos],0], 
# [0.1182,0.0012,0.0012,0.0012,0.0012,"FLAG estimate",  ['s','k','k',0,tpos],0],
# 
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datR8oR10,datR6oR8,datR10,datR8,datR6,datR4]



################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


