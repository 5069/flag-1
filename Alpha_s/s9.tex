
{

\subsection{$\alpha_s$ from the eigenvalue spectrum of the Dirac operator}

% ----------------------------------------------------------------------

\label{s:eigenvalue}

% ----------------------------------------------------------------------

\subsubsection{General considerations}

Consider the spectral density of the continuum
Dirac operator 
\begin{equation}
    \label{eq:def_rho}
    \rho(\lambda) = \frac{1}{V} \left\langle
    \sum_k (\delta(\lambda-i\lambda_k) + \delta(\lambda+i\lambda_k))  
    \right\rangle\,,
\end{equation}
where $V$ is the volume and $\lambda_k$ are the 
eigenvalues of the Dirac operator 
in a gauge background. 

Its perturbative expansion 
\begin{equation}
    \label{eq:exp_rho}
    \rho(\lambda) = {3 \over 4\pi^2} \,\lambda^3 (1-\rho_1\gbar^2 
     -\rho_2\gbar^4 -\rho_3\gbar^6 + \rmO(\gbar^8) )\,,
\end{equation}
is known including $\rho_3$  in the $\overline{\mathrm{MS}}$ scheme  
\cite{Chetyrkin:1994ex,Kneur:2015dda}.
In renormalization group improved form one sets the renormalization
scale $\mu$ to $\mu=s\lambda$ with $s=\rmO(1)$ and
the $\rho_i$ are pure numbers. 
Nakayama 18~\cite{Nakayama:2018ubk} initiated a study 
of $\rho(\lambda)$ in the perturbative regime. They prefer to consider 
$\mu$ independent from $\lambda$. Then $\rho_i$ are 
polynomials in $\log(\lambda/\mu)$ of degree $i$.
One may consider 
\begin{equation}
    \label{eq:expF}
    F(\lambda) \equiv {\partial \log(\rho(\lambda)) 
                       \over \partial \log(\lambda)}
    = 3 -F_1\gbar^2 
     -F_2\gbar^4 -F_3\gbar^6 - F_4\gbar^8 +  \rmO(\gbar^{10}) \,,
\end{equation}
where the four coefficients $F_i$, again polynomials
of degree $i$ in $\log(\lambda/\mu)$, are known. 
Choosing instead the renormalization group improved
form with $\mu=s\lambda$ in \eq{eq:exp_rho} would have led to
\begin{equation}
    \label{eq:expFRGimpr}
    F(\lambda) 
    = 3 -\bar F_2\gbar^4(\lambda) -\bar F_3\gbar^6(\lambda) - 
    \bar F_4\gbar^8(\lambda) +  \rmO(\gbar^{10}) \,,
\end{equation}
with pure numbers $\bar F_i$ and $\bar F_1=0$. 
Determinations of $\alpha_s$ can be carried out by a computation
and continuum extrapolation of $\rho(\lambda)$ and/or 
$F(\lambda)$ at large $\lambda$. 
Such computations are made possible by the techniques of 
\cite{Giusti:2008vb,Cossu:2016eqs,Nakayama:2018ubk}.

We note that according to our general
discussions in terms of an effective coupling, we have $n_\mathrm{l}=2$;
the 3-loop $\beta$ function of a coupling defined from
\eq{eq:exp_rho} or \eq{eq:expFRGimpr} is known.~\footnote{In the present situation, the
effective coupling would be defined by 
$\gbar^2_\lambda(\mu) = \bar F_2^{-1/2}\,(3 - F(\lambda))^{1/2}$ with $\mu=\lambda$, preferably taken as the renormalization group invariant eigenvalue. 
}
}

%----------------------------------------------------------------------


% --------------------------------------------------------------------

\subsubsection{Discussion of computations}

% --------------------------------------------------------------------

There is one pioneering result to date using this method by
Nakayama 18 \cite{Nakayama:2018ubk}. They computed the eigenmode 
distributions of the Hermitian operator 
$a^2 D^{\dagger}_{\rm ov}(m_f=0,am_{\rm PV}) D_{\rm ov}(m_f=0,am_{\rm PV})$
where $D_{\rm ov}$ is the overlap operator and $m_{\rm PV}$ is the
Pauli--Villars regulator on ensembles with 2+1 flavours using
M\"obius domain-wall quarks for three lattice cutoff
$a^{-1}= 2.5, 3.6, 4.5 $ GeV, where $am_{\rm PV} = 3$ or $\infty$.
The bare eigenvalues are converted
to the $\msbar$ scheme at $\mu= 2\,\mbox{GeV}$ by multiplying with the 
renormalization constant $Z_m(2\,\mbox{GeV})$, which is then transformed
to those renormalized at $\mu=6\,\mbox{GeV}$ using the renormalization
group equation. The scale is set by $\sqrt{t_0}=0.1465(21)(13)\,\mbox{fm}$.
The continuum limit is taken assuming a linear dependence in $a^2$, while
the volume size is kept about constant: 2.6--2.8~fm.

Choosing the renormalization scale 
$\mu= 6\,\mbox{GeV}$, Nakayama~18~\cite{Nakayama:2018ubk}, extracted
$\alpha_{\overline{\rm MS}}^{(3)}(6\,\mbox{GeV})=0.204(10)$.
The result is converted to 
\begin{eqnarray}
   \alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.1226(36) \,.
\end{eqnarray}
The lattice cutoff ranges over  $a^{-1} = 2.5-4.5\,\mbox{GeV}$
with $\mu=\lambda =0.8-1.25\,\mbox{GeV}$ yielding quite
small values $a\mu$. However, our continuum limit criterion does not
apply as it requires us to consider $\alpha_s=0.3$. We thus deviate
from the general rule and give a \soso\,  which would result at the
smallest value  $\alpha_\msbar(\mu)=0.4$ considered by Nakayama~18~
\cite{Nakayama:2018ubk}. The values of $\alpha_\msbar$ lead to a $\bad$
for the renormalization scale, while perturbative behavior is rated \soso.

In Tab.~\ref{tab_eigenvalue} we list this result.
\begin{table}[!htb]
   \vspace{3.0cm}
   \footnotesize
   \begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}rllllllll}
   Collaboration & Ref. & $\Nf$ &
   \hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{renormalization scale}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{perturbative behaviour}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}
      \hspace{-0.25cm} & %\rule{0.2cm}{0cm} 
                         scale & $\Lambda_\msbar[\MeV]$ & $r_0\Lambda_\msbar$ \\
   & & & & & & & & & \\[-0.1cm]
   \hline
   \hline
   & & & & & & & & & \\[-0.1cm] 
   Nakayama 18 & \cite{Nakayama:2018ubk} & 2+1 & \gA
            &     \bad   &  \soso      & \soso  
            & $\sqrt{t_0}$ 
            & $409(60)$\,$^*$   
            & $0.978(144)$                   \\ 
   & & & & & & & & & \\[-0.1cm]
   \hline
   \hline
\end{tabular*}
\begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}llllllll}
\multicolumn{8}{l}{\vbox{\begin{flushleft}
   $^*$ $\alpha_\msbar^{(5)}(M_Z)=0.1226(36)$. $\Lambda_\msbar$ determined 
        by us using $\alpha_\msbar^{(3)}(6\,\mbox{GeV})=0.204(10)$.
        Uses $r_0 = 0.472\,\mbox{fm}$   \\
\end{flushleft}}}
\end{tabular*}
\vspace{-0.3cm}
\normalsize
\caption{Dirac eigenvalue result.}
\label{tab_eigenvalue}
\end{table}


% ----------------------------------------------------------------------
