%\clearpage

\subsection{$\alpha_s$ from heavy-quark current two-point functions}

% ----------------------------------------------------------------------

\label{s:curr}

% ----------------------------------------------------------------------

\subsubsection{General considerations}

% ----------------------------------------------------------------------

The method has been introduced in HPQCD 08, Ref.~\cite{Allison:2008xk},
and updated in HPQCD 10, Ref.~\cite{McNeile:2010ji}, see also
Ref.~\cite{Bochkarev:1995ai}.  In addition
there is a 2+1+1 flavour result, HPQCD 14A \cite{Chakraborty:2014aca}.
Since FLAG 16 two new results have appeared:
JLQCD 16 \cite{Nakayama:2016atf} and Maezawa 16 \cite{Maezawa:2016vgv}.

The basic observable is constructed from a current 
\begin{eqnarray}
  J(x) = i m_c\overline\psi_c(x)\gamma_5\psi_{c'}(x)
  \label{e:Jx}
\end{eqnarray}
of two mass-degenerate heavy-valence quarks, $c$, $c^\prime$,
usually taken to be at or around the charm quark mass.
The pre-factor $m_c$ denotes the bare mass of the quark.
When the lattice discretization respects chiral symmetry, 
$J(x)$ is a renormalization group
invariant local field, i.e., it requires no renormalization.
Staggered fermions and twisted mass fermions have such a residual
chiral symmetry. The (Euclidean) time-slice correlation function
\begin{eqnarray}
   G(x_0) = a^3 \sum_{\vec{x}} \langle J^\dagger(x) J(0) \rangle \,,
\end{eqnarray}
($J^\dagger(x) = im_c\overline\psi_{c'}(x)\gamma_5\psi_c(x)$)
has a $\sim x_0^{-3}$  singularity at short distances and moments
\begin{eqnarray}
   G_n = a \sum_{x_0=-(T/2-a)}^{T/2-a} x_0^n \,G(x_0) \,
\label{Gn_smu}
\end{eqnarray}
are nonvanishing for even $n$ and furthermore finite for $n \ge 4$. 
Here $T$ is the time extent of the lattice.
The moments are dominated by contributions at $t$ of order $1/m_c$.
For large mass $m_c$ these are short distances and the moments
become increasingly perturbative for decreasing $n$.
Denoting the lowest-order perturbation theory moments by $G_n^{(0)}$,
one defines the
normalized moments 
\begin{eqnarray}
    R_n = \left\{ \begin{array}{cc}
          G_4/G_4^{(0)}          & \mbox{for $n=4$} \,, \\[0.5em]
          {am_{\eta_c}\over 2am_c} 
                \left( { G_n \over G_n^{(0)}} \right)^{1/(n-4)}
                               & \mbox{for $n \ge 6$} \,, \\
                 \end{array}
         \right.
\label{Rn}
\end{eqnarray}
of even order $n$. Note that \eq{e:Jx} contains the variable
(bare) heavy-quark mass $m_c$. 
The normalization $G_n^{(0)}$ is introduced to help in
reducing lattice artifacts.
In addition, one can also define moments with different normalizations,
\begin{eqnarray}
   \tilde R_n = 2 R_n / m_{\eta_c} \qquad \mbox{for $n \ge 6$}\,.
\end{eqnarray}
While $\tilde R_n$ also remains renormalization group invariant,
it now also has a scale which might introduce an
additional ambiguity \cite{Nakayama:2016atf}.

The normalized moments can then be parameterized in terms of functions
\begin{eqnarray}
   R_n \equiv \left\{ \begin{array}{cc}
                         r_4(\alpha_s(\mu))
                                        & \mbox{for $n=4$} \,,     \\[0.5em]
                         {r_n(\alpha_s(\mu)) \over \bar{m}_c(\mu)}
                                        & \mbox{for $n \ge 6$} \,, \\
                      \end{array}
              \right.
              \label{e:Rn}
\end{eqnarray}
with $\bar{m}_c(\mu)$ being the renormalized charm-quark mass.
The reduced moments $r_n$ have a perturbative expansion
\begin{eqnarray}
   r_n = 1 + r_{n,1}\alpha_s + r_{n,2}\alpha_s^2 + r_{n,3}\alpha_s^3 + \ldots\,,
\label{rn_expan}
\end{eqnarray}
where the written terms $r_{n,i}(\mu/\bar{m}_c(\mu))$, $i \le 3$ are known
for low $n$ from Refs.~\cite{Chetyrkin:2006xg,Boughezal:2006px,Maier:2008he,
Maier:2009fz,Kiyo:2009gb}. In practice, the expansion is performed in
the $\overline{\rm MS}$ scheme. Matching nonperturbative lattice results
for the moments to the perturbative expansion, one determines an
approximation to $\alpha_{\overline{\rm MS}}(\mu)$ as well as $\bar m_c(\mu)$.
With the lattice spacing (scale) determined from some extra physical input,
this calibrates $\mu$. As usual suitable pseudoscalar masses
determine the bare quark masses, here in particular the charm mass, 
and then through \eq{e:Rn} the renormalized charm-quark mass.

A difficulty with this approach is that large masses are needed to enter
the perturbative domain. Lattice artifacts can then be sizeable and
have a complicated form. The ratios in Eq.~(\ref{Rn}) use the
tree-level lattice results in the usual way for normalization.
This results in unity as the leading term in Eq.~(\ref{rn_expan}),
suppressing some of the kinematical lattice artifacts.
We note that in contrast to, e.g., the definition of $\alpha_\mathrm{qq}$,
here the cutoff effects are of order $a^k\alpha_s$, while there the
tree-level term defines $\alpha_s$ and therefore the cutoff effects
after tree-level improvement are of order $a^k\alpha_s^2$.

Finite-size effects (FSE) due to the omission of
$|t| > T /2$ in Eq.~(\ref{Gn_smu}) grow with $n$ as 
$(m_{\eta_c}T/2)^n\, \exp{(-m_{\eta_c} T/2)}$. 
In practice, however, since the (lower) moments
are short-distance dominated, the FSE are expected to be irrelevant
at the present level of precision.  

Moments of correlation functions of the quark's electromagnetic
current can also be obtained from experimental data for $e^+e^-$
annihilation~\cite{Kuhn:2007vp,Chetyrkin:2009fv}.  This enables a
nonlattice determination of $\alpha_s$ using a similar analysis
method.  In particular, the same continuum perturbation theory
computation enters both the lattice and the phenomenological determinations.

% ----------------------------------------------------------------------

\subsubsection{Discussion of computations}

% ----------------------------------------------------------------------
\begin{table}[!htb]
   \vspace{3.0cm}
   \footnotesize
   \begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}rllllllll}
      Collaboration & Ref. & $N_f$ &
      \hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{renormalization scale}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{perturbative behaviour}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}
      \hspace{-0.25cm} & %\rule{0.2cm}{0cm} 
                         scale & $\Lambda_\msbar[\MeV]$ 
                       & $r_0\Lambda_\msbar$ \\
      &&&&&&&&& \\[-0.1cm]
      \hline
      \hline
      &&&&&&&&& \\[-0.1cm]

      HPQCD 14A   &  \cite{Chakraborty:2014aca} 
                                              & 2+1+1   & \gA & \soso
                   & \good      & \soso
                   & $w_0=0.1715(9)\,\mbox{fm}^a$
                   & 294(11)$^{bc}$
                   & 0.703(26)             \\

      &&&&&&&&& \\[-0.1cm]
      \hline
      &&&&&&&&& \\[-0.1cm]

      {Maezawa 16}
                   & \textcolor{blue}{\cite{Maezawa:2016vgv}}  & 2+1   & \gA & \soso
                   & \bad  & \soso          
                   & {$r_1 = 0.3106(18)\,\mbox{fm}$$^{d}$}  
                   & 309(10)$^{e}$             & 0.739(24)$^{e}$  \\
       {JLQCD 16}   & \cite{Nakayama:2016atf}  
                   & 2+1     & \gA & \soso 
                   & \soso  & \soso           
                   & {$\sqrt{t_0} = 0.1465(25)\,\mbox{fm}$}
                   & {331(38)$^{f}$}  &  0.792(89)$^{f}$ \\
      HPQCD 10     & \cite{McNeile:2010ji}  & 2+1       & \gA & \soso
                   & \good   & \soso           
                   & $r_1 = 0.3133(23)\, \mbox{fm}$$^\dagger$
                   & 338(10)$^\star$           &  0.809(25)           \\
      HPQCD 08B    & \cite{Allison:2008xk}  & 2+1       & \gA & \bad 
                   & \bad  & \bad           
                   & $r_1 = 0.321(5)\,\mbox{fm}$$^\dagger$  
                   & 325(18)$^+$             &  0.777(42)            \\
      &&&&&&&&& \\[-0.1cm]
      \hline
      \hline\\
\end{tabular*}\\[-0.2cm]
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
   \item[$^a$]  Scale determined in \cite{Dowdall:2013rya} using $f_\pi$. \\[-5mm]
   \item[$^b$]  $\alpha^{(4)}_\msbar(5\,\mbox{GeV}) = 0.2128(25)$, 
         $\alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.11822(74)$.         \\[-5mm]
   \item[$^c$] We evaluated $\Lambda_{\overline{\rm MS}}^{(4)}$ from $\alpha^{(4)}_\msbar$. 
         We also used $r_0 = 0.472\,\mbox{fm}$.\\[-5mm]
   \item[$^{d}$] 
   Scale is determined from $f_\pi$ . 
    \\[-5mm]
   \item[$^{e}$]       $\alpha^{(3)}_\msbar(m_c=1.267\,\mbox{GeV}) = 0.3697(85)$,
               $\alpha^{(5)}_\msbar(M_Z) = 0.11622(84)$. Our conversion with $r_0 = 0.472\,\mbox{fm}$.         
               \\[-5mm]
    \item[$^{f}$]  We evaluated $\Lambda_{\overline{\rm MS}}^{(3)}$ from the given $\alpha^{(4)}_\msbar(3\,\mbox{GeV}) = 0.2528(127)$.
          $\alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.1177(26)$.
          We also used $r_0 = 0.472\,\mbox{fm}$ to convert.   
   \\[-5mm]
   \item[$^\star$]  $\alpha^{(3)}_\msbar(5\,\mbox{GeV}) = 0.2034(21)$,
            $\alpha^{(5)}_\msbar(M_Z) = 0.1183(7)$.         \\[-4mm]
   \item[$^\dagger$] Scale is determined from $\Upsilon$ mass splitting.    \\[-5mm]
   \item[$^+$]  We evaluated $\Lambda_{\overline{\rm MS}}^{(3)}$ from the given $\alpha^{(4)}_\msbar(3\,\mbox{GeV}) = 0.251(6)$. $\alpha^{(5)}_\msbar(M_Z) = 0.1174(12)$.       

\end{itemize}
}
\end{minipage}
\normalsize
\caption{Heavy-quark current two-point function results. Note that all analysis using $2+1$
  flavour simulations perturbatively add a dynamical charm quark.
  Partially they then quote results in $\Nf=4$-flavour 
  QCD, which we converted back to $\Nf=3$, corresponding to the
  nonperturbative sea quark content.}
\label{tab_current_2pt}
\end{table}

The method has originally been applied in HPQCD 08B \cite{Allison:2008xk}
and in HPQCD 10 \cite{McNeile:2010ji}, based on the MILC ensembles with
$2 + 1$ flavours of Asqtad staggered quarks and HISQ valence quarks.  
Both use $R_n$ while the latter also used a range of
quark masses $m_c$ in addition to the physical charm mass.

The scale was set using $r_1 = 0.321(5)\,\mbox{fm}$ in HPQCD 08B 
\cite{Allison:2008xk} and the updated value  $r_1 = 0.3133(23)\,\mbox{fm}$
in HPQCD 10 \cite{McNeile:2010ji}. The effective range of couplings used
is here given for $n = 4$, which is the moment most dominated by short
(perturbative) distances and important in the determination of $\alpha_s$.
The range is similar for other ratios. With $r_{4,1} = 0.7427$ and 
$R_4 = 1.28$ determined in the continuum limit at the charm mass in 
Ref.~\cite{Allison:2008xk}, we have $\alpha_{\rm eff} = 0.38$ at the 
charm-quark mass, which is the mass value where HPQCD 08B 
\cite{Allison:2008xk} carries out the analysis.
In HPQCD 10 \cite{McNeile:2010ji} a set of masses is used,
with $\tilde{R}_4 \in [1.09, 1.29]$, which corresponds 
to $\alpha_{\rm eff} \in [0.12, 0.40]$. The available data of 
HPQCD 10 \cite{McNeile:2010ji} is reviewed in FLAG 13.
For the continuum limit criterion, we choose the scale $\mu = 2\bar m_c
\approx m_{\eta_c}/1.1$, where we have taken $\bar m_c$ in the $\msbar$
scheme at scale $\bar m_c$ and the numerical value $1.1$ was determined in
HPQCD 10B \cite{Na:2010uf}. With these choices for $\mu$, 
the continuum limit criterion is satisfied
for three lattice spacings when $\alpha_\mathrm{eff} \leq 0.3$ and $n=4$.

Larger-$n$ moments are more influenced by nonperturbative effects.
For the $n$ values considered, adding a gluon condensate term only
changed error bars slightly in HPQCD's analysis.
We note that HPQCD in their papers perform a global fit to all data
using a joint expansion in powers of $\alpha_s^n$, $(\Lambda/(m_{\eta_c}/2))^j$
to parameterize the heavy-quark mass dependence, and $( am_{\eta_c}/2)^{2i}$
to parameterize the lattice-spacing dependence. To obtain a good fit,
they must exclude data with $am_{\eta_c} > 1.95$ and include
lattice-spacing terms $a^{2i}$ with $i$ greater than $10$.  Because
these fits include many more fit parameters than data points, HPQCD
uses their expectations for the sizes of coefficients as Bayesian
priors. The fits include data with masses as large as 
$am_{\eta_c}/2 \sim0.86$, so there is only minimal suppression of the many
high-order contributions for the heavier masses.  It is not clear,
however, how sensitive the final results are to the larger
$am_{\eta_c}/2$ values in the data. The continuum limit of the
fit is in agreement with a perturbative scale dependence (a
5-loop running $\alpha_{\overline{\rm MS}}$ with a fitted
5-loop coefficient in the $\beta$-function is used). Indeed, Fig.~2
of Ref.~\cite{McNeile:2010ji} suggests that HPQCD's fit describes
the data well.

A more recent computation, HPQCD 14A  \cite{Chakraborty:2014aca}
uses $\tilde{R}_n$ and is based on MILC's 2+1+1 HISQ staggered ensembles.
Compared to HPQCD 10 \cite{McNeile:2010ji} valence- and 
sea-quarks now use the same discretization and the scale is set 
through the gradient flow scale $w_0$, determined to 
$w_0=0.1715(9)\,\fm$ in Ref.~\cite{Dowdall:2012ab}.
A number of data points satisfy our continuum limit criterion
$a\mu < 1.5$, at two different lattice spacings. 
This does not by itself lead to a \soso\ but the next-larger 
lattice spacing does not miss the criterion by much
\ifx\reducedapptables\undefined
, see Tab.~\ref{tab_Nf=4_continuumlimit}
\fi.
We therefore assign a \soso\ in that criterion.

%\textcolor{blue}{
Two new computations have appeared since the last FLAG report.
Maezawa and Petreczky, \cite{Maezawa:2016vgv} computed the two-point
functions of the $c\bar{c}$ pseudoscalar operator and obtained 
$R_4$, $R_6/R_8$ and $R_8/R_{10}$ based on the HotQCD collaboration
HISQ staggered ensembles, \cite{Bazavov:2014pvz}. The scale is set by measuring
$r_1=0.3106(18)$ fm. Continuum limits are taken fitting the lattice
spacing dependence with $a^2+a^4$ form as the best fit. For $R_4$,
they also employ other forms for fit functions such as $a^2$,
$\alpha_s^{\rm boosted} a^2+a^4$, etc., the results agreeing within errors.
Matching $R_4$ with the 3-loop formula Eq. (\ref{rn_expan}) 
through order $\alpha_\msbar^3$ \cite{Chetyrkin:2006xg}, 
where $\mu$ is fixed to $m_c$, they obtain
$\alpha^{(3)}_{\overline{\rm MS}}(\mu=m_c) = 0.3697(54)(64)(15)$. The 
first error is statistical, the second is the uncertainty 
in the continuum extrapolation, and the third is the truncation error
in the perturbative approximation of $r_4$. This last error is estimated
by the ``typical size'' of the missing 4-loop contribution,  
which they assume to be $\alpha^4_{\overline{\rm MS}}(\mu)$ multiplied by
2 times the 3-loop coefficient 
$2 \times r_{4,3} \times \alpha^4_{\msbar}(\mu) 
= 0.2364 \times \alpha^4_{\msbar}(\mu)$.
The result is converted to
\begin{eqnarray}
   \alpha^{(5)}_{\msbar}(M_Z) = 0.11622(84) \,.
\end{eqnarray}
Since $\alpha_{\rm eff}(2m_c)$ reaches 0.25, we assign
$\soso$ for the criterion of the renormalization scale. 
As $\Delta \Lambda / \Lambda \sim \alpha_{\rm eff}^2$, we assign
$\bad$ for the criterion of perturbative behaviour.
The lattice cutoff ranges as  $a^{-1} $ = 1.42--4.89~GeV with 
$\mu=2m_c\sim 2.6$ GeV so that we assign $\soso$ for continuum extrapolation.

JLQCD 16 \cite{Nakayama:2016atf} also computed the two-point functions
of the $c\bar{c}$ pseudoscalar operator and obtained $R_6$, $R_8$, $R_{10}$
and their ratios  based on 2+1 flavour QCD with M\"obius domain-wall
quark for three lattice cutoff $a^{-1}$ = 2.5, 3.6, 4.5~GeV.
The scale is set by $\sqrt{t_0}=0.1465(21)(13)\,\mbox{fm}$.
The continuum limit is taken assuming linear dependence on $a^2$.
They find a sizeable lattice-spacing dependence of $R_4$, which is 
therefore not used in their analysis, but for $R_6,R_8, R_{10}$
the dependence is mild giving reasonable control over the continuum limit.
They use the perturbative formulae for the vacuum polarization in the
pseudoscalar channel $\Pi_{PS}$ through order $\alpha_\msbar^3$ in the 
$\overline{\rm MS}$ scheme \cite{Maier:2008he, Maier:2009fz} to obtain 
$\alpha^{(4)}_{\overline{\rm MS}}$. Combining the matching of lattice
results with continuum perturbation theory for $R_6$, $R_6/R_8$ and $R_{10}$,
they obtain $\alpha^{(4)}_{\overline{\rm MS}}(\mu=3\,\GeV)=0.2528(127)$,
where the error is dominated by the perturbative truncation error.
To estimate the truncation error they study the dependence of the final result 
on the choice of the renormalization  scales $\mu_\alpha, \;\mu_\mathrm{m}$ which are used as renormalization scales for
$\alpha$ and the quark mass. Independently~\cite{Dehnadi:2015fra} the two scales
are varied in the range of 2~GeV to 4~GeV. 
The above result is converted to $\alpha^{(5)}_\msbar(M_Z)$ as
\begin{eqnarray}
   \alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.1177(26) \,.
\end{eqnarray}%
Since $\alpha_{\rm eff}$ roughly reaches 0.25, they have $\soso$ 
for the renormalization scale criterion. Since 
$\Delta \Lambda / \Lambda >  \alpha_{\rm eff}^2$, we also assign
$\soso$ for the criterion of perturbative behaviour. The lattice cutoff
ranges over  $a^{-1}$ = 2.5--4.5~GeV with $\mu=3$ GeV so we also give
them a $\soso$ for continuum extrapolation.

\vspace*{0.15cm}
There is a significant difference in the perturbative error
estimate of JLQCD 16 \cite{Nakayama:2016atf} 
and Maezawa 16 \cite{Maezawa:2016vgv}, both of which use the moments at
the charm mass. JLQCD 16 uses the scale dependence (see also 
\sect{s:trunc}) but  Maezawa 16 looks at the perturbative coefficients at 
$\mu=m_*$, with $\mbar_c(m_*)=m_*$. While the 
Maezawa 16 result derives from $R_4$, JLQCD 16 did not use that moment and
therefore did not show its renormalization-scale dependence. We provide it here
and show $\alpha(m_*)$ extracted from 
$R_4$ expanded in $\alpha(\mu)$ for $\mu=s\,m_*$ (and evolved to  $\mu=m_*$)
in \fig{scaledepR4}.
Note that the perturbative error estimated by Maezawa 16 is a small contribution 
to the total error, while the scale dependence in \fig{scaledepR4} is significant
between, e.g.,  $s=1$ and $s=4$.
This is a confirmation of our \bad\ in the perturbative error criterion 
which is linked to the cited overall error as spelled out in 
\sect{s:crit}.
\begin{figure}[!htb]
   \hspace{0.5cm}
   %\hspace{-2cm}
   \begin{center}
      \includegraphics[width=9.0cm] {Alpha_s/Figures/scaledep_alpha_R4}
      \end{center}
   %\vspace{-1cm}
\caption{Renormalization-scale ($\mu$) dependence of $\alpha(m_*)$ extracted from $R_4$. We have evaluated this dependence for the case where the same renormalization scale is used for the quark mass and for $\alpha_s$.}
\label{scaledepR4}
\end{figure}

Aside from the final results for $\alpha_s(m_Z)$ obtained by matching with perturbation theory, it is 
interesting to make a comparison of the short distance quantities 
in the continuum limit $R_n$ which are available from 
HPQCD 08~\cite{Allison:2008xk}, 
JLQCD 16 \cite{Nakayama:2016atf} and Maezawa 16 \cite{Maezawa:2016vgv}
(all using $2+1$ flavours). In Fig~\ref{Rm0p3R6m0p5current2pt}
\begin{figure}[!htb]
   \hspace{-2cm}
   \begin{center}
      \includegraphics[width=10.0cm] {Alpha_s/Figures/Rm0p3R6m0p5current2pt}
      \end{center}
   \vspace{-0cm}
\caption{Ratios from Tab.~\protect\ref{Rn_moments}. Note that constants
         have been subtracted from $R_4$, $R_6$ and $R_{10}$, to
         be able to plot all results in a similar range.}
\label{Rm0p3R6m0p5current2pt}
\end{figure}
we plot the various results based on the numbers collated in 
Tab.~\ref{Rn_moments}.
\begin{table}[h]
\begin{center}
\begin{tabular}{c|cccc}
\hline
      & HPQCD 08  & Maezawa 16 & JLQCD 16       \\
\hline
$R_4$ &   1.281(5)  &   1.274(7)          & -\\
$R_6$ & 1.528(11)  & 1.520(4)& 1.509(7)\\
$R_8$ & 1.370(10)   & 1.367(8)& 1.359(4)\\
$R_{10}$ &1.304(9)  & 1.302(8)& 1.297(4)\\
$R_6/R_8$ &1.113(2) &  1.114(2)& 1.111(2)\\
$R_8/R_{10}$ &1.049(2) &  1.0495(7)& 1.0481(9)\\
\hline
\end{tabular}
\end{center}
\caption{Moments from $N_f=3$ simulations at the charm mass.
The moments have been corrected 
perturbatively to include the effect of a charm sea quark.}
\label{Rn_moments}
\end{table} 
These results are in quite good agreement with each other.
For future studies it is of course interesting to check
agreement of these numbers before turning to the more
involved determination of $\alpha_s$.

In Tab.~\ref{tab_current_2pt} we summarize the results for the latter.


% ----------------------------------------------------------------------
