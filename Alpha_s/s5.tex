
\subsection{$\alpha_s$ from the vacuum polarization at short distances}

% ----------------------------------------------------------------------

\label{s:vac}

% ----------------------------------------------------------------------

\subsubsection{General considerations}

% ----------------------------------------------------------------------

The vacuum polarization function for the flavour nonsinglet 
currents $J^a_\mu$ ($a=1,2,3$) in the momentum representation is
parameterized as 
\begin{eqnarray}
   \langle J^a_\mu J^b_\nu \rangle 
      =\delta^{ab} [(\delta_{\mu\nu}Q^2 - Q_\mu Q_\nu) \Pi^{(1)}(Q) 
                                     - Q_\mu Q_\nu\Pi^{(0)}(Q)] \,,
\end{eqnarray}
where $Q_\mu$ is a space-like momentum and $J_\mu\equiv V_\mu$
for a vector current and $J_\mu\equiv A_\mu$ for an axial-vector current. 
Defining $\Pi_J(Q)\equiv \Pi_J^{(0)}(Q)+\Pi_J^{(1)}(Q)$,
the operator product expansion (OPE) of the vacuum polarization
function $\Pi_{V+A}(Q)=\Pi_V(Q)+\Pi_A(Q)$ is given by
\begin{eqnarray}
   \lefteqn{\Pi_{V+A}|_{\rm OPE}(Q^2,\alpha_s)}
      & &                                             \nonumber  \\
      &=& c + C_1(Q^2) + C_m^{V+A}(Q^2)
                       \frac{\bar{m}^2(Q)}{Q^2}
            + \sum_{q=u,d,s}C_{\bar{q}q}^{V+A}(Q^2)
                        \frac{\langle m_q\bar{q}q \rangle}{Q^4}
                                                      \nonumber  \\
      & &   + C_{GG}(Q^2) 
                \frac{\langle \alpha_s GG\rangle}{Q^4}+{\cO}(Q^{-6}) \,,
\label{eq:vacpol}
\end{eqnarray}
for large
$Q^2$. The perturbative coefficient functions $C_X^{V+A}(Q^2)$ for the
operators $X$ ($X=1$, $\bar{q}q$, $GG$) are given as $C_X^{V+A}(Q^2)=\sum_{i\geq0}\left( C_X^{V+A}\right)^{(i)}\alpha_s^i(Q^2)$  and $\bar m$ is the running 
mass of the mass-degenerate up and down quarks.
$C_1$ is known including $\alpha_s^4$
in a continuum renormalization scheme such as the
%{\cred 
$\overline{\rm MS}$ scheme
\cite{Chetyrkin:1979bj,Surguladze:1990tg,Gorishnii:1990vf,Baikov:2008jh}.
%}
Nonperturbatively, there are terms in $C_X$ that do not have a 
series expansion in $\alpha_s$. For an example for the unit
operator see Ref.~\cite{Balitsky:1993ki}.
The term $c$ is $Q$-independent and divergent in the limit of infinite
ultraviolet cutoff. However the Adler function defined as 
\begin{eqnarray}
   D(Q^2) \equiv - Q^2 { d\Pi(Q^2) \over dQ^2} \,,
\label{eq:adler}
\end{eqnarray}
is a scheme-independent finite quantity. Therefore one can determine
the running coupling constant in the $\overline{\rm MS}$ scheme
from the vacuum polarization function computed by a lattice-QCD
simulation. 
%
In more detail, the lattice data of the vacuum polarization is fitted with the 
perturbative formula Eq.~(\ref{eq:vacpol}) with fit parameter 
$\Lambda_{\overline{\rm MS}}$ parameterizing the running coupling 
$\alpha_{\overline{\rm MS}}(Q^2)$.  

While there is no problem in discussing the OPE at the
nonperturbative level, the `condensates' such as ${\langle \alpha_s
  GG\rangle}$ are ambiguous, since they mix with lower-dimensional
operators including the unity operator.  Therefore one should work in
the high-$Q^2$ regime where power corrections are negligible within
the given accuracy. Thus setting the renormalization scale as
$\mu\equiv \sqrt{Q^2}$, one should seek, as always, the window
$\Lambda_{\rm QCD} \ll \mu \ll a^{-1}$.


% --------------------------------------------------------------------

\subsubsection{Discussion of computations}

% --------------------------------------------------------------------

Results using this method are, to date, only available using
overlap fermions or domain wall fermions. Since the last review, FLAG 16,
there has been one new computation, Hudspith 18~\cite{Hudspith:2018bpz}.
These are collected in Tab.~\ref{tab_vac} for
\begin{table}[!htb]
   \vspace{3.0cm}
   \footnotesize
   \begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}rllllllll}
   Collaboration & Ref. & $\Nf$ &
   \hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{renormalization scale}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{perturbative behaviour}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}
      \hspace{-0.25cm} & %\rule{0.2cm}{0cm} 
                         scale & $\Lambda_\msbar[\MeV]$ & $r_0\Lambda_\msbar$ \\
   & & & & & & & & & \\[-0.1cm]
   \hline
   \hline
   & & & & & & & & & \\[-0.1cm]

   Hudspith 18 & \cite{Hudspith:2018bpz} & 2+1 & \oP
            & \soso  & \soso   & \bad  
            &  $m_\Omega$$^\star$
            & $337(40)$
            & $0.806(96)$$^a$               \\
   & & & & & & & & & \\[-0.1cm]
   \hline
   & & & & & & & & & \\[-0.1cm]

   Hudspith 15 & \cite{Hudspith:2015xoa} & 2+1 &\rC 
            & \soso  & \soso   & \bad  
            & $m_\Omega$$^\star$
            & $300(24)^+$
            & $0.717(58)$              \\
   & & & & & & & & & \\[-0.1cm]
   \hline
   & & & & & & & & & \\[-0.1cm]
   JLQCD 10 & \cite{Shintani:2010ph} & 2+1 &\gA & \bad 
            & \soso & \bad
            & $r_0 = 0.472\,\mbox{fm}$
            & $247(5)$$^\dagger$
            & $0.591(12)$              \\
   & & & & & & & & & \\[-0.1cm]
   \hline
   & & & & & & & & & \\[-0.1cm]
   JLQCD/TWQCD 08C & \cite{Shintani:2008ga} & 2 & \gA & \soso 
            & \soso & \bad
            & $r_0 = 0.49\,\mbox{fm}$
            & $234(9)(^{+16}_{-0})$
            & $0.581(22)(^{+40}_{-0})$    \\
            
   & & & & & & & & & \\[-0.1cm]
   \hline
   \hline
\end{tabular*}
\begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}llllllll}
\multicolumn{8}{l}{\vbox{\begin{flushleft}
   $^\star$ Determined in \cite{Blum:2014tka}.  \\
   $^a$ 
        $\alpha_\msbar^{(5)}(M_Z)=0.1181(27)(^{+8}_{-22})$. $\Lambda_\msbar$
        determined by us from $\alpha_\msbar^{(3)}(2\,\mbox{GeV})=0.2961(185)$.
        In conversion to $r_0\Lambda$ we used
        $r_0 = 0.472\,\mbox{fm}$.  \\
        $^+$ Determined by us from $\alpha_\msbar^{(3)}(2\,\GeV)=0.279(11)$. 
       Evaluates to $\alpha_\msbar^{(5)}(M_Z)=0.1155(18)$. \\
   $^\dagger$  $\alpha_\msbar^{(5)}(M_Z)=0.1118(3)(^{+16}_{-17})$. \\
   
\end{flushleft}}}
\end{tabular*}
\vspace{-0.3cm}
\normalsize
\caption{Vacuum polarization results.}
\label{tab_vac}
\end{table}
$N_f=2$, JLQCD/TWQCD 08C \cite{Shintani:2008ga} and for $N_f = 2+1$, JLQCD 10
\cite{Shintani:2010ph} and Hudspith 18~ \cite{Hudspith:2018bpz}.

We first discuss the results of JLQCD/TWQCD 08C 
\cite{Shintani:2008ga} and JLQCD 10 \cite{Shintani:2010ph}.
The fit to \eq{eq:vacpol} is done with the 4-loop relation between
the running coupling and $\lms$. It is found that without introducing
condensate contributions, the momentum scale where the perturbative
formula gives good agreement with the lattice results is very narrow,
$aQ \simeq 0.8-1.0$. When a condensate contribution is included the
perturbative formula gives good agreement with the lattice results for
the extended range $aQ \simeq 0.6-1.0$. Since there is only a single
lattice spacing $a \approx 0.11\,\mbox{fm}$ there is a 
\bad\ for the continuum limit. The renormalization scale $\mu$ is in
the range of $Q=1.6-2\,\mbox{GeV}$. Approximating 
$\alpha_{\rm eff}\approx \alpha_{\overline{\rm MS}}(Q)$, we estimate that
$\alpha_{\rm eff}=0.25-0.30$ for $N_f=2$ and $\alpha_{\rm  eff}=0.29-0.33$
for $N_f=2+1$. Thus we give a \soso\ and \bad\ for $\Nf=2$ and 
$\Nf=2+1$, respectively, for the renormalization scale and a \bad\ for
the perturbative behaviour.

A further investigation of this method was initiated in
Hudspith 15 \cite{Hudspith:2015xoa} and completed by Hudspith 18 
\cite{Hudspith:2018bpz} (see also \cite{Hudspith:2018zlq}) based
on domain wall fermion configurations at three lattice spacings, 
$a^{-1} = 1.78,\, 2.38,\, 3.15 \,\GeV$, with three different 
light quark masses on the two coarser lattices and one on the fine lattice.
An extensive discussion of condensates, using continuum
finite energy sum rules was employed to estimate where their contributions
might be negligible. It was found that even up to terms
of $O((1/Q^2)^8)$ (a higher order than depicted in Eq.~(\ref{eq:vacpol})
but with constant coefficients) 
no single condensate dominates
and apparent convergence was poor for low $Q^2$
due to cancellations between contributions of similar size
with alternating signs. (See, e.g., \ the list given by Hudspith 15
\cite{Hudspith:2015xoa}.) Choosing $Q^2$ to be at least
$\sim 3.8\,\mbox{GeV}^2$ mitigated the problem, but then the coarest
lattice had to be discarded, due to large lattice artifacts.
So this gives a $\bad$ for continuum extrapolation.
With the higher $Q^2$ the quark-mass dependence of the
results was negligible, so ensembles with different quark masses were
averaged over.
A range of $Q^2$ from $3.8$ -- $16\,\mbox{GeV}^2$ gives 
$\alpha_{\rm eff} = 0.31$ -- $0.22$,  so there is a $\soso$ for the
renormalization scale.
The value of $\alpha_{\rm eff}^3$ reaches
$\Delta \alpha_{\rm eff}/(8\pi b_0 \alpha_{\rm eff})$ and thus
gives a $\soso$ for perturbative behaviour.
In Hudspith 15 \cite{Hudspith:2015xoa} (superseded by Hudspith 18 
\cite{Hudspith:2018bpz}) about a 20\% difference in 
$\Pi(Q^2)$ was seen between the two lattice lattice spacings and a
result is quoted only for the smaller $a$.


% ----------------------------------------------------------------------
