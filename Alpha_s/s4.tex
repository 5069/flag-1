
\subsection{$\alpha_s$ from the potential at short distances}
\label{s:qq}

% ----------------------------------------------------------------------

\subsubsection{General considerations}

% --------------------------------------------------------------------


The basic method was introduced in Ref.~\cite{Michael:1992nj} and developed in
Ref.~\cite{Booth:1992bm}. The force or potential between an infinitely
massive quark and antiquark pair defines an effective coupling
constant via
\begin{eqnarray}
   F(r) = {d V(r) \over dr} 
        = C_F {\alpha_\mathrm{qq}(r) \over r^2} \,.
\label{force_alpha}
\end{eqnarray}
The coupling can be evaluated nonperturbatively from the potential
through a numerical differentiation, see below. In perturbation theory
one also defines couplings in different schemes $\alpha_{\bar{V}}$,
$\alpha_V$ via 
\begin{eqnarray}
   V(r) = - C_F {\alpha_{\bar{V}}(r) \over r} \,, 
   \qquad \mbox{or} \quad
   \tilde{V}(Q) = - C_F {\alpha_V(Q) \over Q^2} \,,
\label{pot_alpha}
\end{eqnarray}
where one fixes the unphysical constant in the potential
by $\lim_{r\to\infty}V(r)=0$ and $\tilde{V}(Q)$ is the
Fourier transform of $V(r)$. Nonperturbatively, the subtraction
of a constant in the potential introduces an additional 
renormalization constant, the value of $V(r_\mathrm{ref})$ at some 
distance $r_\mathrm{ref}$.  Perturbatively, it is believed to entail a 
renormalon ambiguity. In perturbation theory, the different definitions
are all simply related to each other, and their perturbative
expansions are known including the $\alpha_s^4,\,\alpha_s^4 \log\alpha_s$ 
and $\alpha_s^5 \log\alpha_s ,\,\alpha_s^5 (\log\alpha_s)^2$  terms
% EB: PLEASE KEEP CITES IN ONE LINE -- otherwise the script for acronym replacement will fail
\cite{Fischler:1977yf,Billoire:1979ih,Peter:1997me,Schroder:1998vy,Brambilla:1999qa,Smirnov:2009fh,Anzai:2009tm,Brambilla:2009bi}.
 
The potential $V(r)$ is determined from ratios of Wilson loops,
$W(r,t)$, which behave as
\begin{eqnarray}
   \langle W(r, t) \rangle 
      = |c_0|^2 e^{-V(r)t} + \sum_{n\not= 0} |c_n|^2 e^{-V_n(r)t} \,,
      \label{e:vfromw}
\end{eqnarray}
where $t$ is taken as the temporal extension of the loop, $r$ is the
spatial one and $V_n$ are excited-state potentials.  To improve the
overlap with the ground state, and to suppress the effects of excited
states, $t$ is taken large. Also various additional techniques are
used, such as a variational basis of operators (spatial paths) to help
in projecting out the ground state.  Furthermore some
lattice-discretization effects can be reduced by averaging over Wilson
loops related by rotational symmetry in the continuum.

In order to reduce discretization errors it is of advantage 
to define the numerical derivative giving the force as
\begin{eqnarray}
   F(r_\mathrm{I}) = { V(r) - V(r-a) \over a } \,,
\end{eqnarray}
where $r_\mathrm{I}$ is chosen so that at tree level the force is the
continuum force. $F(r_\mathrm{I})$ is then a `tree-level improved' quantity
and similarly the tree-level improved potential can be defined
\cite{Necco:2001gh}.

Lattice potential results are in position space,
while perturbation theory is naturally computed in momentum space at
large momentum.
Usually, the Fourier transform 
of the perturbative expansion is then matched to  lattice data.

Finally, as was noted in Sec.~\ref{s:crit}, a determination
of the force can also be used to determine the scales $r_0,\,r_1$,
by defining them from the static force by
\begin{eqnarray}
   r_0^2 F(r_0) = {1.65} \,, \quad r_1^2 F(r_1) = 1\,.
\end{eqnarray}

% --------------------------------------------------------------------

\subsubsection{Discussion of computations}
\label{short_dist_discuss}

% --------------------------------------------------------------------


In Tab.~\ref{tab_short_dist}, we list results of determinations
\begin{table}[!htb]
   \vspace{3.0cm}
   \footnotesize
   \begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}rlllllllll}
      Collaboration & Ref. & $N_f$ &
      \hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{renormalization scale}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{perturbative behaviour}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}
                               \hspace{-0.25cm} & %\rule{0.2cm}{0cm} 
                         scale & $\Lambda_\msbar[\MeV]$ & $r_0\Lambda_\msbar$ \\
      & & & & & & & & & \\[-0.1cm]
      \hline
      \hline
% \good \soso  \bad
      & & & & & & & & & \\[-0.1cm]

     Takaura 18
                   & \cite{Takaura:2018lpw,Takaura:2018vcy} & 2+1  & \oP 
                   &  \bad     &  \soso     & \soso 
                   & $\sqrt{t_0}=0.1465(25)\fm$, $^a$
                   & $334(10)(^{+20}_{-18})^b$
                   & $0.799(51)$$^+$                                 \\

      {Bazavov 14}
                   & \cite{Bazavov:2014soa}  & 2+1       & \gA & \soso
                   & \good  & \soso
                   & $r_1 = 0.3106(17)\,\mbox{fm}^c$
                   & $315(^{+18}_{-12})^d$
                   & $0.746(^{+42}_{-27})$                              \\

      {Bazavov 12}
                   & \cite{Bazavov:2012ka}   & 2+1       & \gA & \soso$^\dagger$
                   & \soso   & \soso$^\#$
                   & $r_0 = 0.468\,\mbox{fm}$ 
                   & $295(30)$\,$^\star$ 
                   & $0.70(7)$$^{\star\star}$                                   \\
      & & & & & & & & & \\[-0.1cm]
      \hline
      & & & & & & & & & \\[-0.1cm]

     Karbstein 18 
                   & \cite{Karbstein:2018mzo} & 2        & \gA
                   & \soso        &  \soso    & \soso
                   & $r_0 = 0.420(14)\,\mbox{fm}$$^e$
                   & $302(16)$
                   & $0.643(34)$                                       \\

     Karbstein 14 
                   & \cite{Karbstein:2014bsa} & 2        & \gA & \soso
                   & \soso & \soso
                   & $r_0 = 0.42\,\mbox{fm}$
                   & $331(21)$
                   & 0.692(31)                                         \\

      ETM 11C      & \cite{Jansen:2011vv}    & 2         & \gA & \soso  
                   & \soso  & \soso
                   & $r_0 = 0.42\,\mbox{fm}$
                   & $315(30)$$^\S$ 
                   & $0.658(55)$                                        \\
      & & & & & & & & & \\[-0.1cm]
      \hline
      & & & & & & & & & \\[-0.1cm]

      Husung 17    & \cite{Husung:2017qjz}   & 0         & C
                   & \soso & \good   & \good
                   &  $r_0 = 0.50\,\mbox{fm}$ 
                   & 232(6) & $0.590(16)$  \\

      Brambilla 10 & \cite{Brambilla:2010pp} & 0         & \gA & \soso 
                   & \good\ & \soso$^{\dagger\dagger}$ &  & $266(13)$$^{+}$&
                   $0.637(^{+32}_{-30})$$^{\dagger\dagger}$                  \\
      UKQCD 92     & \cite{Booth:1992bm}    & 0         & \gA & \good 
                                  & \soso$^{++}$   & \bad   
                                  & $\sqrt{\sigma}=0.44\,\GeV$ 
                                  & $256(20)$
                                  & 0.686(54)                             \\
      Bali 92     & \cite{Bali:1992ru}    & 0         & \gA & \good 
                                  & \soso$^{++}$   & \bad 
                                  & $\sqrt{\sigma}=0.44\,\GeV$
                                  & $247(10)$                             
                                  & 0.661(27)                             \\
      & & & & & & & & & \\[-0.1cm]
      \hline
      \hline\\
\end{tabular*}\\[-0.2cm]
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
   \item[$^a$] Scale determined from $t_0$ in
               Ref.~\cite{Borsanyi:2012zs}.
   \item[$^b$]             
               $\alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.1179(7)(^{+13}_{-12})$.  
   \item[$^c$]
   Determination on lattices with $m_\pi L=2.2 - 2.6$. 
   Scale from $r_1$ \cite{Bazavov:2014pvz}
   as determined from  $f_\pi$ in Ref.~\cite{Bazavov:2010hj}.      \\[-5mm]
   \item[$^d$]
         $\alpha^{(3)}_{\overline{\rm MS}}(1.5\,\mbox{GeV}) = 0.336(^{+12}_{-8})$, 
         $\alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.1166(^{+12}_{-8})$.
         \\[-5mm]
   \item[$^e$] 
         Scale determined from $f_\pi$, see \cite{Baron:2009wt}.   \\[-5mm]
   \item[$^\dagger$]
   Since values of $\alpha_\mathrm{eff}$ within our designated range are used,
   we assign a \soso\ despite
   values of $\alpha_\mathrm{eff}$ up to $\alpha_\mathrm{eff}=0.5$ being used.  
   \\[-5mm]
   \item[$^\#$]Since values of $2a/r$ within our designated range are used,
   we assign a \soso\ although
   only values of $2a/r\geq1.14$ are used at $\alpha_\mathrm{eff}=0.3$.
   \\[-5mm]
   \item[$^\star$] Using results from Ref.~\cite{Bazavov:2011nk}.  \\[-5mm]
   \item[$^{\star\star}$]
         $\alpha^{(3)}_{\overline{\rm MS}}(1.5\,\mbox{GeV}) = 0.326(19)$, 
         $\alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.1156(^{+21}_{-22})$.  \\[-5mm]
   \item[$^\S$] Both potential and $r_0/a$ are determined on a small 
   ($L=3.2r_0$) lattice.   \\[-5mm]
   \item[$^{\dagger\dagger}$] Uses lattice results of Ref.~\cite{Necco:2001xg}, 
   some of which have very small lattice spacings where 
   according to more recent investigations a bias due to the freezing of
   topology may be present.  \\[-5mm] 
   \item[$^+$] Our conversion using $r_0 = 0.472\,\mbox{fm}$.   \\[-5mm]
   \item[$^{++}$] We give a $\soso$ because only a NLO formula is used and
       the error bars are very large; our criterion does not apply 
       well to these very early calculations.           
\end{itemize}
}
\end{minipage}
\normalsize
\caption{Short-distance potential results.}
\label{tab_short_dist}
\end{table}
of $r_0\Lambda_{\msbar}$ (together with $\Lambda_{\msbar}$
using the scale determination of the authors).
Since the last review, FLAG 16, there have been
three new computations, Husung 17 \cite{Husung:2017qjz},
Karbstein 18 \cite{Karbstein:2018mzo} and 
Takaura 18 \cite{Takaura:2018lpw,Takaura:2018vcy}.

The first determinations in the three-colour Yang Mills theory are by
UKQCD 92 \cite{Booth:1992bm} and Bali 92 \cite{Bali:1992ru} who used
$\alpha_\mathrm{qq}$ as explained above, but not in the tree-level
improved form. Rather a phenomenologically determined lattice artifact
correction was subtracted from the lattice potentials.  The comparison
with perturbation theory was on a more qualitative level on the basis
of a 2-loop $\beta$-function ($n_l=1$) and a continuum extrapolation
could not be performed as yet. A much more precise computation of
$\alpha_\mathrm{qq}$ with continuum extrapolation was performed in
Refs.~\cite{Necco:2001xg,Necco:2001gh}. Satisfactory agreement with
perturbation theory was found \cite{Necco:2001gh} but the stability of
the perturbative prediction was not considered sufficient to be able
to extract a $\Lambda$ parameter.

In Brambilla 10 \cite{Brambilla:2010pp} the same quenched lattice
results of Ref.~\cite{Necco:2001gh} were used and a fit was performed to
the continuum potential, instead of the force. Perturbation theory to
$n_l=3$ loop
was used including a resummation of terms $\alpha_s^3 (\alpha_s \ln\alpha_s)^n $ 
and $\alpha_s^4 (\alpha_s \ln\alpha_s)^n $. Close
agreement with perturbation theory was found when a renormalon
subtraction was performed. Note that the renormalon subtraction
introduces a second scale into the perturbative formula which is
absent when the force is considered.

Bazavov 14 \cite{Bazavov:2014soa} updates 
Bazavov 12 \cite{Bazavov:2012ka} and modifies this procedure
somewhat. They consider the 
perturbative expansion
for the force. 
They set $\mu = 1/r$
to eliminate logarithms and then integrate the force to obtain an
expression for the potential. 
The resulting integration constant is fixed by requiring
the perturbative potential to be equal to the nonperturbative 
one exactly at a reference distance $r_{\rm ref}$ and the two are then
compared at other values of $r$. As a further check,
the force is also used directly.

For the quenched calculation Brambilla 10 \cite{Brambilla:2010pp}
very small lattice spacings,
$a \sim 0.025\,\mbox{fm}$, were available from Ref.~\cite{Necco:2001gh}.
For ETM 11C \cite{Jansen:2011vv}, Bazavov 12 \cite{Bazavov:2012ka},
Karbstein 14 \cite{Karbstein:2014bsa}
and Bazavov 14 \cite{Bazavov:2014soa} using dynamical
fermions such small lattice spacings are not yet realized 
(Bazavov 14 reaches down to $a \sim 0.041\,\mbox{fm}$). They
all use the tree-level improved potential as described above. 
We note that the value of $\Lambda_\msbar$ in physical units by
ETM 11C \cite{Jansen:2011vv} is based on a value of $r_0=0.42$~fm. 
This is at least 10\% smaller than the large majority of
other values of $r_0$. Also the values of $r_0/a$ 
on the finest lattices in ETM 11C \cite{Jansen:2011vv}
and $r_1/a$ for Bazavov 14 \cite{Bazavov:2014soa} come from
rather small lattices with $m_\pi L \approx 2.4$, $2.2$ respectively.

Instead of the procedure discussed previously, Karbstein 14 
\cite{Karbstein:2014bsa} reanalyzes the data of ETM 11C 
\cite{Jansen:2011vv} by first estimating
the Fourier transform $\tilde V(p)$ of $V(r)$ and then fitting  
the perturbative expansion of $\tilde V(p)$ in terms of 
$\alpha_\msbar(p)$. Of course, the Fourier transform requires
some modelling of the $r$-dependence of $V(r)$
at short and at large distances. The authors fit a linearly rising
potential at large distances together with string-like
corrections of order $r^{-n}$ and define the potential at large 
distances by this fit.\footnote{Note that at large distances,
where string breaking is known to occur, this is not 
any more the ground state potential defined by \eq{e:vfromw}.}
Recall that for observables in momentum space
we take the renormalization scale entering our criteria as $\mu=q$,
Eq.~(\ref{mu_def}). The analysis (as in ETM 11C \cite{Jansen:2011vv})
is dominated by the data at the smallest lattice spacing, where
a controlled determination of the overall scale  is difficult due to 
possible finite-size effects.
Karbstein 18  \cite{Karbstein:2018mzo} is a
reanalysis of Karbstein 14 and supersedes it. Some data with a different
discretization of the static quark is added (on the same configurations)
and the discrete lattice results for the static potential in position
space are first parameterized by a continuous function, which then
allows for an analytical Fourier transformation to momentum space.

Similarly also for Takaura 18~\cite{Takaura:2018lpw,Takaura:2018vcy}
the momentum space potential $\tilde{V}(Q)$ is the central object. 
Namely, they assume that renormalon / power law effects are absent in
$\tilde{V}(Q)$ and only come in through the Fourier transformation.
They provide evidence that renormalon effects (both $u=1/2$ and $u=3/2$) can be 
subtracted and arrive at a nonperturbative term $k\,\Lambda_\msbar^3 r^2$. 
Two different analysis are carried out with the final result 
taken from ``Analysis II''. Our numbers including the evaluation of
the criteria refer to it. Together with the 
perturbative 3-loop (including the $ \alpha_s^4\log \alpha_s$ term) 
expression, this term is fitted to the 
nonperturbative results for the potential in the region
$0.04\,\fm \, \leq \, r \,\leq 0.35\,\fm$, where $0.04\,\fm$ is 
$r=a$ on the finest lattice.
The NP potential data originate from JLQCD ensembles (Symanzik improved
gauge action and M\"obius domain-wall quarks) at three lattice spacings with 
a pion mass around $300\,\MeV$. Since at the maximal distance in the analysis
we find $\alpha_\msbar(2/r) = 0.43$, the renormalization scale 
criterion yields a \bad. 
The perturbative
behavior is \soso\ because of the high order in PT known. The continuum 
limit criterion yields a $\soso$.


One of the main issues for all these computations is whether the
perturbative running of the coupling constant
has been reached.  
While for $N_f=0$ fermions Brambilla~10
\cite{Brambilla:2010pp} reports agreement with perturbative behavior
at the smallest distances, Husung~17 (which goes to
shorter distances) finds relatively large corrections beyond the 3-loop
$\alpha_\mathrm{qq}$.
For dynamical fermions,  
 Bazavov 12 \cite{Bazavov:2012ka}
and Bazavov 14 \cite{Bazavov:2014soa} report good agreement with perturbation
theory after the renormalon is subtracted or eliminated. 

A second issue is the coverage of configuration space in some of the
simulations, which use very small lattice spacings with periodic
boundary conditions. Affected are the smallest two lattice spacings
of Bazavov 14 \cite{Bazavov:2014soa} where very few tunnelings of
the topological charge occur \cite{Bazavov:2014pvz}.
With present knowledge, it also seems  possible that the older data
by Refs.~\cite{Necco:2001xg,Necco:2001gh} used by Brambilla 10 
\cite{Brambilla:2010pp} are partially obtained with (close to) frozen topology.

The recent computation Husung 17~\cite{Husung:2017qjz},
for $N_f = 0$ flavours first determines the coupling 
$\gbar_{\rm qq}^2(r,a)$ from the force and then performs a continuum extrapolation
on lattices down to $a \approx 0.015\,\mbox{fm}$, using a step-scaling method at short distances, $r/r_0 \lsim 0.5$. 
Using the $4$-loop $\beta^{\rm qq}$ function this allows $r_0\Lambda_{\rm qq}$
to be estimated, which is then converted to the $\overline{\rm MS}$ scheme.
$\alpha_{\rm eff} = \alpha_{\rm qq}$ ranges from $\sim 0.17$ to large 
values; we 
give $\soso$ for renormalization scale and \good\ for perturbative behaviour. The range
$a\mu = 2a/r \approx 0.37$ - $0.14$ leads to a $\good$
in the continuum extrapolation.

%{\cred 
We note that the $\Nf=3$ determinations of $r_0 \Lambda_{\overline{\rm MS}}$ agree
within their errors of 4-6\%.
%}
% ----------------------------------------------------------------------
