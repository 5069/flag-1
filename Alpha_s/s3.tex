
\subsection{$\alpha_s$ from Step-Scaling Methods}
\label{s:SF}
% ----------------------------------------------------------------------

\subsubsection{General considerations}

% --------------------------------------------------------------------

The method of step-scaling functions avoids the scale problem,
\eq{eq:scaleproblem}. It is in principle independent of the particular
boundary conditions used and was first developed with periodic
boundary conditions in a two-dimensional model~\cite{Luscher:1991wu}.

The essential idea of the step-scaling strategy
is to split the determination of the running coupling at large
$\mu$ and of a hadronic scale into two lattice calculations and
connect them by `step-scaling'. In the former part, we determine the
running coupling constant in a finite-volume scheme
in which the renormalization scale is set by the inverse lattice size
$\mu = 1/L$. In this calculation, one takes a high renormalization scale
while keeping the lattice spacing sufficiently small as
\begin{eqnarray}
   \mu \equiv 1/L \sim 10\,\ldots\, 100\,\mbox{GeV}\,, \qquad a/L \ll 1 \,.
\end{eqnarray}
In the latter part, one chooses a certain 
$\gbar^2_\mathrm{max}=\gbar^2(1/L_\mathrm{max})$, 
typically such that $L_\mathrm{max}$ is around $0.5$--$1$~fm. With a 
common discretization, one then determines $L_\mathrm{max}/a$ and
(in a large volume $L \ge$ 2--3~fm) a hadronic scale
such as a hadron mass, $\sqrt{t_0}/a$ or $r_0/a$ at the same bare
parameters. In this way one gets numbers for, e.g., $L_\mathrm{max}/r_0$
and by changing the lattice spacing $a$ carries out a continuum
limit extrapolation of that ratio. 
 
In order to connect $\gbar^2(1/L_\mathrm{max})$ to $\gbar^2(\mu)$ at
high $\mu$, one determines the change of the coupling in the continuum
limit when the scale changes from $L$ to $L/s$, starting from
$L=L_{\rm max}$ and arriving at $\mu = s^k /L_{\rm max}$. This part of
the strategy is called step-scaling. Combining these results yields
$\gbar^2(\mu)$ at $\mu = s^k \,(r_0 / L_\mathrm{max})\, r_0^{-1}$,
where $r_0$ stands for the particular chosen hadronic scale.
Most applications use a scale factor $s=2$.


At present most applications in QCD use Schr\"odinger 
functional boundary conditions~\cite{Luscher:1992an,Sint:1993un}
%{\cblu 
and we discuss this below in a little more detail.
(However, other boundary conditions are also possible, such as
twisted boundary conditions %, TBC,%
and the discussion also applies to them.)
%}
An important reason is that these boundary conditions avoid zero modes
for the quark fields and quartic modes \cite{Coste:1985mn} in the
perturbative expansion in the gauge fields. Furthermore the corresponding
renormalization scheme is well studied in perturbation
theory~\cite{Luscher:1993gh,Sint:1995ch,Bode:1999sm} with the
3-loop $\beta$-function and 2-loop cutoff effects (for the
standard Wilson regularization) known.
%

In order to have a perturbatively well-defined scheme,
the SF scheme uses Dirichlet boundary conditions at time 
$t = 0$ and $t = T$. These break translation invariance and permit
${\cO}(a)$ counter terms at the boundary through quantum corrections. 
Therefore, the leading discretization error is ${\cO}(a)$.
Improving the lattice action is achieved by adding
counter terms at the boundaries whose coefficients are denoted
as $c_t,\tilde c_t$. In practice, these coefficients are computed
with $1$-loop or $2$-loop perturbative accuracy.
A better precision in this step yields a better 
control over discretization errors, which is important, as can be
seen, e.g., in Refs.~\cite{Takeda:2004xha,Necco:2001xg}.
%

Also computations with Dirichlet boundary conditions do in principle
suffer from the insufficient change of topology in the HMC algorithm
at small lattice spacing. However, in a small volume the weight of
nonzero charge sectors in the path integral is exponentially
suppressed~\cite{Luscher:1981zf}~\footnote{We simplify here and assume
  that the classical solution associated with the used boundary
  conditions has charge zero.  In practice this is the case.} and in a Monte Carlo run of
  typical length very few configurations
  with nontrivial topology should appear. Considering
the issue quantitatively Ref.~\cite{Fritzsch:2013yxa} finds a
strong suppression below $L\approx 0.8\,\fm$. Therefore the lack of
topology change of the HMC is not a serious issue. 
%{\cblu 
Still Ref.~\cite{DallaBrida:2016kgh} includes a projection to zero topology 
into the {\em definition} of the coupling.
%}
We note also that a mix of Dirichlet and open boundary conditions is
expected to 
%{\cblu 
remove the topology issue entirely \cite{Luscher:2014kea}
%} 
and may be
considered in the future.

%{\color{blue}
Apart from the boundary conditions, the very definition
of the coupling needs to be chosen. 
We briefly discuss in turn, the two schemes used at present, 
namely, the `Schr\"odinger
Functional' (SF) and `Gradient Flow' (GF) schemes.
%}

%{\cblu
The SF scheme is the first one, which was used in step-scaling studies
in gauge theories \cite{Luscher:1992an}. Inhomogeneous
Dirichlet boundary conditions are imposed in time,
\begin{eqnarray}
    A_k(x)|_{x_0=0} = C_k\,,
    \quad
    A_k(x)|_{x_0=L} = C_k'\,,    
\end{eqnarray}
for $k=1,2,3$.
Periodic boundary conditions (up to a phase for the fermion fields)  with period $L$ are imposed in space.
The matrices 
\begin{align}
LC_k &= i \,{\rm diag}\big( \eta- \pi/3, -\eta/2 , -\eta/2  + \pi/3 \big) \,,
\nonumber \\
LC^\prime_k &= i \,{\rm diag}\big( -(\eta+\pi), \eta/2 + \pi/3,\eta/2 + 2\pi/3 \big)\,,
\nonumber
\end{align}
just depend on the dimensionless parameter $\eta$.
The coupling $\bar{g}_\mathrm{SF}$ is obtained from
the $\eta$-derivative of the effective action,
\begin{eqnarray}
  \langle \partial_\eta S|_{\eta=0} \rangle = \frac{12\pi}{\gbar^2_\mathrm{SF}}\,.
\end{eqnarray}
For this scheme, the finite $c^{(i)}_g$, \eq{eq:g_conversion}, are 
known for $i=1,2$ 
\cite{Sint:1995ch,Bode:1999sm}.


More recently, gradient flow couplings have been used frequently
because of their small statistical errors at large couplings (in contrast to 
$\gbar_\mathrm{SF}$, which has small statistical errors at small couplings). 
The gradient flow is introduced as follows \cite{Narayanan:2006rf,Luscher:2010iy}.
Consider the flow gauge field $B_\mu(t,x)$ with the flow time $t$, 
which is a one parameter deformation of the bare gauge field 
$A_\mu(x)$, where $B_\mu(t,x)$ is the solution to the gradient 
flow equation
\begin{eqnarray}
   \partial_t B_\mu(t,x) 
            &=& D_\nu G_{\nu\mu}(t,x)\,,
                                                      \nonumber \\
   G_{\mu\nu} &=& \partial_\mu B_\nu - \partial_\nu B_\mu + [B_\mu,B_\nu] \,,
\end{eqnarray}
with initial condition $B_\mu(0,x) = A_\mu(x)$.
The renormalized coupling is defined by \cite{Luscher:2010iy}
\begin{eqnarray}
   \bar{g}^2_{\rm GF}(\mu) 
      = \left. {\cal N} t^2 \langle E(t,x)\rangle
                                        \right|_{\mu=1/\sqrt{8t}} \,,
\end{eqnarray}
with ${\cal N} = 16\pi^2/3 + O((a/L)^2)$
and where $E(t,x)$ is the action density given by
\begin{eqnarray}
   E(t,x) = \frac{1}{4} G^a_{\mu\nu}(t,x) G^a_{\mu\nu}(t,x). 
                                        \label{eq:Et}
\end{eqnarray}
In a finite volume, one needs to specify additional conditions.
In order not to introduce two independent scales one sets 
\begin{eqnarray}
   \sqrt{8t} = cL \,,
\end{eqnarray}
for some fixed number $c$ \cite{Fodor:2012td}. 
Schr\"odinger functional boundary conditions~\cite{Fritzsch:2013je}
or twisted boundary conditions \cite{Ramos:2014kla,Ishikawa:2017xam}  
have been employed.
Matching of the GF coupling to the $\overline{\rm MS}$ scheme coupling
is known to 1-loop for twisted boundary conditions with zero
quark flavours and $SU(3)$ group \cite{Ishikawa:2017xam} and to 2-loop with SF boundary conditions with zero
quark flavours \cite{DallaBrida:2017tru}.
The former is based on a MC evaluation at small couplings and the 
latter on numerical stochastic perturbation theory.

%}

% --------------------------------------------------------------------

\subsubsection{Discussion of computations}

% --------------------------------------------------------------------

In Tab.~\ref{tab_SF3} we give results from various determinations
%
\begin{table}[!htb]
   \vspace{3.0cm}
   \footnotesize
   \begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}rlllllllll}
      Collaboration & Ref. & $\Nf$ &
      \hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{renormalization scale}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{perturbative behaviour}\end{rotate}
                                                       \hspace{-0.15cm} &
      \hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}
                               \hspace{-0.25cm} & %\rule{0.2cm}{0cm} 
                         scale & $\Lambda_\msbar[\MeV]$ & $r_0\Lambda_\msbar$ \\
      & & & & & & & & \\[-0.1cm]
      \hline
      \hline
      & & & & & & & & \\
      ALPHA 10A & \cite{Tekin:2010mm} & 4 
                    & \gA &\good & \good & \good 
                    & \multicolumn{3}{l}{only running of $\alpha_s$ in Fig.~4}
                    \\  
      Perez 10 & \cite{PerezRubio:2010ke} & 4 
                    & \rC &\good & \good & \soso  
                    & \multicolumn{3}{l}{only step-scaling function in Fig.~4}
                    \\           
      & & & & & & & & & \\[-0.1cm]
      \hline
      & & & & & & & & & \\[-0.1cm]
      ALPHA 17   &  \cite{Bruno:2017gxd} &2+1 
                    & \gA & \good & \good & \good 
                    & $\sqrt{8t_0}= 0.415\,\mbox{fm}$ & 341(12) & 0.816(29)
                    \\  
      PACS-CS 09A& \cite{Aoki:2009tf} & 2+1 
                    & \gA &\good &\good &\soso
                    & $m_\rho$ & $371(13)(8)(^{+0}_{-27})$$^{\#}$
                    & $0.888(30)(18)(^{+0}_{-65})$$^\dagger$
                    \\ % RS 22.6.13, 1.11.13
                    &&&\gA &\good &\good &\soso 
                    & $m_\rho$  & $345(59)$$^{\#\#}$
                    & $0.824(141)$$^\dagger$
                    \\ % RS 22.6.13
      & & & & & & & & \\[-0.1cm]
      \hline  \\[-1.0ex]
      & & & & & & & & \\[-0.1cm]
      ALPHA 12$^*$  & \cite{Fritzsch:2012wq} & 2 
                    & \gA &\good &\good &\good
                    &  $f_{\rm K}$ & $310(20)$ &  $0.789(52)$
                    \\
      ALPHA 04 & \cite{DellaMorte:2004bc} & 2 
                    & \gA &\bad &\good &\good
                    & $r_0 = 0.5\,\mbox{fm}$$^\S$  & $245(16)(16)^\S$ 
                                                   & $0.62(2)(2)^\S$
                    \\
      ALPHA 01A & \cite{Bode:2001jv} & 2 
                    &\gA & \good & \good & \good 
                    &\multicolumn{3}{l}{only running of $\alpha_s$  in Fig.~5}
                    \\
      & & & & & & & & \\[-0.1cm]
      \hline  \\[-1.0ex]
      & & & & & & & & \\[-0.1cm]
      Ishikawa 17   & \cite{Ishikawa:2017xam} & 0 
                    & \gA & \good & \good & \good
                    & $r_0$, $[\sqrt{\sigma}]$ & $253(4)(^{+13}_{-2})$$^\dagger$
                                              & $0.606(9)(^{+31}_{-5})^+$
                                              % arXiv: $0.593(12)^{12}_9$
                    \\
      CP-PACS 04$^\&$  & \cite{Takeda:2004xha} & 0 
                    & \gA & \good & \good & \soso  
                    & \multicolumn{3}{l}{only tables of $g^2_{\rm SF}$}
                    \\
      ALPHA 98$^{\dagger\dagger}$ & \cite{Capitani:1998mq} & 0 
                    & \gA & \good & \good & \soso 
                    &  $r_0=0.5\fm$ & $238(19)$ & 0.602(48) 
                    \\
      L\"uscher 93  & \cite{Luscher:1993gh} & 0 
                    & \gA & \good & \soso & \soso
                    & $r_0=0.5\fm$ & 233(23)  & 0.590(60)$^{\S\S}$ 
                    \\
      &&&&&&& \\[-0.1cm]
      \hline
      \hline\\
\end{tabular*}\\[-0.2cm]
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
\item[$^{\#}$] Result with a constant (in $a$) continuum extrapolation
              of the combination $L_\mathrm{max}m_\rho$.             \\[-5mm]
\item[$^\dagger$] In conversion from $\Lambda_\msbar$ to
                 $r_0\Lambda_{\overline{\rm MS}}$ and vice versa, $r_0$ is
                 taken to be $0.472\,\mbox{fm}$.                   \\[-5mm]
\item[$^{\#\#}$] Result with a linear continuum extrapolation
             in $a$ of the combination $L_\mathrm{max}m_\rho$.        \\[-5mm]
\item[$^*$]  Supersedes ALPHA 04.                                   \\[-5mm]
\item[$^\S$] The $N_f=2$ results were based on values for $r_0/a$
             which have later been found to be too small by
             \cite{Fritzsch:2012wq}. The effect will be of the order of
             10--15\%, presumably an increase in $\Lambda r_0$.
             We have taken this into account by a $\bad$ in the 
             renormalization scale.                                  \\[-5mm]
\item[$^\&$] This investigation was a precursor for PACS-CS 09A
          and confirmed two step-scaling functions as well as the
          scale setting of ALPHA~98.                              \\[-5mm]
\item[$^{\dagger\dagger}$] Uses data of L\"uscher~93 and therefore supersedes it.
                                                                  \\[-5mm]
\item[$^{\S\S}$] Converted from $\alpha_\msbar(37r_0^{-1})=0.1108(25)$.
\item[$^+$] Also $\Lambda_\msbar/\sqrt{\sigma} = 0.532(8)(^{+27}_{-5})$ is quoted.

\end{itemize}
}
\end{minipage}
\caption{Results for the $\Lambda$ parameter from computations using 
         step-scaling of the SF-coupling. Entries without values for $\Lambda$
         computed the running and established perturbative behaviour
         at large $\mu$. 
         }
\label{tab_SF3}
\end{table}
of the $\Lambda$ parameter. For a clear assessment of the $N_f$-dependence, the last column also shows results that refer to a common
hadronic scale, $r_0$. As discussed above, the renormalization scale
can be chosen large enough such that $\alpha_s < 0.2$ and the
perturbative behaviour can be verified.  Consequently only $\good$ is
present for these criteria except for early work
where the $n_l=2$ loop connection to $\msbar$ was not yet known and we assigned a $\bad$ concerning the renormalization scale.
With dynamical fermions, results for the
step-scaling functions are always available for at least $a/L = \mu a
=1/4,1/6, 1/8$.  All calculations have a nonperturbatively
$\cO(a)$ improved action in the bulk. For the discussed
boundary $\cO(a)$ terms this is not so. In most recent
calculations 2-loop $\cO(a)$ improvement is employed together
with at least three lattice spacings.\footnote{With 2-loop
  $\cO(a)$ improvement we here mean $c_\mathrm{t}$ including
  the $g_0^4$ term and $\tilde c_\mathrm{t}$ with the $g_0^2$
  term. For gluonic observables such as the running coupling this is
  sufficient for cutoff effects being suppressed to $\cO(g^6
  a)$.} This means a \good\ for the continuum extrapolation.  In 
other computations only 1-loop $c_t$ was available and we arrive at \soso. We
note that the discretization errors in the step-scaling functions 
of the SF coupling are
usually found to be very small, at the percent level or
below. However, the overall desired precision is very high as well,
and the results in CP-PACS 04~\cite{Takeda:2004xha} show that
discretization errors at the below percent level cannot be taken for
granted.  In particular with staggered fermions (unimproved except for
boundary terms) few percent effects are seen in
Perez~10~\cite{PerezRubio:2010ke}.

In the work by PACS-CS 09A~\cite{Aoki:2009tf}, the continuum
extrapolation in the scale setting is performed using a constant
function in $a$ and with a linear function.
Potentially the former leaves a considerable residual discretization 
error. We here use, as discussed with the collaboration, 
the continuum extrapolation linear in $a$,
as given in the second line of PACS-CS 09A \cite{Aoki:2009tf}
results in Tab.~\ref{tab_SF3}.
After perturbative conversion from a three-flavour result to five flavours
(see \sect{s:crit}), they obtain
\begin{eqnarray}
 \alpha_\msbar^{(5)}(M_Z)=0.118(3)\,. 
\end{eqnarray}

%{\cblu
In Ref.~\cite{Bruno:2017gxd}, the ALPHA collaboration determined 
$\Lambda^{(3)}_{\msbar}$ combining step-scaling in $\gbar^2_{\rm GF}$
in the lower scale region $\mu_{\rm had} \leq \mu \leq \mu_0$, and 
step-scaling in $\gbar^2_{\rm SF}$ for higher scales  
$\mu_0 \leq \mu \leq \mu_{\rm PT}$. 
Both schemes are defined with SF boundary conditions. For $\gbar^2_{\rm GF}$ a projection to the sector of zero 
topological charge is included, \eq{eq:Et} is restricted to the 
magnetic components, and $c=0.3$.
The scales $\mu_{\rm had}$, $\mu_0$, and 
$\mu_{\rm PT}$ are defined by $\gbar^2_{\rm GF} (\mu_{\rm had})= 11.3$,
$\gbar^2_{\rm SF}(\mu_0) = 2.012$, and $\mu_{\rm PT} = 16 \mu_0$ which
are roughly estimated as
\begin{eqnarray}
   1/L_\mathrm{max}\equiv \mu_{\rm had} \approx 0.2 \mbox{ GeV}, & \mu_0 \approx 4 \mbox{ GeV} \,, 
      & \mu_{\rm PT}\approx 70 \mbox{ GeV} \,.
\end{eqnarray}
Step-scaling is carried out with an $O(a)$-improved Wilson quark action
\cite{Bulava:2013cta}
and L\"uscher-Weisz gauge action \cite{Luscher:1984xn} in the low-scale region
and an $O(a)$-improved Wilson quark action
\cite{Yamada:2004ja}
and Wilson gauge action in the high-energy part. 
For the step-scaling using steps of
$L/a \,\to\,2L/a$, three lattice sizes $L/a=8,12,16$ were simulated for
$\gbar^2_{\rm GF}$ and four lattice sizes $L/a=(4,)\, 6, 8, 12$ for 
$\gbar^2_{\rm SF}$. The final results do not use the small lattices given
in parenthesis. The parameter $\Lambda^{(3)}_{\msbar}$ is then obtained via 
\begin{eqnarray}
   \Lambda^{(3)}_{\msbar} 
      = \underbrace{\frac{\Lambda^{(3)}_{\msbar}}{\mu_{\rm PT}}}_{\rm perturbation  ~ theory}
           \times \underbrace{\frac{\mu_{\rm PT}}{\mu_{\rm had}}}_{\rm step-scaling}
           \times \underbrace{\frac{\mu_{\rm had}}
                                         {f_{\pi K}}}_{\rm large ~ volume~ simulation}
           \times \underbrace{f_{\pi K}}_{\rm experimental ~data} \,, 
\label{eq:Lambda3}
\end{eqnarray}
where the hadronic scale $f_{\pi K}$ is 
$f_{\pi K}= \frac{1}{3}(2 f_K + f_\pi) = 147.6 (5)\mbox{ MeV}$.
The first term on the right hand side of Eq.~(\ref{eq:Lambda3}) is 
obtained from $\alpha_{\rm SF}(\mu_{\rm PT})$ which is the output from
SF step-scaling using Eq.~(\ref{eq:Lambda}) with 
$\alpha_{\rm SF}(\mu_{\rm PT})\approx 0.1$ and
the 3-loop $\beta$-function 
and the exact conversion to the $\msbar$-scheme.
The second term is essentially obtained
from step-scaling in the GF scheme and the measurement of 
$\gbar^2_{\rm SF}(\mu_0)$ except for the trivial scaling factor of 16 
in the SF running. The third term is obtained from a measurement
of the hadronic quantity at large volume.
%}

%{\color{blue}
A large volume simulation is done for three lattice spacings with 
sufficiently large volume and reasonable control over the chiral
extrapolation so that the scale determination is precise enough.  
The step-scaling results in both schemes
satisfy renormalization criteria, perturbation theory criteria,
and continuum limit criteria just as previous studies using step-scaling.
So we assign green stars for these criteria.

The dependence of $\Lambda $, eq.~(\ref{eq:Lambda}) with 3-loop $\beta$-function, on $\alpha_s$ and on the chosen scheme is discussed
in \cite{Brida:2016flw}. This investigation provides a warning on estimating the 
truncation error of perturbative series. Details are explained in \sect{s:trunc}.

The result for the $\Lambda$ parameter is  
%\begin{eqnarray}
$\Lambda^{(3)}_{\overline{\rm MS}} = 341(12)~\mbox{MeV}$, 
%\end{eqnarray}
where the dominant error comes from the error of 
$\alpha_{\rm SF}(\mu_{\rm PT})$ after step-scaling in SF scheme.
Using 4-loop matching at the charm and bottom thresholds 
and 5-loop running one finally obtains
\begin{eqnarray}
   \alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.11852(84)\,.
\end{eqnarray}
%}
%{\color{red}
Several other results do not have a sufficient number of
quark flavours  or do not yet contain the conversion
of the scale to physical units (ALPHA~10A \cite{Tekin:2010mm}, 
Perez~10 \cite{PerezRubio:2010ke}). Thus no value for $\alpha_\msbar^{(5)}(M_Z)$
is quoted.
%}

%{\cblu
The computation of Ishikawa et al. \cite{Ishikawa:2017xam} 
is based on the gradient flow coupling with twisted boundary conditions
\cite{Ramos:2014kla} (TGF coupling)
in the pure gauge theory. Again they use 
$c=0.3$. Step-scaling with a scale factor $s=3/2$ is employed,
covering a large range of couplings from $\alpha_s\approx 0.5$ to
$\alpha_s\approx 0.1$ and taking the continuum limit through global
fits to the step-scaling function on $L/a=12,16,18$ lattices with between 6 and 
8 parameters. Systematic errors due to variations of the fit functions
are estimated. Two physical scales are considered:
$r_0/a$ is taken from \cite{Necco:2001xg} and $\sigma a^2$ from 
\cite{Allton:2008pn} and \cite{GonzalezArroyo:2012fx}.  
As the ratio $\Lambda_\mathrm{TGF}/\Lambda_\mathrm{\msbar}$   
has not yet been computed analytically, Ref.~\cite{Ishikawa:2017xam}
determines the 1-loop relation between $\gbar_\mathrm{SF}$ and 
$\gbar_\mathrm{TGF}$ from  MC simulations performed
in the weak coupling region and then uses the known
$\Lambda_\mathrm{SF}/\Lambda_\mathrm{\msbar}$. Systematic errors 
due to variations of the fit functions dominate the overall uncertainty.
%}
 

% ----------------------------------------------------------------------
