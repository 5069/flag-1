
\subsection{$\alpha_s$ from QCD vertices}

% ----------------------------------------------------------------------
\label{s:glu}

% ----------------------------------------------------------------------

\subsubsection{General considerations}

% --------------------------------------------------------------------

The most intuitive and in principle direct way to determine the
coupling constant in QCD is to compute the appropriate
three- or four-point gluon vertices
or alternatively the
quark-quark-gluon vertex or ghost-ghost-gluon vertex (i.e., \ $
q\overline{q}A$ or $c\overline{c}A$ vertex, respectively).  A suitable
combination of renormalization constants then leads to the relation
between the bare (lattice) and renormalized coupling constant. This
procedure requires the implementation of a nonperturbative
renormalization condition and the fixing of the gauge. For the study
of nonperturbative gauge fixing and the associated Gribov ambiguity,
we refer to Refs.~\cite{Cucchieri:1997dx,Giusti:2001xf,Maas:2009ph} and
references therein.
%
In practice the Landau gauge is used and the
renormalization constants are defined 
by requiring that the vertex is equal to the tree level
value at a certain momentum configuration.
The resulting renormalization
schemes are called `MOM' scheme (symmetric momentum configuration)
or `$\rm \widetilde{MOM}$' (one momentum vanishes), 
which are then converted perturbatively
to the $\overline{\rm MS}$ scheme.

A pioneering work to determine the three-gluon vertex in the $N_f = 0$
theory is Alles~96~\cite{Alles:1996ka} (which was followed by
Ref.~\cite{Boucaud:2001qz} for two flavour QCD); a more recent $N_f = 0$
computation was Ref.~\cite{Boucaud:2005gg} in which the three-gluon vertex
as well as the ghost-ghost-gluon vertex was considered.  (This
requires a computation of the propagator of the
Faddeev--Popov ghost on the lattice.) The latter paper concluded that
the resulting $\Lambda_{\overline{\rm MS}}$ depended strongly on the
scheme used, the order of perturbation theory used in the matching and
also on nonperturbative corrections \cite{Boucaud:2005xn}.

Subsequently in Refs.~\cite{Sternbeck:2007br,Boucaud:2008gn} a specific
$\widetilde{\rm MOM}$ scheme with zero ghost momentum for the
ghost-ghost-gluon vertex was used. In this scheme, dubbed
the `MM' (Minimal MOM) or `Taylor' (T) scheme, the vertex
is not renormalized, and so the renormalized coupling reduces to
\begin{eqnarray}
   \alpha_{\rm T}(\mu) 
      = D^{\rm gluon}_{\rm lat}(\mu, a) D^{\rm ghost}_{\rm lat}(\mu, a)^2 \,
                      {g_0^2 \over 4\pi} \,,
\end{eqnarray}
where $D^{\rm ghost}_{\rm lat}$ and $D^{\rm gluon}_{\rm lat}$ are the
(bare lattice) dressed ghost and gluon `form factors' of these
propagator functions in the Landau gauge,
\begin{eqnarray}
   D^{ab}(p) = - \delta^{ab}\, {D^{\rm ghost}(p) \over p^2}\,, \qquad
   D_{\mu\nu}^{ab}(p) 
      = \delta^{ab} \left( \delta_{\mu\nu} - {p_\mu p_\nu \over p^2} \right) \,
        {D^{\rm gluon}(p) \over p^2 } \,,
\end{eqnarray}
and we have written the formula in the continuum with 
$D^{\rm ghost/gluon}(p)=D^{\rm ghost/gluon}_{\rm lat}(p, 0)$.
Thus there is now no need to compute the ghost-ghost-gluon vertex,
just the ghost and gluon propagators.


% --------------------------------------------------------------------

\subsubsection{Discussion of computations}
\label{s:glu_discuss}

% --------------------------------------------------------------------

\begin{table}[!h]
   \vspace{3.0cm}
   \footnotesize
   \begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}rllllllll}
   Collaboration & Ref. & $\Nf$ &
   \hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{renormalization scale}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{perturbative behaviour}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}
      \hspace{-0.25cm} & 
                         scale & $\Lambda_\msbar[\MeV]$ & $r_0\Lambda_\msbar$ \\
      & & & & & & & & \\[-0.1cm]
      \hline
      \hline
      & & & & & & & & \\[-0.1cm]
       ETM 13D      & \cite{Blossier:2013ioa}   & {2+1+1} & {\gA}
                    & \soso & \soso  & \bad  
                    & $f_\pi$
                    & $314(7)(14)(10)$$^a$
                    & $0.752(18)(34)(81)$$^\dagger$                        \\
       ETM 12C        & \cite{Blossier:2012ef}   & 2+1+1 & \gA 
                    & \soso & \soso  & \bad  
                    & $f_\pi$
                    & $324(17)$$^\S$
                    & $0.775(41)$$^\dagger$                                \\
      ETM 11D       & \cite{Blossier:2011tf}   & 2+1+1 & \gA 
                    & \soso & \soso  & \bad  
                    & $f_\pi$
                    & $316(13)(8)(^{+0}_{-9})$$^\star$
                    & $0.756(31)(19)(^{+0}_{-22})$$^\dagger$                 \\
      & & & & & & & & \\[-0.1cm]
      \hline
      & & & & & & & & \\[-0.1cm]
      Sternbeck 12  & \cite{Sternbeck:2012qs}  & 2+1  & \rC
                    &     &        & 
                    & \multicolumn{3}{l}{only running of 
                                         $\alpha_s$ in Fig.~4}            \\
      & & & & & & & & \\[-0.1cm]
      \hline
      & & & & & & & & \\[-0.1cm]
      Sternbeck 12  & \cite{Sternbeck:2012qs}  & 2  & \rC
                    &  &  & 
                    & \multicolumn{3}{l}{Agreement with $r_0\Lambda_\msbar$ 
                                         value of \cite{Fritzsch:2012wq} } \\
      Sternbeck 10  & \cite{Sternbeck:2010xu}  & 2  & \rC 
                    & \soso  & \good & \bad
                    &
                    & $251(15)$$^\#$
                    & $0.60(3)(2)$                                       \\
      ETM 10F       & \cite{Blossier:2010ky}   & 2  & \gA 
                    & \soso  & \soso  & \soso 
                    & $f_\pi$
                    & $330(23)(22)(^{+0}_{-33})$\hspace{-2mm}
                    & $0.72(5)$$^+$                                       \\
      Boucaud 01B    & \cite{Boucaud:2001qz}    & 2 & \gA 
                    & \soso & \soso  & \bad
                    & $K^{\ast}-K$
                    & $264(27)$$^{\star\star}$
                    & 0.669(69)                              \\
      & & & & & & & & \\[-0.1cm]
      \hline
      & & & & & & & & \\[-0.1cm]
      Sternbeck 12  & \cite{Sternbeck:2012qs}   & 0 & \rC 
                    &  &  &
                    &  \multicolumn{3}{l}{Agreement with $r_0\Lambda_\msbar$
                                          value of \cite{Brambilla:2010pp}} \\
      Sternbeck 10  & \cite{Sternbeck:2010xu}   & 0 & \rC
                    & \good & \good & \bad
                    &
                    & $259(4)$$^\#$
                    & $0.62(1)$                                            \\
      Ilgenfritz 10 & \cite{Ilgenfritz:2010gu}  & 0 & \gA
                    &    \good    &  \good      & \bad 
                    & \multicolumn{2}{l}{only running of
                                         $\alpha_s$ in Fig.~13}           \\
{Boucaud 08}    & \cite{Boucaud:2008gn}       & 0         &\gA  
                    & \soso & \good   & \bad 
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & $224(3)(^{+8}_{-5})$
                    & $0.59(1)(^{+2}_{-1})$         
 \\
{Boucaud 05}    & \cite{Boucaud:2005gg}       & 0       &\gA  
                    & \bad & \good   & \bad 
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & 320(32)
                    & 0.85(9)           
 \\
   Soto 01        & \cite{DeSoto:2001qx}        & 0         & \gA  
                    & \soso & \soso  & \soso
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & 260(18)
                    & 0.69(5)          
 \\
{Boucaud 01A}    & \cite{Boucaud:2001st}      & 0         &\gA  
                    & \soso & \soso  & \soso
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & 233(28)~MeV
                    & 0.62(7)       
 \\
{Boucaud 00B}   & \cite{Boucaud:2000nd}      & 0         &\gA  
                    & \soso & \soso  & \soso
                    & 
                    & \multicolumn{2}{l}{only running of
                                         $\alpha_s$}
 \\
{Boucaud 00A}     &\cite{Boucaud:2000ey}     &  0    &\gA  
                    & \soso & \soso  & \soso
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & $237(3)(^{+~0}_{-10})$
                    & $0.63(1)(^{+0}_{-3})$            
 \\
{Becirevic 99B}  & \cite{Becirevic:1999hj} & 0 &\gA  
                    & \soso & \soso  & \bad 
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & $319(14)(^{+10}_{-20})$
                    & $0.84(4)(^{+3}_{-5})$   
 \\
{Becirevic 99A}  & \cite{Becirevic:1999uc} & 0 &\gA  
                    & \soso & \soso  & \bad 
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & $\lesssim 353(2)(^{+25}_{-15})$
                    & $\lesssim 0.93 (^{+7}_{-4})$         
 \\
{Boucaud 98B}  & \cite{Boucaud:1998xi} & 0 &\gA  
                    & \bad  & \soso  & \bad 
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & 295(5)(15)
                    & 0.78(4)           
 \\
{Boucaud 98A}    & \cite{Boucaud:1998bq} & 0 &\gA  
                    & \bad  & \soso  & \bad 
                    & $\sqrt{\sigma} = 445\,\mbox{MeV}$
                    & 300(5)
                    & 0.79(1)         
\\
{Alles 96}    & \cite{Alles:1996ka} & 0 &\gA  
                    & \bad  & \bad   & \bad 
                    & $\sqrt{\sigma} = 440\,\mbox{MeV}$\hspace{0.3mm}$^{++}$\hspace{-0.3cm}        
                    & 340(50)
                    & 0.91(13)   
\\
      & & & & & & & & \\[-0.1cm]
      \hline
      \hline\\
\end{tabular*}\\[-0.2cm]
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
   \item[$^a$] $\alpha_{\overline{\rm MS}}^{(5)}(M_Z)=0.1196(4)(8)(6)$.                   \\[-5mm]
   \item[$^\dagger$] We use the 2+1 value $r_0=0.472$~fm.                        \\[-5mm]
   \item[$^\S$] $\alpha_{\overline{\rm MS}}^{(5)}(M_Z)=0.1200(14)$.                   \\[-5mm]
   \item[$^\star$] First error is statistical; second is due to the lattice
           spacing and third is due to the chiral extrapolation.
           $\alpha_{\overline{\rm MS}}^{(5)}(M_Z)=0.1198(9)(5)(^{+0}_{-5})$.    \\[-5mm]
   \item[$^\#$] In the paper only $r_0\Lambda_{\overline{\rm MS}}$ is given,
         we converted to $\MeV$ with $r_0=0.472$~fm.                    \\[-5mm]
   \item[$^+$] The determination of $r_0$
        from the $f_\pi$ scale is found in Ref.~\cite{Baron:2009wt}.          \\[-5mm]
   \item[$^{\star\star}$]  $\alpha_{\overline{\rm MS}}^{(5)}(M_Z)=0.113(3)(4)$.         \\[-5mm]
   \item[$^{++}$]  The scale is taken from the string tension computation
           of Ref.~\cite{Bali:1992ru}.
\end{itemize}
}
\end{minipage}
\normalsize
\caption{Results for the gluon--ghost vertex.}
\label{tab_vertex}
\end{table}

For the calculations considered here, to match to perturbative
scaling, it was first necessary to reduce lattice artifacts by an
$H(4)$ extrapolation procedure (addressing $O(4)$ rotational
invariance), e.g., ETM 10F \cite{Blossier:2010ky} or by lattice
perturbation theory, e.g., Sternbeck 12 \cite{Sternbeck:2012qs}.  To
match to perturbation theory, collaborations vary in their approach.
In ETM 10F \cite{Blossier:2010ky},  it was necessary to include the
operator $A^2$ in the OPE of the ghost and gluon propagators, while in
{Sternbeck 12 \cite{Sternbeck:2012qs}} very large momenta are used and
$a^2p^2$ and $a^4p^4$ terms are included in their fit to the momentum
dependence. A further later refinement was the introduction of
higher nonperturbative OPE power corrections in ETM 11D
\cite{Blossier:2011tf} and ETM 12C \cite{Blossier:2012ef}.
Although
the expected leading power correction, $1/p^4$, was tried, ETM finds
good agreement with their data only when they fit with the
next-to-leading-order term, $1/p^6$.  The update ETM 13D
\cite{Blossier:2013ioa} investigates this point in more detail, using
better data with reduced statistical errors.  They find that after
again including the $1/p^6$ term they can describe their data over a
large momentum range from about 1.75~GeV to 7~GeV.

In all calculations except for Sternbeck 10 \cite{Sternbeck:2010xu},
Sternbeck 12 \cite{Sternbeck:2012qs} ,
the matching with the perturbative formula is performed including
power corrections in the form of 
condensates, in particular $\langle A^2 \rangle$. 
Three lattice spacings are present in almost all 
calculations with $N_f=0$, $2$, but the scales $ap$ are rather large.
This mostly results in a $\bad$ on the continuum extrapolation
%
%
(Sternbeck 10 \cite{Sternbeck:2010xu},
  Boucaud 01B \cite{Boucaud:2001qz} for $N_f=2$.
 Ilgenfritz 10 \cite{Ilgenfritz:2010gu},   
 Boucaud 08 \cite{Boucaud:2008gn},
 Boucaud 05 \cite{Boucaud:2005gg}, 
 Becirevic 99B \cite{Becirevic:1999hj},
  Becirevic 99A \cite{Becirevic:1999uc},
 Boucaud 98B \cite{Boucaud:1998xi},
 Boucaud 98A \cite{Boucaud:1998bq},
 Alles 96 \cite{Alles:1996ka} for $N_f=0$).
%
%
A \soso\ is reached in the $\Nf=0$ computations 
Boucaud 00A \cite{Boucaud:2000ey}, 00B \cite{Boucaud:2000nd},
01A \cite{Boucaud:2001st}, Soto 01 \cite{DeSoto:2001qx} due to
a rather small lattice spacing,  but this is done on a lattice
of a small physical size. 
The $N_f=2+1+1$ calculation, fitting with condensates, 
is carried out for two lattice spacings
and with $ap>1.5$, giving $\bad$
for the continuum extrapolation as well. 
In ETM 10F \cite{Blossier:2010ky} we have
$0.25 < \alpha_{\rm eff} < 0.4$, while in ETM 11D \cite{Blossier:2011tf}, 
ETM 12C \cite{Blossier:2012ef} (and ETM 13 \cite{Cichy:2013gja})
we find $0.24 < \alpha_{\rm eff} < 0.38$,  which gives a green circle
in these cases for the renormalization scale.
In ETM 10F \cite{Blossier:2010ky} the values of $ap$ violate our criterion
for a continuum limit only slightly, and 
we give a \soso.

In {Sternbeck 10 \cite{Sternbeck:2010xu}}, the coupling ranges over
$0.07 \leq \alpha_{\rm eff} \leq 0.32$ for $N_f=0$ and $0.19 \leq
\alpha_{\rm eff} \leq 0.38$ for $N_f=2$ giving $\good$ and $\soso$ for
the renormalization scale,  respectively.  The fit with the perturbative
formula is carried out without condensates, giving a satisfactory
description of the data.  In {Boucaud 01A \cite{Boucaud:2001st}},
depending on $a$, a large range of $\alpha_{\rm eff}$ is used which
goes down to $0.2$ giving a $\soso$ for the renormalization scale and
perturbative behaviour, and several lattice spacings are used leading
to $\soso$ in the continuum extrapolation.  The $\Nf=2$ computation
Boucaud 01B \cite{Boucaud:2001st}, fails the continuum limit criterion
because both $a\mu$ is too large and an unimproved Wilson fermion
action is used.  Finally in the conference proceedings
Sternbeck 12 \cite{Sternbeck:2012qs}, the $N_f$ = 0, 2, 3 coupling
$\alpha_\mathrm{T}$ is studied.  Subtracting 1-loop lattice artifacts
and subsequently fitting with $a^2p^2$ and $a^4p^4$ additional lattice
artifacts, agreement with the perturbative running is found for large
momenta ($r_0^2p^2 > 600$) without the need for power corrections.  In
these comparisons, the values of $r_0\Lambda_\msbar$ from other
collaborations are used. As no numbers are given, we have not
introduced ratings for this study.

In Tab.~\ref{tab_vertex} we summarize the results. Presently there
are no $N_f \geq 3$ calculations of $\alpha_s$ from QCD vertices that
satisfy the FLAG criteria to be included in the range.

% ----------------------------------------------------------------------
