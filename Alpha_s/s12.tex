
\subsection{Introduction}

% ----------------------------------------------------------------------

\label{introduction}

% ----------------------------------------------------------------------

The strong coupling $\gbar_s(\mu)$ defined at scale $\mu$, plays a key
role in the understanding of QCD and in its application to collider
physics. For example, the parametric uncertainty from $\alpha_s$ is one of
the dominant sources of uncertainty in the Standard Model prediction for
the $H \to b\bar{b}$ partial width, and the largest source of uncertainty
for $H \to gg$.  
%{\cred 
Thus higher precision determinations of $\alpha_s$ are
needed to maximize the potential of experimental measurements at the LHC,
and for high-precision Higgs studies at future
colliders
and the study of the stability of the 
% EB: PLEASE KEEP CITES IN ONE LINE -- otherwise the script for acronym replacement will fail
vacuum~\cite{Dittmaier:2012vm,Heinemeyer:2013tqa,Adams:2013qkq,Dawson:2013bba,Accardi:2016ndt,Lepage:2014fla,Buttazzo:2013uya,Espinosa:2013lma}. 
%} 
The value of $\alpha_s$ also yields one of the essential boundary conditions
for completions of the standard model at high energies. 

In order to determine the running coupling at scale $\mu$
\begin{eqnarray}
   \alpha_s(\mu) = { \gbar^2_{s}(\mu) \over 4\pi} \,,
\end{eqnarray}
we should first ``measure'' a short-distance quantity ${\oO}$ at scale
$\mu$ either experimentally or by lattice calculations, and then 
match it to a perturbative expansion in terms of a running coupling,
conventionally taken as $\alpha_{\overline{\rm MS}}(\mu)$,
\begin{eqnarray}
   {\oO}(\mu) = c_1 \alpha_{\overline{\rm MS}}(\mu)
              +  c_2 \alpha_{\overline{\rm MS}}(\mu)^2 + \cdots \,.
\label{eq:alpha_MSbar}
\end{eqnarray}
The essential difference between continuum determinations of
$\alpha_s$ and lattice determinations is the origin of the values of
$\oO$ in \eq{eq:alpha_MSbar}.

The basis of continuum determinations are 
experimentally measurable cross sections or decay widths from which $\oO$ is
defined. These cross sections have to be sufficiently inclusive 
and at sufficiently high scales such that perturbation theory 
can be applied. Often hadronization corrections have to be used
to connect the observed hadronic cross sections to the perturbative
ones. Experimental data at high $\mu$, where perturbation theory
is progressively more precise, usually have increasing experimental errors, 
and it is  not easy to find processes that allow one
to follow the $\mu$-dependence of a single $\oO(\mu)$ over
a range where $\alpha_s(\mu)$ changes significantly and precision is 
maintained.

In contrast, in lattice gauge theory, one can design $\oO(\mu)$ as
Euclidean short-distance quantities that are not directly related to
experimental observables. This allows us to follow the $\mu$-dependence until the perturbative regime is reached and
nonperturbative ``corrections'' are negligible.  The only
experimental input for lattice computations of $\alpha_s$ is the
hadron spectrum which fixes the overall energy scale of the theory and
the quark masses. Therefore experimental errors are completely
negligible and issues such as hadronization do not occur.  We can
construct many short-distance quantities that are easy to calculate
nonperturbatively in lattice simulations with small statistical
uncertainties.  We can also simulate at parameter values that do not
exist in nature (for example, with unphysical quark masses between
bottom and charm) to help control systematic uncertainties.  These
features mean that precise results for $\alpha_s$ can be achieved
with lattice gauge theory computations.  Further, as in the continuum,
the different methods available to determine $\alpha_s$ in
lattice calculations with different associated systematic
uncertainties enable valuable cross-checks.  Practical limitations are
discussed in the next section, but a simple one is worth mentioning
here. Experimental results (and therefore the continuum
determinations) of course have all quarks present, while in lattice
gauge theories in practice only the lighter ones are included and one
is then forced to use the matching at thresholds, as discussed in the following
subsection.

It is important to keep in mind that the dominant source of uncertainty
in most present day lattice-QCD calculations of $\alpha_s$ are from
the truncation of continuum/lattice perturbation theory and from
discretization errors. Perturbative truncation errors are of particular concern because they often cannot easily be estimated
from studying the data itself. Further, the size
of higher-order coefficients in the perturbative series can sometimes
turn out to be larger than naive expectations based on power counting
from the behaviour of lower-order terms.  We note
that perturbative truncation errors are also the dominant
source of uncertainty in several of the phenomenological
determinations of $\alpha_s$.  

The various phenomenological approaches to determining the running
coupling, $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)$ are summarized by the
Particle Data Group \cite{Tanabashi:2018oca}.
The PDG review lists five categories of phenomenological results 
used to obtain the running coupling: using hadronic
$\tau$ decays, hadronic final states of $e^+e^-$ annihilation,
deep inelastic lepton--nucleon scattering, electroweak precision data, and high energy hadron collider data.
Excluding lattice results, the PDG quotes the weighted average as
\begin{eqnarray}
   \alpha^{(5)}_{\overline{\rm MS}}(M_Z) &=& 0.1174(16) \,, \quad 
   \mbox{PDG 2018 \cite{Tanabashi:2018oca}}
\label{PDG_nolat}
\end{eqnarray}
compared to 
$
   \alpha^{(5)}_{\overline{\rm MS}}(M_Z) = 0.1183(12) 
$
of the older review \cite{Agashe:2014kda}.
For a general overview of the various phenomenological
and lattice approaches 
%{\cred s
see, e.g., Ref.~%{\cred \cite{Bethke:2011tr} ??} 
 \cite{Salam:2017qdl}. The extraction of
$\alpha_s$ from $\tau$ data, which is the most precise and has the
largest impact on the nonlattice average in Eq.~(\ref{PDG_nolat}) is
especially sensitive to the treatment of higher-order perturbative
terms as well as the treatment of nonperturbative effects.  
This is important to keep in mind when comparing our chosen
range for $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)$ from lattice
determinations in Eq.~(\ref{eq:alpmz}) with the nonlattice average
from the PDG.

\subsubsection{Scheme and scale dependence of $\alpha_s$ and $\Lambda_{\rm QCD}$}

Despite the fact that the notion of the QCD coupling is 
initially a perturbative concept, the associated $\Lambda$ parameter
is nonperturbatively defined
\begin{eqnarray}
   \Lambda 
      \equiv \mu\,(b_0\gbar_s^2(\mu))^{-b_1/(2b_0^2)} 
              e^{-1/(2b_0\gbar_s^2(\mu))}
             \exp\left[ -\int_0^{\gbar_s(\mu)}\,dx 
                        \left( {1\over \beta(x)} + {1 \over b_0x^3} 
                                                 - {b_1 \over b_0^2x}
                        \right) \right] \,,
\label{eq:Lambda}
\end{eqnarray}
where $\beta$ is the full renormalization group function in the scheme
which defines $\gbar_s$, and $b_0$ and $b_1$ are the first two
scheme-independent coefficients of the perturbative expansion
\begin{eqnarray}
   \beta(x) \sim -b_0 x^3 -b_1 x^5 + \ldots \,,
\label{eq:beta_pert}
\end{eqnarray}
with
\begin{eqnarray}
   b_0 = {1\over (4\pi)^2}
           \left( 11 - {2\over 3}N_f \right) \,, \qquad
   b_1 = {1\over (4\pi)^4}
           \left( 102 - {38 \over 3} N_f \right) \,.
\label{b0+b1}
\end{eqnarray}
Thus the $\Lambda$ parameter is renormalization-scheme-dependent but in an
exactly computable way, and lattice gauge theory is an ideal method to
relate it to the low-energy properties of QCD.
In the $\overline{\rm MS}$ scheme presently $b_{n_l}$
with $n_l = 4$ is known.

The change in the coupling from one scheme $S$ to another (taken here
to be the $\overline{\rm MS}$ scheme) is perturbative,
\begin{eqnarray}
   g_{\overline{\rm MS}}^2(\mu) 
      = g_{\rm S}^2(\mu) (1 + c^{(1)}_g g_{\rm S}^2(\mu) + \ldots ) \,,
\label{eq:g_conversion}
\end{eqnarray}
where $c^{(i)}_g, \, i\geq 1$ are finite renormalization coefficients.  The
scale $\mu$ must be taken high enough for the error in keeping only
the first few terms in the expansion to be small.
On the other hand, the conversion to the $\Lambda$ parameter
in the $\overline{\rm MS}$ scheme is given exactly by
\begin{eqnarray}
   \Lambda_{\overline{\rm MS}} 
      = \Lambda_{\rm S} \exp\left[ c_g^{(1)}/(2b_0)\right] \,.
      \label{eq:Lambdaconversion}
\end{eqnarray}

The fact that $\Lambda_\msbar$ can be obtained exactly from
$\Lambda_S$ in any scheme $S$ where $c^{(1)}_g$ is known 
together with the high order knowledge (5-loop by now) of
$\beta_\msbar$ means that the errors in $\alpha_\msbar(m_\mathrm{Z})$
are dominantly due to the  errors of $\Lambda_S$. We will therefore
mostly discuss them in that way. 
Starting from \eq{eq:Lambda}, we have to consider (i) the
error of $\gbar_S^2(\mu)$ (denoted as $\left(\frac{\Delta \Lambda}{\Lambda}\right)_{\Delta \alpha_S}$ ) and (ii) the truncation error in $\beta_S$ (denoted as $\left( \frac{\Delta \Lambda}{\Lambda}\right)_{\rm trunc}$).
Concerning (ii), note that knowledge of $c_g^{(n_l)}$ for the scheme $S$ means that $\beta_S$ is known to $n_l+1$ loop order; $b_{n_l}$ is known. We thus see that in the region where 
perturbation theory can be applied, the following errors of $\Lambda_S$ (or consequently $\Lambda_{\overline{\rm MS}}$) have to be considered
\begin{eqnarray}
  \left(\frac{\Delta \Lambda}{\Lambda}\right)_{\Delta \alpha_S} &=& \frac{\Delta \alpha_{S}(\mu)}{ 8\pi b_0 \alpha_{S}^2(\mu)} \times \left[1 + \rmO(\alpha_S(\mu))\right]\,,
  \label{eq:i}\\
 \left( \frac{\Delta \Lambda}{\Lambda}\right)_{\rm trunc} &=& k \alpha_{S}^{n_\mathrm{l}}(\mu) + \rmO(\alpha_S^{n_\mathrm{l}+1}(\mu))\,,
  \label{eq:ii}  
\end{eqnarray}
where $k$ is proportional to $b_{n_\mathrm{l}+1}$ and in typical 
good schemes such as $\msbar$ it is numerically of order one. 
Statistical and systematic errors such as discretization
effects contribute to  $\Delta \alpha_{S}(\mu)$.  In the above we dropped a 
scheme subscript for the $\Lambda$-parameters because of
      \eq{eq:Lambdaconversion}.

 


By convention $\alpha_\msbar$ is usually quoted at a scale $\mu=M_Z$
where the appropriate effective coupling is the one in the
5-flavour theory: $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)$.  In
order to obtain it from a result with fewer flavours, one connects effective
theories with different number of flavours as discussed by Bernreuther
and Wetzel~\cite{Bernreuther:1981sg}.  For example, one considers the
$\msbar$ scheme, matches the 3-flavour theory to the 4-flavour
theory at a scale given by the charm-quark mass~\cite{Chetyrkin:2005ia,Schroder:2005hy,Kniehl:2006bg}, runs with the
5-loop $\beta$-function~\cite{vanRitbergen:1997va,Czakon:2004bu,Luthe:2016ima,Herzog:2017ohr,Baikov:2016tgj} of the 4-flavour theory to a scale given by
the $b$-quark mass, and there matches to the 5-flavour theory, after
which one runs up to $\mu=M_Z$ with the 5-loop $\beta$ function.
For the matching relation at a given
quark threshold we use the mass $m_\star$ which satisfies $m_\star=
\overline{m}_\msbar(m_\star)$, where $\overline{m}$ is the running
mass (analogous to the running coupling). Then
\begin{eqnarray}
\label{e:convnfm1}
 \gbar^2_{N_f-1}(m_\star) =  \gbar^2_{N_f}(m_\star)\times 
      [1+ 0\times\gbar^{2}_{N_f}(m_\star) + \sum_{n\geq 2}t_n\,\gbar^{2n}_{N_f}(m_\star)]
\label{e:grelation}
\end{eqnarray}
{with  \cite{Grozin:2011nk,Kniehl:2006bg,Chetyrkin:2005ia} }
\def\nli{(N_f-1)}
\begin{eqnarray}
  t_2 &=&  {1 \over (4\pi^2)^2} {11\over72}\,,\\
  t_3 &=&  {1 \over (4\pi^2)^3} \left[- {82043\over27648}\zeta_3 + 
                     {564731\over124416}-{2633\over31104}(N_f-1)\right]\,, \\
  t_4 &=& {1 \over (4\pi^2)^4} \big[5.170347 - 1.009932 \nli - 0.021978 \,\nli^2\big]\,,
\end{eqnarray}
(where $\zeta_3$ is the Riemann zeta-function) provides the matching
at the thresholds in the $\msbar$ scheme.  Often the package {\tt RunDec}
is used for quark-threshold matching and running in the $\msbar$-scheme \cite{Chetyrkin:2000yt,Herren:2017osy}.

While $t_2,\,t_3,\,t_4$ are
numerically small coefficients, the charm threshold scale is also
relatively low and so there are nonperturbative
uncertainties in the matching procedure, which are difficult to
estimate but which we assume here to be negligible.
Obviously there is no perturbative matching formula across
the strange ``threshold''; here matching is entirely nonperturbative.
Model dependent extrapolations of $\gbar^2_{N_f}$ from $N_f=0,2$ to
$N_f=3$ were done in the early days of lattice gauge theory. We will
include these in our listings of results but not in our estimates,
since such extrapolations are based on untestable assumptions.

%{\cred 
\subsubsection{Overview of the review of $\alpha_s$}
%{\bf update}

We begin by explaining lattice-specific difficulties in \sect{s:crit}
and the FLAG criteria designed to assess whether the
associated systematic uncertainties can be controlled and estimated in
a reasonable manner.  We then discuss, in \sect{s:SF} -- \sect{s:glu},
the various lattice approaches. For completeness, we present results
from calculations with $N_f = 0, 2, 3$, and 4 flavours.  Finally, in
Sec.~\ref{s:alpsumm}, we present averages together with our best
estimates for $\alpha_{\overline{\rm MS}}^{(5)}$. These are determined
from 3- and 4-flavour QCD simulations. The earlier $N_f = 0, 2$
works obtained results for $N_f = 3$ by extrapolation in
$N_f$. Because this is not a theoretically controlled procedure, we do
not include these results in our averages.  For the $\Lambda$
parameter, we also give results for other number of flavours,
including $N_f=0$. Even though the latter numbers should not be used
for phenomenology, they represent valuable nonperturbative
information concerning field theories with variable numbers of quarks.
%}


\subsubsection{Additions with respect to the FLAG 13 report}

Computations added in FLAG 16 were 
\begin{itemize}
\item[]
    Karbstein 14 \cite{Karbstein:2014bsa}
    and Bazavov 14 \cite{Bazavov:2014soa}
    based on the static-quark potential (\sect{s:qq}),
\item[]
    FlowQCD 15 \cite{Asakawa:2015vta} based on a tadpole-improved
    bare coupling (\sect{s:WL}),
\item[]    
    HPQCD 14A  \cite{Chakraborty:2014aca} based on heavy-quark current 
    two-point functions (\sect{s:curr}).
\end{itemize}
They influenced the final ranges marginally.


\subsubsection{Additions with respect to the FLAG 16 report}
For the benefit of the readers who are familiar with our previous 
report, we list here where changes and additions can be found which
go beyond slight improvements of the presentation.

Our criteria are slightly updated, keeping up-to-date with the
cited precisions of computations. In particular, in the criterion for
perturbative behaviour we specify that the requirement may be
less stringent if a larger uncertainty is quoted. 

The FLAG 19 additions are
\begin{itemize}
\item[]
   ALPHA 17 \cite{Bruno:2017gxd}
   and Ishikawa 17 \cite{Ishikawa:2017xam}
   from step-scaling methods (\sect{s:SF}).
\item[]
    Husung 17 \cite{Husung:2017qjz},
    Karbstein 18 \cite{Karbstein:2018mzo} and
    Takaura 18 \cite{Takaura:2018lpw,Takaura:2018vcy}
    from the static-quark potential (\sect{s:qq}).
\item[]
    Hudspith 18 \cite{Hudspith:2018bpz}
    based on the vacuum polarization (\sect{s:vac}).
\item[]
    Kitazawa 16 \cite{Kitazawa:2016dsl} based on a
    tadpole-improved bare coupling (\sect{s:WL}).
\item[]  
    JLQCD 16 \cite{Nakayama:2016atf}
    and Maezawa 16 \cite{Maezawa:2016vgv}
    based on heavy-quark current two-point functions (\sect{s:curr}).
\item[]
    Nakayama 18 \cite{Nakayama:2018ubk}
    from the eigenvalue spectrum of the Dirac operator (\sect{s:eigenvalue}).

\end{itemize}

% ----------------------------------------------------------------------
\subsection{General issues}

\subsubsection{Discussion of criteria for computations entering the averages}

% ----------------------------------------------------------------------

\label{s:crit}

% ----------------------------------------------------------------------

As in the PDG review, we only use calculations of $\alpha_s$ published
in peer-reviewed journals, and that use NNLO or higher-order
perturbative expansions, to obtain our final range in
Sec.~\ref{s:alpsumm}.  We also, however, introduce further
criteria designed to assess the ability to control important
systematics, which we describe here.  Some of these criteria, 
e.g., that for the continuum extrapolation, are associated with
lattice-specific systematics and have no continuum analogue.  Other
criteria, e.g., that for the renormalization scale, could in
principle be applied to nonlattice determinations.
Expecting that lattice calculations
will continue to improve significantly in the near future, our goal in
reviewing the state of the art here is to be conservative and avoid
prematurely choosing an overly small range.


In lattice calculations, we generally take ${\oO}$ to be some
combination of physical amplitudes or Euclidean correlation functions
which are free from UV and IR divergences and have a well-defined
continuum limit.  Examples include the force between static quarks and
two-point functions of quark bilinear currents.

In comparison to values of observables ${\oO}$ determined
experimentally, those from lattice calculations require two more
steps.  The first step concerns setting the scale $\mu$ in \mbox{GeV},
where one needs to use some experimentally measurable low-energy scale
as input. Ideally one employs a hadron mass. Alternatively convenient
intermediate scales such as $\sqrt{t_0}$, $w_0$, $r_0$, $r_1$,
\cite{Luscher:2010iy,Borsanyi:2012zs,Sommer:1993ce,Bernard:2000gd} can
be used if their relation to an experimental dimensionful observable
is established. The low-energy scale needs to be computed at the same
bare parameters where ${\oO}$ is determined, at least as long as
one does not use the step-scaling method (see below).  This induces a
practical difficulty given present computing resources.  In the
determination of the low-energy reference scale the volume needs to be
large enough to avoid finite-size effects. On the other hand, in order
for the perturbative expansion of Eq.~(\ref{eq:alpha_MSbar}) to be
reliable, one has to reach sufficiently high values of $\mu$,
i.e., short enough distances. To avoid uncontrollable discretization
effects the lattice spacing $a$ has to be accordingly small.  This
means
\begin{eqnarray}
   L \gg \mbox{hadron size}\sim \Lambda_{\rm QCD}^{-1}\quad 
   \mbox{and} \quad  1/a \gg \mu \,,
   \label{eq:scaleproblem}
\end{eqnarray}
(where $L$ is the box size) and therefore
\begin{eqnarray} 
   L/a \ggg \mu/\Lambda_{\rm QCD} \,.
   \label{eq:scaleproblem2}
\end{eqnarray}
The currently available computer power, however, limits $L/a$, 
typically to
$L/a = 32-96$. 
Unless one accepts compromises in controlling  discretization errors
or finite-size effects, this means one needs to set 
the scale $\mu$ according to
\begin{eqnarray}
   \mu \lll L/a \times \Lambda_{\rm QCD} & \sim 10-30\, \mbox{GeV} \,.
\end{eqnarray}
(Here $\lll$ or $\ggg$ means at least one order of magnitude smaller or larger.) 
Therefore, $\mu$ can be $1-3\, \mbox{GeV}$ at most.
This raises the concern whether the asymptotic perturbative expansion
truncated at $1$-loop, $2$-loop, or $3$-loop in Eq.~(\ref{eq:alpha_MSbar})
is sufficiently accurate. There is a finite-size scaling method,
usually called step-scaling method, which solves this problem by identifying 
$\mu=1/L$ in the definition of ${\oO}(\mu)$, see \sect{s:SF}. 

For the second step after setting the scale $\mu$ in physical units
($\mbox{GeV}$), one should compute ${\oO}$ on the lattice,
${\oO}_{\rm lat}(a,\mu)$ for several lattice spacings and take the continuum
limit to obtain the left hand side of Eq.~(\ref{eq:alpha_MSbar}) as
\begin{eqnarray}
   {\oO}(\mu) \equiv \lim_{a\rightarrow 0} {\oO}_{\rm lat}(a,\mu) 
              \mbox{  with $\mu$ fixed}\,.
\end{eqnarray}
This is necessary to remove the discretization error.

Here it is assumed that the quantity ${\oO}$ has a continuum limit,
which is regularization-independent. 
The method discussed in \sect{s:WL}, which is based on the perturbative
expansion of a lattice-regulated, divergent short-distance quantity
$W_{\rm lat}(a)$ differs in this respect and must be
treated separately.

In summary, a controlled determination of $\alpha_s$ 
needs to satisfy the following:
\begin{enumerate}

   \item The determination of $\alpha_s$ is based on a
         comparison of a
         short-distance quantity ${\oO}$ at scale $\mu$ with a well-defined
         continuum limit without UV and IR divergences to a perturbative
         expansion formula in Eq.~(\ref{eq:alpha_MSbar}).

   \item The scale $\mu$ is large enough so that the perturbative expansion
         in Eq.~(\ref{eq:alpha_MSbar}) is precise 
         to the order at which it is truncated,
         i.e., it has good {\em asymptotic} convergence.
         \label{pt_converg}

   \item If ${\oO}$ is defined by physical quantities in infinite volume,  
         one needs to satisfy \eq{eq:scaleproblem2}.
         \label{constraints}

   \item[] Nonuniversal quantities need a separate discussion, see
        \sect{s:WL}.

\end{enumerate}

Conditions \ref{pt_converg}. and \ref{constraints}. give approximate lower and
upper bounds for $\mu$ respectively. It is important to see whether there is a
window to satisfy \ref{pt_converg}. and \ref{constraints}. at the same time.
If it exists, it remains to examine whether a particular lattice
calculation is done inside the window or not. 

Obviously, an important issue for the reliability of a calculation is
whether the scale $\mu$ that can be reached lies in a regime where
perturbation theory can be applied with confidence. However, the value
of $\mu$ does not provide an unambiguous criterion. For instance, the
Schr\"odinger Functional, or SF-coupling (Sec.~\ref{s:SF}) is
conventionally taken at the scale $\mu=1/L$, but one could also choose
$\mu=2/L$. Instead of $\mu$ we therefore define an effective
$\alpha_{\rm eff}$.  For schemes such as SF (see Sec.~\ref{s:SF}) or
$qq$ (see Sec.~\ref{s:qq}) this is directly the coupling  of
the scheme. For other schemes such as the vacuum polarization we use
the perturbative expansion \eq{eq:alpha_MSbar} for the observable
${\oO}$ to define
\begin{eqnarray}
   \alpha_{\rm eff} =  {\oO}/c_1 \,.
   \label{eq:alpeff}
\end{eqnarray}
If there is an $\alpha_s$-independent term it should first be subtracted.
Note that this is nothing but defining an effective,
regularization-independent coupling,
a physical renormalization scheme.

Let us now comment further on the use of the perturbative series.
Since it is only an asymptotic expansion, the remainder $R_n({\oO})={\oO}-\sum_{i\leq n}c_i \alpha_s^i$ of a truncated
perturbative expression ${\oO}\sim\sum_{i\leq n}c_i \alpha_s^i$
cannot just be estimated as a perturbative error $k\,\alpha_s^{n+1}$.
The error is nonperturbative. Often one speaks of ``nonperturbative
contributions'', but nonperturbative and perturbative cannot be
strictly separated due to the asymptotic nature of the series (see,
e.g., Ref.~\cite{Martinelli:1996pk}).

Still, we do have some general ideas concerning the 
size of nonperturbative effects. The known ones such as instantons
or renormalons decay for large $\mu$ like inverse powers of $\mu$
and are thus roughly of the form 
\begin{eqnarray}
   \exp(-\gamma/\alpha_s) \,,
\end{eqnarray}
with some positive constant $\gamma$. Thus we have,
loosely speaking,
\begin{eqnarray}
   {\oO} = c_1 \alpha_s + c_2 \alpha_s^2 + \ldots + c_n\alpha_s^n
                  + \cO(\alpha_s^{n+1}) 
                  + \cO(\exp(-\gamma/\alpha_s)) \,.
   \label{eq:Owitherr}
\end{eqnarray}
For small $\alpha_s$, the $\exp(-\gamma/\alpha_s)$ is negligible.
Similarly the perturbative estimate for the magnitude of
relative errors in \eq{eq:Owitherr} is small; as an
illustration for $n=3$ and $\alpha_s = 0.2$ the relative error
is $\sim 0.8\%$ (assuming coefficients $|c_{n+1} /c_1 | \sim 1$).

For larger values of $\alpha_s$ nonperturbative effects can become
significant in Eq.~(\ref{eq:Owitherr}). An instructive example comes
from the values obtained from $\tau$
decays, for which $\alpha_s\approx 0.3$. Here, different applications
of perturbation theory (fixed order and contour improved)
each look reasonably asymptotically convergent but the difference does
not seem to decrease much with the order (see, e.g., the contribution
of Pich in Ref.~\cite{Bethke:2011tr}). In addition nonperturbative terms
in the spectral function may be nonnegligible even after the
integration up to $m_\tau$ (see, e.g., Refs.~\cite{Boito:2014sta}, \cite{Boito:2016oam}). %{\cred ?? scheme variations: \cite{Boito:2016pwf}??}). 
All of this is because $\alpha_s$ is not really small.

Since the size of the nonperturbative effects is very hard to
estimate one should try to avoid such regions of the coupling.  In a
fully controlled computation one would like to verify the perturbative
behaviour by changing $\alpha_s$ over a significant range instead of
estimating the errors as $\sim \alpha_s^{n+1}$ .  Some computations
try to take nonperturbative power `corrections' to the perturbative
series into account by including such terms in a fit to the $\mu$-dependence. We note that this is a delicate procedure, both because
the separation of nonperturbative and perturbative is theoretically
not well defined and because in practice a term like, e.g.,
$\alpha_s(\mu)^3$ is hard to distinguish from a $1/\mu^2$ term when
the $\mu$-range is restricted and statistical and systematic errors
are present. We consider it safer to restrict the fit range to the
region where the power corrections are negligible compared to the
estimated perturbative error.

The above considerations lead us to the following special
criteria for the determination of $\alpha_s$: 

%\eject
\begin{itemize}
   \item Renormalization scale         
         \begin{itemize}
            \item[\good] all points relevant in the analysis have
             $\alpha_\mathrm{eff} < 0.2$
            \item[\soso] all points have $\alpha_\mathrm{eff} < 0.4$
                         and at least one 
                         $\alpha_\mathrm{eff} \le 0.25$
            \item[\bad]  otherwise                                   
         \end{itemize}

   \item Perturbative behaviour 
        \begin{itemize}
           \item[\good] verified over a range of a factor $4$ change
                        in $\alpha_\mathrm{eff}^{n_\mathrm{l}}$ without power
                        corrections  or alternatively 
                        $\alpha_\mathrm{eff}^{n_\mathrm{l}} \le \frac12 \Delta \alpha_\mathrm{eff} / (8\pi b_0 \alpha_\mathrm{eff}^2) $ is reached
           \item[\soso] agreement with perturbation theory 
                        over a range of a factor
                        $(3/2)^2$ in $\alpha_\mathrm{eff}^{n_\mathrm{l}}$ 
                        possibly fitting with power corrections or
                        alternatively 
                        $\alpha_\mathrm{eff}^{n_\mathrm{l}} \le \Delta \alpha_\mathrm{eff} / (8\pi b_0 \alpha_\mathrm{eff}^2)$
                        is reached
           \item[\bad]  otherwise
       \end{itemize}
        Here {$\Delta \alpha_\mathrm{eff}$ is the accuracy cited for the determination of 
        $\alpha_\mathrm{eff}$}
        and $n_\mathrm{l}$ is the loop order to which the 
        connection of $\alpha_\mathrm{eff}$ to the $\msbar$ scheme is known.
        Recall the discussion around Eqs.~(\ref{eq:i},\ref{eq:ii})
        The $\beta$-function of $\alpha_\mathrm{eff}$ is then known to 
        $n_\mathrm{l}+1$ loop order.%
        \footnote{Once one is in the perturbative region with 
        $\alpha_{\rm eff}$, the error in 
        extracting the $\Lambda$ parameter due to the truncation of 
        perturbation theory scales like  $\alpha_{\rm eff}^{n_\mathrm{l}}$,
        as discussed around Eq.~(\ref{eq:ii}). In order to 
        detect/control such corrections properly, one needs to change
        the correction term significantly; 
        we require a factor of four for a $\good$ and a factor $(3/2)^2$
        for a $\soso$. 
        % In comparison to FLAG 13, where $n_l=2$ was taken
        % as the default, we have made the $n_l$ dependence explicit and
        % list it in Tabs.~\ref{tab_Nf=0_renormalization_a} --
        % \ref{tab_Nf=4_renormalization}. 
        An exception to the above is the situation 
        where the correction terms are small anyway, i.e.,  
        $\alpha_{\rm eff}^{n_\mathrm{l}} = \Delta \Lambda/\Lambda\approx \Delta \alpha_{\rm eff} / (8\pi b_0 \alpha_{\rm eff}^2)$ is reached.}
         
   \item Continuum extrapolation 
        
        At a reference point of $\alpha_{\rm eff} = 0.3$ (or less) we require
         \begin{itemize}
            \item[\good] three lattice spacings with
                         $\mu a < 1/2$ and full $\cO(a)$
                         improvement, \\
                         or three lattice spacings with
                         $\mu a \leq 1/4$ and $2$-loop $\cO(a)$
                         improvement, \\
                         or $\mu a \leq 1/8$ and $1$-loop $\cO(a)$
                         improvement 
            \item[\soso] three lattice spacings with $\mu a < 3/2$
                         reaching down to $\mu a =1$ and full
                         $\cO(a)$ improvement, \\
                         or three lattice spacings with
                         $\mu a \leq 1/4$ and 1-loop $\cO(a)$
                         improvement        
            \item[\bad]  otherwise 
         \end{itemize}
\end{itemize}  

We also need to specify what is meant by $\mu$. Here are our choices:
\begin{eqnarray}
%   \text{Schr\"odinger Functional} &:& \mu=1/L\,,
   \text{step-scaling} &:& \mu=1/L\,,
   \nonumber  \\
   \text{heavy quark-antiquark potential} &:& \mu=2/r\,,
   \nonumber  \\
   \text{observables in momentum space} &:& \mu =q \,,
   \nonumber   \\ 
    \text{moments of heavy-quark currents} 
                                        &:& \mu=2\bar{m}_\mathrm{c} \,,
   \nonumber   \\ 
    \text{eigenvalues of the Dirac operator}
                                        &:& \mu= \lambda_\msbar
\label{mu_def}
\end{eqnarray}
where $q$ is the magnitude of the momentum, $\bar{m}_\mathrm{c}$
the heavy-quark mass, usually taken around the charm quark mass and
$\lambda_\msbar$ is the eigenvalue of the Dirac operator, see 
\sect{s:eigenvalue}.  We note again that the above criteria cannot
be applied when regularization dependent quantities
$W_\mathrm{lat}(a)$ are used instead of ${\cO}(\mu)$. These cases
are specifically discussed in \sect{s:WL}.

In principle one should also
account for electro-weak radiative corrections. However, both in the
determination of $\alpha_{s}$ at intermediate scales $\mu$ and
in the running to high scales, we expect electro-weak effects to be
much smaller than the presently reached precision. Such effects are
therefore not further discussed.

The attentive reader will have noticed that bounds such as $\mu a <
3/2$ or at least one value of $\alpha_\mathrm{eff}\leq 0.25$
which we require for a $\soso$ are
not very stringent. There is a considerable difference between
$\soso$ and $\good$. We have chosen the above bounds, unchanged as compared 
to FLAG 16, since not too many
computations would satisfy more stringent ones at present.
Nevertheless, we believe that the \soso\ criteria already give
reasonable bases for estimates of systematic errors. In the future, we
expect that we will be able to tighten our criteria for inclusion in
the average, and that many more computations will reach the present
\good\ rating in one or more categories. 

%{\cblu 
In addition to our explicit criteria, the following effects may influence
the precision of results: 
%}

{\em Topology sampling:}
    In principle a good way to improve the quality 
    of determinations of $\alpha_s$ is to push to very small lattice 
    spacings thus enabling large $\mu$. It is known  
    that the sampling of field space becomes very difficult for the 
    HMC algorithm when the lattice spacing is small and one has the
    standard periodic boundary conditions. In practice, for all known
    discretizations the topological charge slows down dramatically for
    $a\approx 0.05\,\fm$ and smaller 
    % EB: PLEASE KEEP CITES IN ONE LINE -- otherwise the script for acronym replacement will fail
    \cite{DelDebbio:2002xa,Bernard:2003gq,Schaefer:2010hu,Chowdhury:2013mea,Brower:2014bqa,Bazavov:2014pvz,Fukaya:2015ara}. Open boundary conditions solve the problem 
    \cite{Luscher:2011kk} but are not frequently used. Since the effect of
    the freezing on short distance observables is not known, we also do need to pay
    attention to this issue. Remarks are added in the text when appropriate.
      
{\em Quark-mass effects:} We assume that effects
of the finite masses of the light quarks (including strange) 
are negligible in the effective
coupling itself where large, perturbative, $\mu$ is considered.

{\em Scale determination:}
    The scale does not need
    to be very precise, since using the lowest-order $\beta$-function
    shows that a 3\% error in the scale determination corresponds to a
    $\sim 0.5\%$ error in $\alpha_s(M_Z)$.  So as long as systematic
    errors from chiral extrapolation and finite-volume effects are  well below
    3\% we do not need to be concerned about those at the present level of 
    precision in $\alpha_s(M_Z)$. This may change in the future.

% ----------------------------------------------------------------------

\subsubsection{Physical scale}

% ----------------------------------------------------------------------

{
A popular scale choice has been the intermediate $r_0$ scale. One
should bear in mind that its determination from physical
observables also has to be taken into account.  The phenomenological
value of $r_0$ was originally determined as $r_0 \approx
0.49\,\mbox{fm}$ through potential models describing quarkonia
\cite{Sommer:1993ce}. Of course the quantity is precisely defined,
independent of such model considerations.
But a lattice computation with the correct sea-quark content is 
needed to determine a completely sharp value. When the quark 
content is not quite realistic, the value of $r_0$ may depend to
some extent on
which experimental input is used to determine (actually define) it. 

The latest determinations from two-flavour QCD are
$r_0$ = 0.420(14)--0.450(14)~fm by the ETM collaboration
\cite{Baron:2009wt,Blossier:2009bx}, using as input $f_\pi$ and $f_K$
and carrying out various continuum extrapolations. On the other hand,
the ALPHA collaboration \cite{Fritzsch:2012wq} determined $r_0$ =
0.503(10)~fm with input from $f_K$, and the QCDSF
collaboration \cite{Bali:2012qs} cites 0.501(10)(11)~fm from
the mass of the nucleon (no continuum limit).  Recent determinations
from three-flavour QCD are consistent with $r_1$ = 0.313(3)~fm
and $r_0$ = 0.472(5)~fm
\cite{Davies:2009tsa,Bazavov:2010hj,Bazavov:2011nk}. Due to the
uncertainty in these estimates, and as many results are based directly
on $r_0$ to set the scale, we shall often give both the dimensionless
number $r_0 \Lambda_{\overline{\rm MS}}$, as well as $\Lambda_{\overline{\rm MS}}$.
In the cases where no physical $r_0$ scale is given in
the original papers or we convert to
the $r_0$ scale, we use the value $r_0$ = 0.472~fm. In case
$r_1 \Lambda_{\overline{\rm MS}}$ is given in the publications,
we use $r_0 /r_1 = 1.508$ \cite{Bazavov:2011nk}, to convert,
which remains well consistent with the update \cite{Bazavov:2014pvz} 
neglecting the error on this ratio. In some, mostly early,
computations the string tension, $\sqrt{\sigma}$ was used.
We convert to $r_0$ using $r_0^2\sigma = 1.65-\pi/12$,
which has been shown to be an excellent approximation 
in the relevant pure gauge theory \cite{Necco:2001xg,Luscher:2002qv}.

The new scales $t_0,w_0$ based on the gradient flow are very attractive
alternatives to $r_0$ but their
discretization errors are still under discussion 
\cite{Ramos:2014kka,Fodor:2014cpa,Bazavov:2015yea,Bornyakov:2015eaa}
and their values at the physical point are not yet determined 
with great precision.
We remain with $r_0$ as our main reference scale for now.
A general discussion of the various scales is given in 
\cite{Sommer:2014mea}.


}


% ----------------------------------------------------------------------

{
\subsubsection{Studies of truncation errors of perturbation theory}
\label{s:trunc}

As discussed previously, we have to determine $\alpha_s$ in a region
where the perturbative expansion for the $\beta$-function, 
Eq.~(\ref{eq:beta_pert}) in the integral Eq.~(\ref{eq:Lambda}),
is reliable. In principle this must be checked, however this is
difficult to achieve as we need to reach up to a  sufficiently high scale.
A frequently used recipe to estimate the size of truncation errors
of the perturbative series is to vary the renormalization-scale
dependence around the chosen `optimal' scale $\mu_*$, of an observable
evaluated at a fixed order in the coupling from $\mu=\mu_*/2$ to $2\mu_*$. For an example see \fig{scaledepR4}.
Alternatively, or in addition, the renormalization scheme chosen
can be varied, which investigates the perturbative
conversion of the chosen scheme to the perturbatively defined
$\overline{\rm MS}$ scheme and in particular `fastest apparent
convergence' when the `optimal' scale is chosen so that the
$O(\alpha_s^2)$ coefficient vanishes.

The ALPHA collaboration in Ref.~\cite{Brida:2016flw} and ALPHA 17~%, 
\cite{DallaBrida:2018rfy}, within the SF approach defined
a set of $\nu$ schemes where the third scheme-dependent
coefficient of the $\beta$-function for $N_f = 2+1$ flavours was
computed to be
$b_2^\nu = -(0.064(27)+1.259(1)\nu)/(4\pi)^3$. 
The standard SF scheme has $\nu = 0$. For comparison, $b_2^\msbar = 0.324/(4\pi)^3$.
A range of scales from about 
$4\,\mbox{GeV}$ to $128\,\mbox{GeV}$ was investigated.
It was found that while the procedure of varying the
scale by a factor 2 up and down gave a correct estimate
of the residual perturbative error for $\nu \approx 0 \ldots  0.3$,  
for negative values, e.g.,  $\nu = -0.5$, the estimated perturbative
error is much too small to account for the mismatch in the 
$\Lambda$-parameter of $\approx 8\%$ at $\alpha_s=0.15$.
This mismatch, however, did, as expected, still scale with $\alpha_s^{n_l}$ with $n_l=2$. In the schemes
with negative $\nu$, the coupling
$\alpha_s$ has to be quite small for
scale-variations of a factor 2 to correctly
signal the perturbative errors. 

{%\color{blue}
A similar $\approx 8\%$ deviation in the 
$\Lambda$-parameter extracted from the qq-scheme (c.f. \sect{s:qq}) 
is found by Husung~17 \cite{Husung:2017qjz}, 
but at $\alpha_s\approx 0.2$ and with $n_l=3$.
}
