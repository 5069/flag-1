
\subsection{$\alpha_s$ from observables at the lattice spacing scale}
\label{s:WL}

% ----------------------------------------------------------------------

\subsubsection{General considerations}

% ----------------------------------------------------------------------

The general method is to evaluate a short-distance quantity ${\oO}$
at the scale of the lattice spacing $\sim 1/a$ and then determine
its relationship to $\alpha_{\overline{\rm MS}}$ via a 
perturbative 
expansion.

This is epitomized by the strategy of the HPQCD collaboration
\cite{Mason:2005zx,Davies:2008sw}, discussed here for illustration,
which computes and then fits to a variety of short-distance quantities, $Y$,
\begin{eqnarray}
   Y = \sum_{n=1}^{n_{\rm max}} c_n \alphah^n(q^*) \,.
\label{Ydef}
\end{eqnarray}
The quantity $Y$ is taken as the logarithm of small Wilson loops (including some
nonplanar ones), Creutz ratios, `tadpole-improved' Wilson loops and
the tadpole-improved or `boosted' bare coupling ($\cO(20)$ quantities in
total). The perturbative coefficients $c_n$  (each depending on the
choice of $Y$) are known to $n = 3$ with additional coefficients up to
$n_{\rm max}$ being fitted numerically.   The running
coupling $\alphah$ is related to $\alphav$ from the static-quark potential
(see Sec.~\ref{s:qq}).\footnote{ $\alphah$ is defined by
  $\Lambda_\mathrm{V'}=\Lambda_\mathrm{V}$ and
  $b_i^\mathrm{V'}=b_i^\mathrm{V}$ for $i=0,1,2$ but $b_i^\mathrm{V'}=0$ for
  $i\geq3$. }

 The coupling
constant is fixed at a scale $q^* = d/a$.
The latter  is chosen as the mean value of $\ln q$ with the one gluon loop
as measure
\cite{Lepage:1992xa,Hornbostel:2002af}. (Thus a different result
for $d$ is found for every short-distance quantity.)
A rough estimate yields $d \approx \pi$, and in general the
renormalization scale is always found to lie in this region.

For example, for the Wilson loop $W_{mn} \equiv \langle W(ma,na) \rangle$
we have
\begin{eqnarray}
   \ln\left( \frac{W_{mn}}{u_0^{2(m+n)}}\right)
      = c_1 \alphah(q^*) +  c_2 \alphah^2(q^*)  + c_3 \alphah^3(q^*)
        + \cdots \,,
\label{short-cut}
\end{eqnarray}
for the tadpole-improved version, where $c_1$, $c_2\,, \ldots$
are the appropriate perturbative coefficients and $u_0 = W_{11}^{1/4}$.
Substituting the nonperturbative simulation value in the left hand side,
we can determine $\alphah(q^*)$, at the scale $q^*$.
Note that one finds empirically that perturbation theory for these
tadpole-improved quantities have smaller $c_n$ coefficients and so
the series has a faster apparent convergence compared to the case
without tadpole improvement.


Using the $\beta$-function in the $\rm V'$ scheme,
results can be run to a reference value, chosen as
$\alpha_0 \equiv \alphah(q_0)$, $q_0 = 7.5\,\mbox{GeV}$.
This is then converted perturbatively to the continuum
$\msbar$ scheme
\begin{eqnarray}
   \alpha_{\overline{\rm MS}}(q_0)
      = \alpha_0 + d_1 \alpha_0^2 + d_2 \alpha_0^3 + \cdots \,,
\end{eqnarray}
where $d_1, d_2$ are known 1- and 2-loop coefficients.

Other collaborations have focused more on the bare `boosted'
coupling constant and directly determined its relationship to
$\alpha_{\overline{\rm MS}}$. Specifically, the boosted coupling is
defined by 
\begin{eqnarray}
   \alphap(1/a) = {1\over 4\pi} {g_0^2 \over u_0^4} \,,
\end{eqnarray}
again determined at a scale $\sim 1/a$. As discussed previously, 
since the plaquette expectation value in the boosted coupling
contains the tadpole diagram contributions to all orders, which
are dominant contributions in perturbation theory,
there is an expectation that the perturbation theory using
the boosted coupling has 
smaller perturbative coefficients \cite{Lepage:1992xa}, and hence smaller 
perturbative errors.
 
% ----------------------------------------------------------------------

\subsubsection{Continuum limit}

% ----------------------------------------------------------------------

Lattice results always come along with discretization errors,
which one needs to remove by a continuum extrapolation.
As mentioned previously, in this respect the present
method differs in principle from those in which $\alpha_s$ is determined
from physical observables. In the general case, the numerical
results of the lattice simulations at a value of $\mu$ fixed in physical 
units can be extrapolated to the continuum limit, and the result can be 
analyzed as to whether it shows perturbative running as a function of 
$\mu$ in the continuum. For observables at the cutoff-scale ($q^*=d/a$),  
discretization effects cannot easily be separated out
from perturbation theory, as the scale for the coupling
comes from the lattice spacing. 
Therefore the restriction  $a\mu  \ll 1$ (the `continuum extrapolation'
criterion) is not applicable here. Discretization errors of 
order $a^2$ are, however, present. Since 
$a\sim \exp(-1/(2b_0 g_0^2)) \sim \exp(-1/(8\pi b_0 \alpha(q^*))$, 
these errors now appear as power corrections to the perturbative 
running, and have to be taken into account in the study of the 
perturbative behaviour, which is to be verified by changing $a$. 
One thus usually fits with power corrections in this method.

In order to keep a symmetry with the `continuum extrapolation' 
criterion for physical observables and to remember that discretization 
errors are, of course, relevant, 
we replace it here by one for the lattice spacings used:
\begin{itemize}
   \item Lattice spacings
         \begin{itemize}
            \item[\good] 
               3 or more lattice spacings, at least 2 points below
               $a = 0.1\,\mbox{fm}$
            \item[\soso]
               2 lattice spacings, at least 1 point below
               $a = 0.1\,\mbox{fm}$
            \item[\bad]
               otherwise 
         \end{itemize}
\end{itemize}


% ----------------------------------------------------------------------

\subsubsection{Discussion of computations}

% ----------------------------------------------------------------------
\begin{table}[!p]
%\begin{table}[!h]
   \vspace{3.0cm}
   \footnotesize
   \begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}rllllllll}
   Collaboration & Ref. & $N_f$ &
   \hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{renormalization scale}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{perturbative behaviour}\end{rotate}
                                                    \hspace{-0.15cm} &
   \hspace{0.15cm}\begin{rotate}{60}{lattice spacings}\end{rotate}
      \hspace{-0.25cm} & %\rule{0.2cm}{0cm} 
                         scale & $\Lambda_\msbar[\MeV]$ & $r_0\Lambda_\msbar$ \\
   & & & & & & & & \\[-0.1cm]
   \hline
   \hline
   & & & & & & & & \\[-0.1cm]
   HPQCD 10$^a$$^\S$& \cite{McNeile:2010ji}& 2+1 & \gA & \soso
            & \good & \good
            & $r_1 = 0.3133(23)\, \mbox{fm}$
            & 340(9) 
            & 0.812(22)                                   \\ 
%            &                     &     &     &
   HPQCD 08A$^a$& \cite{Davies:2008sw} & 2+1 & \gA & \soso
            & \good & \good
            & $r_1 = 0.321(5)\,\mbox{fm}$$^{\dagger\dagger}$
            & 338(12)$^\star$
            & 0.809(29)                                   \\
%            &                     &     &     &
   Maltman 08$^a$& \cite{Maltman:2008bx}& 2+1 & \gA & \soso
            & \soso & \good
            & $r_1 = 0.318\, \mbox{fm}$
            & 352(17)$^\dagger$
            & 0.841(40)                                   \\ 
%            &                     &     &     &
   HPQCD 05A$^a$ & \cite{Mason:2005zx} & 2+1 & \gA & \soso
            & \soso & \soso
            & $r_1$$^{\dagger\dagger}$
            & 319(17)$^{\star\star}$
            & 0.763(42)                                   \\
%            &                     &     &     &
   & & & & & & & & &  \\[-0.1cm]
   \hline
   & & & & & & & & &  \\[-0.1cm]
   QCDSF/UKQCD 05 & \cite{Gockeler:2005rv}  & 2 & \gA & \good
            & \bad  & \good
            & $r_0 = 0.467(33)\,\mbox{fm}$
            & 261(17)(26)
            & 0.617(40)(21)$^b$                           \\
%            &                     &     &     &
   SESAM 99$^c$ & \cite{Spitz:1999tu} & 2 & \gA & \soso
            & \bad  & \bad
            & $c\bar{c}$(1S-1P)
            & 
            &                                             \\
   Wingate 95$^d$ & \cite{Wingate:1995fd} & 2 & \gA & \good
            & \bad  & \bad
            & $c\bar{c}$(1S-1P)
            & 
            &                                             \\
   Davies 94$^e$ & \cite{Davies:1994ei} & 2 & \gA & \good
            & \bad & \bad
            & $\Upsilon$
            & 
            &                                             \\
   Aoki 94$^f$ & \cite{Aoki:1994pc} & 2 & \gA & \good
            & \bad & \bad
            & $c\bar{c}$(1S-1P)
            & 
            &                                             \\
   & & & & & & & & &  \\[-0.1cm]
   \hline
   & & & & & & & & &  \\[-0.1cm]

   {Kitazawa 16}
            & \cite{Kitazawa:2016dsl}        & 0 & \gA
            & \good  & \good   & \good
            & $w_0$
            & $260(5)$$^j$
            & $0.621(11)$$^j$                            \\

   FlowQCD 15
            & \cite{Asakawa:2015vta}        & 0 & \oP 
            & \good  & \good   & \good
            & $w_{0.4}$$^i$
%            & {\color{red}$w_{0.4} = 0.182(3)\,\mbox{fm}$$^i$}
%            & $w_{0.4} = 0.193(3)\,\mbox{fm}$$^i$
            & $258(6)$$^i$
            & 0.618(11)$^i$                             \\

   QCDSF/UKQCD 05 & \cite{Gockeler:2005rv}  & 0 & \gA & \good
            & \soso & \good
            & $r_0 = 0.467(33)\,\mbox{fm}$
            & 259(1)(20)
            & 0.614(2)(5)$^b$                              \\
%            &                     &     &     &
   SESAM 99$^c$ & \cite{Spitz:1999tu} & 0 & \gA & \good
            & \bad  & \bad
            & $c\bar{c}$(1S-1P)
            & 
            &                                             \\
   Wingate 95$^d$ & \cite{Wingate:1995fd} & 0 & \gA & \good
            & \bad  & \bad
            & $c\bar{c}$(1S-1P)
            & 
            &                                             \\
   Davies 94$^e$ & \cite{Davies:1994ei}  & 0 & \gA & \good
            & \bad & \bad
            & $\Upsilon$
            & 
            &                                             \\
   El-Khadra 92$^g$ & \cite{ElKhadra:1992vn} & 0 & \gA & \good
            & \bad    & \soso
            & $c\bar{c}$(1S-1P)
            & 234(10)
            & 0.560(24)$^h$                               \\
   & & & & & & & & &  \\[-0.1cm]
   \hline
   \hline\\
\end{tabular*}\\[-0.2cm]
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
   \item[$^a$]The numbers for $\Lambda$ have been converted from the values for 
              $\alpha_s^{(5)}(M_Z)$. \\[-5mm]
   \item[$^{\S}$]     $\alpha_{\overline{\rm MS}}^{(3)}(5\ \mbox{GeV})=0.2034(21)$,
              $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.1184(6)$,
              only update of intermediate scale and $c$-, $b$-quark masses,
              supersedes HPQCD 08A.\\[-5mm]
   \item[$^\dagger$] $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.1192(11)$. \\[-4mm]
   \item[$^\star$]    $\alpha_V^{(3)}(7.5\,\mbox{GeV})=0.2120(28)$, 
              $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.1183(8)$,
              supersedes HPQCD 05. \\[-5mm]
   \item[$^{\dagger\dagger}$] Scale is originally determined from $\Upsilon$
              mass splitting. $r_1$ is used as an intermediate scale.
              In conversion to $r_0\Lambda_{\overline{\rm MS}}$, $r_0$ is
              taken to be $0.472\,\mbox{fm}$. \\[-5mm]
   \item[$^{\star\star}$] $\alpha_V^{(3)}(7.5\,\mbox{GeV})=0.2082(40)$,
              $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.1170(12)$. \\[-5mm]
   \item[$^b$]       This supersedes 
              Refs.~\cite{Gockeler:2004ad,Booth:2001uy,Booth:2001qp}.
              $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.112(1)(2)$.
              The $N_f=2$ results were based on values for $r_0 /a$
              which have later been found to be too 
              small~\cite{Fritzsch:2012wq}. The effect will  
              be of the order of 10--15\%, presumably an increase in 
              $\Lambda r_0$. \\[-5mm]
   \item[$^c$]       $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.1118(17)$. \\[-4mm]
   \item[$^d$]    
   $\alpha_V^{(3)}(6.48\,\mbox{GeV})=0.194(7)$ extrapolated from $\Nf=0,2$.
              $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.107(5)$.   \\[-4mm]
   \item[$^e$]  
              $\alpha_P^{(3)}(8.2\,\mbox{GeV})=0.1959(34)$ extrapolated
              from $N_f=0,2$. $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.115(2)$.
              \\[-5mm]
   \item[ $^f$]Estimated $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.108(5)(4)$. \\[-5mm]
   \item[$^g$]       This early computation violates our requirement that
              scheme conversions are done at the 2-loop level.
              $\Lambda_{\overline{\rm MS}}^{(4)}=160(^{+47}_{-37})\mbox{MeV}$, 
              $\alpha^{(4)}_{\overline{\rm MS}}(5\mbox{GeV})=0.174(12)$.
              We converted this number to give
              $\alpha^{(5)}_{\overline{\rm MS}}(M_Z)=0.106(4)$.  \\[-5mm]
   \item[$^h$]We used $r_0=0.472\,\mbox{fm}$ to convert to $r_0 \lms$. \\[-5mm]
   \item[$^i$]       Reference scale $w_{0.4}$ where $w_x$ is defined 
              by $\left. t\partial_t[t^2 \langle E(t)\rangle]\right|_{t=w_x^2}=x$
              in terms of the action density $E(t)$ at positive flow time $t$ 
              \cite{Asakawa:2015vta}. Our conversion to $r_0$ scale
              using \cite{Asakawa:2015vta} $r_0/w_{0.4}=2.587(45)$ and
              $r_0=0.472\,\mbox{fm}$. 
   \item[$^j$]{Our conversion from $w_0\Lambda_\msbar=0.2154(12)$ to $r_0$ scale
              using  $r_0/w_0=(r_0/w_{0.4}) \cdot   (w_{0.4}/w_0) = 2.885(50)$ 
              with the factors cited by the collaboration \cite{Asakawa:2015vta} 
              and with $r_0=0.472\,\mbox{fm}$. }
\end{itemize}
}
\end{minipage}
\normalsize
\caption{Wilson loop results. Some early results for $\Nf=0, 2$ did not determine  $\Lambda_\msbar$.
}
\label{tab_wloops}
\end{table}

Note that due to $\mu \sim 1/a$ being relatively large the
results easily have a $\good$ or $\soso$ in the rating on 
renormalization scale.

The work of El-Khadra 92 \cite{ElKhadra:1992vn} employs a 1-loop
formula to relate $\alpha^{(0)}_{\overline{\rm MS}}(\pi/a)$
to the boosted coupling for three lattice spacings
$a^{-1} = 1.15$, $1.78$, $2.43\,\mbox{GeV}$. (The lattice spacing
is determined from the charmonium 1S-1P splitting.) They obtain
$\Lambda^{(0)}_{\overline{\rm MS}}=234\,\mbox{MeV}$, corresponding
to $\alpha_{\rm eff} = \alpha^{(0)}_{\overline{\rm MS}}(\pi/a)
\approx$ 0.15--0.2. The work of Aoki 94 \cite{Aoki:1994pc}
calculates $\alpha^{(2)}_V$ and $\alpha^{(2)}_{\overline{\rm MS}}$
for a single lattice spacing $a^{-1}\sim 2\,\mbox{GeV}$,  again
determined from charmonium 1S-1P splitting in two-flavour QCD.
Using 1-loop perturbation theory with boosted coupling,
they obtain $\alpha^{(2)}_V=0.169$ and $\alpha^{(2)}_{\overline{\rm MS}}=0.142$.
Davies 94 \cite{Davies:1994ei} gives a determination of $\alphav$
from the expansion 
\begin{equation}
   -\ln W_{11} \equiv \frac{4\pi}{3}\alphav^{(N_f)}(3.41/a)
        \times [1 - (1.185+0.070N_f)\alphav^{(N_f)} ]\,,
\end{equation}
neglecting higher-order terms.  They compute the $\Upsilon$ spectrum
in $N_f=0$, $2$ QCD for single lattice spacings at $a^{-1} = 2.57$,
$2.47\,\mbox{GeV}$ and obtain $\alphav(3.41/a)\simeq$ 0.15, 0.18, respectively.  Extrapolating the inverse coupling linearly in $N_f$, a
value of $\alphav^{(3)}(8.3\,\mbox{GeV}) = 0.196(3)$ is obtained.
SESAM 99 \cite{Spitz:1999tu} follows a similar strategy, again for a
single lattice spacing. They linearly extrapolated results for
$1/\alphav^{(0)}$, $1/\alphav^{(2)}$ at a fixed scale of
$9\,\mbox{GeV}$ to give $\alphav^{(3)}$, which is then perturbatively
converted to $\alpha_{\overline{\rm MS}}^{(3)}$. This finally gave
$\alpha_{\overline{\rm MS}}^{(5)}(M_Z) = 0.1118(17)$.  Wingate 95
\cite{Wingate:1995fd} also follows this method.  With the scale
determined from the charmonium 1S-1P splitting for single lattice
spacings in $N_f = 0$, $2$ giving $a^{-1}\simeq 1.80\,\mbox{GeV}$ for
$N_f=0$ and $a^{-1}\simeq 1.66\,\mbox{GeV}$ for $N_f=2$, they obtain
$\alphav^{(0)}(3.41/a)\simeq 0.15$ and $\alphav^{(2)}\simeq 0.18$, 
respectively. Extrapolating the coupling linearly in $N_f$, they
obtain $\alphav^{(3)}(6.48\,\mbox{GeV})=0.194(17)$.

The QCDSF/UKQCD collaboration, QCDSF/UKQCD 05
\cite{Gockeler:2005rv}, \cite{Gockeler:2004ad,Booth:2001uy,Booth:2001qp},
use the 2-loop relation (re-written here in terms of $\alpha$)
\begin{eqnarray}
   {1 \over \alpha_{\overline{\rm MS}}(\mu)} 
      = {1 \over \alphap(1/a)} 
        + 4\pi(2b_0\ln a\mu - t_1^{\rm P}) 
        + (4\pi)^2(2b_1\ln a\mu - t_2^{\rm P})\alphap(1/a) \,,
\label{gPtoMSbar}
\end{eqnarray}
where $t_1^{\rm P}$ and $t_2^{\rm P}$ are known. (A 2-loop relation corresponds
to a 3-loop lattice $\beta$-function.)  This was used to
directly compute $\alpha_{\rm \overline{\rm MS}}$, and the scale was
chosen so that the $\cO(\alphap^0)$ term vanishes, i.e., \
\begin{eqnarray}
   \mu^* = {1 \over a} \exp{[t_1^{\rm P}/(2b_0)] } 
        \approx \left\{ \begin{array}{cc}
                           2.63/a  & N_f = 0 \\
                           1.4/a   & N_f = 2 \\
                        \end{array}
                 \right. \,.
\label{amustar}
\end{eqnarray}
The method is to first compute $\alphap(1/a)$ and from this,  using
Eq.~(\ref{gPtoMSbar}) to find $\alpha_{\overline{\rm MS}}(\mu^*)$.
The RG equation, Eq.~(\ref{eq:Lambda}), then determines
$\mu^*/\Lambda_{\overline{\rm MS}}$ and hence using
Eq.~(\ref{amustar}) leads to the result for $r_0\Lambda_{\overline{\rm MS}}$.
This avoids giving the scale in $\mbox{MeV}$ until the end.
In the $\Nf=0$ case seven lattice spacings were used
\cite{Necco:2001xg}, giving a range $\mu^*/\Lambda_{\overline{\rm MS}}
\approx$ 24--72 (or $a^{-1} \approx$ 2--7~GeV) and
$\alpha_{\rm eff} = \alpha_{\overline{\rm MS}}(\mu^*) \approx$ 0.15--0.10. Neglecting higher-order perturbative terms (see discussion
after Eq.~(\ref{qcdsf:ouruncert}) below) in Eq.~(\ref{gPtoMSbar}) this
is sufficient to allow a continuum extrapolation of
$r_0\Lambda_{\overline{\rm MS}}$.
A similar computation for $N_f = 2$ by QCDSF/UKQCD~05 \cite{Gockeler:2005rv}
gave $\mu^*/\Lambda_{\overline{\rm MS}} \approx$ 12--17
(or roughly $a^{-1} \approx$ 2--3~GeV) 
and $\alpha_{\rm eff} = \alpha_{\overline{\rm MS}}(\mu^*)
\approx$ 0.20--0.18.
%
The $N_f=2$ results of QCDSF/UKQCD~05 \cite{Gockeler:2005rv} are affected by an 
uncertainty which was not known at the time of publication: 
It has been realized that the values of $r_0/a$ of Ref.~\cite{Gockeler:2005rv}
were significantly too low~\cite{Fritzsch:2012wq}. 
As this effect is expected to depend on $a$, it
influences the perturbative behaviour leading us to assign 
a \bad\ for that criterion. 

Since FLAG 13, there has been one new result for $N_f = 0$ 
by FlowQCD 15 \cite{Asakawa:2015vta}, later 
updated and published in Kitazawa 16 \cite{Kitazawa:2016dsl}.
They also use the techniques
as described in Eqs.~(\ref{gPtoMSbar}), (\ref{amustar}), but together
with the gradient flow scale $w_0$ (rather than the $r_0$ scale)
leading to a determination of $w_0\Lambda_{\overline{\rm MS}}$.
The continuum limit is estimated by extrapolating the data at $6$
lattice spacings linearly in $a^2$. The data range used is
$\mu^*/\Lambda_{\overline{\rm MS}} \approx$ 50--120 (or 
$a^{-1} \approx$ 5--11~GeV) and
$\alpha_{\overline{\rm MS}}(\mu^*) \approx$ 0.12--0.095.
Since a very small value of $\alpha_\msbar$ is reached, there is a $\good$ 
in the perturbative behaviour. Note that our conversion to the common
$r_0$ scale unfortunately leads to a significant increase of the error of the
$\Lambda$ parameter compared to using $w_0$ directly \cite{Sommer:2014mea}. 
Again we note that the results of QCDSF/UKQCD 05
\cite{Gockeler:2005rv} ($N_f = 0$) and Kitazawa 16 \cite{Kitazawa:2016dsl}
may be affected by frozen topology as they have
lattice spacings significantly below $a = 0.05\,\mbox{fm}$.
Kitazawa 16 \cite{Kitazawa:2016dsl} investigate this by evaluating
$w_0/a$ in a fixed topology and estimate any effect at about $\sim 1\%$.

The work of HPQCD 05A \cite{Mason:2005zx} (which supersedes
the original work \cite{Davies:2003ik}) uses three lattice spacings
$a^{-1} \approx 1.2$, $1.6$, $2.3\,\mbox{GeV}$ for $2+1$
flavour QCD. Typically the renormalization scale
$q \approx \pi/a \approx$ 3.50--7.10~GeV, corresponding to
$\alpha_\mathrm{V'} \approx$ 0.22--0.28. 

In the later update HPQCD 08A \cite{Davies:2008sw} twelve data sets
(with six lattice spacings) are now used reaching up to $a^{-1}
\approx 4.4\,\mbox{GeV}$,  corresponding to $\alpha_\mathrm{V'}\approx
0.18$. The values used for the scale $r_1$ were further updated in
HPQCD 10 \cite{McNeile:2010ji}. Maltman 08 \cite{Maltman:2008bx}
uses most of the same lattice ensembles as HPQCD
08A~\cite{Davies:2008sw}, but not the one at the smallest lattice spacing, $a\approx0.045$~fm. Maltman 08 \cite{Maltman:2008bx} also
considers a much smaller set of
quantities (three versus 22) that are less sensitive to condensates.
They also use different strategies for evaluating the condensates and
for the perturbative expansion, and a slightly different value for the
scale $r_1$. The central values of the final results from 
Maltman 08 \cite{Maltman:2008bx} and HPQCD 08A \cite{Davies:2008sw}
differ by 0.0009 (which would be decreased to 0.0007
taking into account a reduction of 0.0002 in the value of the $r_1$
scale used by Maltman 08 \cite{Maltman:2008bx}).
 
As mentioned before, the perturbative coefficients are computed
through $3$-loop order~\cite{Mason:2004zt}, while the higher-order
perturbative coefficients $c_n$ with $ n_{\rm max} \ge n > 3$ (with
$n_{\rm max} = 10$) are numerically fitted using the
lattice-simulation data for the lattice spacings with the help of
Bayesian methods.  It turns out that corrections in \eq{short-cut} are
of order $|c_i/c_1|\alpha^i=$ 5--15\% and 3--10\% for $i$ = 2, 3,
respectively.  The inclusion of a fourth-order term is necessary to
obtain a good fit to the data, and leads to a shift of the result by
$1$ -- $2$ sigma. For all but one of the 22 quantities, central values
of $|c_4/c_1|\approx$ 2--4 were found, with errors from the fits of
$\approx 2$.

An important source of uncertainty is the truncation 
of perturbation theory. In HPQCD 08A \cite{Davies:2008sw}, 10
\cite{McNeile:2010ji} it
is estimated to be about $0.4$\% of $\alpha_\msbar(M_Z)$.  In \flagold\
we included a rather detailed discussion of the issue with the result
that we prefer for the time being a more conservative error
based on the above estimate $|c_4/c_1| = 2$. 
From Eq.~(\ref{Ydef}) this gives an estimate of the uncertainty
in $\alpha_{\rm eff}$ of
\begin{eqnarray}
  \Delta \alpha_{\rm eff}(\mu_1) = 
          \left|{c_4 \over c_1}\right|\alpha_{\rm eff}^4(\mu_1) \,,
\label{qcdsf:ouruncert}
\end{eqnarray}
at the scale $\mu_1$ where $\alpha_{\rm eff}$ is computed from
the Wilson loops. This can be used with a variation
in $\Lambda$ at lowest order of perturbation theory and also
applied to $\alpha_s$ evolved to a different scale $\mu_2$,%
\footnote{From Eq.~(\ref{e:grelation}) we see that at low order in PT
the coupling $\alpha_s$ is continuous and differentiable across
the mass thresholds (at the same scale). Therefore 
to leading order $\alpha_s$ and $\Delta \alpha_s$
are independent of $N_f$.}
\begin{eqnarray}
   {\Delta\Lambda \over \Lambda} 
      = {1\over 8\pi b_0 \alpha_s} 
                  {\Delta \alpha_s \over \alpha_s}
                                                         \,, \qquad
   {\Delta \alpha_s(\mu_2) \over \Delta \alpha_s(\mu_1)}
      = {\alpha_s^2(\mu_2) \over \alpha_s^2(\mu_1)} \,.
   \label{e:dLL}   
\end{eqnarray}
With $\mu_2 = M_Z$
and $\alpha_s(\mu_1)=0.2$ (a typical value extracted 
from Wilson loops in HPQCD 10 \cite{McNeile:2010ji}, HPQCD 08A
\cite{Davies:2008sw} at $\mu = 5\,\mbox{GeV}$) we have 
\begin{eqnarray}
  \Delta \alpha_\msbar(m_Z) = 0.0012 \,,
\label{hpqcd:ouruncert}
\end{eqnarray}
which we shall later use as the typical perturbative uncertainty 
of the method with $2+1$ fermions.
}

Tab.~\ref{tab_wloops} summarizes the results. Within the errors of 3--5\% $N_f=3$ determinations of $r_0 \Lambda$ nicely agree.

% ----------------------------------------------------------------------
