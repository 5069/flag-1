# FLAG plot for L_5
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm 10^3 L_5$"			# plot title
plotnamestring	= "su3_L5"			# filename for plot
plotxticks	= ([[-1,0,1,2]])                # locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [-1.8,06.0,"$\\rm N_f=2+1$"],
                   [-1.8,12.3,"$\\rm N_f=2+1+1$"]
		    ]
xlimits		= [-1.1,4]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 2.2					# x-position for the data labels


# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white, g=green
#		2	marker color (errorbar and frame), color coding as above
#		3	color intensity 0=full, 1=soso, 2=bleak

dat2p1p1=[
 [1.19,0.25,0.25,0,0, "HPQCD 13A",['s','g','g',0,tpos]]
]

dat2p1=[
 [0.87,0.10,0.10,0.10,0.10, "RBC/UKQCD 08", ['s','w','r',0,tpos]],
 [1.45,0.07,0.07,0.07,0.07, "PACS-CS 08",   ['s','w','r',0,tpos]],
 [1.4 ,0.2 ,0.2 ,0.22,0.28, "MILC 09",      ['s','l','g',0,tpos]],
 [0.84,0.12,0.12,0.38,0.38, "MILC 09A",     ['s','l','g',0,tpos]],
 [0.98,0.16,0.16,0.37,0.44, "MILC 10",      ['s','g','g',0,tpos]]
]

datphen=[
 [1.4,0.5,0.5,0.5,0.5,"Gasser 85",['o','b','b',0,tpos]],
 [0.58,0.13,0.13,0.13,0.13,"Bijnens 11",['o','b','b',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      #
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.00,0.00,0.00,0.00, "placeholderxxxxxxxxxxxxxxx",       ['s','k','k',0,tpos],0],
 [  0.98,0.37,0.44,0.37,0.44, "FLAG average for $\\rm N_f=2+1$",  ['s','k','k',0,tpos],0],
 [  1.19,0.25,0.25,0.25,0.25, "FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datphen,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

