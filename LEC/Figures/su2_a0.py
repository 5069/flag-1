# FLAG plot for a_0^0
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm a_0^0 M_{\pi}$"        # plot title
plotnamestring	= "su2_a0"			# filename for plot
plotxticks	= ([[0.18,0.19,0.20,0.21,0.22]])                # locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [0.16,02.7,"$\\rm N_f=2$"],
                   [0.16,05.7,"$\\rm N_f=2+1$"]
		    ]
xlimits		= [0.17,0.25]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 0.235					# x-position for the data labels


# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white, g=green
#		2	marker color (errorbar and frame), color coding as above
#		3	color intensity 0=full, 1=soso, 2=bleak

dat2p1=[
#[0.186,0.002,0.002,np.nan,np.nan, "Fu 11",         ['s','w','r',0,tpos]],
 [0.214,0.004,0.004,0.008 ,0.008 , "Fu 13",         ['s','w','r',0,tpos]],
 [0.217,0.009,0.009,0.010 ,0.010 , "Fu 17",         ['s','w','r',0,tpos]]
]

dat2=[
 [0.189,0.009,0.009,0.011,0.011, "ETM 16C",         ['s','w','r',0,tpos]]
]

datphen=[
 [0.220 ,0.005 ,0.005 ,0.005 ,0.005 ,"Colangelo 01",['o','b','b',0,tpos]],
 [0.2198,0.0046,0.0046,0.0080,0.0080,"Caprini 11",  ['o','b','b',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      #
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.00,0.00,0.00,0.00, "placeholderxxxxxxxxxxxxxx",        ['s','k','k',0,tpos],0],
 [np.nan,0.00,0.00,0.00,0.00, "FLAG average for $\\rm N_f=2$",    ['s','k','k',0,tpos],0],
 [np.nan,0.40,0.61,0.40,0.61, "FLAG average for $\\rm N_f=2+1$",  ['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datphen,dat2,dat2p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

