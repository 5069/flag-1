# FLAG plot for l6bar
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\bar{\\ell}_6$"         # plot title
plotnamestring	= "su2_l6bar"			# filename for plot
plotxticks	= ([[8, 10, 12, 14, 16, 18]])      # locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [8.5,11.0,"$\\rm N_f=2+1$"],
		   [8.5, 4.5,"$\\rm N_f=2$"]
		    ]
xlimits		= [7,26]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 19					# x-position for the data labels
SHIFT=-.1
# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

dat2p1=[
 [12.2 ,0.9 ,0.9 ,np.nan,np.nan, "RBC/UKQCD 08A"  , ['D','w','r',0,tpos]],
 [ 7.5 ,1.3 ,1.3 ,2.0   ,2.0   , "JLQCD 14"       , ['<','w','r',0,tpos]],
 [13.49,0.89,0.89,1.21  ,1.21  , "JLQCD 15A"      , ['D','w','r',0,tpos]]
]

dat2=[
 [14.9 ,1.2 ,1.2 ,1.4   ,1.4   , "ETM 08"         , ['D','g','g',0,tpos]],
 [11.9 ,0.7 ,0.7 ,1.2   ,1.2   , "JLQCD/TWQCD 09" , ['D','w','r',0,tpos]],
 [15.5 ,1.7 ,1.7 ,2.14  ,2.14  , "Brandt 13"      , ['D','g','g',0,tpos]],
 [16.21,0.76,0.76,1.03  ,1.03  , "ETM 17F"        , ['D','w','r',0,tpos]]
]

datphen=[
 [16.5 ,1.1 ,1.1 ,1.1   ,1.1   , "Gasser 84"      , ['o','b','b',0,tpos]],
 [16.0 ,0.5 ,0.5 ,0.9   ,0.9   , "Bijnens 98"     , ['o','b','b',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN" with respect to the above
	      # there is another column at the end specifying the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.000,0.000,0.000,0.000, "placeholder"                   , ['s','w','w',0,tpos],0],
 [15.078,1.165,1.165,0.000,0.000, "FLAG average for $\\rm N_f=2$" , ['s','k','k',0,tpos],0],
 [np.nan,0.000,0.000,0.000,0.000, "placeholder"                   , ['s','w','w',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datphen,dat2,dat2p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

