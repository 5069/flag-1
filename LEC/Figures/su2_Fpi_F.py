# FLAG plot for Fpi/F
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm F_\pi/F$"		# plot title
plotnamestring	= "su2_Fpi_F"			# filename for plot
plotxticks	= ([[1.00,1.04,1.08, 1.12, 1.16,1.20]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [0.93,25.7,"$\\rm N_f=2+1+1$"],
		   [0.93,17.0,"$\\rm N_f=2+1$"],
		   [0.93, 5.0,"$\\rm N_f=2$"]
		    ]
xlimits		= [1.00,1.31]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.21					# x-position for the data labels

nan=np.nan

# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
 [1.076 ,0.002 ,0.002 ,0.003 ,0.003 , "ETM 10"                , ['s','w','r',0,tpos]],
 [1.0773,0.0017,0.0017,0.0026,0.0026, "ETM 11"                , ['s','g','g',0,tpos]]  # update to and systematic error taken from ETM 10
]

dat2p1=[
#[1.080 ,0.008 ,0.008 ,np.nan,np.nan, "RBC/UKQCD 08"          , ['s','w','r',0,tpos]], # no systematic error
#[1.060 ,0.007 ,0.007 ,np.nan,np.nan, "PACS-CS 08, SU(2)-fit" , ['s','w','r',0,tpos]], # no systematic error
#[1.062 ,0.008 ,0.008 ,np.nan,np.nan, "PACS-CS 08, SU(3)-fit" , ['s','w','r',0,tpos]], # no systematic error
 [1.052 ,0.002 ,0.002 ,0.004 ,0.006 , "MILC 09"               , ['s','l','g',0,tpos]],
 [1.054 ,0.007 ,0.007 ,0.013 ,0.014 , "MILC 09A, $SU(2)$-fit" , ['s','l','g',0,tpos]],
 [1.062 ,0.001 ,0.001 ,0.003 ,0.003 , "MILC 09A, $SU(3)$-fit" , ['s','l','g',0,tpos]],
 [1.054 ,0.012 ,0.012 ,0.015 ,0.033 , "MILC 10A"              , ['s','l','g',0,tpos]],
 [1.060 ,0.005 ,0.005 ,0.008 ,0.008 , "MILC 10"               , ['s','g','g',0,tpos]],
 [1.062 ,0.026 ,0.026 ,0.048 ,0.049 , "NPLQCD 11"             , ['s','g','g',0,tpos]],
 [1.0627,0.0006,0.0006,0.0028,0.0028, "Borsanyi 12"           , ['s','g','g',0,tpos]],
 [1.0550,0.0070,0.0070,0.0073,0.0073, "BMW 13"                , ['s','g','g',0,tpos]],
 [1.0645,0.0015,0.0015,0.0015,0.0015, "RBC/UKQCD 14B"         , ['s','l','g',0,tpos]], # systematic error is zero
 [1.0641,0.0021,0.0021,0.0053,0.0053, "RBC/UKQCD 15E"         , ['s','g','g',0,tpos]]
]

dat2=[
 [1.167 ,0.037 ,0.037 ,0.073 ,0.038 , "JLQCD/TWQCD 08A"       , ['s','w','r',0,tpos]],
 [1.067 ,0.009 ,0.009 ,0.013 ,0.013 , "ETM 08"                , ['D','g','g',0,tpos]],
#[1.02  ,0.05  ,0.05  ,np.nan,np.nan, "Hasenfratz 08"         , ['<','w','r',0,tpos]], # no systematic error
#[1.00  ,0.05  ,0.05  ,np.nan,np.nan, "ETM 09B"               , ['<','w','r',0,tpos]], # no systematic error
 [1.0755,0.0006,0.0006,0.0094,0.0010, "ETM 09C"               , ['s','g','g',0,tpos]],
 [1.106 ,0.006 ,0.006 ,0.007 ,0.007 , "TWQCD 11"              , ['s','w','r',0,tpos]],
#[1.17  ,0.05  ,0.05  ,np.nan,np.nan, "Bernardoni 11"         , ['s','w','r',0,tpos]], # no systematic error
#[1.07  ,0.01  ,0.01  ,np.nan,np.nan, "QCDSF 13"              , ['s','l','g',0,tpos]], # no systematic error
 [1.080 ,0.016 ,0.016 ,0.017 ,0.017 , "Brandt 13"             , ['D','g','g',0,tpos]],
 [1.075 ,0.009 ,0.009 ,0.031 ,0.031 , "Engel 14"              , ['s','g','g',0,tpos]]  # nothing to do with SD
#[1.069 ,0.035 ,0.035 ,0.035 ,0.035 , "ETM 15A"               , ['s','w','r',0,tpos]]  # no systematic error
]
datphen=[
[1.0719,0.0000,0.0000,0.0052,0.0052, "Colangelo 03"           , ['o','b','b',0,tpos]]]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN" with respect to the above
	      # there is another column at the end specifying the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.0000,0.0000,0.0000,0.0000, "placeholder pheno",                 ['s','k','k',0,tpos],0],
 [1.0729,0.0149,0.0149,0.0060,0.0060, "FLAG estimate for $\\rm N_f=2$",    ['s','k','k',0,tpos],0],
 [1.0620,0.0073,0.0073,0.0022,0.0022, "FLAG estimate for $\\rm N_f=2+1$",  ['s','k','k',0,tpos],0],
 [1.0773,0.0026,0.0026,0.0026,0.0026, "FLAG average for $\\rm N_f=2+1+1$", ['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datphen,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

