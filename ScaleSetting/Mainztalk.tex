%%%%%%%%
\subsection{From Rainer's Mainz proceedings}
\newcommand{\fk}{F_\mathrm{K}}
\newcommand{\prot}{\mathrm{prot}}
\newcommand{\nf}{\Nf}
\def\Rho{{\cal R}}
\def\mlat{m^\mathrm{lat}}
\def\prot{\mathrm{p}}
\def\quark{\mathrm{quark}}
\def\sm{S^\mathrm{m}}
\def\sa{S^\mathrm{a}}
\def\metas{m_{\eta_s}}
\def\texp{\tau_\mathrm{exp}}
\def\tauexp{\tau_\mathrm{exp}}
\def\vud{V_\mathrm{ud}}
\def\vus{V_\mathrm{us}}
\def\expe{{\cal E}}
\def\tauexp{\tau_\mathrm{exp}}
\def\tauint{\tau_\mathrm{int}}
\def\tmc{t_\mathrm{MC}}
\def\mprot{m_\prot}
\def\Mprot{M_\prot}

\subsubsection{Introduction}
Presumably the most natural scale for low energy QCD is 
the mass of the proton, $m_\prot$. It is very well knwown.
Alternatively, as a theorist, one might 
like the pseudo scalar decay constant in the chiral limit, called $f$. 
It sets the scale for interactions of the very low
energy description of QCD.
% by chiral perturbation theory.
Indeed, 
let us consider for a little while just QCD in the chiral limit, 
in order to simplify the discussion. 
In this limit, ratios of pairs of these scales or any
other observables of dimension mass, $m_i$,  are predictions of the
theory, but not the scale itself. The latter, and only it, has to be taken from experiment.
In lattice QCD this manifests itself in the fact that 
dimensionless ratios,
$$
    \Rho_i = m_i / \mprot
$$
can be computed and {\em have a continuum limit}. Taking 
$\mprot$ as a reference here is what we call scale setting.
Equivalently, a lattice computation determines dimensionless
quantities $M_i=m_i a,\; \Mprot=\mprot a$ as a function of the
bare coupling $g_0$. The prediction for the physical, dimensionful quantity, $m_i$ is then obtained by
$$
  m_i = \Rho_i \mprot \,,\quad \Rho_i = 
    \lim_{\Mprot\to0} \frac{M_i}{\Mprot} 
  = \lim_{g_0\to 0} \frac{M_i(g_0)}{\Mprot(g_0)} \,.
$$
In lattice slang, scale setting usually is refered to the 
equivalent point of view of determining the lattice spacing $a$
at a given $g_0$ from the specific quantity $\mprot$,
$$
    a_\prot(g_0) = \frac{\Mprot(g_0)}{\mprot}\,,
$$
with the purpose of then defining any other dimensionful
quantity 
$$
  \mlat_i(g_0) = \frac{M_i(g_0)}{a_\prot(g_0)} \,.
$$
Clearly $\mlat_i(g_0)$ has a continuum limit and 
at finite $g_0$ (finite $a$) it has lattice artifacts 
(deviations from this limit)
which depend also on the choice of scale, $\mprot$. 

In practise, we also approximate QCD by an effective 
theory where heavy quarks are removed (one often says integrated out).
How good this approximation is also depends on the
choice of the scale as is apparent from the previous discussion.



\subsubsection{Scale setting is important}

Why is scale setting discussed in a plenary talk?
Both in planning simulations and in the analysis
of the results, the scale (as explained above, whether I say the 
scale or the lattice spacing is just the same) is usually very important.
When quark masses are neglected, an error made in the
scale, systematic or statistical, propagates linearly into a 
hadron mass. 

Now thinking about the real theory, the one with quark masses, 
the scale already enters decisively into fixing the 
bare quark masses in the Lagarangian, namely in planning the simulations.
Usually we do this by adjusting the pseudo scalar meson masses
to their physical values, since those are most sensitive to 
quark masses. The scale is needed for that. It is very important
to perform simulations at or close to the right quark mass, or 
on a given desired trajectory running through the physical point, 
in the parameter space given by the 
quark masses. For mass-degenerate u,d quarks, a  
trajectory may be given by fixed 
(physical or bare PCAC) strange 
(and ultimately charm quark) masses or for example
by a fixed trace of the quark mass matrix,  $\tr m_\quark = \rm const$.
In the past several collaborations missed the proper trajectory by more 
than what they would have liked to.

Furthermore, the scale enters where a momentum in a form factor
is fixed or into the overall size of the lattice. 
It is very useful to know the scale beforehand.
All of this calls for care in the selection and computation of the 
scale.


\subsubsection{What is a good scale?}
Unfortunately, the proton mass is not easily determined with good precision 
in lattice QCD computations due to a 
large noise/signal ratio in the proton correlation function,
see \sect{s:momega}.  The chiral scale $f$ is related to
experiments only through the chiral perturbation theory 
expansion. Consequently it is not common to use these
observables to set the scale. 
In general we should search for a quantity which\\  
{\vspace*{-1mm}
\bi \setlength{\itemsep}{0pt}
\item[(P1)]
is computable with a low numerical effort, 
\item[(P2)] 
has a good statistical precision,
\item[(P3)]
has small systematic uncertainties and
\item[(P4)]
has a weak quark mass dependence.
\ei
}
The first two properties are self explanatory, but 
the others require some details. By a good 
systematic precision we mean first of all that the
systematic error in the determination of the numbers
$M_i=a m_i$ for given bare coupling and quark masses is small.
For example such a systematic error may come from 
finite size effects or the contamination by excited states.
A second systematic uncertainty is the discretisation error. 
This is not easily judged. We will comment on it as we go along.
Concerning the quark masses, it is of course useful
to have a scale which depends weakly on them as this 
makes the tuning of the quark masses rather independent 
from the scale and, when the simulations are not at the physical point,
the extrapolation/interpolation to it is easier. Note that the
sensitivity to quark masses also depends on the trajectory 
one chooses to reach the physical point. For example the
mass of the Omega-baryon has a weak dependence on the light
quark masses when the strange quark mass is fixed, but not when
one is on the trajectory  $\tr m_\quark = \rm const$. On such a 
trajectory an appropriate average baryon mass has a weak quark-mass
dependence, see e.g. \cite{Cooke:2013qqa}.

For later use I define a measure 
for the quark mass dependence, 
$$
  \sm_{Q} = \frac{Q|_{\mpi=500~\MeV} - Q|_{\mpi=130~\MeV}}
      {Q|_{\mpi=130~\MeV}}\,,
      \label{e:sm}
$$
where $Q$ labels different scales, e.g. $Q=m_\prot$, and 
we assume that we are on a trajectory with strange and charm (bare, PCAC) mass 
fixed. Selfconsistently, MeV come from the scale 
$Q$. Since there is usually a rather linear dependence of
scales on $\mpi^2$, one has 
$\sm_Q \approx \frac{0.23~\GeV^2}{Q|_{\mpi=130~\MeV}}\frac{\partial Q}{\partial_{\mpi^2}}$. 

The statistical precision is determined both by the 
integrated autocorrelation time and by the variance.
These depend on the update algorithm and on the
chosen estimators, respectively. 
E.g. in the simplest case, using or not-using or stochastically-using
translation invariance are different estimators of a correlation function. 

 %{\cred xxx give a rating according to properties? xxx}

After this preparation, let me come to a discussion of a few 
scales which are in frequent use or newly proposed. I differentiate
between phenomenological scales
and theory scales. The former are related to physical
observables through a minimum amount of theory, while
the latter are constructed to be well computable in lattice
QCD but their values can (at present) only be 
computed in lattice QCD using a phenomenological scale
as an input. Thus a phenomenological scale is
needed in any case. The distinction between one category and the other 
is not sharp. For example I place $r_0$ \cite{Sommer:1993ce} with the 
theory scales, although (vague) phenomenological considerations
led to the prediction $r_0\approx 0.49$fm which is not far 
from our present knowledge.


\begin{table}[t!]
\begin{tabular}{lll lll | llllll } 
    \multicolumn{3}{c}{Wilson, $\Nf=2$} &
    \multicolumn{3}{c}{tmQCD,  $\Nf=2$} & 
    \multicolumn{4}{c}{$\Nf>2$} 
    \\
     $r_0$[fm] &  \multicolumn{2}{c}{from} & 
     $r_0$[fm] & \multicolumn{2}{c}{from} &
     $\Nf$ & $r_0$[fm] & $r_1$[fm] & \multicolumn{2}{c}{from} 
     \\[0.5ex] \hline \\[-1.5ex]
   $    0.503 (  10 ) $& $\fk$ & \cite{Fritzsch:2012wq} &      
   $    0.438 (  14 ) $& $\fk$ & \cite{Blossier:2009bx} &
   2+1 & $ 0.466(4)^a$ & $ 0.313(2)$ & div. & 
   \cite{Davies:2009tsa}
   \\
   $    0.491 (  ~6 )^c $& $\fk$ & \cite{Lottini:2013rfa} &&&&
      2+1 &  & $ 0.321(5)$ & $\Upsilon$ & \cite{Follana:2007uv}
\
   \\
   $    0.485 (  ~9 )^c $& $\fpi$ & \cite{Lottini:2013rfa}&    
   $    0.420 (  20 ) $& $\fpi$ & \cite{Baron:2009wt} %ETMC 08 from
   &
   2+1 & $0.470(4)$ & $0.311(2)$ & $\fpi$ & 
   \cite{Bazavov:2010hj,Bazavov:2011nk}
\\  
    $0.501(15)^b$    & $m_{\prot}$ & \cite{Bali:2012qs} &
   $    0.465 (  16 ) $& $m_{\prot}$ & \cite{Alexandrou:2009qu} 
   &   2+1 & $    0.492 (10)^b $ & & $m_\Omega$ & \cite{Aoki:2009ix} \\ 
   $    0.471 (  17) $& $m_\Omega$ &  &
     &&&
   2+1 & $    0.480 (  11 ) $ & $0.323(9)$ & $m_\Omega$ & \cite{Arthur:2012opa}\\ 
   &&& &&&
   2+1+1 & & 0.311(3) & $\fpi$ & \cite{Dowdall:2013rya} 
\\
\end{tabular}
\footnotesize
$^a$ with $r_0/r_1$ and $r_1/a$ from \cite{Bazavov:2009bb} 
\hspace*{2cm} $^c$ preliminary, at this conference
\\
$^b$ no continuum extrapolation
\caption{Values for $r_0,r_1$.  Column ``from'' shows the phenomenological scale 
used. All results except for those marked with $^b$ have been obtained by a
continuum extrapolation. 
\label{t:r0}}
\end{table}

\begin{table}[b!]
\begin{tabular}{llll} 
    $\nf$ & $\sqrt{t_0}$~[fm] & $w_0$~[fm]  & from\\[0.5ex] \hline \\[-1ex]
   0 &$   0.1638 (  10 ) $&$   0.1670 (  10 ) $& $r_0=0.49\,\fm$ \cite{Bruno:2013gha,Luscher:2010iy} \\ 
   2 &$   0.1539 (  12 ) $&$   0.1760 (  13 ) $& $\fk$ \cite{Bruno:2013gha,Lottini:2013rfa} \\ 
   3 &$   0.153~(7)         $&$   0.179~(6)         $& $m_\prot$\cite{lat13:roger} \\ 
   3 &$   0.1465 (  25 ) $&$   0.1755 (  18 ) $& $m_\Omega$ \cite{Borsanyi:2012zs} \\ 
   4 &$   0.1420 (   8 ) $&$   0.1715 (   9 ) $& $\fpi$ \cite{Dowdall:2013rya}\\ 
    \hline \\[-1ex]
\end{tabular}
\caption{Scales from the gradient flow. Note that these depend in principle
on the phenomenological scale they are determined from. 
\label{t:scales}
}
\end{table}
\clearpage
