# Using the magic encoding
# -*- coding: utf-8 -*-
# FLAG plot for mc_pdg
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\mathrm{w_0}$"	        		# plot title
plotnamestring	= "w0"			# filename for plot
#plotxticks	= ([[0.95, 1.00, 1.05, 1.10]])# locations for x-ticks
plotxticks	= ([[0.170, 0.175,0.180]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [0.160,5.0,"$\\rm N_f=2+1+1$"],
		   [0.160,-0.3,"$\\rm N_f=2+1$"],
                   #[1.07,2.0,"$\\rm N_f=2$"]
		   ]
		   
LABEL=1
xaxisstrings	= [[0.185,-2.1,"fm"]]		# x,y-location for x-strings

		   
xlimits		= [0.1675,0.19]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 0.184					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.1715, 0.0009, 0.0009,0,0, "HPQCD 13A"         ,['s','l','g',0,tpos]],
[0.1714, 0.0012, 0.0015,0,0, "MILC 15" 	,['s','l','g',0,tpos]],
[0.1706, 0.0018, 0.0018, 0.0, 0.0, "ETM 20eqd" 	,['s','l','g',0,tpos]],
[0.1724, 0.00097, 0.00097,0.0003,0.0003, "BMWc 20mff" 	,['s','l','g',0,tpos]],
[0.1711,0.0012, 0.0012, 0.0, 0.0, "Miller 20evg" 	,['s','l','g',0,tpos]],
] 
dat2p1=[
#[1.268, 0.009, 0.009,  0.0,     0.0,     "CLS 16plf" ,['s','l','g',0,tpos]],
[0.1755, 0.00184, 0.00184, 0.00, 0.00, "BMW 12A"  ,['s','l','g',0,tpos]],
[0.1749, 0.0014, 0.0014, 0.00, 0.00, "HotQCD 14"  ,['s','l','g',0,tpos]],
[0.17250, 0.00091, 0.00091,  0.0,   0.0,   "RBC/UKQCD 14B"   ,['s','l','g',0,tpos]],
[0.1808, 0.00246,0.00246,  0.00246,     0.00246,     "QCDSF/UKQCD 15eaa"  ,['s','w','r',0,tpos]],
]
#datapheno=[
#[1.275, 0.035, 0.025, 0, 0, "PDG",['^','k','k',0,tpos]],
#]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.


FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan, 0, 0, 0, 0, "",['^','k','k',0,tpos],1],
[np.nan, 0, 0, 0, 0, "",['^','k','k',0,tpos],1]]
#[0.177, 0.005, 0.005, 0, 0, "",['^','k','k',0,tpos],1],
#[0.1713, 0.002, 0.002, 0, 0, "",['^','k','k',0,tpos],1]]
#[1.275, 0.005, 0.005, 0.0, 0.0, "FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],1],
#[1.280, 0.013, 0.013, 0.0, 0.0, "FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1]
# The follwing list should contain the list names of the previous DATA-blocks
#datasets=[datapheno,dat2p1,dat2p1p1]
datasets=[dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#execfile('FLAGplot.py')
#import FLAGplot.py
#exec(open(FLAGplot.py).read())
exec(compile(open('FLAGplot.py').read(), 'flagplot.py', 'exec'))

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

