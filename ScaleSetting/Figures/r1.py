# Using the magic encoding
# -*- coding: utf-8 -*-
# FLAG plot for mc_pdg
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\mathrm{r_1}$"	        		# plot title
plotnamestring	= "r1"			# filename for plot
#plotxticks	= ([[0.95, 1.00, 1.05, 1.10]])# locations for x-ticks
plotxticks	= ([[0.305,0.315,0.325]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [0.25,6.0,"$\\rm N_f=2+1+1$"],
		   [0.25,1.0,"$\\rm N_f=2+1$"],
                   #[1.07,2.0,"$\\rm N_f=2$"]
		   ]
		   
LABEL=1
xaxisstrings	= [[0.335,-2.1,"fm"]]		# x,y-location for x-strings

		   
xlimits		= [0.3,0.34]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 0.33					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.3209, 0.0026, 0.0026,0,0, "HPQCD 11B"         ,['s','l','g',0,tpos]],
[0.3112, 0.0030, 0.0030,0,0, "HPQCD 13"         ,['s','l','g',0,tpos]],
] 
dat2p1=[
[0.317, 0.007, 0.007,  0.0,   0.0,   "MILC 04"   ,['s','l','g',0,tpos]],
[0.317, 0.0076,0.0076,  0.0,  0.0,     "Aubin 04wf"  ,['s','l','g',0,tpos]],
[0.321, 0.005, 0.005,  0.0,     0.0,     "HPQCD 05B" ,['s','l','g',0,tpos]],
[0.3133, 0.00232, 0.00232,  0.0,     0.0,     "HPQCD 09B" ,['s','l','g',0,tpos]],
[0.3117, 0.0037, 0.0018,  0.0,     0.0,     "MILC 09A" ,['s','l','g',0,tpos]],
[0.3108, 0.0094, 0.0041,  0.0,     0.0,     "MILC 09" ,['s','l','g',0,tpos]],
]
#datapheno=[
#[1.275, 0.035, 0.025, 0, 0, "PDG",['^','k','k',0,tpos]],
#]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.


FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan, 0, 0, 0, 0, "",['^','k','k',0,tpos],1],
[np.nan, 0, 0, 0, 0, "",['^','k','k',0,tpos],1]]
#[0.177, 0.005, 0.005, 0, 0, "",['^','k','k',0,tpos],1],
#[0.1713, 0.002, 0.002, 0, 0, "",['^','k','k',0,tpos],1]]
#[1.275, 0.005, 0.005, 0.0, 0.0, "FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],1],
#[1.280, 0.013, 0.013, 0.0, 0.0, "FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1]
# The follwing list should contain the list names of the previous DATA-blocks
#datasets=[datapheno,dat2p1,dat2p1p1]
datasets=[dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#execfile('FLAGplot.py')
#import FLAGplot.py
#exec(open(FLAGplot.py).read())
exec(compile(open('FLAGplot.py').read(), 'flagplot.py', 'exec'))

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

