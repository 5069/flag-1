# FLAG plot for RFKpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print("numpy library not found. If you want the FLAG-logo to be added to the")
 print("plot, please install this library")
 
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm f_{B_s}\; [MeV]$"			# plot title
plotnamestring	= "fBs"			# filename for plot
plotxticks	= ([[ 210,  230,  250, 270, 290]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
                   [+195,24.4,"$\\rm N_f=2+1+1$"],
		   [+195,14.3,"$\\rm N_f=2+1$"],
		   [+195,3.5,"$\\rm N_f=2$"]
		    ]
xlimits		= [200,362]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 302					# x-position for the data labels


# column        item
# 0             central value
# 1             neg. error 1
# 2             pos. error 1
# 3             neg. error 2
# 4             pos. error 2
# 5             Collaboration string
# 6             this column contains layout parameters for the plot-symbol and
#               and the collaboration text:
#               column  item
#               0       marker style (see bottom of this file for a full list)
#               1       marker face color r=red, b=blue, k=black, w=white
#                                         g=green
#               2       marker color (errorbar and frame), color coding as
#                       for face color
#               3       color intensity 0=full, 1=soso, 2=bleak
#               4       position of the collaboration string on the x-axis
dat2p1p1=[
[224,   5,5,5,5,         "HPQCD 13"            ,['s','g','g',0,tpos]],
[235,   9,9,9,9,         "ETM 13E"             ,['s','l','g',0,tpos]],
[229,   4,4,5,5,         "ETM 16B"           ,['s','g','g',0,tpos]],
[236,   7,7,7,7,         "HPQCD 17A"         ,['s','g','g',0,tpos]],
[230.7, 0.8,0.8,1.2,1.2,  "FNAL/MILC 17"     ,['s','g','g',0,tpos]],
]
dat2p1=[
[231,   5,5,15,15,  "HPQCD 09"        ,['s','l','g',0,tpos]],
[242,   3.4,3.4,10,10,  "FNAL/MILC 11"        ,['s','g','g',0,tpos]],
[225,   3,3,4,4,  "HPQCD 11A"            ,['s','g','g',0,tpos]],
[228,    1.4,1.4,10,10,   "HPQCD 12"      ,['s','g','g',0,tpos]],
[233,   5,5,5,5,  "RBC/UKQCD 13A (stat. err. only)"  ,['s','l','g',0,tpos]],
[263.5,    4.8,4.8,37,37,   "RBC/UKQCD 14A"      ,['s','g','g',0,tpos]],
[235.4,    5.2,5.2,12.2,12.2,   "RBC/UKQCD 14"      ,['s','g','g',0,tpos]],
] 
dat2=[                        
[235,   12,12,12,12,  "ETM 09D"                 ,['s','l','g',0,tpos]],
[232,   10,10,10,10,  "ETM 11A"          ,['s','l','g',0,tpos]],
[234,   6,6,6,6,  "ETM 12B"              ,['s','l','g',0,tpos]] ,
[219,   12,12,12,12,  "ALPHA 12A"                ,['s','l','g',0,tpos]]  ,
[228,   8,8,8,8,  "ETM 13B, 13C"          ,['s','g','g',0,tpos]]  ,
[224,   13,13,13,13,  "ALPHA 13"          ,['s','l','g',0,tpos]]  ,
[224,   14,14,14,14,  "ALPHA 14"          ,['s','g','g',0,tpos]]  ,
[215,	10,10,5,3,	"Balasubramamian 19net"	,['s','g','g',0,tpos]]
]


# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
#[227,7,7,7,7,"our average for $\\rm N_f=2$"  ,['s','k','k',0,tpos],1],
[225.3,6.6,6.6,6.6,6.6,"our average for $\\rm N_f=2$"  ,['s','k','k',0,tpos],1], #INCLUDE Balasubramamian 19net
[227.5,4.5,4.5,4.5,4.5,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[230.3,1.3,1.3,1.3,1.3,"our average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1],
]

# The follwing list should contain the list names of the previous DATA-blocks
#datasets=[dat2,dat2p1,dat2p1p1]
datasets=[dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
# execfile('FLAGplot.py')
#exec(open('FLAGplot.py').read())
exec(open('FLAGplotYA.py').read())

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

