# FLAG plot for fB sqrt(BB) for B and Bs
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= " $\\rm f_{B_d}\\sqrt{\\hat{B}_{B_d}}$"		# plot title
plotnamestring	= "fBsqrtBB2"			# filename for plot
plotxticks	= ([[180,220,260]])# locations for x-ticks 
#nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

FS = 12


yaxisstrings	= [				# x,y-location for y-strings
		   [142, 9.8, "$\\rm N_f=2+1+1$"],
		   [142, 4.5, "$\\rm N_f=2+1$"],
		   [142,-0.5, "$\\rm N_f=2$"]
		    ]
		    
LABEL=1

xaxisstrings	= [				# x,y-location for x-strings
#		   [+300.0,-1.78,"MeV"] 	 		    
                   ]	

		    
#xlimits		= [180,280]			# plot's xlimits
#xlimits		= [175,295]			# plot's xlimits
xlimits		= [170,340]			# plot's xlimits
logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos   		= 290				# x-position for the data labels 
					        # move data labels of l.h.s side
						# plot to nirvana
						
tpos2		= 1000				# x-position for the data labels
						# here a reasonable choice
REV    		= 1				# reverse order of data points
#PRELIM 		= 1				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
SHIFT=-.1					# one figure
REMOVEWHITESPACE=0
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

################################################################################
# data for l.h.s. scatter plot
################################################################################
#-- Nf=4 --
datBdNf4=[
[210.6, 5.5, 5.5, 5.5, 5.5, "HPQCD 19bea", ['s','g','g',0,tpos]]
]
#-- Nf=3 --
datBdNf3=[
[227.7, 9.5, 9.5, 9.5, 9.5, "FNAL/MILC 16", ['s','g','g',0,tpos]],
[240, 36, 36, 15, 15, "RBC/UKQCD 14A", ['s','g','g',0,tpos]],
[250, 23, 23, 23, 23, "FNAL/MILC 11 A", ['s','l','g',0,tpos]],
[216,  9,  9, 15, 15, "HPQCD 09",       ['s','g','g',0,tpos]],
[  0,  0,  0,  0,  0, "HPQCD 06A",       ['s','r','r',0,tpos]]
]
#-- Nf=2 --
datBdNf2=[
[216, 6, 6, 10, 10, "ETM 13B", ['s','g','g',0,tpos]]
]
 
################################################################################
# data for r.h.s. scatter plot
################################################################################
datBsNf4=[
[256.1, 5.7, 5.7, 5.7, 5.7, "HPQCD 19bea", ['s','g','g',0,tpos2]]
]
#-- Nf=3 --
datBsNf3=[
[274.6, 8.4, 8.4, 8.4, 8.4, "FNAL/MILC 16", ['s','g','g',0,tpos2]],
[290, 41, 41,  9,  9, "RBC/UKQCD 14A", ['s','g','g',0,tpos2]],
[291, 18, 18, 18, 18, "FNAL/MILC 11 A", ['s','l','g',0,tpos2]],
[266,  6,  6, 18, 18, "HPQCD 09",       ['s','g','g',0,tpos2]],
[281, 21, 21, 21, 21, "HPQCD 06A",       ['s','w','r',0,tpos2]]
]

#-- Nf=2 --
datBsNf2=[
[262, 6, 6, 10, 10, "ETM 13B", ['s','g','g',0,tpos2]]
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.



# first define FLAGaverage for l.h.s. plot, the one for the r.h.s. one follows below
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0], 
 [216,10,10,0.000,0.000,"FLAG average $\\rm N_f=2$",['s','k','k',0,tpos],0],
 [225, 9, 9,0.000,0.000,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
 [210.6,5.5,5.5,5.5,5.5,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0], 
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datBdNf2,datBdNf3,datBdNf4]
#datasets=[datBdNf3]

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 


# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
# execfile('FLAGplot.py')
exec(open("./FLAGplot.py").read())

logo='none'
# now some definitions for the r.h.s. plot
plotnamestring	= "fBsqrtBB2"			# filename for plot

titlestring	= "$\\rm f_{B_s}\\sqrt{\\hat{B}_{B_s}}$"		# plot title
xlimits		= [170,340]			# plot's xlimits
plotxticks	= ([[220,260,300]])# locations for x-ticks

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
# [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos2],0], 
 [262,10,10,0.000,0.000," for $\\rm N_f=2$",['s','k','k',0,tpos2],0],
 [274, 8, 8,0.000,0.000,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos2],0],
 [256.1,5.7,5.7,5.7,5.7,"for $\\rm N_f=2+1+1$",['s','k','k',0,tpos2],0], 
]
yaxisstrings	= [				# x,y-location for y-strings
#		   [+0,2.0,"$\\rm N_f=2+1$"]
		    ]

xaxisstrings	= [				# x,y-location for x-strings
		   [+325.0,-2.3,"MeV"] 	 
		    ]
		    
# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datBsNf2,datBsNf3,datBsNf4]
#datasets=[datBsNf3]

# do the r.h.s. plot
ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
# execfile('FLAGplot.py')
exec(open("./FLAGplot.py").read())

################################################################################
# We are done
################################################################################


# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

