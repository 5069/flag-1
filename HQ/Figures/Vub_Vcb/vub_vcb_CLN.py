import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as implt
import matplotlib.patches as mpatches
import matplotlib.patches as polygon
from matplotlib.patches import Ellipse 
from numpy import *
import os.path
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
PRELIM=0
flagyear=2019

#checks
try: 
 import numpy
 blogo=1
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
 blogo=0
try: 
 from PIL import Image
 blogo=1
except ImportError:
 print "Image library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
 blogo=0

#From FLAG import *
blogo=0
plt.rc('font', family='sans-serif')

#axes limits
xlims = 35,45
ylims = 2.7,4.75

#Read from File. Note that each line must be a float and there cannot be empty lines.
data = np.loadtxt("vub_vcb_CLN.txt")
Incl=data[0],data[1],data[2],data[3]
Excl=data[4],data[5],data[6],data[7]
ellipse1sigma=data[8],data[9],data[10]
ellipse2sigma=data[11],data[12],data[13]
B2pi = data[14],data[15]
B2taunu = data[16],data[17]
B2D = data[18],data[19]
B2DS = data[20],data[21]
Lambdab = data[22],data[23]

#Create figure
fig=plt.figure(figsize=[6.4,6.4])
ax=fig.add_subplot(111)
ax.set_aspect(aspect=(xlims[1]-xlims[0])/(ylims[1]-ylims[0]))
plt.subplots_adjust(left=0.18, right=0.97, top=0.94, bottom=0.1)

tsize=18
x=np.linspace(xlims[0],xlims[1],10)
y=np.linspace(ylims[0],ylims[1],10)

#Band for B2taunu
ax.fill_between(x, B2taunu[0],B2taunu[1],facecolor=(1,0.7778,0.7778),alpha=1)
plt.text(xlims[0]+0.4,(B2taunu[0]+B2taunu[1])/2,"$B\\to\\tau\\nu$",verticalalignment='center',fontsize=tsize)

#Band for Lambdab
ax.fill_between(x, x*Lambdab[0],  x*Lambdab[1], alpha=1, color=(0.8519,0.7037,0.8519))
plt.text(36.4,36.4*(Lambdab[0] + Lambdab[1])/2,"$\\frac{\\Lambda_b \\to p \\ell\\nu}{\\Lambda_b \\to \\Lambda_c \\ell\\nu}$",verticalalignment='center',horizontalalignment='center',rotation=math.atan((xlims[1]-xlims[0])/(ylims[1]-ylims[0])*(Lambdab[0]+Lambdab[1])/2)*180/math.pi,fontsize=tsize*1.5)

#Band for B2pi
ax.fill_between(x, B2pi[0],B2pi[1],facecolor=(0.12, 0.5, 0.76),alpha=1)
plt.text(xlims[0]+0.4,(B2pi[0]+B2pi[1])/2,"$B\\to\\pi\\ell\\nu$",verticalalignment='center',fontsize=tsize)

#Band for B2D
ax.axvspan(B2D[0],B2D[1], alpha=1, color=(1,0.6666,0.3333))
plt.text((B2D[0]+B2D[1])/2,Incl[2]+0.05,"$B\\to D$",verticalalignment='center',horizontalalignment='center',fontsize=tsize)

#Band for B2Dstar
ax.axvspan(B2DS[0],B2DS[1], alpha=1, color=(0.95,0.95,0))
plt.text((B2DS[0]+B2DS[1])/2,Incl[2]-0.1,"$(B\\to D^*)_{\\rm CLN}$",verticalalignment='center',horizontalalignment='center',fontsize=tsize)

#line for B2taunu
ax.plot(x, B2taunu[0]*x/x, x, B2taunu[1]*x/x, color=(1,0.7778,0.7778))
#lines for Lambdab
ax.plot(x, x*Lambdab[0], x, x*Lambdab[1], color=(0.8519,0.7037,0.8519))
#lines for B2pi
ax.plot(x, B2pi[0]*x/x, x, B2pi[1]*x/x, color=(0.12, 0.5, 0.76))
#lines for B2D
ax.plot(B2D[0]*y/y, y,B2D[1]*y/y, y, color=(1,0.6666,0.3333))

#Inclusive with error bar
plt.errorbar(Incl[0],Incl[2],xerr=Incl[1],yerr=Incl[3],marker='o',ecolor='blue',markerfacecolor='blue',markeredgecolor='blue',elinewidth=2,capsize=4,zorder = 300)
plt.text(Incl[0]+0.2,Incl[2]+0.05,"inclusive",fontsize=tsize)

#Exclusive with error bar
plt.errorbar(Excl[0],Excl[2],xerr=Excl[1],yerr=Excl[3],marker='o',ecolor='black',markerfacecolor='black',markeredgecolor='black',elinewidth=2,capsize=4,zorder = 300)

#fit contour (1 sigma)
e = Ellipse(xy=[Excl[0],Excl[2]], width= ellipse1sigma[0], height= ellipse1sigma[1], angle=ellipse1sigma[2], edgecolor='black', linewidth=1, facecolor='none',zorder = 300) #construct the ellipse
ax.add_artist(e) #add the ellipse

#fit contour (2 sigma)
e = Ellipse(xy=[Excl[0],Excl[2]], width= ellipse2sigma[0], height= ellipse2sigma[1], angle=ellipse2sigma[2], edgecolor='black', linewidth=1, linestyle=(0,(8,4)),facecolor='none',zorder = 300) #construct the ellipse
ax.add_artist(e) #add the ellipse

#Axes labels
plt.xlim(xlims)
plt.ylim(ylims)
plt.xlabel("$|V_{cb}|\\times 10^{3}$",fontsize=tsize)
plt.ylabel("$|V_{ub}|\\times 10^{3}$",fontsize=tsize)
plt.tick_params(axis='both', which='major', labelsize=tsize)
plt.minorticks_on()
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
plt.yticks([2.75,3,3.25,3.5,3.75,4,4.25,4.5,4.75], ['',3,'',3.5,'',4,'',4.5])

# insert logo if specified #####################################################
logo = 'upper left'
if 2==2:
 if (logo)>0:
  if not os.path.isfile("FLAG_Logo.png"):
   print "FLAG_Logo.png is missing! Use mkFLAGlogo in FLAGplot.py to generate"
   print "it"
   print ""
  plotbox=ax.bbox.extents
  ax = plt.axis()
  if logo=='upper right':
   xpos=0.777
   ypos=0.855
   Pxpos=.0
   Pypos=+250
  elif logo=='upper left':
   xpos=0.17
   ypos=0.84
   Pxpos=45.10
   Pypos=+250
  elif logo=='lower left':
   xpos=0.12
   ypos=0.015
   Pxpos=45.10
   Pypos=-50
  elif logo=='lower right':
   xpos=0.777
   ypos=0.015
   Pxpos=.0
   Pypos=-50
  else:
   print "FLAGplot.py: There seems to be sth. wrong with the key word <<logo>>"
   exit()
  # draw the logo
  axicon = fig.add_axes([xpos,ypos,.20,.20],frameon=False,xticks=[],yticks=[])
  im  = Image.open('FLAG_Logo.png')
  im2 = numpy.array(im).astype(numpy.float) / 255
  axicon.imshow(im2)

if PRELIM == 1:
 plt.annotate('PRELIMINARY',rotation=30,fontsize=45,color=[.2,.2,.2],
		 xy=(1.25,-4.75), horizontalalignment='center',
		 verticalalignment='center',
		 xycoords='axes fraction',alpha=.5,zorder=0)

if PRELIM==2:
  plt.text(Pxpos,Pypos,'PRELIMINARY',rotation=0,fontsize=10,color=[.2,.2,.2])


plt.savefig("vub_vcb_CLN.eps",format="eps",dpi=600)
plt.savefig("vub_vcb_CLN.pdf",format="pdf",dpi=600)
plt.savefig("vub_vcb_CLN.png",format="png",dpi=400)
#plt.show()

