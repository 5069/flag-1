(*******************)
(* External Files  *)
(*******************)

FlagLogoLocation = "Flag_Logo.pdf";

(* 
   I need MaTeX to include actual LaTeX in the Mathematica plots.
   If it is not installed, one needs to download the paclet from
   https://github.com/szhorvat/MaTeX/releases/download/v1.7.4/MaTeX-1.7.4.paclet  
   and install it with
   Needs["PacletManager`"];
   PacletInstall["MaTeX-1.7.4.paclet"];
*)

(**********************************************)
(* Inputs (Both Vub and Vcb in units of 10^-3 *)
(**********************************************)

(******************)
(* Vub from B->pi *)
(******************)

(* From Table 48 *)
vubsl = {3.73, 0.14}; 

(* Lattice contribution to the error. This is assumed be identical to the relative uncertainty on a_0^+ in the lattice-only fit in Table 39 *)
DvubslLAT = 0.013/0.404 vubsl[[1]]; 

(* Experimental contribution to the error. *)
DvubslEXP = Sqrt[vubsl[[2]]^2 - DvubslLAT^2]; 

(*****************)
(* Vcb from B->D *)
(*****************)

(* From Table 49 *) 
vcbD = {40.1, 1.0}; 

(* Lattice contribution to the error. This is assumed be identical to the relative uncertainty on a_0^+ in the lattice-only fit in Table 45 *)
DvcbDLAT = 0.014/0.909 vcbD[[1]]; 

(* Experimental contribution to the error. *)
DvcbDEXP = Sqrt[vcbD[[2]]^2 - DvcbDLAT^2];

(******************)
(* Vcb from B->D* *)
(******************)

(* Using CLN experimental fits (from Eq. (272)) *)
DvcbDsLAT[CLN] = 0.52;
DvcbDsEXP[CLN] = 0.47
vcbDs[CLN] = {39.12, Sqrt[DvcbDsLAT[CLN]^2 + DvcbDsEXP[CLN]^2]};

(* Using BGL experimental fits (from Eq. (273)) *)
DvcbDsLAT[BGL] = 0.57;
DvcbDsEXP[BGL] = 0.71;
vcbDs[BGL] = {42.55, Sqrt[DvcbDsLAT[BGL]^2 + DvcbDsEXP[BGL]^2]};

(***********************************************)
(* Covariance matrix for vubsl, vcbD and vcbDs *)
(***********************************************)

(* 
Estimate of the correlation matrix between vubsl and vcbD.
It originates from the use of the same MILC configurations.
We assume that the lattice-stat error in the determination of vubsl (dominated by FNAL), vcbD (FNAL=HPQCD), vcbDs (FNAL=HPQCD) are 100% correlated.
B->pi: lattice is dominated by FNAL/MILC for which stat/tot = 3.1/3.4 = 91%
B->D: lattice is obtained by FNAL/MILC and HPQCD which already have 100% stat uncertainty.  (stat/tot)_FNAL = 58% and (stat/tot)_HPQCD=49%. We take stat/tot = (58+49)/2 = 54%
B->D*: lattice is obtained by FNAL/MILC and HPQCD which already have 100% stat uncertainty. (stat/tot)_FNAL = 10/24 = 42% and (stat/tot)_HPQCD=12/28 = 42%. We take stat/tot = 42%
*)

Svubsl = 0.91;
SvcbD = 0.54;
SvcbDs = 0.42;

cov[CLN] = {
  {vubsl[[2]]^2, DvubslLAT Svubsl DvcbDLAT SvcbD, 
   DvubslLAT Svubsl DvcbDsLAT[CLN] SvcbDs},
  {DvubslLAT Svubsl DvcbDLAT SvcbD, vcbD[[2]]^2, 
   DvcbDLAT SvcbD DvcbDsLAT[CLN] SvcbDs},
  {DvubslLAT Svubsl DvcbDsLAT[CLN] SvcbDs, 
   DvcbDLAT SvcbD DvcbDsLAT[CLN] SvcbDs, vcbDs[CLN][[2]]^2}
  };

cov[BGL] = {
  {vubsl[[2]]^2, DvubslLAT Svubsl DvcbDLAT SvcbD, 
   DvubslLAT Svubsl DvcbDsLAT[BGL] SvcbDs},
  {DvubslLAT Svubsl DvcbDLAT SvcbD, vcbD[[2]]^2, 
   DvcbDLAT SvcbD DvcbDsLAT[BGL] SvcbDs},
  {DvubslLAT Svubsl DvcbDsLAT[BGL] SvcbDs, 
   DvcbDLAT SvcbD DvcbDsLAT[BGL] SvcbDs, vcbDs[BGL][[2]]^2}
  };

(**********************)
(* Vub from B->tau nu *)
(**********************)

(* From the 2+1+1 result in Eq. (268) *)
vubbtn = {4.05,0.64};

(*************************************)
(* Vub and Vcb from inclusive decays *)
(*************************************)

(* From 1606.06174 *)
vcbincl = {42.00, 0.65};

(* From 1604.07598 *)
vubincl = {4.52, Sqrt[0.15^2 + ((0.12 + 0.14)/2)^2]};

(********************************)
(* Vub/Vcb from baryonic decays *)
(********************************)

(* From 1504.01568 (LHCb) and 1503.01421 (Detmold et al) *)
vubovcb = {0.083, 0.006};




(************************************************)
(* Various user-defined functions and variables *)
(************************************************)

FlagLogo = Import[FlagLogoLocation];

<< MaTeX`;

<< ErrorBarPlots`;

cleanContourPlot[cp_] := 
 Module[{points, groups, regions, lines}, 
  groups = Cases[cp, {style__, g_GraphicsGroup} :> {{style}, g}, 
    Infinity];
  points = 
   First@Cases[cp, GraphicsComplex[pts_, ___] :> pts, Infinity];
  regions = 
   Table[Module[{group, style, polys, edges, cover, 
      graph}, {style, group} = g;
     polys = Join @@ Cases[group, Polygon[pt_, ___] :> pt, Infinity];
     edges = Join @@ (Partition[#, 2, 1, 1] & /@ polys);
     cover = Cases[Tally[Sort /@ edges], {e_, 1} :> e];
     graph = Graph[UndirectedEdge @@@ cover];
     {Sequence @@ style, 
      FilledCurve[
       List /@ Line /@ 
         First /@ 
          Map[First, 
           FindEulerianCycle /@ (Subgraph[graph, #] &) /@ 
             ConnectedComponents[graph], {3}]]}], {g, groups}];
  lines = Cases[cp, _Tooltip, Infinity];
  Graphics[GraphicsComplex[points, {regions, lines}], 
   Sequence @@ Options[cp]]];

(* Weighted Averages *)

WAnaive[inputs__] := 
 Module[{a, label, cv, err, err2, norm, weight, centralvalue, 
   errorNAIVE, CorrelatedErrors, cmatrix, errorCORRELATED, INVcmatrix,
    errorCORRELATED2, centralvalueCORRELATED2, chisquarefactor, 
   rescalingfactor},
  a = {inputs};
  label = Transpose[a][[1]];
  cv = Transpose[a][[2]];
  err = Transpose[a][[3]];
  err2 = Apply[Plus, (err /. m[_] -> 0)^2, 2];
  norm = Apply[Plus, 1/err2];
  weight = 1/err2/norm;
  centralvalue = cv.weight;
  errorNAIVE = 1/Sqrt[norm];
  Return[{centralvalue, errorNAIVE}]];

PDGfactor[inputs__] := 
 Module[{a, label, cv, err, err2, norm, weight, centralvalue, 
   errorNAIVE, CorrelatedErrors, cmatrix, errorCORRELATED, INVcmatrix,
    errorCORRELATED2, centralvalueCORRELATED2, chisquarefactor, 
   rescalingfactor},
  a = {inputs};
  label = Transpose[a][[1]];
  cv = Transpose[a][[2]];
  err = Transpose[a][[3]];
  err2 = Apply[Plus, (err /. m[_] -> 0)^2, 2];
  norm = Apply[Plus, 1/err2];
  weight = 1/err2/norm;
  centralvalue = cv.weight;
  chisquarefactor = 
   Sqrt[Sum[(-centralvalue + cv[[i]])^2/err2[[i]], {i, 
       Length[a]}]/(Length[a] - 1)];
  rescalingfactor = If[chisquarefactor > 1, chisquarefactor, 1];
  Return[rescalingfactor]
  ];

WAnaivePDG[inputs__] := WAnaive[inputs] {1, PDGfactor[inputs]};

(*
Input format for WAcorrelated is :

mA = {"labelA", xA, {Sqrt[errA1^2 + errA2^2 + errA3^2], Sqrt[errA1^2 + errA2^2] m["labelB"], errA3 m["labelC"]}}
mA = {"labelA", xA, {Sqrt[errA1^2 + errA2^2 + errA3^2] + Sqrt[errA1^2 + errA2^2] m["labelB"] + errA3 m["labelC"]}}
mA = {"labelA", xA, {Sqrt[errA1^2 + errA2^2 + errA3^2], errA1 m["labelB"], errA2 m["labelB"], errA3 m["labelC"]}}
mA = {"labelA", xA, {errA1, errA2, errA3, errA1 m["labelB"], errA2 m["labelB"], errA3 m["labelC"]}}
mA = {"labelA", xA, {Sqrt[ errA2^2 + errA3^2], errA1 (1 + m["labelB"]), errA2 m["labelB"], errA3 m["labelC"]}}
*)

WAcorrelated[inputs__] := 
 Module[{a, label, cv, err, err2, norm, weight, centralvalue, errorNAIVE, 
   CorrelatedErrors, cmatrix, errorCORRELATED, INVcmatrix, errorCORRELATED2, 
   centralvalueCORRELATED2, chisquarefactor, rescalingfactor},
  a = {inputs};
  label = Transpose[a][[1]];(* list of labels *)
  cv = Transpose[a][[2]]; (* 
  list of central values Subscript[x, i] *)
  err = Transpose[a][[3]];   
  If[Apply[And, 
    MemberQ[m /@ label, #] & /@ Variables[Apply[Plus, Flatten[err]]]], 0, 
   Print["You have mispelled one of the labels!"]];
  err2 = Apply[Plus, (err /. m[_] -> 0)^2, 2]; 
    norm = Apply[Plus, 1/err2]; 
  weight = 1/err2/norm;
  centralvalue = cv.weight;
  CorrelatedErrors = 
   Table[Sqrt[Apply[Plus, Coefficient[err, m[label[[i]]]]^2, 2]], {i, 
     Length[a]}];
  cmatrix = 
   DiagonalMatrix[err2] + CorrelatedErrors Transpose[CorrelatedErrors]; (* 
  Subscript[C, ij] *)
  errorCORRELATED = Sqrt[weight.cmatrix.weight]; 
  Return[{centralvalue, errorCORRELATED}]];

WAcorrelatedPDG[inputs__] := WAcorrelated[inputs] {1, PDGfactor[inputs]};

RatioError[a_, b_] := 
 a[[1]]/b[[1]] {1, Sqrt[a[[2]]^2/a[[1]]^2 + b[[2]]^2/b[[1]]^2]};

up2D = {2.295, 6.18, 11.829, 19.33};

up1D = {1, 4, 9, 16};

up = up1D;

pvalue[n_, x_] = 1. - CDF[ChiSquareDistribution[n], n x];

Nsigma[n_, x_] = Chop[Sqrt[2] InverseErf[1 - pvalue[n, x]]];

cverr[a_] := {(a[[1]] + a[[2]])/2, Abs[(a[[2]] - a[[1]])/2]};




(***************************)
(* Generation of the plots *)
(***************************)

(*******************************************************************************************************)
(* Generation of the contours for Vub from B->pi, Vub from B->tau nu, Vcb from B->D and Vcb from B->D* *)
(*******************************************************************************************************)

doexcl:=Do[
Print["Parametrization: ",ToString[FFparm]];
chi[vcb_,vub_]={vub-vubsl[[1]],vcb-vcbD[[1]],vcb-vcbDs[FFparm][[1]]}.Inverse[cov[FFparm]].{vub-vubsl[[1]],vcb-vcbD[[1]],vcb-vcbDs[FFparm][[1]]}+(vub-vubbtn[[1]])^2/vubbtn[[2]]^2;
sol=FindMinimum[chi[vcb,vub],{vcb,40},{vub,4}];
Print["pvalue(exlcusive) = " , pvalue[4-2,sol[[1]]/(4-2)]];
Print["Nsigma = ",Nsigma[4-2,sol[[1]]/(4-2)]];
Print["PDG factor = ",Max[{Sqrt[sol[[1]]],1}]];
cv={vcb,vub}/.sol[[2]];
contEXCL[FFparm]=Show[
ContourPlot[chi[vcb,vub],{vcb,37,45},{vub,3,4.7},ContourShading->False,Contours->(sol[[1]]+up2D[[{1,2}]]),ContourStyle->{{Thickness[0.004],Black},{Thickness[0.004],Dashed}}],
Graphics[{PointSize[0.022],Point[cv]}]];
chi1[vub_?NumericQ]:=FindMinimum[chi[vcb,vub],{vcb,40}][[1]];
solvub={
x/.FindRoot[chi1[x]-(sol[[1]]+1),{x,(vub/.sol[[2]]) 1.25}][[1]],
x/.FindRoot[chi1[x]-(sol[[1]]+1),{x,(vub/.sol[[2]]) 0.75}][[1]]
}//cverr;
Print["Vub(exclusive) = "<>ToString[SetAccuracy[Apply[PlusMinus,solvub],3]]];
chi1[vcb_?NumericQ]:=FindMinimum[chi[vcb,vub],{vub,4}][[1]];
solvcb={
x/.FindRoot[chi1[x]-(sol[[1]]+1),{x,(vcb/.sol[[2]]) 1.25}][[1]],
x/.FindRoot[chi1[x]-(sol[[1]]+1),{x,(vcb/.sol[[2]]) 0.75}][[1]]
}//cverr;
Print["Vcb(exclusive) = "<>ToString[SetAccuracy[Apply[PlusMinus,solvcb],3]]];
EXCL[FFparm]=Show[ErrorListPlot[{{{solvcb[[1]],solvub[[1]]},ErrorBar[solvcb[[2]],solvub[[2]]]}},PlotStyle->{Thickness[0.004],Black},PlotRange->{{35,45},{2.8,4.7}}]];
chi[vcb_,vub_]={vub-vubsl[[1]],vcb-vcbD[[1]],vcb-vcbDs[FFparm][[1]]}.Inverse[cov[FFparm]].{vub-vubsl[[1]],vcb-vcbD[[1]],vcb-vcbDs[FFparm][[1]]}+(vub-vubincl[[1]])^2/vubincl[[2]]^2+(vcb-vcbincl[[1]])^2/vcbincl[[2]]^2;
sol=FindMinimum[chi[vub,vcb],{vub,4},{vcb,40}];
Print["pvalue(total) = " , pvalue[6-2,sol[[1]]/(6-2)]];
Print["Nsigma = ",Nsigma[6-2,sol[[1]]/(6-2)]]
Print[" "];];

FFparm = CLN;
doexcl;

FFparm = BGL;
doexcl;

(********************************************************************************************************************************)
(* Individual bands for the various inputs, including Vub/Vcb from baryonic decays which, as of now, is not included in the fit *)
(********************************************************************************************************************************)

colvubsl=RGBColor[0.12,0.5,0.76];
chi[vcb_,vub_]=(vub-vubsl[[1]])^2/vubsl[[2]]^2;
sol=FindMinimum[chi[vub,vcb],{vub,4},{vcb,40}];
contSL=cleanContourPlot[RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.7,4.8},BoundaryStyle->None,PlotStyle->colvubsl,PlotRange->{{35,45},{2.8,4.7}}]];
contSL1=RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.7,4.8},BoundaryStyle->{colvubsl,Thickness[0.004]},PlotStyle->None,PlotRange->{{35,45},{2.8,4.7}}];

colbtn=Lighter[Lighter[Pink]];
chi[vcb_,vub_]=(vub-vubbtn[[1]])^2/vubbtn[[2]]^2;
sol=FindMinimum[chi[vub,vcb],{vub,4},{vcb,40}];
contBTN=cleanContourPlot[RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.8,4.7},BoundaryStyle->None,PlotStyle->colbtn,PlotRange->{{35,45},{2.8,4.7}}]];
contBTN1=RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.8,4.8},BoundaryStyle->{colbtn,Thickness[0.004]},PlotStyle->None,PlotRange->{{35,45},{2.8,4.7}}];

colvcbd=Lighter[Orange];
chi[vcb_,vub_]=(vcb-vcbD[[1]])^2/vcbD[[2]]^2;
sol=FindMinimum[chi[vub,vcb],{vub,4},{vcb,40}];
contD=cleanContourPlot[RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,46},{vub,2.8,4.7},BoundaryStyle->None,PlotStyle->colvcbd,PlotRange->{{35,45},{2.8,4.7}}]];
contD1=RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.8,4.7},BoundaryStyle->{colvcbd,Thickness[0.004]},PlotStyle->None,PlotRange->{{35,45},{2.8,4.7}}];

colvcbds=RGBColor[0.95,0.95,0];
chi[vcb_,vub_]=(vcb-vcbDs[CLN][[1]])^2/vcbDs[CLN][[2]]^2;
sol=FindMinimum[chi[vub,vcb],{vub,4},{vcb,40}];
contDs[CLN]=cleanContourPlot[RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,34,46},{vub,2.8,4.7},BoundaryStyle->None,PlotStyle->colvcbds,PlotRange->{{35,45},{2.8,4.7}}]];
contDs1[CLN]=RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.8,4.7},BoundaryStyle->{colvcbds,Thickness[0.004]},PlotStyle->None,PlotRange->{{35,45},{2.8,4.7}}];

colvcbds=RGBColor[0.95,0.95,0];
chi[vcb_,vub_]=(vcb-vcbDs[BGL][[1]])^2/vcbDs[BGL][[2]]^2;
sol=FindMinimum[chi[vub,vcb],{vub,4},{vcb,40}];
contDs[BGL]=cleanContourPlot[RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,34,46},{vub,2.8,4.7},BoundaryStyle->None,PlotStyle->colvcbds,PlotRange->{{35,45},{2.8,4.7}}]];
contDs1[BGL]=RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.8,4.7},BoundaryStyle->{colvcbds,Thickness[0.004]},PlotStyle->None,PlotRange->{{35,45},{2.8,4.7}}];

colvubovcb=Lighter[Lighter[Lighter[Purple]]];
chi[vcb_,vub_]=(vub/vcb-vubovcb[[1]])^2/vubovcb[[2]]^2;
sol=FindMinimum[chi[vub,vcb],{vub,4},{vcb,40}];
contRATIO=cleanContourPlot[RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.8,4.8},BoundaryStyle->None,PlotStyle->colvubovcb,PlotRange->{{35,45},{2.8,4.7}}]];
contRATIO1=RegionPlot[chi[vcb,vub]<=sol[[1]]+up[[1]],{vcb,35,45},{vub,2.8,4.8},BoundaryStyle->{colvubovcb,Thickness[0.004]},PlotStyle->None,PlotRange->{{35,45},{2.8,4.7}}];

INCL=Show[ErrorListPlot[{{{vcbincl[[1]],vubincl[[1]]},ErrorBar[vcbincl[[2]],vubincl[[2]]]}},PlotStyle->{Thickness[0.004],Blue}],Graphics[{PointSize[0.022],Blue,Point[{vcbincl[[1]],vubincl[[1]]}]}],PlotRange->{{35,45},{2.8,4.7}}];

(********************)
(* Plotting routine *)
(********************)

mag=1.3;

plo:=Show[
contBTN,contRATIO,contSL,contD,contDs[FFparm],contRATIO1,contDs1[FFparm],contD1,contBTN1,contSL1,contEXCL[FFparm],INCL,EXCL[FFparm],
Graphics[Text[MaTeX["B \\to \\tau \\nu",Magnification->mag],{37,4.2}]],
Graphics[Text[MaTeX["B \\to \\pi \\ell \\nu",Magnification->mag],{37,3.72}]],
Graphics[Text[MaTeX["B \\to D",Magnification->mag],{40.1,4.2}]],
Graphics[Text[MaTeX["\\frac{\\Lambda_b \\to p \\ell\\nu}{\\Lambda_b \\to \\Lambda_c \\ell\\nu}",Magnification->mag] ,{37,3.07},Automatic,{1,0.43}]],
Graphics[Text[MaTeX["\\rm inclusive",Magnification->mag],{43.1,4.6}]],
FrameStyle->Thin,FrameTicks->{{{36,MaTeX["36",Magnification->mag]},{38,MaTeX["38",Magnification->mag]},{40,MaTeX["40",Magnification->mag]},{42,MaTeX["42",Magnification->mag]},{44,MaTeX["44",Magnification->mag]}},{{3,MaTeX["3",Magnification->mag]},{3.5,MaTeX["3.5",Magnification->mag]},{4,MaTeX["4",Magnification->mag]},{4.5,MaTeX["4.5",Magnification->mag]}},None,None},FrameLabel->{MaTeX["|V_{\\rm cb}|\\times 10^{3}",Magnification->mag],MaTeX["|V_{\\rm ub}|\\times 10^{3}",Magnification->mag]," "," "}
];


FFparm=CLN;
ploCLN=Show[plo,Graphics[Text[MaTeX["(B \\to D^* )_{\\rm CLN}",Magnification->mag],{39.6,4.45}]],
Epilog->Inset[FlagLogo[[1]],Scaled[{0,1.06}],Scaled[{0,1}],Scaled[0.25]],ImagePadding->{{55,20},{55,25}},PlotRangeClipping->False];

FFparm=BGL;
ploBGL=Show[plo,Graphics[Text[MaTeX["(B \\to D^* )_{\\rm BGL}",Magnification->mag],{42.9,4.2}]],
Epilog->Inset[FlagLogo[[1]],Scaled[{0,1.06}],Scaled[{0,1}],Scaled[0.25]],ImagePadding->{{55,20},{55,25}},PlotRangeClipping->False];

(*****************)
(* Export to eps *)
(*****************)

Export["vub_vcb_BGL.eps", ploBGL, "EPS"];
Export["vub_vcb_CLN.eps", ploCLN, "EPS"];



