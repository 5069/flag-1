# Library for generating FLAG-scatter plots
# v0.13 04/14
# for questions/suggestins please email juettner@soton.ac.uk
# 
# parameter that can be adjusted from within the inputfile:
# MULTIPLE  1 or 0 (deftault 0)
#	    set to one if you wish to have two scatter plots in one figure
# MS	    Marker Size (default 8)
# FS        Font Size   (default 10)
# LW        line width  (default 1.0)
# SHIFT     relative y-shift between data points and descriptor
#           (default -0.3)
# REV       1 or 0 (default 0)  set to one if you want to reverse the
#           order of the data points within each block
# PRELIM    0 nothing
#           1 adds large diagonal PRELIMINARY watermark
# 	    2 adds smaller PRELIMINARY watermark after FLAG logo
# LABEL     1 if you want to  add xlabels which you need to specify
#             in xaxisstrings
# SIZE      size of figure window can be specified via variable
#           SIZE=[w,h] where w and h are the width and height of the 
#           resulting figure in cm (default [20.32,15.24])
# AVPOSITION values are TOP or BOTTOM (default TOP) and this decides whether
#	    the FLAG average is plotted above or below the data points
# REMOVEWHITESPACE (default=0) removes excess whitespace around 
#           plot if set to 1
#
# Changes
# v0.7 -> v0.8: Logo now drawn by script, no external logo-file necessary
# v0.8 -> v0.9: I automated the setting of the x-position of the y-labels.
#               The corresponding entry in yaxisstrings is now obsolete
# 		if MULTIPLE = 0
# v0.9 -> v0.10:new logo-implementation which should have less artefacts when
#		zooming or for certain viewers
# v0.10-> v0.11:added optional label for the x-axis which can be switched on 
#               via setting LABEL=1 and then specifying the label x- and y-
#               position, e.g. as
#		xaxisstrings	= [[+4.2,-1.73, "ps$^{-1}$",25]]
#               where the first two entries are the x- and y-position of the 
#               label, the third entry is the label itself and the fourth
#               entry is optional and fixes the font-size of the label
# v0.11-> v0.12:added the option "dontshow" in the block with the FLAG-averages
#               such that the grey band will be plotted but not the 
#               corresponding data point.  
# v0.12-> v0.13 size of figure window can now be specified via variable SIZE
# v0.13-> v0.14 Logo position can now be modified
#               excess whitespace can now be removed
# Yasumichi tweaks to resolve some problem with anaconda3.7 on mac Nov.24.2020

import matplotlib.pyplot as plt
import matplotlib.image as implt
import matplotlib.patches as mpatches
import os.path
################################################################################
flagyear="2020"
TOP=1    
BOTTOM=0
#plt.rc('font', family='sans-serif')
from matplotlib import rcParams
rcParams['mathtext.fontset']= 'stixsans'
# Yasumichi sets below
rcParams['errorbar.capsize']= 3

################################################################################
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("FLAGplot.py: numpy library not found. If you want the FLAG-logo to")
 print ("             be added to the plot, please install this library.")
## check whether Image library is present (for inclusion of logo)
try: 
 from PIL import Image
 blogo=1
except ImportError:
 print ("FLAGplot.py: Image library not found. If you want the FLAG-logo ")
 print ("             to be added to the plot, please install this library")
 blogo=0
# check whether directory for plots exists
if not os.path.exists("./plots/"):
 print ("FLAGplot.py: Directory ./plots/ does not exist. It will now be created.")
 os.makedirs("./plots/")
################################################################################
try: 
 MULTIPLE
except NameError:
 MULTIPLE=0 
try: 
 MS
except NameError:
 MS=8
try:
 FS
except NameError:
 FS=10
try:
 LW
except NameError:
 LW=1.0
try:
 SHIFT
except NameError:
 SHIFT=-.3
try:
 REV
except NameError:
 REV=0
try:
 PRELIM
except NameError:
 PRELIM=0
try:
 LABEL
except NameError:
 LABEL=0
try:
 SIZE 
except NameError:
 SIZE=[8*2.54,6*2.54] # that's the default size
try:
 AVPOSITION
except NameError:
 AVPOSITION=TOP
try:
 REMOVEWHITESPACE
except:
 REMOVEWHITESPACE=1

R=[[1,0,0],[1,.5,.5],[1,.6,.6]]
B=[[0,0,1],[.5,.5,1],[.6,.6,1]]
K=[[0,0,0],[.3,.3,.3],[.6,.6,.6]]
G=[[0,.7,0],[.5,.8,.5],[.6,.8,.6]]
W=[[1,1,1],[1,1,1],[1,1,1]]
L=[[.85,.99,.85],[.7,.9,.7],[.7,.9,.7]]
def colorweight(c,alpha):
 if c=='r':
  res=R[alpha]
 elif c=='k':
  res=K[alpha]
 elif c=='g':
  res=G[alpha]
 elif c=='b':
  res=B[alpha]
 elif c=='w':
  res=W[alpha]
 elif c=='l':
  res=L[alpha]
 else:
  print ("FLAGplot.py in colorweight: uhps, color %s not defined") % (c)
  exit()
 return res

def plot_data(y0,dat):
 """
  dat    input data as in "THE DATA" below
 """
 if REV == 1:
  N=len(dat)
  dat=dat[N::-1]
 for i in dat:
  cc=colorweight(i[6][2],i[6][3])
  cc2=(cc[0],cc[1],cc[2])
  fc=colorweight(i[6][1],i[6][3])
  fc2=(fc[0],fc[1],fc[2])
  plt.errorbar(i[0],y0,xerr=[[i[1]],[i[2]]],yerr=None,
                color=cc2,
  		mec=cc2,mfc=fc2,fmt=i[6][0],markersize=MS,linewidth=LW)
  plt.errorbar(i[0],y0,xerr=[[i[3]],[i[4]]],yerr=None,
                color=cc2,
  		mec=cc2,mfc=fc2,fmt=i[6][0],markersize=MS,linewidth=LW)
  plt.text(i[6][4],y0+SHIFT,i[5],family="sans-serif",fontsize=FS)
  y0 += 1

def mkFLAGlogo():
 # this routine generates the FLAG-logo
 for i in range(2):
  fig2=plt.figure(num=None, figsize=(1, .20))
  axicon = fig2.add_axes([0,0,1,.20],frameon=False,xticks=[],yticks=[])
  plt.text(0.01,0.3,'F',fontsize=14,color='r')
  plt.text(.125,0.3,'G',fontsize=14,color='r')
  plt.text(.03,0.3,' LA ',fontsize=11,color='r')
  plt.xlim((0,.4))
  plt.ylim((0,.4))
  plt.text(.19,.3,flagyear,fontsize=14,rotation=0,color='r')
  if i==0:
   plt.text(.01503,-.30,'I',fontsize=17,color='r',rotation=90)
   plt.text(.055  ,-.30,'I',fontsize=17,color='r',rotation=90)
   plt.savefig('plots/FLAG_Logo.pdf',type='pdf')
   plt.savefig('plots/FLAG_Logo.eps',type='eps')
   fig2.clf()
  if i==1:
# Yasumichi tweaks belos
#   plt.text(.00785,.00,'I',fontsize=17,color='r',rotation=90)
#   plt.text(.055 ,.00,'I',fontsize=17,color='r',rotation=90)
   plt.text(.0065,2.1,'I',fontsize=17,color='r',rotation=90)
   plt.text(.055 ,2.1,'I',fontsize=17,color='r',rotation=90)
   plt.savefig('plots/FLAG_Logo.png',type='png',dpi=640,bbox_inches='tight',pad_inches=0)
   fig2.clf()
  plt.figure()
# generate FLAG logo if not alsready present in sub-directory ./plots
if MULTIPLE==0 and not os.path.isfile("./plots/FLAG_Logo.png"):
 mkFLAGlogo()
elif not os.path.isfile("./plots/FLAG_Logo.png"):
 print ("")
 print ("FLAGplot.py: I need to generate FLAG_Logo.png first. The easiest way")
 print ("             to do this is to run a different plot script with")
 print ("             MULTIPLE=0 first and then get back to the current one.")
 print ("")
 exit()
## start plotting ##############################################################
if MULTIPLE==0:
 fig=plt.figure()
 ax=fig.add_subplot(111)
fig.set_figheight(SIZE[1]/2.54)
fig.set_figwidth(SIZE[0]/2.54)
# insert preliminary disclaimer
if PRELIM == 1:
 plt.annotate('PRELIMINARY',rotation=35,fontsize=45,color=[1.,.8,.8],
		 xy=(0.5, 0.5), horizontalalignment='center',
		 verticalalignment='center',
		 xycoords='axes fraction',alpha=.5,zorder=0)


# add text to the y-axis if specified
Nystrings=len(yaxisstrings)
if Nystrings>0:
 for i in range(len(yaxisstrings)):
  if MULTIPLE:
   xpos = yaxisstrings[i][0]
  else:
   D=abs(xlimits[0]-xlimits[1])
   xpos = xlimits[0]-.04*D
  plt.text(xpos,yaxisstrings[i][1],yaxisstrings[i][2],rotation=90,
	fontsize=16)

  # add text to the x-axis if specified
if LABEL>0:
 for i in range(len(xaxisstrings)):
  if MULTIPLE:
   xpos = xaxisstrings[i][0]
  else:
   D=abs(xlimits[0]-xlimits[1])
   xpos = xaxisstrings[0][0]
  if len(xaxisstrings[i])==4: # check if font size for xaxisstrings specified
   xaxisstringsFS = xaxisstrings[i][3]
  else:                       # otherwise default to same font-size as 	
	  		      # y axis label, namely 16
   xaxisstringsFS=16
  plt.text(xpos,xaxisstrings[i][1],xaxisstrings[i][2],
	fontsize=xaxisstringsFS)
   
  
# plot the data and the FLAG-average if specified for a given data set
try: 
 FLAGaverage
except NameError:
 FLAGaverage=None
if len(datasets)!=len(FLAGaverage):
 print ("FLAGplot.py: there should be as many entries in FLAGaverage as in datasets")
 exit()


if len(FLAGaverage) == len(datasets):
 Nav=len(FLAGaverage)
 dat=FLAGaverage
 offset = -0.5
 cnt=0
 offsets = [0.]*(Nav+1)
 if AVPOSITION==TOP:
  for i in range(Nav):
   if i!=0:
    plt.axhline(y=offset-.5,color='k',linestyle='-',zorder=5)
    offset+=.5
   plot_data(offset,datasets[i])
   offset += len(datasets[i])+1.
   if np.isnan(dat[i][0])==0 and dat[i][5]!="dontshow":
    offset-=.5
    plot_data(offset,[dat[i]])
   else:
    offset-= 2.0
   offset += 1.5
   cnt+=1
   offsets[i+1]=offset
  plt.ylim((-1.5,offset))
 elif AVPOSITION==BOTTOM:
  for i in range(Nav):
   if i!=0:
    plt.axhline(y=offset-.5,color='k',linestyle='-',zorder=5)
   if np.isnan(dat[i][0])==0 and dat[i][5]!="dontshow":
    offset+=.5
    plot_data(offset,[dat[i]])
   else:
    offset-= .5
   offset += 1.5
   cnt+=1
   plot_data(offset,datasets[i])
   offset += len(datasets[i])+1.
   offsets[i+1]=offset
  plt.ylim((-1.5,offset))
else:
 print ("FLAGplot.py: number of data sets and FLAGaverages in input file ")
 print ("	     must be the same.")
 exit()
offsets[0]=-1.
for i in range(Nav):
 # draw the patch and the data point with label representing the FLAG average
 if np.isnan(dat[i][0])==0:
  if i==Nav-1:
   stretch=.5
  else:
   stretch=0.
  plt.axvspan(dat[i][0]-dat[i][1],dat[i][0]+dat[i][2],
      ymin=(offsets[i]+1.0)/(offset+1.5),
      ymax=(offsets[i+1]+1.0+stretch)/(offset+1.5),
      edgecolor=(0,0,0),facecolor=(.8,.8,.8),alpha=.8)

################################################################################

# format the plot 
plt.xlim((xlimits[0],xlimits[1]))	# tune x-range
plt.yticks(range(0))      		# remove y-ticks
plt.xticks(plotxticks[0],fontsize=16,fontname="sans-serif") 	# your choice of x-ticks
plt.title(titlestring,fontsize=24,verticalalignment='bottom')	# add plot-title

# insert logo if specified #####################################################
if 2==2:
 nologo=0
 #if (logo)>0:
 if (logo):
  plotbox=ax.bbox.extents
  ax = plt.axis()
  if logo=='upper right':
   xpos=0.777
   ypos=0.855
   Pxpos=.0
   Pypos=+250
  elif logo=='upper left':
   xpos=0.1205
   ypos=0.855
   Pxpos=45.10
   Pypos=+250
  elif logo=='lower left':
   xpos=0.12
   ypos=0.015
   Pxpos=45.10
   Pypos=-50
  elif logo=='lower right':
   xpos=0.777
   ypos=0.015
   Pxpos=.0
   Pypos=-50
  elif logo=='bespoke':
   try:
    LOGOxpos
    LOGOypos
    LOGOPxpos
    LOGOPypos
   except:
    print ("For bespoke logo position xpos, ypods, Pxpos and Pypos need to be specified")
    exit()
   xpos =LOGOxpos
   ypos =LOGOypos
   Pxpos=LOGOPxpos
   Pypos=LOGOPypos
   print (xpos,ypos,Pxpos,Pypos)
  elif logo=='none':
   nologo=1
  else:
   print ("FLAGplot.py: There seems to be sth. wrong with the key word <<logo>>")
   exit()
  # draw the logo
  if nologo==0:
   axicon = fig.add_axes([xpos,ypos,.13,.13],frameon=False,xticks=[],yticks=[])
   im  = Image.open('./plots/FLAG_Logo.png')
   im2 = numpy.array(im).astype(numpy.float) / 255
   axicon.imshow(im2)
if PRELIM==2:
  plt.text(Pxpos,Pypos,'PRELIMINARY',rotation=0,fontsize=10,color=[.2,.2,.2])
   
# print the stuff to a file
if REMOVEWHITESPACE==0:
 filename="plots/"+plotnamestring+".eps"
 plt.savefig(filename, format="eps",dpi=800)
 filename="plots/"+plotnamestring+".pdf"
 plt.savefig(filename, format="pdf",dpi=800)
 filename="plots/"+plotnamestring+".png"
 plt.savefig(filename, format="png",dpi=400)
else:
 filename="plots/"+plotnamestring+".eps"
 plt.savefig(filename, format="eps",dpi=800,bbox_inches='tight')
 filename="plots/"+plotnamestring+".pdf"
 plt.savefig(filename, format="pdf",dpi=800,bbox_inches='tight')
 filename="plots/"+plotnamestring+".png"
 plt.savefig(filename, format="png",dpi=400,bbox_inches='tight')

