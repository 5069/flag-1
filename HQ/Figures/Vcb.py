# FLAG plot for RFKpi
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm |V_{cb}| x 10^3$"			# plot title
plotnamestring	= "Vcb"			# filename for plot
plotxticks	= ([[36,38,40,42,44,46]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   #[+1.108,26.0,"$N_f=2+1+1$"],
		   [35.6,6.5,"$\\rm N_f=2+1$"],
		   [35.6,1.7,"$\\rm N_f=2$"],
		   [35.6,0.2,"$\\rm non-latt.$"]
		    ]
xlimits		= [36,52]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 46.0					# x-position for the data labels
SHIFT=-0.1
REMOVEWHITESPACE=0
FS = 18
# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
#dat2p1p1=[
#[1.224,	0.013,0.013,0.013,0.013,  "ETM 10E$^+$"        	,['*','r','r',0,tpos]],
#[1.187,	0.004,0.004,0.000,0.000,  "MILC 11$^+$" 	,['s','r','r',0,tpos]]
#]
datOther=[
[42.00,	0.65,0.65,0.65,0.65,  "HFLAV inclusive" 	        ,['o','b','b',0,tpos]]
,
]
#dat2p1=[
#[39.55,0.72,0.72,0.88,0.88,  "FNAL/MILC 10"		,['s','g','g',0,tpos]]
dat2p1=[
[39.41,0.60,0.60,0.60,0.60,  "$B\\to (D,D^*)\\ell\\nu$ (CLN)"		,['^','w','g',0,tpos]],
[39.08,0.91,0.91,0.91,0.91,  	"$B\\to (D,D^*)\\ell\\nu$ (BGL)"	,['^','g','g',0,tpos]],
[40.1,1.0,1.0,1.0,1.0,  	"$B\\to D\\ell\\nu$"			,['^','g','g',0,tpos]],
[39.05,0.55,0.55,0.72,0.72,  "$B\\to D^*\\ell\\nu$ (CLN)"		,['^','w','g',0,tpos]],
[38.30,0.53,0.53,0.87,0.87,  "$B\\to D^*\\ell\\nu$ (BGL)"		,['^','g','g',0,tpos]]
]
dat2=[
[41.0,	3.8,3.8,4.1,4.1,  "$B\\to D\\ell\\nu$" 		,['^','w','g',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      #
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
 #[1.194 ,0.010,0.010,0.010,0.010,"our estimate for $N_f=2$"  ,['o','k','k',0,tpos],1],
# [39.36,0.75,0.75,0.00,0.00,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0]
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datOther,dat2,dat2p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker
