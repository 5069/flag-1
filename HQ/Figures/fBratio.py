# FLAG plot for RFKpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print("numpy library not found. If you want the FLAG-logo to be added to the")
 print("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm f_{B_s}/f_B\; $"			# plot title
plotnamestring	= "fBratio"			# filename for plot
plotxticks	= ([[1.1 ,1.15,     1.20,   1.25  ]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
                   [+1.005,25.2,"$\\rm N_f=2+1+1$"],
		   [+1.005,14.1,"$\\rm N_f=2+1$"],
		   [+1.005,3.5,"$\\rm N_f=2$"]
		    ]
xlimits		= [1.02,1.5]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.30					# x-position for the data labels

FS = 11

SHIFT = -0.1
# column        item
# 0             central value
# 1             neg. error 1
# 2             pos. error 1
# 3             neg. error 2
# 4             pos. error 2
# 5             Collaboration string
# 6             this column contains layout parameters for the plot-symbol and
#               and the collaboration text:
#               column  item
#               0       marker style (see bottom of this file for a full list)
#               1       marker face color r=red, b=blue, k=black, w=white
#                                         g=green
#               2       marker color (errorbar and frame), color coding as
#                       for face color
#               3       color intensity 0=full, 1=soso, 2=bleak
#               4       position of the collaboration string on the x-axis
dat2p1p1=[
[1.205,   0.007,0.007,0.007,0.007,  "HPQCD 13"            ,['s','g','g',0,tpos]],
[1.201,   0.025,0.025,0.025,0.025,  "ETM 13E"            ,['s','l','g',0,tpos]],
[1.184,   0.018,0.018,0.025,0.025,  "ETM 16B"           ,['s','g','g',0,tpos]],
[1.207,   0.007,0.007,0.007,0.007,         "HPQCD 17A"         ,['s','g','g',0,tpos]],
[1.2180,  0.0033,0.0033,0.0049,0.0049,  "FNAL/MILC 17 2"      ,['s','g','g',0,tpos]],
[1.2109,  0.0029,0.0029,0.0041,0.0041,  "FNAL/MILC 17 1"      ,['s','g','g',0,tpos]],
]
dat2p1=[
[1.226, 0.02,0.02,0.026,0.026,   "HPQCD 09"      ,['s','l','g',0,tpos]],
[1.15,	0.12,0.12,0.12,0.12,  "RBC/UKQCD 10C"        ,['s','w','r',0,tpos]],
[1.229, 0.01,0.01,0.026,0.026,  "FNAL/MILC 11"        ,['s','g','g',0,tpos]],
[1.188, 0.012,0.012,0.018,0.018,  "HPQCD 12"            ,['s','g','g',0,tpos]],
[1.20,	0.02,0.02,0.02,0.02,  "RBC/UKQCD 13A (stat. err. only)"        ,['s','l','g',0,tpos]],
[1.193, 0.020,0.020,0.048,0.048,  "RBC/UKQCD 14A"            ,['s','g','g',0,tpos]],
[1.223, 0.014,0.014,0.070,0.070,  "RBC/UKQCD 14 2"            ,['s','g','g',0,tpos]],
[1.197, 0.013,0.013,0.051,0.051,  "RBC/UKQCD 14 1"            ,['s','g','g',0,tpos]],
[1.1949, 0.0060,0.0060,0.0175,0.0095,  "RBC/UKQCD 18knm"            ,['s','l','g',0,tpos]],
] 
dat2=[                          
[1.19,	0.05,0.05,0.05,0.05,  "ETM 11A" 		,['s','l','g',0,tpos]],
[1.19,	0.05,0.05,0.05,0.05,  "ETM 12B" 		,['s','l','g',0,tpos]],
[1.13,	0.06,0.06,0.06,0.06,  "ALPHA 12A" 		,['s','l','g',0,tpos]],
[1.206,	0.024,0.024,0.024,0.024,  "ETM 13B, 13C" 		,['s','g','g',0,tpos]],
[1.195,	0.064,0.064,0.064,0.064,  "ALPHA 13" 		,['s','l','g',0,tpos]],
[1.203,	0.062,0.062,0.065,0.065,  "ALPHA 14" 		,['s','g','g',0,tpos]],
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [1.206,0.024,0.024,0.024,0.024,"our average for $\\rm N_f=2$"  ,['s','k','k',0,tpos],1],
 [1.202,0.022,0.022,0.022,0.022,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
 [1.209,0.005,0.005,0.005,0.005,"our average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1],
]

# The follwing list should contain the list names of the previous DATA-blocks
#datasets=[dat2,dat2p1,dat2p1p1]
datasets=[dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#exec(open('FLAGplot.py').read())
exec(open('FLAGplotYA.py').read())

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

