# FLAG plot for fK and fpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm \\xi$"			# plot title
plotnamestring	= "xiandRBB"			# filename for plot
plotxticks	= ([[1.1,1.2,1.3]])# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

FS = 12

SHIFT = -0.1
yaxisstrings	= [				# x,y-location for y-strings
		   [+0.94, 12.,"$\\rm N_f=2+1+1$"],
		   [+0.94, 6.,"$\\rm N_f=2+1$"],
		   [+0.94, 0.,"$\\rm N_f=2$"]
		    ]
xlimits		= [1.00, 1.45]			# plot's xlimits

logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos   		= 1.35				# x-position for the data labels 
					        # move data labels of l.h.s side
						# plot to nirvana
tpos2		= 1000				# x-position for the data labels
						# here a reasonable choice
REV    		= 1				# reverse order of data points
PRELIM 		= 0				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
						# one figure
REMOVEWHITESPACE=0
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

################################################################################
# data for l.h.s. scatter plot
################################################################################
 
#-- Nf=4 --
datxiNf4=[
[1.216,0.016,0.016,0.016,0.016,"HPQCD 19bea",     ['s','g','g',0,tpos]]
]

#-- Nf=3 --
datxiNf3=[
[1.1853,0.0054,0.0054,0.00156,0.0115,"RBC/UKQCD 18knm", ['s','l','g',0,tpos]],
[1.206,0.018,0.018,0.018,0.018,"FNAL/MILC 16", ['s','g','g',0,tpos]],
[1.208,0.066,0.066,0.041,0.041,"RBC/UKQCD 14A", ['s','g','g',0,tpos]],
[1.268,0.063,0.063,0.063,0.063,"FNAL/MILC 12", ['s','l','g',0,tpos]],
[1.13 ,0.12 ,0.12, 0.12, 0.12 ,"RBC/UKQCD 10C", ['s','w','r',0,tpos]],
[1.258,0.025,0.025,0.025,0.025,"HPQCD 09",     ['s','l','g',0,tpos]]
]

#-- Nf=2 --                      
datxiNf2=[                         
[1.225 ,0.016 ,0.016 ,0.031 ,0.031 ,"ETM 13B",       ['s','g','g',0,tpos]],
[1.21 ,0.06 ,0.06 ,0.06 ,0.06 ,"ETM 12A,12B",       ['s','l','g',0,tpos]]
]  
################################################################################
# data for r.h.s. scatter plot
################################################################################
datRBBNf4=[
[1.008,0.025,0.025,0.025,0.025,"HPQCD 19bea",       ['s','g','g',0,tpos2]]
]
datRBBNf3=[
#[0,0,0,0,0,"placeholder",['s','g','g',0,tpos2]],
[1.0002,0.0043,0.0043,0.0082,0.0059,"RBC/UKQCD 18knm", ['s','l','g',0,tpos2]],
[1.033,0.040,0.040,0.040,0.040,"FNAL/MILC 16", ['s','g','g',0,tpos2]],
[1.028,0.077,0.077,0.049,0.049,"RBC/UKQCD 14A", ['s','g','g',0,tpos2]],
[1.06,0.11,0.11,0.11,0.11,"FNAL/MILC 12", ['s','l','g',0,tpos2]],
[0,0,0,0,0,"placeholder",['s','g','g',0,tpos2]],
[1.05,0.07,0.07,0.07,0.07,"HPQCD 09",       ['s','l','g',0,tpos2]]
]
datRBBNf2=[                          
[1.007 ,0.015 ,0.015 ,0.021 ,0.021 ,"ETM 13B",       ['s','g','g',0,tpos2]],
[1.03 ,0.02 ,0.02 ,0.02 ,0.02 ,"ETM 12A,12B",       ['s','l','g',0,tpos2]]
]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.



# first define FLAGaverage for l.h.s. plot, the one for the r.h.s. one follows below
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
#[np.nan,0.00,0.00,0.00,0.00,"place holde       ",['s','k','k',0,tpos],0],
[1.225,0.031,0.031,0.031,0.031,"FLAG average for $\\rm N_f=2$",   ['s','k','k',0,tpos],0],
[1.206,0.017,0.017,0.017,0.017,"FLAG average for $\\rm N_f=2+1$",   ['s','k','k',0,tpos],0],
[1.216,0.016,0.016,0.016,0.016,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]
# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datxiNf2,datxiNf3,datxiNf4]

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 


# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
# execfile('FLAGplot.py')
exec(open("./FLAGplot.py").read())

logo='none'
# now some definitions for the r.h.s. plot
plotnamestring	= "xiandRBB"			# filename for plot

titlestring	= "$\\rm B_{B_s}/B_{B_d}$"			# plot title
xlimits		= [0.73, 1.18]			# plot's xlimits
plotxticks	= ([[0.9,1.0,1.1]])# locations for x-ticks

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
#[np.nan,0.00,0.00,0.00,0.00,"place holde       ",['s','k','k',0,tpos2],0],
 [1.007,0.021,0.021,0.021,0.021," for $\\rm N_f=2$",   ['s','k','k',0,tpos2],0],
 [1.032,0.038,0.038,0.038,0.038,"our average for $\\rm N_f=2+1$",   ['s','k','k',0,tpos2],0],
 [1.008,0.025,0.025,0.025,0.025," for $\\rm N_f=2+1+1$",['s','k','k',0,tpos2],0],
 ]

yaxisstrings	= [				# x,y-location for y-strings
		   [+0,25.0,"$\\rm N_f=2+1+1$"],
		   [+0,14.0,"$\\rm N_f=2+1$"],
		   [+0,2.0,"$\\rm N_f=2$"]
		    ]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datRBBNf2,datRBBNf3,datRBBNf4]

# do the r.h.s. plot
ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
# execfile('FLAGplot.py')
exec(open("./FLAGplot.py").read())

################################################################################
# We are done
################################################################################


# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

