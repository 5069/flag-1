# FLAG plot for fK and fpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm|V_{cd}|$"			# plot title
plotnamestring	= "VcdandVcs"			# filename for plot
plotxticks	= ([[0.20,.22,0.24]])# locations for x-ticks
nminorticks	= 1			# number of intermediate minor ticks
					# 0 if none

FS = 12

SHIFT = -0.15
yaxisstrings	= [				# x,y-location for y-strings
		   [0.172,2.7,"$\\rm non-lattice$"],
                   [0.172,22.5,"$\\rm N_f=2+1+1$"],
		   [0.172,13.5,"$\\rm N_f=2+1$"],
		   [0.172,6.3,"$\\rm N_f=2$"]
		    ]
xlimits		= [0.18,0.27]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos   		= .25				# x-position for the data labels 
					        # move data labels of l.h.s side
						# plot to nirvana
tpos2		= 1.0	   		        # x-position for the data labels
						# here a reasonable choice
REV    		= 1				# reverse order of data points
PRELIM 		= 0				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
						# one figure
REMOVEWHITESPACE=1
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

################################################################################
# data for l.h.s. scatter plot (Vcd)
################################################################################
# Nf=4
datfpiNf4=[
[0.2165 ,0.0006 ,0.0006 ,0.0050,0.0050,"FNAL/MILC 17 ",['s','g','g',0,tpos]],
[0.2341,0.0074,0.0074,0.0074,0.0074,"ETM 17D/Riggio 17",['^','g','g',0,tpos]],
[0.2330,0.0133,0.0133,0.0137,0.0137,"ETM 17D ($q^2=0$)",['^','l','g',0,tpos]],
[0.2214,0.0041,0.0041,0.0065,0.0065,"ETM 14E",['s','g','g',0,tpos]]]
#-- Nf=3 --
datfpiNf3=[
[0.2200,0.0036,0.0036,0.0062,0.0062,"RBC/UKQCD 17",['s','g','g',0,tpos]],       
[0.0,0.0,0.0,0.0,0.0,"Meinel 16",['^','g','g',0,tpos]],       
[2.2088     ,0.023      ,0.023      ,0.016      ,0.016      ,"$\chi$QCD 14",['s','g','g',0,tpos]],
[0.2140,0.0093,0.0093,0.0097,0.0097,"HPQCD 11/10B",['^','g','g',0,tpos]],       
[0.2204,0.0036,0.0036,0.0061,0.0061,"HPQCD 12A/10A",['s','g','g',0,tpos]],       
[0.2097 ,0.0108 ,0.0108 ,0.0118,0.0118,"FNAL/MILC 11",['s','g','g',0,tpos]]]
#-- Nf=2 --                      
datfpiNf2=[                                          
[0.0,0.0,0.0,0.0,0.0    ,"Blossier 18",['s','g','g',0,tpos]],
[0.2207    ,0.0074  ,0.0074  ,0.0089     ,0.0089     ,"ETM 13B",['s','g','g',0,tpos]]]

datfpiother=[
[0.0,0.0,0.0,0.0,0.0,"",['s','g','g',0,tpos]],       
[0.230,0.011,0.011,0.0,0.0,"neutrino scattering"                    ,['o','b','b',0,tpos]],
[0.2254,0.0007,0.0007, 0.0, 0.0,"CKM unitarity"                    ,['o','b','b',0,tpos]],
[0.0,0.0,0.0,0.0,0.0,"",['s','g','g',0,tpos]]]
 
################################################################################
# data for r.h.s. scatter plot (Vcs)
################################################################################
# Nf=4
datfKNf4=[
[1.004 ,0.002 ,0.002 ,0.016,0.016,"",['s','g','g',0,tpos2]],
[0.970,0.033,0.033,0.033,0.033,"",['^','g','g',0,tpos2]],
[0.945,0.038,0.038,0.038,0.038,"",['^','l','g',0,tpos2]],
[1.015,0.017,0.017,0.023,0.023,"",['s','g','g',0,tpos2]]]
#-- Nf=3 --
datfKNf3=[
[1.018,0.009,0.009,0.018,0.018 ,"",['s','g','g',0,tpos2]],       
[0.949  ,0.024      ,0.024      ,0.056 ,0.056 ,"",['^','g','g',0,tpos2]],       
[0.988     ,0.023      ,0.023      ,0.016      ,0.016      ,"",['s','g','g',0,tpos2]],
[0.975  ,0.025      ,0.025      ,0.026 ,0.026 ,"",['^','g','g',0,tpos2]],       
[1.012  ,0.010      ,0.010      ,0.019 ,0.019 ,"",['s','g','g',0,tpos2]],       
[0.965     ,0.0404      ,0.0404      ,0.043      ,0.043      ,"",['s','g','g',0,tpos2]]
]                     
#-- Nf=2 --
datfKNf2=[
[1.054,0.024,0.024,0.029,0.029            ,""             ,['s','g','g',0,tpos2]],
[1.004 ,0.028,0.028,0.032,0.032            ,""             ,['s','g','g',0,tpos2]]]

datfKother=[
[0.0,0.0,0.0,0.0,0.0,""                    ,['o','b','b',0,tpos2]],
[0.0,0.0,0.0,0.0,0.0,""                    ,['o','b','b',0,tpos2]],
[0.9733,0.00022,0.00022,0.0,0.0,""                    ,['o','b','b',0,tpos2]],
[0.0,0.0,0.0,0.0,0.0,""                    ,['o','b','b',0,tpos2]]]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.



# first define FLAGaverage for l.h.s. plot, the one for the r.h.s. one follows below
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0], 
 [0.2207    ,0.0089,0.0089,0.000,0.000,  "our estimate for $\\rm N_f=2$",['s','k','k',0,tpos],0],
 [0.2182,0.0050,0.0050,0.000,0.000,"our estimate for $\\rm N_f=2+1$",['s','k','k',0,tpos],0], 
 [0.2219,0.0043,0.0043,0.000,0.000,"our estimate for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datfpiother,datfpiNf2,datfpiNf3,datfpiNf4]

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 


# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
execfile('FLAGplot.py')


# now some definitions for the r.h.s. plot
plotnamestring	= "VcdandVcs"			# filename for plot

titlestring	= "             $\\rm |V_{cs}|$"			# plot title
xlimits		= [0.80,1.10]			# plot's xlimits
plotxticks	= ([[0.95,1.00,1.05]])# locations for x-ticks

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos2],0], 
 [1.031,0.030,0.030,0.0000,0.0000,    "",['s','k','k',0,tpos2],0],
 [0.999,0.014,0.014,0.000,0.000,"",['s','k','k',0,tpos2],0], 
[1.002,0.014,0.014,0.000,0.000,"",['s','k','k',0,tpos2],0]
]
yaxisstrings    = [                             # x,y-location for y-strings
                   [0.172,2.7,""],
                   [0.172,20.5,""],
                   [0.172,12.0,""],
                   [0.172,5.7,""]
                    ]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datfKother,datfKNf2,datfKNf3,datfKNf4]

logo		= 'none'
# do the r.h.s. plot
ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
execfile('FLAGplot.py')

################################################################################
# We are done
################################################################################


# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

