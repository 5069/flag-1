# FLAG plot for fK and fpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm f_D$"			# plot title
plotnamestring	= "fDandfDs"			# filename for plot
plotxticks	= ([[180, 200,  220,  240 ]])# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

#FS = 12

SHIFT = -0.15

yaxisstrings	= [				# x,y-location for y-strings
		   [+153,25.7,"$\\rm N_f=2+1+1$"],
		   [+153,14.3,"$\\rm N_f=2+1$"],
		   [+153,3.2,"$\\rm N_f=2$"]
		    ]
		    
LABEL=1

xaxisstrings	= [				# x,y-location for x-strings
#		   [+255.0,-2.3,"MeV"] 	 		    
                   ]	
		    
xlimits		= [160,260]			# plot's xlimits
logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos   		= 236				# x-position for the data labels 
					        # move data labels of l.h.s side
						# plot to nirvana
tpos2		= 1000				# x-position for the data labels
						# here a reasonable choice
REV    		= 0				# reverse order of data points
PRELIM 		= 0				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
						# one figure

REMOVEWHITESPACE=0
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

################################################################################
# data for l.h.s. scatter plot
################################################################################
#-- Nf=4 --
datfDNf4=[
[209,   3,3,5,5,  "FNAL/MILC 12B"            ,['s','l','g',0,tpos]],
[212.3, 0.3,0.3,1.0,1.0, "FNAL/MILC 13"      ,['s','l','g',0,tpos]],
[201.9, 8.0,8.0,8.0,8.0, "ETM 13F"            ,['s','l','g',0,tpos]],
[207.4, 3.8 , 3.8, 3.8, 3.8, "ETM 14E"      ,['s','g','g',0,tpos]],
[212.6, 0.4 , 0.4, 1.1, 1.3, "FNAL/MILC 14A"      ,['s','l','g',0,tpos]],
[212.1, 0.3 , 0.3, 0.6, 0.6, "FNAL/MILC 17"      ,['s','g','g',0,tpos]],
]

#-- Nf=3 --
datfDNf3=[
[201,	3,3,17,17,  "FNAL/MILC 05"        ,['s','w','r',0,tpos]],
[207,   4,4,4,4,  "HPQCD/UKQCD 07"        ,['s','l','g',0,tpos]],
[213,   4,4,4,4,  "HPQCD 10A"            ,['s','l','g',0,tpos]],
[226,   6,6,9,9,  "PACS-CS 11"             ,['s','w','r',0,tpos]],
[218.9, 2.2,2.2,11.3,11.3,  "FNAL/MILC 11"        ,['s','g','g',0,tpos]],
[208.3,    1.,1.,3.4,3.4,   "HPQCD 12A"      ,['s','g','g',0,tpos]],
[100,   2,2,4.5,4.5, "$\chi$QCD 14"    ,['s','g','g',0,tpos]],
[208.7,  2.8,2.8,3.4,3.4,  "RBC/UKQCD 17"          ,['s','g','g',0,tpos]],
 [213,     5  , 5, 5  , 5 ,  "$\chi$QCD 20qma"         ,['s','w','r',0,tpos]],
]

#-- Nf=2 --                      
datfDNf2=[                         
[197,	9,9,9,9,  "ETM 09" 		,['s','l','g',0,tpos]]  ,
[212,   8,8,8,8,  "ETM 11A"              ,['s','l','g',0,tpos]]  ,
[208,   7,7,7,7,  "ETM 13B"               ,['s','g','g',0,tpos]] ,
[216,   7,7,8.6,8.6,  "ALPHA 13B"              ,['s','l','g',0,tpos]]  ,
[202.3,  2.2,2.2,3.4,3.4,  "TWQCD 14"              ,['s','w','r',0,tpos]]  ,
[100,   2,2,4.5,4.5, "Blossier 18"    ,['s','g','g',0,tpos]],
[100,   2,2,4.5,4.5, "Balasubramamian 19net"    ,['s','g','g',0,tpos]],
]
 
################################################################################
# data for r.h.s. scatter plot
################################################################################

datfDsNf4=[
[246.4,   .5,.5,3.7,3.7,  "FNAL/MILC 12B"            ,['s','l','g',0,tpos2]],
[248.7,   .2,.2,1.0,1.0,  "FNAL/MILC 13M"            ,['s','l','g',0,tpos2]],
[242.1,  8.3,8.3,8.3,8.3, "ETM 13M"            ,['s','l','g',0,tpos2]],
[247.2, 4.1 , 4.1, 4.1, 4.1, "ETM 14E"      ,['s','g','g',0,tpos2]],
[249.0, 0.3 , 0.3, 1.1, 1.5, "FNAL/MILC 14A"      ,['s','l','g',0,tpos2]],
[249.9, 0.3 , 0.3, 0.5, 0.5, "FNAL/MILC 17"      ,['s','g','g',0,tpos2]],
]
datfDsNf3=[
[249,	3,3,17,17,  "FNAL/MILC 05"        ,['s','w','r',0,tpos2]],
[241,   3,3,3,3,  "HPQCD/UKQCD 07"        ,['s','l','g',0,tpos2]],
[248.0, 1.5,1.5,2.5,2.5,  "HPQCD 10A", ['s','g','g',0,tpos2]],
[257,   2,2,6,6,  "PACS-CS 11"             ,['s','w','r',0,tpos2]],
[260.1, 2.5,2.5,10.8,10.8,  "FNAL/MILC 11"        ,['s','g','g',0,tpos2]],
[246.0,    .7,.7,3.6,3.6,   "HPQCD 12A"      ,['s','l','g',0,tpos2]],
[254,   2,2,4.5,4.5, "$\chi$QCD 14"    ,['s','g','g',0,tpos2]],
[246.4, 1.3,1.3,2.06,2.06, "RBC/UKQCD 17"          ,['s','g','g',0,tpos2]],
 [249,     7  , 7, 7  , 7 ,  "$\chi$QCD 20qma"         ,['s','w','r',0,tpos2]],
] 
datfDsNf2=[                          
[244,   8,8,8,8,  "ETM 09"              ,['s','l','g',0,tpos2]]  ,
[248,	6,6,6,6,  "ETM 11A" 		,['s','l','g',0,tpos2]]  ,
[250,   7,7,7,7,  "ETM 13B"              ,['s','g','g',0,tpos2]] ,
[247,   5,5,7.1,7.1,  "ALPHA 13B"              ,['s','l','g',0,tpos2]]  ,
[258.7,  1.1,1.1,3.1,3.1,  "TWQCD 14"              ,['s','w','r',0,tpos2]]  ,
[238,    5.4,5.4,5,5,      "Blossier 18"           ,['s','l','g',0,tpos2]]  ,
[244,    4.5,4.5,4,4,      "Balasubramamian 19net"           ,['s','g','g',0,tpos2]]  ,
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.



# first define FLAGaverage for l.h.s. plot, the one for the r.h.s. one follows below
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
[208,7,7,7,7,"our average for $\\rm N_f=2$"  ,['s','k','k',0,tpos],1],
[208.96,2.37,2.37,2.37,2.37,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[212.0,0.73,0.73,0.73,0.73,"our average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datfDNf2,datfDNf3,datfDNf4]

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 


# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
execfile('FLAGplot.py')


# now some definitions for the r.h.s. plot
plotnamestring	= "fDandfDs"			# filename for plot
logo            = 'none'
titlestring	= "$\\rm f_{D_s}$"			# plot title
xlimits		= [190,310]			# plot's xlimits
plotxticks	= ([[230,  250, 270]])# locations for x-ticks

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
[245.8,3.8,3.8,3.8,3.8,"our average for $\\rm N_f=2$"  ,['s','k','k',0,tpos2],1],
[248.04,1.63,1.63,1.63,1.63,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos2],0],
[249.9,0.5,0.5,0.5,0.5,"our average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos2],1],
]
yaxisstrings	= [				# x,y-location for y-strings
		   [+0,25.0,"$\\rm N_f=2+1+1$"],
		   [+0,14.0,"$\\rm N_f=2+1$"],
		   [+0,2.0,"$\\rm N_f=2$"]
		    ]

xaxisstrings	= [				# x,y-location for x-strings
		   [+292,-2.6,"MeV"] 	 		    
                   ]		    
 
# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datfDsNf2,datfDsNf3,datfDsNf4]

# do the r.h.s. plot
ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
execfile('FLAGplot.py')

################################################################################
# We are done
################################################################################


# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

