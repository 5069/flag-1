# FLAG plot for B_B for B and Bs
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm \\hat{B}_{B_d}$"		# plot title
plotnamestring	= "BB2"			# filename for plot
plotxticks	= ([[1.0,1.2,1.4]])# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

FS = 12

yaxisstrings	= [				# x,y-location for y-strings
		   [0.82, 9.8,"$\\rm N_f=2+1+1$"],
		   [0.82, 5.0,"$\\rm N_f=2+1$"],
		   [0.82, 0.0,"$\\rm N_f=2$"]
		    ]
xlimits		= [0.9, 1.75]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'

					        # move data labels of l.h.s side
						# plot to nirvana
#tpos  		= 1.5				# x-position for the dat
tpos   		= 1.54				# x-position for the data labels 
tpos2		= 20			# x-position for the data labels
						# here a reasonable choice
REV    		= 1				# reverse order of data points
#PRELIM 		= 1				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
						# one figure
SHIFT = -.1
REMOVEWHITESPACE=0
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

################################################################################
# data for l.h.s. scatter plot
################################################################################
datBd2=[
[1.30 ,0.05 ,0.05 ,0.06 ,0.06 ,"ETM 13B",          ['s','g','g',0,tpos]],
[1.32 ,0.08 ,0.08 ,0.08 ,0.08 ,"ETM 12A,12B",       ['s','l','g',0,tpos]]
] 

datBd2p1=[
[1.38,0.13,0.13,0.13,0.13,"FNAL/MILC 16", ['s','g','g',0,tpos]],
[1.17,0.26,0.26,0.11,0.11,"RBC/UKQCD 14A", ['s','g','g',0,tpos]],
[1.27,0.10,0.10,0.10,0.10,"HPQCD 09", ['s','g','g',0,tpos]],
[0.00,0.00,0.00,0.00,0.00,"HPQCD 06A", ['s','w','r',0,tpos]]
]

datBd2p1p1=[
[1.222,0.061,0.061,0.061,0.061,"HPQCD 19bea", ['s','g','g',0,tpos]]
]

################################################################################
# data for r.h.s. scatter plot
################################################################################
datBs2=[
[1.32 ,0.05 ,0.05 ,0.05 ,0.05 ,"ETM 13B",            ['s','g','g',0,tpos2]],
[1.36 ,0.08 ,0.08 ,0.08 ,0.08 ,"ETM 12A,12B",       ['s','l','g',0,tpos2]]
] 

datBs2p1=[
[1.443,0.100,0.100,0.100,0.100,"FNAL/MILC 16", ['s','g','g',0,tpos2]],
[1.22,0.20,0.20,0.09,0.09,"RBC/UKQCD 14A", ['s','g','g',0,tpos2]],
[1.33,0.05,0.05,0.06,0.06,"HPQCD 09", ['s','g','g',0,tpos2]],
[1.17,0.17,0.17,0.17,0.17,"HPQCD 06A", ['s','w','r',0,tpos2]]
]

datBs2p1p1=[
[1.232,0.053,0.053,0.053,0.053,"HPQCD 19bea", ['s','g','g',0,tpos2]]
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.



# first define FLAGaverage for l.h.s. plot, the one for the r.h.s. one follows below
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
# [np.nan,0.00,0.00,0.00,0.00,"place holde       ",['s','k','k',0,tpos],0],
 [1.30,0.06,0.06,0.06,0.06,"FLAG average for $\\rm N_f=2$",   ['s','k','k',0,tpos],0],
 [1.30,0.10,0.10,0.10,0.10,"FLAG average for $\\rm N_f=2+1$",   ['s','k','k',0,tpos],0],
 [1.222,0.061,0.061,0.061,0.061,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datBd2,datBd2p1,datBd2p1p1]

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 


# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
# execfile('FLAGplot.py')
exec(open("./FLAGplot.py").read())
logo='none'

# now some definitions for the r.h.s. plot
plotnamestring	= "BB2"			# filename for plot

titlestring	= "$\\rm \\hat{B}_{B_s}$"		# plot title
xlimits		= [0.70, 1.55]			# plot's xlimits
plotxticks	= ([[1.0,1.2,1.4]])# locations for x-ticks
nminorticks	= 1			# number of intermediate minor ticks
					# 0 if none
SHIFT = -.1
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
# [np.nan,0.00,0.00,0.00,0.00,"place holde       ",['s','k','k',0,tpos2],0], 
 [1.32,0.05,0.05,0.05,0.05," for $\\rm N_f=2$",   ['s','k','k',0,tpos2],0],
 [1.35,0.06,0.06,0.06,0.06,"our average for $\\rm N_f=2+1$",   ['s','k','k',0,tpos2],0],
 [1.232,0.053,0.053,0.053,0.053,"for $\\rm N_f=2+1+1",['s','k','k',0,tpos2],0], 
]
yaxisstrings	= [				# x,y-location for y-strings
		   [-1, 10.,"$\\rm N_f=2+1+1$"],
		   [-1, 5.0,"$\\rm N_f=2+1$"],
		   [-1, 0.5,"$\\rm N_f=2$"]
		    ]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datBs2,datBs2p1,datBs2p1p1]

# do the r.h.s. plot
ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
# execfile('FLAGplot.py')
exec(open("./FLAGplot.py").read())

################################################################################
# We are done
################################################################################


# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

