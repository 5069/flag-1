# FLAG plot for RFKpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm f_{D_s}/f_D\; $"			# plot title
plotnamestring	= "fDratio"			# filename for plot
plotxticks	= ([[ 1.10, 1.15,      1.20,   1.25  ]])# locations for x-ticks

yaxisstrings	= [				# x,y-location for y-strings
                   [+1.065,23.3,"$\\rm N_f=2+1+1$"],
		   [+1.065,11.3,"$\\rm N_f=2+1$"],
		   [+1.065,1.8,"$\\rm N_f=2$"]
		    ]
xlimits		= [1.08,1.50]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.32					# x-position for the data labels

FS = 14

SHIFT = -0.1
REMOVEWHITESPACE=0
# column        item
# 0             central value
# 1             neg. error 1
# 2             pos. error 1
# 3             neg. error 2
# 4             pos. error 2
# 5             Collaboration string
# 6             this column contains layout parameters for the plot-symbol and
#               and the collaboration text:
#               column  item
#               0       marker style (see bottom of this file for a full list)
#               1       marker face color r=red, b=blue, k=black, w=white
#                                         g=green
#               2       marker color (errorbar and frame), color coding as
#                       for face color
#               3       color intensity 0=full, 1=soso, 2=bleak
#               4       position of the collaboration string on the x-axis
dat2p1p1=[
[1.175,   0.016,0.016,0.019,0.019,  "FNAL/MILC 12B"            ,['s','l','g',0,tpos]],
[1.1714,   0.0010,0.0010,0.0025,0.0025,  "FNAL/MILC 13"            ,['s','l','g',0,tpos]],
[1.199,   0.025,0.025,0.025,0.025,  "ETM 13F"                  ,['s','l','g',0,tpos]],
[1.192,   0.022,0.022,0.022,0.022,  "ETM 14E"                  ,['s','g','g',0,tpos]],
[1.1745,   0.00335,0.00335,0.001,0.001,  "FNAL/MILC 14A"            ,['s','l','g',0,tpos]],
[1.1782,   0.0016,0.0016,0.0016,0.0016,  "FNAL/MILC 17 "            ,['s','g','g',0,tpos]],
]
dat2p1=[
[1.24,	0.01,0.01,0.07,0.07,  "FNAL/MILC 05"        ,['s','w','r',0,tpos]],
[1.164, 0.011,0.011,0.011,0.011,  "HPQCD/UKQCD 07"        ,['s','l','g',0,tpos]],
[1.14,  0.03,0.03,0.03,0.03,  "PACS-CS 11"                 ,['s','w','r',0,tpos]],
[1.188, .004,.004,.025,.025,  "FNAL/MILC 11"        ,['s','g','g',0,tpos]],
[1.187, .004,.004,.013,.013,   "HPQCD 12A"      ,['s','g','g',0,tpos]],
[1.1667, .0077, .0077, .0095, .0095, "RBC/UKQCD 17" ,['s','g','g',0,tpos]],
[1.1740, .0051, .0051, .0085, .0085, "RBC/UKQCD 18knm" ,['s','l','g',0,tpos]],
[1.16,   .03  , .03, .03  , .03 ,  "$\chi$QCD 20qma"         ,['s','w','r',0,tpos]],
] 
dat2=[                      
[1.24,  0.03,0.03,0.03,0.03,  "ETM 09"          ,['s','l','g',0,tpos]],
[1.17,	0.05,0.05,0.05,0.05,  "ETM 11A" 		,['s','l','g',0,tpos]]  ,
[1.20,  0.007,0.007,0.02,0.02,  "ETM 13B"           	,['s','g','g',0,tpos]],
[1.14,  0.02,0.02,0.036,0.036, "ALPHA 13B",     ['s','l','g',0,tpos]],
[1.2788, 0.0264, 0.0264, 0.0264, 0.0264, "TWQCD 14",     ['s','w','r',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [1.20,0.02,0.02,0.02,0.02,"our average for $\\rm N_f=2$"  ,['s','k','k',0,tpos],1],
 [1.1742,0.0073,0.0073,0.0073,0.0073,"our average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
 [1.1783,0.0016,0.0016,0.0016,0.0016,"our average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]
#datasets=[dat2,dat2p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

