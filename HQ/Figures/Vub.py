# FLAG plot for RFKpi
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm |V_{ub}| x 10^3$"			# plot title
plotnamestring	= "Vub"			# filename for plot
plotxticks	= ([[3,3.5,4,4.5,5,5.5,6]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
                   [2.35,15.0,"$\\rm N_f=2+1+1$"],
    		   [2.35,8.0,"$\\rm N_f=2+1$"],
		   [2.35,2.6,"$\\rm N_f=2$"],
		   [2.35,0.5,"$\\rm non-latt.$"]
		    ]
xlimits		= [2.5,8.0]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 5.0					# x-position for the data labels
REMOVEWHITESPACE=0
SHIFT=-0.1
FS = 16
# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
#dat2p1p1=[
#[1.224,	0.013,0.013,0.013,0.013,  "ETM 10E$^+$"        	,['*','r','r',0,tpos]],
#[1.187,	0.004,0.004,0.000,0.000,  "MILC 11$^+$" 	,['s','r','r',0,tpos]]
#]
datOther=[
[4.52,	0.15,0.15,0.186,0.205,  "HFLAV inclusive (GGOU)" 	        ,['o','b','b',0,tpos]] 
,
]
dat2p1p1=[
[4.05,0.03,0.03,0.64,0.64,  "                   $B\\to\\tau\\nu$ (average)"		,['s','g','g',0,tpos]],
[3.79,0.03,0.03,0.47,0.47,  "                   $B\\to\\tau\\nu$ (Belle)"		,['s','g','g',0,tpos]],
[5.32,0.04,0.04,0.74,0.74,  "                   $B\\to\\tau\\nu$ (BaBar)"		,['s','g','g',0,tpos]]
]
dat2p1=[
[4.01,0.09,0.09,0.64,0.64,  "                   $B\\to\\tau\\nu$ (average)"		,['s','g','g',0,tpos]],
[3.75,0.08,0.08,0.48,0.48,  "                   $B\\to\\tau\\nu$ (Belle)"		,['s','g','g',0,tpos]],
[5.26,0.12,0.12,0.74,0.74,  "                   $B\\to\\tau\\nu$ (BaBar)"		,['s','g','g',0,tpos]],
[3.73,0.14,0.14,0.14,0.14,  "                   $B\\to\\pi\\ell\\nu$"		        ,['^','g','g',0,tpos]]
]
dat2=[
[4.10,0.15,0.15,0.66,0.66,  "                   $B\\to\\tau\\nu$ (average)"		,['s','g','g',0,tpos]],
[3.83,0.14,0.14,0.50,0.50,  "                   $B\\to\\tau\\nu$ (Belle)"		,['s','g','g',0,tpos]],
[5.37,0.77,0.77,0.77,0.77,  "                   $B\\to\\tau\\nu$ (BaBar)"		,['s','g','g',0,tpos]]
]
#[
#[4.05,0.51,0.51,0.55,0.55,  "$B\\to\\tau\\nu$"		,['s','g','g',0,tpos]]
#]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      #
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
 [3.73,0.14,0.14,0.00,0.00,"FLAG estimate for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datOther,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker
