#!/bin/bash

gnuplot < ff_Bpi_latt.gpl
gnuplot < ff_BsK_latt.gpl
gnuplot < ff_BD_latt.gpl
gnuplot < ff_Bpi_latt+exp.gpl
gnuplot < ff_Bpi_latt+exp_nof0.gpl
gnuplot < ff_BD_latt+exp.gpl
gnuplot < ff_BD_latt+exp_nof0.gpl
gnuplot < ff_BK_latt.gpl
gnuplot < ff_BK_latt_p0.gpl
gnuplot < ff_BK_latt_t.gpl

