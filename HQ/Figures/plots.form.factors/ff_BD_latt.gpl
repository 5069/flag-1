set term tikz standalone color size 5in,3in font ",10" preamble '\renewcommand{\familydefault}{\sfdefault}'
#header "\\usepackage{times}"
set format xy "$%g$"
set output "ff_BD_latt.tex"

set key spacing 1.2 font ",8"

set format x "%.2f"
set format y "%.1f"

set xlabel "$z(q^2,t_{\\rm opt})$"
set ylabel "$B(q^2)\\phi(q^2)f^{B \\to D}(q^2)$" offset -1,0,0

set xrange [-0.035:0.035]
set yrange [0.6:1.3]


set style line  1 lc rgb '#ff8a8a' lt 1 lw 2 ps 1.4 pt 7  # red, full circles
set style line  2 lc rgb '#8a8aff' lt 1 lw 2 ps 1.4 pt 7  # blue, full circles
set style line  3 lc rgb '#999999' lt 1 lw 2 ps 1.4 pt 7  # grey, full circles
set style line  4 lc rgb '#ffa07a' lt 1 lw 2 ps 1.4 pt 7  # light salmon, full circles
set style line  5 lc rgb '#008b22' lt 1 lw 2 ps 1.4 pt 7  # xmgrace's green, full circles
set style line  6 lc rgb '#008b22' lt 1 lw 2 ps 1.3 pt 5  # xmgrace's green, full squares
set style line  7 lc rgb '#008b22' lt 1 lw 2 ps 1.5 pt 9  # xmgrace's green, full triangles
set style line  8 lc rgb '#008b22' lt 1 lw 2 ps 1.4 pt 6  # xmgrace's green, open circles
set style line  9 lc rgb '#008b22' lt 1 lw 2 ps 1.3 pt 4  # xmgrace's green, open squares
set style line 10 lc rgb '#008b22' lt 1 lw 2 ps 1.5 pt 8  # xmgrace's green, open triangles

set style fill transparent solid 0.5 noborder 

set label at screen 0.087,0.97 '\includegraphics[width=0.8in]{FLAG_Logo.eps}'

plot \
'f0_BD_latt_band.dat' using ($1):($2-$3):($2+$3) with filledcurves title '$f_0$ average' ls 4, \
'fp_BD_latt_band.dat' using ($1):($2-$3):($2+$3) with filledcurves title '$f_+$ average' ls 3, \
'latt_fp_BD_hpqcd.dat' using 1:2:3 with errorbars title '$f_+$ HPQCD 15' ls 6, \
'latt_fp_BD_fnal.dat' using 1:2:3 with errorbars title '$f_+$ FNAL/MILC 15C' ls 5, \
'latt_f0_BD_hpqcd.dat' using 1:2:3 with errorbars title '$f_0$ HPQCD 15' ls 9, \
'latt_f0_BD_fnal.dat' using 1:2:3 with errorbars title '$f_0$ FNAL/MILC 15C' ls 8


# compile and exit

set output
system('pdflatex ff_BD_latt.tex && rm ff_BD_latt.aux ff_BD_latt.log ff_BD_latt.tex')
system('pdf2ps ff_BD_latt.pdf && ps2eps -f ff_BD_latt.ps && rm ff_BD_latt.ps')
system('convert -density 600 ff_BD_latt.pdf -quality 100 ff_BD_latt.png')
exit
