%=================================================
\section{$B$-meson decay constants, mixing parameters and form factors}
\label{sec:BDecays}
Authors: Y.~Aoki, D.~Be\v{c}irevi\'c, M.~Della~Morte, E.~Lunghi, S.~Meinel, C.~Monahan, C.~Pena\\
%=================================================

The (semi)leptonic decay and mixing processes of $B_{(s)}$ mesons have been playing
a crucial role in flavour physics.   In particular, they contain
important information for the investigation of the $b{-}d$ unitarity
triangle in the Cabibbo-Kobayashi-Maskawa (CKM) matrix, and can be
ideal probes of physics beyond the Standard Model.
The charged-current decay channels $B^{+} \rightarrow l^{+}
\nu_{l}$ and $B^{0} \rightarrow \pi^{-} l^{+} \nu_{l}$, where $l^{+}$
is a charged lepton with $\nu_{l}$ being the corresponding neutrino, are
essential in extracting the CKM matrix element $|V_{ub}|$.  Similarly,
the $B$ to $D^{(\ast)}$ semileptonic transitions can be used to
determine $|V_{cb}|$.   The flavour-changing neutral current (FCNC)
processes, such as $B\to K^{(*)} \ell^+
\ell^-$ and $B_{d(s)} \to \ell^+ \ell^-$,  occur only beyond the tree level in weak interactions and are suppressed in the Standard
Model. Therefore, these processes can be sensitive to new
physics, since heavy particles can contribute to the loop diagrams.
They are also suitable channels for the extraction of the CKM matrix
elements involving the top quark which can appear in the loop.
%
The decays $B\to D^{(*)}\ell\nu$ and $B\to K^{(*)} \ell\ell$ can also be used 
to test lepton flavour universality by comparing results for $\ell = e$, $\mu$ and $\tau$. 
In particular, anomalies have been seen in the ratios $R(D^{(*)}) = {\cal B} (B\to D^{(*)}\tau\nu) /{\cal B} (B\to D^{(*)}\ell\nu)_{\ell=e,\mu}$ and ${R}(K^{(*)}) = {\cal B} (B\to K^{(*)}\mu\mu) /{\cal B} (B\to K^{(*)}ee)$.
%
In addition, the neutral $B_{d(s)}$-meson mixings are FCNC processes and
are dominated by the 1-loop ``box'' diagrams containing the top quark
and the $W$ bosons.  Thus, using the experimentally measured neutral $B^0_{d(s)}$-meson oscillation
frequencies, $\Delta M_{d(s)}$, and the theoretical calculations for
the relevant hadronic mixing matrix elements, one can obtain
$|V_{td}|$ and $|V_{ts}|$ in the Standard Model.\footnote{The neutral
  $B$-meson leptonic
  decays, $B_{d,s} \to \mu^{+} \mu^{-}$, were initially observed at
  the LHC experiments, and the corresponding branching fractions were
  obtained by combining the data from the CMS and the LHCb
  collaborations~\cite{CMS:2014xfa}, resulting in some tension with the SM
  prediction.  More recently, the LHCb collaboration~\cite{Aaij:2017vad}
   has improved the 
  measurement of $B^0_s\to\mu^+\mu^-$ and provided a bound on $B^0\to\mu^+\mu^-$  that eliminate the tension with the SM.
  Nevertheless, the errors of these experimental
  results are currently too large to enable a precise determination of
  $|V_{td}|$ and $|V_{ts}|$.}

Accommodating the light quarks and the $b$ quark simultaneously in
lattice-QCD computations is a challenging endeavour. To incorporate
the pion and the $b$ hadrons with their physical masses, the simulations have to be performed using the lattice
size $\hat{L} = L/a \sim \cO(10^{2})$, where $a$ is the lattice spacing and $L$
is the physical (dimensionful) box size.   
The most ambitious calculations are now using such volumes; 
however, many ensembles are smaller.
Therefore, in addition to employing Chiral Perturbation Theory for the extrapolations in the
light-quark mass, current lattice calculations for quantities involving
$b$ hadrons often make use of effective theories that allow one to
expand in inverse powers of $m_{b}$. In this regard, two general
approaches are widely adopted.  On the one hand, effective field theories
such as Heavy-Quark Effective Theory (HQET) and Nonrelativistic
QCD (NRQCD) can be directly implemented in numerical computations. On
the other hand, a relativistic quark action can be improved {\it \`{a} la}
Symanzik to suppress cutoff errors, and then re-interpreted in a manner
that is suitable for heavy-quark physics calculations.   
This latter strategy is often referred to as the method of the Relativistic
Heavy-Quark Action (RHQA).
The utilization of such effective theories inevitably introduces systematic
uncertainties that are not present in light-quark calculations.  These
uncertainties 
can arise from the truncation of the expansion in constructing the
effective theories (as in HQET and NRQCD),
or from more intricate
cutoff effects (as in NRQCD and RQHA).  They can also be introduced
through more complicated renormalization
procedures which often lead to significant systematic effects in
matching the lattice operators to their continuum counterparts.  For
instance, due to the use of different actions for the heavy and the
light quarks, it is more difficult to construct absolutely 
normalized bottom-light currents.  

Complementary to the above ``effective theory approaches'', 
another popular method is to simulate the heavy and the light quarks
using the same (normally improved) lattice action at several values of
the heavy-quark mass $m_{h}$ with $a m_{h} < 1$ and $m_{h} < m_{b}$.   
This enables one to employ HQET-inspired relations to extrapolate the
computed quantities to the physical $b$ mass.  When combined with
results obtained in the static heavy-quark limit, this approach can be
rendered into an interpolation, instead of extrapolation, in
$m_{h}$. The discretization errors are the main source of the
systematic effects in this method, and very small lattice spacings are
needed to keep such errors under control.

In recent years, it has also been
possible to perform lattice simulations at very fine lattice
spacings and treat heavy quarks as
fully relativistic fermions without resorting to effective field
theories.  
Such simulations are of course very demanding in computing
resources.  

Because of the challenge described above, the efforts that have been
made to obtain reliable, accurate lattice-QCD results for physics of the $b$ quark
have been enormous.   These efforts include significant theoretical progress in
formulating QCD with heavy quarks on the lattice. This aspect is
briefly reviewed in Appendix~\ref{app:HQactions}.

In this section, we summarize the results of the $B$-meson leptonic
decay constants, the neutral $B$-mixing parameters, and the
semileptonic form factors, from lattice QCD.  To be focused on the
calculations that have strong phenomenological impact, we limit the
review to results based on modern simulations containing dynamical
fermions with reasonably light pion masses (below
approximately 500~MeV).
There has been significant progress for $b$-quark 
physics
since the
previous review.  There are also a number of calculations that are still
in a preliminary stage.  We have made note of some of these in anticipation
of later publications, whose results will contribute to future averages.

Following our review of $B_{(s)}$-meson
leptonic decay constants, the neutral $B$-meson mixing parameters, and
semileptonic form factors, we then interpret our results within the
context of the Standard Model.  We combine our best-determined values
of the hadronic matrix elements with the most recent
experimentally-measured branching fractions to obtain $|V_{ub}|$
and  $|V_{cb}|$,
and compare these results to those obtained from inclusive
semileptonic $B$ decays.

Recent lattice-QCD averages for $B^+$- and $B_s$-meson decay constants
were also presented by the Particle Data Group (PDG) in~Ref.~\cite{Rosner:2015wva}.  The PDG three-
and four-flavour averages 
for these quantities differ from those quoted here because the PDG
provides the charged-meson decay constant $f_{B^+}$, while we present 
the isospin-averaged meson-decay constant $f_B$.

\input{HQ/HQSubsections/BL.tex}


\input{HQ/HQSubsections/BB.tex}

\input{HQ/HQSubsections/BSL.tex}

\input{HQ/HQSubsections/BCKM.tex}

