\subsection{Determinations of $|V_{cd}|$ and $|V_{cs}|$ and test of  second-row CKM unitarity}
\label{sec:Vcd}

We now interpret the lattice-QCD results for the $D_{(s)}$ meson decays
as determinations of the CKM
matrix elements $|V_{cd}|$ and $|V_{cs}|$ in the Standard Model.

For the leptonic decays, we use the latest experimental averages from
Rosner, Stone and Van de Water for the Particle Data Group~\cite{Tanabashi:2018oca}
%
\begin{equation}
	f_D |V_{cd}| = 45.91(1.05)~{\rm MeV} \,, \qquad f_{D_s} |V_{cs}| = 250.9(4.0)~{\rm MeV} \,.
\end{equation}
%
By combining these with the average values of $f_D$ and $f_{D_s}$ from
the individual $N_f = 2$, $N_f = 2+1$ and $N_f=2+1+1$ lattice-QCD 
calculations that
satisfy the FLAG criteria, we obtain the results for the CKM
matrix elements $|V_{cd}|$ and $|V_{cs}|$ in
Tab.~\ref{tab:VcdVcsIndividual}.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
For our preferred values we use the
averaged $N_f=2$ and $N_f = 2+1$ results for $f_D$ and $f_{D_s}$ in
Eqs.~(\ref{eq:fD2}-\ref{eq:fDratio2+1+1}).
We obtain
%
\begin{align}
&{\rm leptonic~decays}, N_f=2+1+1:&|V_{cd}| &= 0.2166(7)(50)\,, &|V_{cs}| &= 1.004  (2)(16) \,, \\
&{\rm leptonic~decays}, N_f=2+1:  &|V_{cd}| &= 0.2197(25)(50)\,, &|V_{cs}| &= 1.012  (7)(16) \,, \\
&{\rm leptonic~decays}, N_f=2:    &|V_{cd}| &= 0.2207(74)(50)\,, &|V_{cs}| &= 1.035 (25)(16) \,,
\end{align}
%
where the errors shown are from the lattice calculation and experiment
(plus nonlattice theory), respectively.  For the $N_f = 2+1$ and the $N_f=2+1+1$
determinations, the uncertainties from the lattice-QCD calculations of
the decay constants are smaller than the
experimental uncertainties in the branching fractions.
Although the results for
$|V_{cs}|$ are slightly larger than one, they are  consistent with
unity within at most 1.5 standard deviations.

The leptonic determinations of these CKM matrix elements have uncertainties that are reaching the few-percent level.
However, higher-order electroweak and hadronic-structure dependent corrections to the rate have not been computed for the case of $D_{(s)}$ mesons,
whereas they have been estimated to be around 1--2\% for pion and kaon decays~\cite{Cirigliano:2007ga}. 
It is therefore important that such theoretical calculations are tackled soon, perhaps directly on the lattice, as proposed
in Ref.~\cite{Carrasco:2015xwa}. 
\begin{table}[tb]
\begin{center}
\noindent
%\footnotesize
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}lrlcr}
Collaboration & Ref. &$\Nf$&from&\rule{0.8cm}{0cm}$|V_{cd}|$ or $|V_{cs}|$\\
&&&& \\[-2ex]
\hline \hline &&&&\\[-2ex]
FNAL/MILC~17 & \cite{Bazavov:2017lyh} & 2+1+1 & $ f_D$ & 0.2165(6)(50) \\
ETM~17D/Riggio 17 & \cite{Lubicz:2017syv,Riggio:2017zwh} & 2+1+1 & $D\to\pi\ell\nu$ & 0.2341(74) \\
ETM~14E & \cite{Carrasco:2014poa} &  2+1+1 & $ f_D$ & 0.2214(41)(51) \\
RBC/UKQCD~17 & \cite{Boyle:2017jwu} & 2+1 & $f_D$ & 0.2200(36)(50) \\
HPQCD 12A & \cite{Na:2012iu} & 2+1 & $f_{D}$  & 0.2204(36)(50) \\
HPQCD 11 & \cite{Na:2011mc} & 2+1 & $D \to \pi \ell \nu$  & 0.2140(93)(29) \\
FNAL/MILC 11  & \cite{Bazavov:2011aa} & 2+1 & $f_{D}$  &  0.2097(108)(48)  \\
ETM 13B  & \cite{Carrasco:2013zta} & 2 & $f_{D}$  &  0.2207(74)(50)  \\
&&&& \\[-2ex]
 \hline
&&&& \\[-2ex]
FNAL/MILC~17 & \cite{Bazavov:2017lyh} & 2+1+1 & $ f_{D_s}$ & 1.004(2)(16) \\
ETM~17D/Riggio 17 & \cite{Lubicz:2017syv,Riggio:2017zwh} & 2+1+1 & $D\to K\ell\nu$ & 0.970(33) \\
ETM~14E & \cite{Carrasco:2014poa} &  2+1+1 & $ f_{D_s}$ & 1.015(17)(16) \\
RBC/UKQCD~17 & \cite{Boyle:2017jwu} & 2+1 & $f_{D_s}$ & 1.018(9)(16) \\
Meinel~16 & \cite{Meinel:2016dqj} & 2+1 & $\Lambda_c\to\Lambda\ell\nu$ & 0.949(24)(51) \\
$\chi$QCD~14 & \cite{Yang:2014sea} & 2+1 & $f_{D_s}$  &  0.988(17)(16) \\
FNAL/MILC 11 & \cite{Bazavov:2011aa} & 2+1 & $f_{D_s}$  &  0.965(40)(16) \\
HPQCD 10A & \cite{Davies:2010ip} & 2+1 & $f_{D_s}$  & 1.012(10)(16)  \\
HPQCD 10B & \cite{Na:2010uf} & 2+1 & $D \to K \ell \nu$  & 0.975(25)(7) \\
Blossier 18 & \cite{Blossier:2018jol} & 2 &  $f_{D_s}$  &  1.054(24)(17) \\
ETM 13B & \cite{Carrasco:2013zta} & 2 & $f_{D_s}$  &  1.004(28)(16) \\
&&&& \\[-2ex]
 \hline \hline 
\end{tabular*}
\caption{Determinations of $|V_{cd}|$ (upper panel) and $|V_{cs}|$
  (lower panel) obtained from lattice calculations of $D$-meson
  leptonic decay constants
 and semileptonic form factors.
The errors
  shown are from the lattice calculation and experiment (plus
  nonlattice theory), respectively, save for ETM~17D/Riggio 17,
  where the joint fit to lattice and experimental data does
  not provide a separation of the two sources of error (although
  the latter is still largely theory-dominated). \label{tab:VcdVcsIndividual}}
\end{center}
\end{table}


\vskip 5mm

For $D$ meson semileptonic decays, there is no update on the lattice side
from the previous version of our review for $N_f=2+1$, where the only
works entering the FLAG averages are HPQCD~10B/11~\cite{Na:2010uf,Na:2011mc},
that provide values for $f_+^{DK}(0)$ and $f_+^{D\pi}(0)$, respectively,
cf. Eq.~(\ref{eq:Nf=2p1Dsemi}).
The latter can be combined with the latest experimental averages
from the HFLAV collaboration~\cite{Amhis:2016xyh}:
%
\begin{equation}
\label{eq:fpDtoPiandKexp}
	f_+^{D\pi}(0) |V_{cd}| =  0.1426(19) \,, \qquad f_+^{DK}(0) |V_{cs}| =  0.7226(34)  \,,
\end{equation}
%
where we have combined the experimental statistical and systematic errors in quadrature,
to determine the CKM parameters.

The new $N_f=2+1+1$ result for form factors in ETM 17D~\cite{Lubicz:2017syv} has a broader scope,
in that a companion paper~\cite{Riggio:2017zwh}
provides a determination of $|V_{cd}|$ and $|V_{cs}|$ from a joint fit
to lattice and experimental data. This procedure is a priori preferable
to the matching at $q^2=0$, and we will therefore use the values in Ref.~\cite{Riggio:2017zwh}
for our CKM averages. It has to be stressed that this entails a measure of
bias in the comparison with the above $N_f=2+1$ result; to quantify
the effect, we also show in Fig.~\ref{fig:VcdVcs} the values of $|V_{cd}|$ and $|V_{cs}|$
obtained by using the values for $f_+(0)$ quoted in~\cite{Lubicz:2017syv},
cf. Eq.~(\ref{eq:Nf=2p1p1Dsemi}), together with Eq.~(\ref{eq:fpDtoPiandKexp}).

Finally, Meinel 16 has determined the form factors for $\Lambda_c\to\Lambda\ell\nu$
decays for $N_f=2+1$, which results in a determination of  $|V_{cs}|$ in combination with the
experimental measurement of the branching fractions for the $e^+$ and $\mu^+$ channels
in Refs.~\cite{Ablikim:2015prg,Ablikim:2016vqd}.
In Ref.~\cite{Meinel:2016dqj} the value $|V_{cs}|=0.949(24)(14)(49)$ is quoted, where
the first error comes from the lattice computation, the second from the $\Lambda_c$ lifetime,
and the third from the branching fraction of the decay.
While the lattice uncertainty is competitive with meson channels,
the experimental uncertainty is far larger.

We thus proceed to quote our estimates from semileptonic decay as
%
\begin{align}
&& |V_{cd}| &=  0.2141(93)(29) &&\Ref~\mbox{\cite{Na:2011mc}},\nonumber\\[-0mm]
&\mbox{SL~averages~for}~N_f=2+1:&\label{eq:Nf=2p1VcdVcsSL}\\[-6mm]
&& |V_{cs}|(D) &=  0.967(25)(5) &&\Ref~\mbox{\cite{Na:2010uf}},\nonumber\\[-0mm]
&& |V_{cs}|(\Lambda_c) &=  0.949(24)(51) &&\Ref~\mbox{\cite{Meinel:2016dqj}},\nonumber\\[3mm]
&& |V_{cd}| &=  0.2341(74) &&\Refs~\mbox{\cite{Lubicz:2017syv,Riggio:2017zwh}},\nonumber\\[-3mm]
&\mbox{SL~averages~for}~N_f=2+1+1:&\label{eq:Nf=2p1p1VcdVcsSL}\\[-3mm]
&& |V_{cs}| &=  0.970(33) &&\Refs~\mbox{\cite{Lubicz:2017syv,Riggio:2017zwh}},\nonumber
\end{align}
%
where the errors for $N_f=2+1$ are lattice and experimental (plus nonlattice theory), respectively.
It has to be stressed that all errors are largely theory-dominated.
The above values are compared
with individual leptonic determinations in Tab.~\ref{tab:VcdVcsIndividual}.

\vskip 5mm


In Tab.~\ref{tab:VcdVcsSummary} we summarize the results for $|V_{cd}|$
and $|V_{cs}|$ from leptonic 
and semileptonic
 decays, and compare
them to determinations from neutrino scattering (for $|V_{cd}|$ only)
and CKM unitarity.  These results are also plotted in
Fig.~\ref{fig:VcdVcs}.  
For both $|V_{cd}|$ and $|V_{cs}|$, the errors in the direct determinations from
leptonic 
and semileptonic 
decays are approximately one order of magnitude larger
than the indirect determination from CKM unitarity.
The direct and indirect determinations are still always
compatible within at most $1.2\sigma$, save for the leptonic
determinations of $|V_{cs}|$---that show a $\sim 2\sigma$ deviation
for all values of $N_f$---and $|V_{cd}|$ using the $N_f=2+1+1$ lattice result,
where the difference is $1.8\sigma$.


In order to provide final estimates, we average all the available results
separately for each value of $N_f$. In all cases, we assume that results
that share a significant fraction of the underlying gauge ensembles
have statistical errors that are 100\% correlated; the same applies to
the heavy-quark discretization and scale setting errors in HPQCD calculations of leptonic and semileptonic
decays.  Finally, we include a 100\% correlation in the fraction of the error
of $|V_{cd(s)}|$ leptonic determinations that comes from the experimental input,
to avoid an artificial reduction of the experimental uncertainty in the averages.
We finally quote
%
%FLAGRESULT BEGIN
% TAG      & Vcd    & Vcs     &END
% REFS     & \cite{Lubicz:2017syv,Riggio:2017zwh,Bazavov:2017lyh,Carrasco:2014poa,Boyle:2017jwu,Na:2012iu,Na:2011mc,Bazavov:2011aa,Carrasco:2013zta} &\cite{Lubicz:2017syv,Riggio:2017zwh,Bazavov:2017lyh,Carrasco:2014poa,Boyle:2017jwu,Meinel:2016dqj,Yang:2014sea,Bazavov:2011aa,Davies:2010ip,Na:2010uf,Blossier:2018jol,Carrasco:2013zta} &END
% UNITS    & 1 & 1 &END
% NUMRESULTS & 3 & 3 &END
% FLAVOURs & 2 & 2+1 & 2+1+1 &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
%
\begin{align}
&{\rm our~average}, N_f=2+1+1:&\FLAGAVBEGIN |V_{cd}| &= 0.2219(43) \FLAGAVEND\,,&\FLAGAVBEGIN |V_{cs}| &= 1.002(14) \FLAGAVEND\,, \\
&{\rm our~average}, N_f=2+1:  &\FLAGAVBEGIN |V_{cd}| &= 0.2182(50) \FLAGAVEND\,,&\FLAGAVBEGIN |V_{cs}| &= 0.999(14) \FLAGAVEND\,, \\
&{\rm our~average}, N_f=2:    &\FLAGAVBEGIN |V_{cd}| &= 0.2207(89) \FLAGAVEND\,,&\FLAGAVBEGIN |V_{cs}| &= 1.031(30) \FLAGAVEND\,, 
\label{eq:Vcdsfinal}
\end{align}
%
%FLAGRESULTFORMULA END
where the errors include both theoretical and experimental
uncertainties. These averages also appear in Fig.~\ref{fig:VcdVcs}.
The mutual consistency between the various lattice results is always
good, save for the case of $|V_{cd}|$ with $N_f=2+1+1$, where a $\sim 2\sigma$
tension between the leptonic and semileptonic determinations shows up. 
Currently, the leptonic and semileptonic determinations of $V_{cd}$
are controlled by experimental and lattice uncertainties, respectively. The leptonic error will 
be reduced by Belle~II and BES~III. It would be valuable to have other lattice calculations of the 
semileptonic form factors.


Using the lattice determinations of $|V_{cd}|$ and $|V_{cs}|$ in
Tab.~\ref{tab:VcdVcsSummary}, we can test the unitarity of the second row
of the CKM matrix.  We obtain
%
\begin{align}
&N_f=2+1+1:   &|V_{cd}|^2 + |V_{cs}|^2 + |V_{cb}|^2 - 1 &= 0.05(3) \,,\\  
&N_f=2+1:     &|V_{cd}|^2 + |V_{cs}|^2 + |V_{cb}|^2 - 1 &= 0.05(3) \,,  \\
&N_f=2:       &|V_{cd}|^2 + |V_{cs}|^2 + |V_{cb}|^2 - 1 &= 0.11(6) \,.  
\end{align}
%
Again, tensions at the 2$\sigma$ level with CKM unitarity are visible, as also
reported in the PDG review~\cite{Rosner:2015wva}, where the value 0.063(34) is quoted for the quantity in the equations above.
Given the
current level of precision, this result does not depend on 
 $|V_{cb}|$, which is of $\cO(10^{-2})$. 
%[see Eq.~(\ref{eq:VcbNf2p1})].

\begin{table}[tb]
\begin{center}
\noindent
%\footnotesize
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}lcrcc}
& from & Ref. &\rule{0.8cm}{0cm}$|V_{cd}|$ & \rule{0.8cm}{0cm}$|V_{cs}|$\\
&& \\[-2ex]
\hline \hline &&\\[-2ex]
$N_f = 2+1+1$ &  $f_D$ \& $f_{D_s}$ && 0.2166(50) & 1.004(16) \\
$N_f = 2+1$   &  $f_D$ \& $f_{D_s}$ && 0.2197(56) & 1.012(17) \\
$N_f = 2$     &  $f_D$ \& $f_{D_s}$ && 0.2207(89) & 1.035(30) \\
&& \\[-2ex]
 \hline
&& \\[-2ex]
$N_f = 2+1+1$ & $D \to \pi \ell\nu$ and $D\to K \ell\nu$ && 0.2341(74) & 0.970(33) \\
$N_f = 2+1$   & $D \to \pi \ell\nu$ and $D\to K \ell\nu$ && 0.2141(97) & 0.967(25) \\
$N_f = 2+1$   & $\Lambda_c \to \Lambda\ell\nu$           &&    n/a     & 0.949(56) \\
&& \\[-2ex]
 \hline
&& \\[-2ex]
PDG & neutrino scattering & \cite{Tanabashi:2018oca} & 0.230(11)&  \\
Rosner 15 ({\it for the} PDG) & CKM unitarity & \cite{Rosner:2015wva} & 0.2254(7) & 0.9733(2) \\
&& \\[-2ex]
 \hline \hline 
\end{tabular*}
\caption{Comparison of determinations of $|V_{cd}|$ and $|V_{cs}|$
  obtained from lattice methods with nonlattice determinations and
  the Standard Model prediction assuming CKM
  unitarity.
\label{tab:VcdVcsSummary}}
\end{center}
\end{table}

\begin{figure}[h]

\begin{center}
\includegraphics[width=0.7\linewidth]{HQ/Figures/VcdandVcs}

\vspace{-2mm}
\caption{Comparison of determinations of $|V_{cd}|$ and $|V_{cs}|$
  obtained from lattice methods with nonlattice determinations and
  the Standard Model prediction based on CKM unitarity.  When two
  references are listed on a single row, the first corresponds to the
  lattice input for $|V_{cd}|$ and the second to that for $|V_{cs}|$.
  The results denoted by squares are from leptonic decays, while those
  denoted by triangles are from semileptonic
  decays. The points indicated as ETM~17D~($q^2=0$) do not contribute
  to the average, and are shown for comparison purposes (see text).
\label{fig:VcdVcs}}
\end{center}
\end{figure}
\clearpage

