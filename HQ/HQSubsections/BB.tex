\subsection{Neutral $B$-meson mixing matrix elements}
\label{sec:BMix}

Neutral $B$-meson mixing is induced in the Standard Model through
1-loop box diagrams to lowest order in the electroweak theory,
similar to those for short-distance effects in neutral kaon mixing. The effective Hamiltonian
is given by
%
\begin{equation}
  {\cal H}_{\rm eff}^{\Delta B = 2, {\rm SM}} \,\, = \,\,
  \frac{G_F^2 M_{\rm{W}}^2}{16\pi^2} ({\cal F}^0_d {\cal Q}^d_1 + {\cal F}^0_s {\cal Q}^s_1)\,\, +
   \,\, {\rm h.c.} \,\,,
   \label{eq:HeffB}
\end{equation}
%
with
%
\begin{equation}
 {\cal Q}^q_1 =
   \left[\bar{b}\gamma_\mu(1-\gamma_5)q\right]
   \left[\bar{b}\gamma_\mu(1-\gamma_5)q\right],
   \label{eq:Q1}
\end{equation}
where $q=d$ or $s$. The short-distance function ${\cal F}^0_q$ in
Eq.~(\ref{eq:HeffB}) is much simpler compared to the kaon mixing case
due to the hierarchy in the CKM matrix elements. Here, only one term
is relevant,
%
\begin{equation}
 {\cal F}^0_q = \lambda_{tq}^2 S_0(x_t)
\end{equation}
where
\begin{equation}
 \lambda_{tq} = V^*_{tq}V_{tb},
\end{equation}
and where $S_0(x_t)$ is an Inami-Lim function with $x_t=m_t^2/M_W^2$,
which describes the basic electroweak loop contributions without QCD
\cite{Inami:1980fz}. The transition amplitude for $B_q^0$ with $q=d$
or $s$ can be written as
%
\begin{eqnarray}
\label{eq:BmixHeff}
&&\langle \bar B^0_q \vert {\cal H}_{\rm eff}^{\Delta B = 2} \vert B^0_q\rangle  \,\, = \,\, \frac{G_F^2 M_{\rm{W}}^2}{16 \pi^2}  
\Big [ \lambda_{tq}^2 S_0(x_t) \eta_{2B} \Big ]  \nn \\ 
&&\times 
  \left(\frac{\gbar(\mu)^2}{4\pi}\right)^{-\gamma_0/(2\beta_0)}
  \exp\bigg\{ \int_0^{\gbar(\mu)} \, dg \, \bigg(
  \frac{\gamma(g)}{\beta(g)} \, + \, \frac{\gamma_0}{\beta_0g} \bigg)
  \bigg\} 
   \langle \bar B^0_q \vert  Q^q_{\rm R} (\mu) \vert B^0_q
   \rangle \,\, + \,\, {\rm h.c.} \,\, ,
   \label{eq:BBME}
\end{eqnarray}
%
where $Q^q_{\rm R} (\mu)$ is the renormalized four-fermion operator
(usually in the NDR scheme of $\msbar$). The running coupling
$\gbar$, the $\beta$-function $\beta(g)$, and the anomalous
dimension of the four-quark operator $\gamma(g)$ are defined in
Eqs.~(\ref{eq:four_quark_operator_anomalous_dimensions})~and~(\ref{eq:four_quark_operator_anomalous_dimensions_perturbative}).
The product of $\mu$-dependent terms on the second line of
Eq.~(\ref{eq:BBME}) is, of course, $\mu$-independent (up to truncation
errors arising from the use of perturbation theory). The explicit expression for
the short-distance QCD correction factor $\eta_{2B}$ (calculated to
NLO) can be found in Ref.~\cite{Buchalla:1995vs}.

For historical reasons the $B$-meson mixing matrix elements are often
parameterized in terms of bag parameters defined as
\begin{equation}
 B_{B_q}(\mu)= \frac{{\left\langle\bar{B}^0_q\left|
   Q^q_{\rm R}(\mu)\right|B^0_q\right\rangle} }{
         {\frac{8}{3}f_{B_q}^2\mB^2}} \,\, .
         \label{eq:bagdef}
\end{equation}
The RGI $B$ parameter $\hat{B}$ is defined as in the case of the kaon,
and expressed to 2-loop order as
\begin{equation}
 \hat{B}_{B_q} = 
   \left(\frac{\gbar(\mu)^2}{4\pi}\right)^{- \gamma_0/(2\beta_0)}
   \left\{ 1+\dfrac{\gbar(\mu)^2}{(4\pi)^2}\left[
   \frac{\beta_1\gamma_0-\beta_0\gamma_1}{2\beta_0^2} \right]\right\}\,
   B_{B_q}(\mu) \,\,\, ,
\label{eq:BBRGI_NLO}
\end{equation}
with $\beta_0$, $\beta_1$, $\gamma_0$, and $\gamma_1$ defined in
Eq.~(\ref{eq:RG-coefficients}). Note, as Eq.~(\ref{eq:BBME}) is
evaluated above the bottom threshold ($m_b<\mu<m_t$), the active number
of flavours here is $N_f=5$.

Nonzero transition amplitudes result in a mass difference between the
CP eigenstates of the neutral $B$-meson system. Writing the mass
difference for a $B_q^0$ meson as $\Delta m_q$, its Standard Model
prediction is
\begin{equation}
 \Delta m_q = \frac{G^2_Fm^2_W m_{B_q}}{6\pi^2} \,
  |\lambda_{tq}|^2 S_0(x_t) \eta_{2B} f_{B_q}^2 \hat{B}_{B_q}.
\end{equation}
Experimentally the mass difference is measured as oscillation
frequency of the CP eigenstates. The frequencies are measured
precisely with an error of less than a percent. Many different
experiments have measured $\Delta m_d$, but the current average
\cite{Agashe:2014kda} is based on measurements from the
$B$-factory experiments Belle and Babar, and from the LHC experiment
LHC$b$. For $\Delta m_s$ the experimental average is dominated by results
from LHC$b$
\cite{Agashe:2014kda}.  With these experimental results and
lattice-QCD calculations of $f_{B_q}^2\hat{B}_{B_q}$ at hand,
$\lambda_{tq}$ can be determined.  In lattice-QCD calculations the
flavour $SU(3)$-breaking ratio
\begin{equation}
 \xi^2 = \frac{f_{B_s}^2B_{B_s}}{f_{B_d}^2B_{B_d}}
 \label{eq:xidef}
\end{equation} 
can be obtained more precisely than the individual $B_q$-mixing matrix
elements because statistical and systematic errors cancel in part.
With this the ratio $|V_{td}/V_{ts}|$ can be determined, which can be used
to constrain the apex of the CKM triangle.

Neutral $B$-meson mixing, being loop-induced in the Standard Model, is
also a sensitive probe of new physics. The most general $\Delta B=2$
effective Hamiltonian that describes contributions to $B$-meson mixing
in the Standard Model and beyond is given in terms of five local
four-fermion operators:
\be
  {\cal H}_{\rm eff, BSM}^{\Delta B = 2} = \sum_{q=d,s}\sum_{i=1}^5 {\cal C}_i {\cal Q}^q_i \;,
\ee
where ${\cal Q}_1$ is defined in Eq.~(\ref{eq:Q1}) and where
% \bd
% {\cal Q}^q_2 =  \left[\bar{b}(1-\gamma_5)q\right]
%    \left[\bar{b}(1-\gamma_5)q\right], \qquad
% {\cal Q}^q_3 =  \left[\bar{b}^{\alpha}(1-\gamma_5)q^{\beta}\right]
%    \left[\bar{b}^{\beta}(1-\gamma_5)q^{\alpha}\right],
%  \ed
%   \be
% {\cal Q}^q_4 =  \left[\bar{b}(1-\gamma_5)q\right]
%    \left[\bar{b}(1+\gamma_5)q\right], \qquad
% {\cal Q}^q_5 =  \left[\bar{b}^{\alpha}(1-\gamma_5)q^{\beta}\right]
%    \left[\bar{b}^{\beta}(1+\gamma_5)q^{\alpha}\right], 
%    \label{eq:Q25}
% \ee 
\begin{align}
{\cal Q}^q_2 & = \left[\bar{b}(1-\gamma_5)q\right]
   \left[\bar{b}(1-\gamma_5)q\right], \qquad
{\cal Q}^q_3  = \left[\bar{b}^{\alpha}(1-\gamma_5)q^{\beta}\right]
   \left[\bar{b}^{\beta}(1-\gamma_5)q^{\alpha}\right],\nonumber \\
{\cal Q}^q_4 & = \left[\bar{b}(1-\gamma_5)q\right]
   \left[\bar{b}(1+\gamma_5)q\right], \qquad
{\cal Q}^q_5 = \left[\bar{b}^{\alpha}(1-\gamma_5)q^{\beta}\right]
   \left[\bar{b}^{\beta}(1+\gamma_5)q^{\alpha}\right], 
   \label{eq:Q25}
\end{align}
with the superscripts $\alpha,\beta$ denoting colour indices, which
are shown only when they are contracted across the two bilinears.
There are three other basis operators in the $\Delta
B=2$ effective Hamiltonian. When evaluated in QCD, however, 
they give identical matrix elements to the ones already listed due to
parity invariance in QCD.
The short-distance Wilson coefficients ${\cal C}_i$ depend on the
underlying theory and can be calculated perturbatively.  In the
Standard Model only matrix elements of ${\cal Q}^q_1$ contribute to
$\Delta m_q$, while all operators do, for example, for general SUSY
extensions of the Standard Model~\cite{Gabbiani:1996hi}.
The matrix elements or bag parameters for the non-SM operators are also 
useful to estimate the width difference in the Standard Model,
where combinations of matrix elements of ${\cal Q}^q_1$,
${\cal Q}^q_2$, and ${\cal Q}^q_3$ contribute to $\Delta \Gamma_q$ 
at $\cO(1/m_b)$~\cite{Lenz:2006hd,Beneke:1996gn}.  

In this section, we report on results from lattice-QCD calculations for
the neutral $B$-meson mixing parameters $\hat{B}_{B_d}$,
$\hat{B}_{B_s}$, $f_{B_d}\sqrt{\hat{B}_{B_d}}$,
$f_{B_s}\sqrt{\hat{B}_{B_s}}$ and the $SU(3)$-breaking ratios
$B_{B_s}/B_{B_d}$ and $\xi$ defined in Eqs.~(\ref{eq:bagdef}),
(\ref{eq:BBRGI_NLO}), and (\ref{eq:xidef}).  The results are
summarized in Tabs.~\ref{tab_BBssumm} and \ref{tab_BBratsumm} and in
Figs.~\ref{fig:fBsqrtBB2} and \ref{fig:xi}. Additional details about
the underlying simulations and systematic error estimates are given in
Appendix~\ref{app:BMix_Notes}.  Some collaborations do not provide the
RGI quantities $\hat{B}_{B_q}$, but quote instead
$B_B(\mu)^{\overline{MS},NDR}$. In such cases we convert the results
to the RGI quantities quoted in Tab.~\ref{tab_BBssumm} using
Eq.~(\ref{eq:BBRGI_NLO}). More details on the conversion factors are
provided below in the descriptions of the individual results.
We do not provide the $B$-meson matrix elements of the other operators
${\cal Q}_{2-5}$ in this report. They have been calculated in
Ref.~\cite{Carrasco:2013zta} for the $N_f=2$ case and 
in Refs.~\cite{Bouchard:2011xj,Bazavov:2016nty} for $N_f=2+1$.

\begin{table}[!htb]
\begin{center}
\mbox{} \\[3.0cm]
\footnotesize
\begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}@{\hspace{1mm}}r@{\hspace{1mm}}c@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{5mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l}
Collaboration \al Ref. \al $\Nf$ \al
\hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}\hspace{-0.15cm} \al
\hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}\hspace{-0.15cm} \al
\hspace{0.15cm}\begin{rotate}{60}{chiral extrapolation}\end{rotate}\hspace{-0.15cm}\al
\hspace{0.15cm}\begin{rotate}{60}{finite volume}\end{rotate}\hspace{-0.15cm}\al
\hspace{0.15cm}\begin{rotate}{60}{renormalization/matching}\end{rotate}\hspace{-0.15cm}  \al
\hspace{0.15cm}\begin{rotate}{60}{heavy-quark treatment}\end{rotate}\hspace{-0.15cm} \al 
\rule{0.12cm}{0cm}
\parbox[b]{1.2cm}{$f_{\rm B_d}\sqrt{\hat{B}_{\rm B_d}}$} \al
\rule{0.12cm}{0cm}
\parbox[b]{1.2cm}{$f_{\rm B_s}\sqrt{\hat{B}_{\rm B_s}}$} \al
\rule{0.12cm}{0cm}
$\hat{B}_{\rm B_d}$ \al 
\rule{0.12cm}{0cm}
$\hat{B}_{\rm B_{\rm s}}$ \\
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
&&&&&&&&&& \\[-0.1cm]

HPQCD 19bea \al \cite{Dowdall:2019bea} \al 2+1+1 \al \gA \al \soso \al \soso \al \good \al \soso
       \al \okay & 210.6(5.5) \al 256.1(5.7) \al 1.222(61) \al 1.232(53)\\[0.5ex]
&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]
FNAL/MILC 16 \al \cite{Bazavov:2016nty} \al 2+1 \al \gA \al \good \al \soso \al
     \good \al \soso
	\al \okay & 227.7(9.5) \al 274.6(8.4) \al 1.38(12)(6)$^\odot$ \al 1.443(88)(48)$^\odot$\\[0.5ex]

        RBC/UKQCD 14A \al \cite{Aoki:2014nga} \al 2+1 \al \gA \al \soso \al \soso \al
     \soso \al \soso
	\al \okay & 240(15)(33) \al 290(09)(40) \al 1.17(11)(24) \al 1.22(06)(19)\\[0.5ex]

FNAL/MILC 11A \al \cite{Bouchard:2011xj} \al 2+1 \al \rC \al \good \al \soso \al
     \good \al \soso
	\al \okay & 250(23)$^\dagger$ \al 291(18)$^\dagger$ \al $-$ \al $-$\\[0.5ex]

HPQCD 09 \al \cite{Gamiz:2009ku} \al 2+1 \al \gA \al \soso \al \soso$^\nabla$ \al \soso \al
\soso 
\al \okay & 216(15)$^\ast$ \al 266(18)$^\ast$ \al 1.27(10)$^\ast$ \al 1.33(6)$^\ast$ \\[0.5ex] 

HPQCD 06A \al \cite{Dalgic:2006gp} \al 2+1 \al \gA \al \tbr \al \tbr \al \good \al 
\soso 
	\al \okay & $-$ \al  281(21) \al $-$ \al 1.17(17) \\
&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]
ETM 13B \al \cite{Carrasco:2013zta} \al 2 \al \gA \al \good \al \soso \al \soso \al
    \good \al \okay & 216(6)(8) \al 262(6)(8) \al  1.30(5)(3) \al 1.32(5)(2) \\[0.5ex]

ETM 12A, 12B \al \cite{Carrasco:2012dd,Carrasco:2012de} \al 2 \al \rC \al \good \al \soso \al \soso \al
    \good \al \okay & $-$ \al $-$ \al  1.32(8)$^\diamond$ \al 1.36(8)$^\diamond$ \\[0.5ex]
&&&&&&&&&& \\[-0.1cm]
\hline
\hline\\
\end{tabular*}\\[-0.2cm]
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
 \item[$^\odot$] PDG averages of decay constant $f_{B^0}$ and $f_{B_s}$ \cite{Rosner:2015wva} are used to obtain these values.\\[-5mm]
   \item[$^\dagger$] Reported $f_B^2B$ at $\mu=m_b$ is converted to RGI by
	multiplying the 2-loop factor
	1.517.\\[-5mm]
   \item[$^\nabla$] While wrong-spin contributions are not included in
		the HMrS$\chi$PT fits, the effect is expected to be
		small for these quantities (see description in FLAG 13
		\cite{Aoki:2013ldr}). \\[-5mm] 
        \item[$^\ast$] This result uses an old determination of
		     $r_1=0.321(5)$~fm from Ref.~\cite{Gray:2005ur} that
		     has since been superseded, which however has
		     only a small effect in the total error budget (see
		     description in FLAG 13 \cite{Aoki:2013ldr}) .\\[-5mm]
        \item[$^\diamond$] Reported $B$ at $\mu=m_b=4.35$ GeV is converted to
     RGI by multiplying the 2-loop factor 1.521.
\end{itemize}
}
\end{minipage}
\caption{Neutral $B$- and $B_{\rm s}$-meson mixing matrix
 elements (in MeV) and bag parameters.}
\label{tab_BBssumm}
\end{center}
\end{table}

\begin{figure}[!htb]
\hspace{-0.8cm}\includegraphics[width=0.57\linewidth]{HQ/Figures/fBsqrtBB2}\hspace{-0.8cm}
\includegraphics[width=0.57\linewidth]{HQ/Figures/BB2}

\vspace{-5mm}
\caption{Neutral $B$- and $B_{\rm s}$-meson mixing matrix
 elements and bag parameters [values in Tab.~\ref{tab_BBssumm} and
 Eqs.~(\ref{eq:avfBB2}), (\ref{eq:avfBB}), (\ref{eq:avBB2}), (\ref{eq:avBB})].
 \label{fig:fBsqrtBB2}}
\end{figure}

\begin{table}[!htb]
\begin{center}
\mbox{} \\[3.0cm]
\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} r l l l l l l l l l}
Collaboration & Ref. & $\Nf$ & 
\hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{chiral extrapolation}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{finite volume}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{renormalization/matching}\end{rotate}\hspace{-0.15cm}  &
\hspace{0.15cm}\begin{rotate}{60}{heavy-quark treatment}\end{rotate}\hspace{-0.15cm} & 
\rule{0.12cm}{0cm}$\xi$ &
 \rule{0.12cm}{0cm}$B_{\rm B_{\rm s}}/B_{\rm B_d}$ \\
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
&&&&&&&&&& \\[-0.1cm]

HPQCD 19bea \al \cite{Dowdall:2019bea} \al 2+1+1 \al \gA \al \soso \al \soso \al \good \al \soso
       \al \okay & 1.216(16) & 1.008(25) \\[0.5ex]

&&&&&&&&&& \\[-0.1cm]

\hline

&&&&&&&&&& \\[-0.1cm]

RBC/UKQCD 18knm \al \cite{Boyle:2018knm} \al 2+1 \al \oP \al \good \al \good \al 
     \good \al \good \al \okay & 1.1853(54)($^{+102}_{-146}$) & 1.0002(43)($^{+41}_{-70}$) \\[0.5ex]

FNAL/MILC 16 & \cite{Bazavov:2016nty} & 2+1 & \gA & \good & \soso &
     \good & \soso & \okay & 1.206(18) & 1.033(31)(26)$^\odot$ \\[0.5ex]

RBC/UKQCD 14A & \cite{Aoki:2014nga} & 2+1 & \gA & \soso & \soso &
     \soso & \soso & \okay & 1.208(41)(52) & 1.028(60)(49) \\[0.5ex]

FNAL/MILC 12 & \cite{Bazavov:2012zs} & 2+1 & \gA & \soso & \soso &
     \good & \soso & \okay & 1.268(63) & 1.06(11) \\[0.5ex]

RBC/UKQCD 10C
 & \cite{Albertus:2010nm} & 2+1 & \gA & \tbr & \tbr & \tbr
  & \soso & \okay & 1.13(12) & $-$ \\[0.5ex]

HPQCD 09 & \cite{Gamiz:2009ku} & 2+1 & \gA & \soso & \soso$^\nabla$ & \soso &
\soso & \okay & 1.258(33) & 1.05(7) \\[0.5ex] 

&&&&&&&&&& \\[-0.1cm]

\hline

&&&&&&&&&& \\[-0.1cm]

ETM 13B & \cite{Carrasco:2013zta} & 2 & \gA & \good & \soso & \soso & \good
			     & \okay & 1.225(16)(14)(22) & 1.007(15)(14) \\

ETM 12A, 12B & \cite{Carrasco:2012dd,Carrasco:2012de} & 2 & \rC & \good & \soso & \soso & \good
			     & \okay & 1.21(6) & 1.03(2) \\
&&&&&&&&&& \\[-0.1cm]
\hline
\hline\\
\end{tabular*}\\[-0.2cm]
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
 \item[$^\odot$] PDG average of the ratio of decay constants
	      $f_{B_s}/f_{B^0}$ \cite{Rosner:2015wva} is used to obtain
	      the value.\\[-5mm] 
   \item[$^\nabla$] Wrong-spin contributions are not included in the
		HMrS$\chi$PT fits. As the effect may not be negligible,
		these results are excluded from the average (see
		description in FLAG 13 \cite{Aoki:2013ldr}).
\end{itemize}
}
\end{minipage}
\caption{Results for $SU(3)$-breaking ratios of neutral $B_{d}$- and 
 $B_{s}$-meson mixing matrix elements and bag parameters.}
\label{tab_BBratsumm}
\end{center}
\end{table}

\begin{figure}[!htb]
\begin{center}
\includegraphics[width=11.5cm]{HQ/Figures/xiandRBB}

\vspace{-2mm}
\caption{The $SU(3)$-breaking quantities $\xi$ and $B_{B_s}/B_{B_d}$
 [values in Tab.~\ref{tab_BBratsumm} and Eqs.~(\ref{eq:avxiBB2}), (\ref{eq:avxiBB})].}\label{fig:xi} 
\end{center}
\end{figure}

There are no new results for $N_f=2$ reported after the previous FLAG
review. 
In this category one work (ETM~13B)~\cite{Carrasco:2013zta} passes
the quality criteria. 
A description of this work can be found in the FLAG 13 review
\cite{Aoki:2013ldr} where it did not enter the average as it had not
appeared in a journal. 
Because this is the only result 
available for $N_f=2$, we quote their values as our averages 
{\color{red} No Update required}:
% in this version:
%FLAGRESULT BEGIN
% TAG      & fBsqrtBB    & fBssqrtBBs & BB  &BBs & xi  &RBB	 &END
% REFS     & \cite{Carrasco:2013zta} & \cite{Carrasco:2013zta} & \cite{Carrasco:2013zta} & \cite{Carrasco:2013zta} & \cite{Carrasco:2013zta} & \cite{Carrasco:2013zta} &END
% UNITS    & '[MeV]' & '[MeV]' & 1 & 1 & 1 & 1 &END
% NUMRESULTS & 1 & 1 & 1& 1& 1& 1 &END
% FLAVOURs & 2& 2& 2& 2& 2 & 2 &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
      &&  \FLAGAVBEGIN f_{B_d}\sqrt{\hat{B}_{b_d}}&= 216(10)\FLAGAVEND\;\; {\rm MeV}
         &\FLAGAVBEGIN f_{B_s}\sqrt{\hat{B}_{B_s}}&= 262(10)\FLAGAVEND\;\; {\rm MeV}
         &\Ref~\mbox{\cite{Carrasco:2013zta}},  \label{eq:avfBB2}\\
N_f=2:&&\FLAGAVBEGIN \hat{B}_{B_d}&= 1.30(6)\FLAGAVEND 
         &\FLAGAVBEGIN \hat{B}_{B_s}&= 1.32(5)\FLAGAVEND 
	 &\Ref~\mbox{\cite{Carrasco:2013zta}},  \label{eq:avBB2}\\
      &&  \FLAGAVBEGIN \xi &=  1.225(31)\FLAGAVEND  
  	& \FLAGAVBEGIN B_{B_s}/B_{B_d} & =  1.007(21)\FLAGAVEND
 	&\Ref~\mbox{\cite{Carrasco:2013zta}}. \label{eq:avxiBB2}
\end{align}
%FLAGRESULTFORMULA END

For the $N_f=2+1$ case 

{\color{red} new stuff in red:

the RBC/UKQCD collaboration reportrd their new results on the
flavour SU(3) breaking ratio of neutral $B$-meson mixing parameters in 2018.
There paper \cite{Boyle:2018knm} has not been publised yet, thus the results will not been
included in our averages at this stage.
Their computation uses ensembles generated by the $2+1$ flavour domain-wall fermion (DWF) formulation.
The use of the DWFs also for the heavy quarks makes the renormalization structure simple.
Due to the chiral symmetry, there is no mixing apart from those that do mix in the continuum theory.
The operators for standard model mixing matrix elements are multiplicatively renormalized.
Since they only report the SU(3) breaking ratio, the renoralization of the operators are not needed.
The lattice spacings employed are not as fine as some of the recent results reported here.
But, by applying successive stout link smearings for in the heavy DWF, the reach to heavy mass is imporoved,
which allows them to simulate up to half of the physical bottom mass.
Two ensembles are of physical $ud$ quark mass at $a=0.11 and 0.09$ fm and there is yet another ensemble off the physica point but finer lattice at $a=0.07$ fm. This is the first computation using physical light quark mass for these quantities, which makes it possible to drastically reduce the chiral extrapolation error.
}

{\color{blue} old stuff in blue, which will be removed: 

the FNAL/MILC collaboration reported their new results on the neutral
$B$-meson mixing parameters in 2016. As the paper \cite{Bazavov:2016nty}
appeared after the closing 
date of FLAG 16 \cite{Aoki:2016frl}, the results had not been taken into our average then.
However, the subsequent web update of FLAG took the results into the
average, and was made public in November 2017.

Their estimate of the $B^0-\overline{B^0}$ mixing matrix elements are far
improved compared to their older ones as well as all the prior $N_f=2+1$ results.
Hence, including the new FNAL/MILC results makes our averages much more
precise. 
The study uses the asqtad action for light quarks and the Fermilab action
for the $b$ quark. 
They use MILC asqtad ensembles spanning four lattice spacings
in the range  $a\approx 0.045-0.12$ fm and RMS
pion mass of $257$ MeV as the lightest. 
The lightest Goldstone pion of 177 MeV, at which the RMS mass is 280 MeV,
helps constraining the combined chiral and continuum limit analysis
with the HMrS$\chi$PT (heavy-meson rooted-staggered chiral perturbation
theory) to NLO with NNLO analytic terms using a Bayesian prior.
The extension to the finer lattice spacing and closer to physical pion 
masses together with the quadrupled statistics of the ensembles compared with
those used in the earlier studies, as well as the inclusion of
the wrong spin contribution \cite{Bernard:2013dfa}, which is a staggered
fermion artifact, make it possible to achieve the
large improvement of the overall precision.
Although for each parameter only one lattice volume is
available, the finite-volume effects are well controlled 
by using a large enough lattice ($m_\pi^{\rm RMS} L\gsim 5$) for all the
ensembles. 
The operator renormalization is done by 1-loop lattice perturbation
theory with the help of the mostly nonperturbative renormalization method 
where
a perturbative computation of the ratio of the four-quark operator
and square of the vector-current renormalization factors is combined
with the nonperturbative estimate of the latter.
Let us note that in the report \cite{Bazavov:2016nty}
not only the SM $B^0-\overline{B^0}$ mixing matrix element,
but also those with all possible four-quark operators are included.
The correlation among the different matrix elements are given, which 
helps to properly assess the error propagation to phenomenological
analyses where combinations of the different matrix elements enter.
The authors estimate the effect of omitting the charm-quark dynamics,
which we have not propagated to our $N_f=2+1$ averages.
It should also be noted that their main new results are for the
$B^0-\overline{B^0}$ mixing matrix elements, that are  
$f_{B_d}\sqrt{B_{B_d}}$, $f_{B_s}\sqrt{B_{B_s}}$ and the ratio
$\xi$. They reported also on $B_{B_d}$, $B_{B_s}$ and
$B_{B_s}/B_{B_d}$. 
However, the $B$-meson decay constants needed in order to isolate the
bag parameters from the four-fermion matrix elements are taken from the 
PDG~\cite{Rosner:2015wva} averages, which are obtained using a
procedure similar to that used by FLAG.
They plan to compute the decay constants on the same 
gauge field ensembles and then complete the bag parameter calculation 
on their own in the future. 
As of now, for the bag parameters we need to use 
%\ifdefined\NESTEDAVERAGINGNOTE
the nested averaging scheme, described in Sec.~\ref{sec:nested_average},
to take into account the possible correlations with this new result
to the other ones through the averaged decay constants.
The detailed procedure to apply the scheme for this particular case 
is provided in Sec.~\ref{sec:err_BB}.
%\else
%a nested averaging scheme.
%\fi
% -- below, commented out, is the FLAG2016 paper version --
% Due to the addition of this new result, the values for $N_f=2+1$ are 
% now averages from multiple results by multiple collaborations,
% rather than being given by the values from a single computation, as it  was done
% in the previous FLAG report. Our averages are:
}

The other results for $N_f=2+1$ are 
{\color{red} (new):
FNAL/MILC~16~\cite{Bazavov:2016nty}, which had been included in the averages at FLAG19
\cite{Aoki:2019cca},
}
RBC/UKQCD~14A~\cite{Aoki:2014nga},
which had been included in the averages at FLAG 16 \cite{Aoki:2016frl},
and HPQCD~09~\cite{Gamiz:2009ku} to which a description is available in
FLAG 13 \cite{Aoki:2013ldr}.
Now our averages for $N_f=2+1$ are:
%FLAGRESULT BEGIN
% TAG      & fBsqrtBB    & fBssqrtBBs & BB  &BBs & xi  &RBB	 &END
% REFS     & \cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty}& \cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty}& \cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty}& \cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty} &\cite{Aoki:2014nga,Bazavov:2016nty}&\cite{Aoki:2014nga,Bazavov:2016nty} &END
% UNITS    & '[MeV]' & '[MeV]' & 1 & 1 & 1 & 1 &END
% NUMRESULTS & 2 & 2 & 2& 2& 2& 2 &END
% FLAVOURs & 2+1& 2+1& 2+1& 2+1& 2+1 & 2+1 &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
        && \FLAGAVBEGIN f_{B_d}\sqrt{\hat{B}_{B_d}} &=  225(9)\FLAGAVEND \, {\rm MeV}  
%          &\Refs~\mbox{\cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty}}, \label{eq:avfBBd}\\
         & \FLAGAVBEGIN f_{B_s}\sqrt{\hat{B}_{B_s}} &=  274(8)\FLAGAVEND \, {\rm MeV}
	  &\Refs~\mbox{\cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty}},  \label{eq:avfBB}\\ % \label{eq:avfBBs}
&N_f=2+1:& \FLAGAVBEGIN \hat{B}_{B_d}  &= 1.30(10)\FLAGAVEND 
%          &\Refs~\mbox{\cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty}}, \label{eq:avBBd}\\
         & \FLAGAVBEGIN \hat{B}_{B_s} &=  1.35(6)\FLAGAVEND 
          &\Refs~\mbox{\cite{Gamiz:2009ku,Aoki:2014nga,Bazavov:2016nty}}, \label{eq:avBB}\\ %\label{eq:avBBs}
        && \FLAGAVBEGIN \xi  &=  1.206(17)\FLAGAVEND 
%          &\Refs~\mbox{\cite{Aoki:2014nga,Bazavov:2016nty}}, \label{eq:avxi}\\
        & \FLAGAVBEGIN B_{B_s}/B_{B_d}  &=  1.032(38)\FLAGAVEND 
          &\Refs~\mbox{\cite{Aoki:2014nga,Bazavov:2016nty}}. \label{eq:avxiBB} % \label{eq:avRBB}
\end{align}
%FLAGRESULTFORMULA END
Here all the above equations have been updated from the paper version of FLAG 16. 
The new results from FNAL/MILC~16~\cite{Bazavov:2016nty}
entered the average for Eqs.~(\ref{eq:avfBB}), (\ref{eq:avBB}), and replaced the earlier
FNAL/MILC~12~\cite{Bazavov:2012zs} for Eq.~(\ref{eq:avxiBB}).

As discussed in detail in the FLAG 13 review~\cite{Aoki:2013ldr}
HPQCD~09 does not include wrong-spin contributions \cite{Bernard:2013dfa},
which are staggered
fermion artifacts, to the chiral extrapolation analysis. 
It is possible that the effect is significant for $\xi$ and
$B_{B_s}/B_{B_d}$, since the chiral extrapolation error is a dominant one
for these flavour $SU(3)$-breaking ratios.
Indeed, a test done by FNAL/MILC~12~\cite{Bazavov:2012zs} indicates
that the omission of the wrong spin contribution in the chiral analysis
may be a significant source of error.
We therefore took the conservative
choice to exclude $\xi$ and $B_{B_s}/B_{B_d}$ by HPQCD~09 from our
average and we follow the same strategy in this report as well.

{\color{red} new stuff in red:

We have the first $N_f=2+1+1$ calculation for these quantities by HPQCD collaboration
HPQCD 19bea \cite{Dowdall:2019bea}, using the MILC collaboration's HISQ ensembles.
Used lattice spacings are $0.15$, $0.12$ and $0.09$ fm, among which the Goldstone
pion mass is as small as $130$ MeV for two coarser lattices.
However, the root-mean-squared pion mass through all taste multiplets reads
241 MeV at smallest, which is a similar size as the FNAL/MILC 16 \cite{Bazavov:2016nty}
with $N_f=2+1$ and makes the rating on the chiral extrapolation to be a green circle.
The heavy quark formulation used is the non-relativistic QCD (NRQCD).
The NRQCD action employed is improved from that used in older calculations, 
especially on including one-loop radiative corrections to most of the coefficients
of the $O(v_b^4)$ terms \cite{Dowdall:2011wh}.
The b-quark mass is pretuned with the spin-averaged kinetic mass of the $\Upsilon$ 
and $\eta_b$ states. Therefore, there is no need for extrapolation or interpolation
on the b-quark mass.
The HISQ-NRQCD four-quark operators are matched through $O(1/M)$ and renormalized to 
one-loop, which includes the effects of $O(\alpha_s)$, $O(\Lambda_{\rm QCD}/M)$, $O(\alpha_s/a M)$, 
and $O(\alpha_s \, \Lambda_{\rm QCD}/M)$.
Remnant error is dominated by $O(\alpha_s \Lambda_{\rm QCD}/M)~2.9$\% 
and $O(\alpha_s^2)~2.1$\% for individual bag parameters.
The bag parameters are the primary quantities of the calculation in this work.
The mixing matrix elements are obtaind by combining the so-obtained bag marameters
with the $B$ meson decay constants
calculated by Fermilab-MILC collaboration (FNAL/MILC 17 \cite{Bazavov:2017lyh}).

Because this is the only result 
available for $N_f=2+1+1$, we quote their values as our averages 
\begin{align}
      &&  \FLAGAVBEGIN f_{B_d}\sqrt{\hat{B}_{b_d}}&= 210.5(5.5)\FLAGAVEND\;\; {\rm MeV}
         &\FLAGAVBEGIN f_{B_s}\sqrt{\hat{B}_{B_s}}&= 256.1(5.7)\FLAGAVEND\;\; {\rm MeV}
         &\Ref~\mbox{\cite{Dowdall:2019bea}},  \label{eq:avfBB4}\\
N_f=2+1+1:&&\FLAGAVBEGIN \hat{B}_{B_d}&= 1.222(61)\FLAGAVEND 
         &\FLAGAVBEGIN \hat{B}_{B_s}&= 1.232(53)\FLAGAVEND 
	 &\Ref~\mbox{\cite{Dowdall:2019bea}},  \label{eq:avBB4}\\
      &&  \FLAGAVBEGIN \xi &=  1.216(16)\FLAGAVEND  
  	& \FLAGAVBEGIN B_{B_s}/B_{B_d} & =  1.008(25)\FLAGAVEND
 	&\Ref~\mbox{\cite{Dowdall:2019bea}}. \label{eq:avxiBB4}
\end{align}
}


We note that the above results 
within same $N_f$ are all correlated with each other,
due to the use of the same 
% Asqtad MILC ensembles by the FNAL/MILC and HPQCD collaborations.
gauge field ensembles for different quantities.
The results are also correlated with the averages obtained in 
Sec.~\ref{sec:fB} and shown in
Eqs.~(\ref{eq:fB2})--(\ref{eq:fBratio2}) for $N_f=2$ and 
Eqs.~(\ref{eq:fB21})--(\ref{eq:fBratio21}) for $N_f=2+1$, 
because the calculations of $B$-meson decay constants and  
mixing quantities 
are performed on the same (or on similar) sets of ensembles, and results obtained by a 
given collaboration 
use the same actions and setups. These correlations must be considered when 
using our averages as inputs to unitarity triangle (UT) fits. 
For this reason, if one were for example to estimate $f_{B_s}\sqrt{\hat{B}_s}$ from the separate averages of $f_{B_s}$ and $\hat{B}_s$, one would obtain a value about one standard deviation below the one quoted above.  While these two estimates lead to compatible results, giving us confidence that all uncertainties have been properly addressed, we do not recommend combining averages this way, as many correlations would have to be taken into account to properly assess the errors. We recommend instead using the numbers quoted above.
In the future, as more independent 
calculations enter the averages, correlations between the lattice-QCD inputs to UT fits will become less significant.



\subsubsection{Error treatment for $B$-meson bag parameters}
\label{sec:err_BB}

{\color{red} this subsection may still be needed and updated when the new RBC/UKQCD calculation is published.}

The latest FNAL/MILC computation (FNAL/MILC 16) uses $B$-meson decay
constants averaged for PDG \cite{Rosner:2015wva} to isolate the bag
parameter from the mixing matrix elements. 
The bag parameters so obtained have correlation to those from the other
computations in two ways: through the mixing matrix
elements of FNAL/MILC 16 and through the PDG average. 
Since the PDG average is obtained similarly as the FLAG average,
estimating the bag parameter average with FNAL/MILC 16 requires a nested scheme.
The nested scheme discussed in Sec.~\ref{sec:nested_average} is applied
as follows.

Three computations contribute to the $N_f=2+1$ average of the $B_d$ meson bag
parameter $B_{B_d}$, FNAL/MILC 16 \cite{Bazavov:2016nty}, RBC/UKQCD 14A
\cite{Aoki:2014nga}, HPQCD 09 \cite{Gamiz:2009ku}.
FNAL/MILC 16 uses $f_{B^0}$ of PDG \cite{Rosner:2015wva}, which is an average of
RBC/UKQCD 14, RBC/UKQCD 14A, HPQCD 12/11A, FNAL/MILC 11 in Tab.~\ref{tab:FBssumm}.\footnote{
%Isospin correction is made except first one before averaging.
In Ref.~\cite{Rosner:2015wva} an ``isospin correction'' is applied to
$f_{B^+}$ to obtain $f_{B^0}$ for RBC/UKQCD 14A, HPQCD 12/11A, FNAL/MILC
11 before averaging.
}
$B_{B_d}$ (RBC/UKQCD 14A) has correlation with that of 
FNAL/MILC 16, through $f_B$ (RBC/UKQCD 14A). Also some
correlation exists through $f_B$ (RBC/UKQCD 14), which uses the same set
of gauge field configurations as $B_{B_d}$ (RBC/UKQCD 14A).

In Eq.~(\ref{eq:FNAL_B_PDG}) for this particular case, $Q_1$ is
$B_{B_d}$ (FNAL/MILC 16), 
$Y_1$ is $f_{B^0}^2 B_{B_d}$ (FNAL/MILC 16), and 
$\overline{Z}$ is the PDG average of $f_{B^0}^2$.
The most nontrivial part of the nested averaging is to construct the
restricted errors 
$\sigma[f_B^2]_{i'\leftrightarrow k}$ [Eq.~(\ref{eq:sigmaZipk})] and 
$\sigma[f_B^2]_{i';j'\leftrightarrow k}$ [Eq.~(\ref{eq:sigmaZipjpk})],
which goes into the final correlation matrix $C_{ij}$ of $B_{B_d}$ 
through $\sigma_{1;k}$ [Eq.~(\ref{eq:sigma1k})].
The restricted summation over $(\alpha)$ labeling the origin of errors
in this analysis turns out to be either the whole error or the
statistical error only. 

For the correlation of $f_B$ and $B_{B_d}$ both with RBC/UKQCD 14A, not
knowing the information of the correlation, we take total errors 100 \%
correlated. For example, the heavy-quark error, which is $O(1/m_b)$ and
most dominant, is common for both. 
For the correlation of $f_B$ (RBC/UKQCD 14) and $B_{B_d}$ (RBC/UKQCD 14A),
which uses different heavy-quark formulations but based on the
same set of gauge field configurations, only the statistical
error is taken as correlated. 
In a similar way, correlation among the other computations is
determined. In principle, we take the whole error as correlated between 
$f_B$ and $B_{B_d}$ if both results are based on the exact same
lattice action for light and heavy quarks and are sharing (at least a
part of) the 
gauge field ensemble. Otherwise, only the statistical error is taken
as correlated if two computations share the gauge field ensemble, or no
correlation for the rest, which is summarized in Tab.~\ref{tab:sigma_fB}.
Also in a similar way, correlations of $f_{B_s}$ and $B_{B_s}$, 
$f_{B_s}/f_{B}$ and $B_{B_s}/B_{B_d}$ are determined, which are also
summarized in Tab.~\ref{tab:sigma_fB}.

The necessary information for constructing the second term in the square
root of Eq.~(\ref{eq:sigma1k}) has already been provided.
For completeness, let us also summarize
the correlation pattern needed to construct the other part of
$\sigma_{i;j}$ for the bag parameters, which is shown
in Tab.~\ref{tab:sigma_ij}.


\begin{table}[!htb]
\footnotesize
\begin{tabular}{l|cc}
 \multicolumn{3}{c}
 {$\sigma[Z]_{i';j'\leftrightarrow k}$ for $k=$[RBC/UKQCD 14A]} \\
 \hline
 \multicolumn{1}{c|}{$i'\; \backslash \; j'$} &  
     \shortstack{RBC/UKQCD\\ 14A} & \shortstack{RBC/UKQCD\\ 14} \\
 \hline
 RBC/UKQCD 14A & all & stat \\
 RBC/UKQCD 14  & stat & stat \\
\end{tabular} 
\hspace{6pt}
\begin{tabular}{l|cc}
 \multicolumn{3}{c}
 {$\sigma[f_B^2]_{i';j'\leftrightarrow k}$ for $k=$[HPQCD 09]} \\
 \hline
 \multicolumn{1}{c|}{$i'\; \backslash \; j'$} &  
     \shortstack{HPQCD\\ 12/11A} & \shortstack{FNAL/MILC\\ 11} \\
 \hline
 HPQCD 12/11A & all & stat \\
 FNAL/MILC 11 & stat & stat \\
\end{tabular} 

\vspace*{12pt}
\begin{tabular}{l|ccc}
 \multicolumn{4}{c}
 {$\sigma[f_{B_s}^2]_{i';j'\leftrightarrow k}$ for $k=$[HPQCD 09]} \\
 \hline
 \multicolumn{1}{c|}{$i'\; \backslash \; j'$} &  
     \shortstack{HPQCD\\ 12} & \shortstack{HPQCD\\ 11A} & \shortstack{FNAL/MILC\\ 11} \\
 \hline
 HPQCD 12  & all & stat & stat \\
 HPQCD 11A & stat & stat & stat \\
 FNAL/MILC 11 & stat & stat & stat \\
\end{tabular} 
\caption{Correlated elements of error composition in the summation over
 $(\alpha)$ for $\sigma[Z]_{i';j'\leftrightarrow k}$
 [Eq.~(\ref{eq:sigmaZipjpk})] for $Z=f_B^2, f_{B_s}^2, f_{B_s}^2/f_B^2$.
 The $i'=j'$ elements express 
 $\sigma[Z]_{i'\leftrightarrow k}$ [Eq.~(\ref{eq:sigmaZipk})].
 The elements not listed here are all null.
}
\label{tab:sigma_fB}
\end{table}

\begin{table}[!htb]
\footnotesize
\begin{center}
\begin{tabular}{l|ccc}
 \multicolumn{1}{c|}{$i\; \backslash \; j$} &  
            FNAL/MILC 16 & RBC/UKQCD 14A & HPQCD 09 \\
 \hline
 FNAL/MILC 16 & $-$  & none & stat \\
 RBC/UKQCD 14A   & all  & $-$  & none \\
 HPQCD 09        & all  & none & $-$  \\
\end{tabular} 
\end{center}
\caption{Correlated elements of error composition in the summation over
 $(\alpha)$ for $\sigma_{i;j}$ of $B_{B_d}$, $B_{B_s}$, $B_{B_s}/B_{B_d}$.
The $i=$[FNAL/MILC 16] row expresses the correlations in the first
term in the square root in Eq.~(\ref{eq:sigma1k}). 
The $j=$[FNAL/MILC 16] column represents the correlations for 
Eq.~(\ref{eq:sigmak1}). For $B_{B_s}/B_{B_d}$ only upper $2\times 2$
 block is relevant.
}
\label{tab:sigma_ij}
\end{table}
