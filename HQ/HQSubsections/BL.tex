\subsection{Leptonic decay constants $f_B$ and $f_{B_s}$}
\label{sec:fB}
%
The $B$- and $B_s$-meson decay constants are crucial inputs for
extracting information from leptonic $B$ decays.  Charged $B$ mesons
can decay to a lepton-neutrino final state  through the
charged-current weak interaction.  On the other hand, neutral
$B_{d(s)}$ mesons can decay to a charged-lepton pair via a
flavour-changing neutral current (FCNC) process.



In the Standard Model the decay rate for $B^+ \to \ell^+ \nu_{\ell}$
is described by a formula identical to Eq.~(\ref{eq:Dtoellnu}), with $D_{(s)}$ replaced by $B$, and the 
relevant CKM matrix element $V_{cq}$ replaced by $V_{ub}$,
\be
\Gamma ( B \to \ell \nu_{\ell} ) =  \frac{ m_B}{8 \pi} G_F^2  f_B^2 |V_{ub}|^2 m_{\ell}^2 
           \left(1-\frac{ m_{\ell}^2}{m_B^2} \right)^2 \;. \label{eq:B_leptonic_rate}
\ee
The only charged-current $B$-meson decay that has been observed so far is 
$B^{+} \to \tau^{+} \nu_{\tau}$, which has been measured by the Belle
and Babar collaborations~\cite{Lees:2012ju,Kronenbitter:2015kls}.
Both collaborations have reported results with errors around $20\%$. These measurements can be used to 
determine $|V_{ub}|$ when combined with lattice-QCD predictions of the corresponding
decay constant. 


Neutral $B_{d(s)}$-meson decays to a charged-lepton pair $B_{d(s)}
\rightarrow l^{+} l^{-}$ is a FCNC process, and can only occur at
one loop in the Standard Model.  Hence these processes are expected to
be rare, and are sensitive to physics beyond the Standard Model.
The corresponding expression for the branching fraction has the form 
\be
B ( B_q \to \ell^+ \ell^-) = \tau_{B_q} \frac{G_F^2}{\pi} \, Y \,
\left(  \frac{\alpha}{4 \pi \sin^2 \Theta_W} \right)^2
m_{B_q} f_{B_q}^2 |V_{tb}^*V_{tq}|^2 m_{\ell}^2 
           \sqrt{1- 4 \frac{ m_{\ell}^2}{m_B^2} }\;, 
\ee
where the light quark $q=s$ or $d$, and the function $Y$ includes NLO QCD and electro-weak
corrections \cite{Inami:1980fz,Buchalla:1993bv}. Evidence for both
 $B_s \to \mu^+ \mu^-$ and $B_s \to \mu^+ \mu^-$ decays was first observed
by the CMS and the LHCb collaborations, and a combined analysis was
presented in 2014 in Ref.~\cite{CMS:2014xfa}.  In 2020, the ATLAS, CMS and LHCb collaborations 
reported their measurements from a preliminary combined analysis as~\cite{ATLAS:2020acx}
%
\begin{eqnarray} 
   B(B_d \to \mu^+ \mu^-) &<& (1.9) \,10^{-10} \;\mathrm{at}\; 95\% \;\mathrm{CL}, \nonumber\\
   B(B_s \to \mu^+ \mu^-) &=& (2.69^{+0.37}_{-0.35}) \,10^{-9} ,
\label{eq:B_to_mumu_ATLAS_2020}
\end{eqnarray}
%
which are compatible with the Standard Model predictions within approximately 2 standard deviations~\cite{Beneke:2019slt}.


The decay constants $f_{B_q}$ (with $q=u,d,s$) parameterize the matrix
elements of the corresponding axial-vector currents $A^{\mu}_{bq}
= \bar{b}\gamma^{\mu}\gamma^5q$ analogously to the definition of
$f_{D_q}$ in Sec.~\ref{sec:fD}:
\be
\langle 0| A^{\mu} | B_q(p) \rangle = i p_B^{\mu} f_{B_q} \;.
\label{eq:fB_from_ME}
\ee
For heavy-light mesons, it is convenient to define and analyse the quantity 
\be
 \Phi_{B_q} \equiv f_{B_q} \sqrt{m_{B_q}} \;,
\ee
which approaches a constant (up to logarithmic corrections) in the
$m_B \to \infty$ limit, because of heavy-quark symmetry.
In the following discussion we denote lattice data for $\Phi$($f$)
obtained at a heavy-quark mass $m_h$ and light valence-quark mass
$m_{\ell}$ as $\Phi_{h\ell}$($f_{hl}$), to differentiate them from
the corresponding quantities at the physical $b$- and light-quark
masses.


The $SU(3)$-breaking ratio $f_{B_s}/f_B$ is of phenomenological
interest, because many systematic effects can be partially reduced in lattice-QCD calculations of this ratio.  The discretization errors, heavy-quark mass
tuning effects, and renormalization/ matching errors may all be partially reduced. 
This $SU(3)$-breaking ratio is, however, still sensitive to the chiral
extrapolation. Provided the chiral extrapolation is under control,
one can then adopt $f_{B_s}/f_B$ as an input in extracting
phenomenologically-interesting quantities.  In addition, it often
happens to be easier to obtain lattice results for $f_{B_{s}}$ with
smaller errors than direct calculations of $f_{B}$.  Therefore, one can combine the $B_{s}$-meson
decay constant with the $SU(3)$-breaking ratio to calculate $f_{B}$.  Such
a strategy can lead to better precision in the computation of the
$B$-meson decay constant, and has been adopted by the
ETM~\cite{Carrasco:2013zta, Bussone:2016iua} and the
HPQCD collaborations~\cite{Na:2012sp}. An alternative strategy, used in \cite{Balasubramanian:2019net}, is to obtain the $B_s$-meson decay constant by combining the $D_{s}$-meson decay constant with the ratio $f_{B_s}/f_{D_s}$.


It is clear that the decay constants for charged and neutral $B$
mesons play different roles in flavour-physics phenomenology.  Knowledge of the $B^{+}$-meson decay constant
$f_{B^{+}}$ is essential for extracting $|V_{ub}|$ from
leptonic $B^{+}$ decays.   The neutral $B$-meson decay constants
$f_{B^{0}}$ and $f_{B_{s}}$ are inputs for the search of new physics in rare leptonic $B^{0}$
decays.  In
view of this, it is desirable to include isospin-breaking effects in
lattice computations for these quantities, and have results for
$f_{B^{+}}$ and $f_{B^{0}}$.   
%
With the increasing precision of recent lattice calculations, isospin splittings for $B$-meson decay constants can be significant, 
%
and will play an important role in the foreseeable
future.    A few collaborations have reported $f_{B^{+}}$ and $f_{B^{0}}$
separately by taking into account strong isospin effects in the
valence sector, and estimated the corrections from
electromagnetism. The $N_{f}=2+1+1$ strong isospin-breaking effect was
computed in HPQCD 13~\cite{Dowdall:2013tga} (see 
Tab.~\ref{tab:FBssumm} in this subsection).  However, since only
unitary points (with equal sea- and valence-quark masses) were considered in
HPQCD 13~\cite{Dowdall:2013tga}, this procedure only correctly accounts for the effect from the
valence-quark masses, while introducing a spurious sea-quark
contribution. The decay constants $f_{B^{+}}$ and $f_{B^{0}}$ are also
separately reported in FNAL/MILC 17~\cite{Bazavov:2017lyh} by
taking into account the strong-isospin effect, and it is found that these two
decay constants are well compatible.  The new FNAL/MILC
results were obtained by keeping the averaged light sea-quark
mass fixed when varying the quark masses in their analysis procedure.
Their finding indicates that the strong isospin-breaking effects, $f_{B^+}-f_B\sim 0.5$ MeV, could be smaller than those suggested by previous computations. One would have to take into
account QED effects in the $B$-meson leptonic decay rates to properly use these results for extracting phenomenologically relevant information.\footnote{See Ref.~\cite{Carrasco:2015xwa} for a strategy that
has been proposed to account for QED effects.}  Currently, errors on the
experimental measurements on these decay rates are still very large.   In this review, we
will then concentrate on the isospin-averaged result $f_{B}$ and the
$B_{s}$-meson decay constant, as well as the $SU(3)$-breaking ratio
$f_{B_{s}}/f_{B}$.

The status of lattice-QCD computations for $B$-meson decay constants
and the $SU(3)$-breaking ratio, using gauge-field ensembles
with light dynamical fermions, is summarized in Tabs.~\ref{tab:FBssumm}
and~\ref{tab:FBratsumm}, while Figs.~\ref{fig:fB} and~\ref{fig:fBratio} contain the graphical
presentation of the collected results and our averages.  Many results
in these tables and plots were
already reviewed in detail in the previous FLAG
report.  Below we will describe the new results
that appeared after January 2019.  
%\begin{table}[htb]
\begin{table}[!htb]
\mbox{} \\[3.0cm]
\footnotesize
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}l@{\hspace{1mm}}r@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{5mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l}
Collaboration & Ref. & $\Nf$ & 
\hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{chiral extrapolation}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{finite volume}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{renormalization/matching}\end{rotate}\hspace{-0.15cm}  &
\hspace{0.15cm}\begin{rotate}{60}{heavy-quark treatment}\end{rotate}\hspace{-0.15cm} & 
 $f_{B^+}$ & $f_{B^0}$   & $f_{B}$ & $f_{B_s}$  \\
%\rule{0.12cm}{0cm} \parbox[b]{1.0cm}
&&&&&&&&&&&&\\[-0.1cm]
\hline
\hline
&&&&&&&&&&&& \\[-0.1cm]

FNAL/MILC 17  & \cite{Bazavov:2017lyh} & 2+1+1 & \gA & \good & \good & \good 
& \good &  \okay &  189.4(1.4) & 190.5(1.3) & 189.9(1.4) & 230.7(1.2) \\[0.5ex]

HPQCD 17A & \cite{Hughes:2017spc} & 2+1+1 & \gA & \soso & \good & \good 
& \soso &  \okay &  $-$ & $-$ & 196(6) & 236(7) \\[0.5ex]

ETM 16B & \cite{Bussone:2016iua} & 2+1+1 & \gA & \good & \soso & \soso 
& \soso &  \okay &  $-$ & $-$ & 193(6) & 229(5) \\[0.5ex]

ETM 13E & \cite{Carrasco:2013naa} & 2+1+1 & \rC & \good & \soso & \soso 
& \soso &  \okay &  $-$ & $-$ & 196(9) & 235(9) \\[0.5ex]

HPQCD 13 & \cite{Dowdall:2013tga} & 2+1+1 & \gA & \soso & \good & \good & \soso
& \okay &  184(4) & 188(4) &186(4) & 224(5)  \\[0.5ex]

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]

RBC/UKQCD 14 & \cite{Christ:2014uea} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & 195.6(14.9) & 199.5(12.6) & $-$ & 235.4(12.2) \\[0.5ex]

RBC/UKQCD 14A & \cite{Aoki:2014nga} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ & 219(31) & 264(37) \\[0.5ex]

RBC/UKQCD 13A & \cite{Witzel:2013sla} & 2+1 & \rC & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ &  191(6)$_{\rm stat}^\diamond$ & 233(5)$_{\rm stat}^\diamond$ \\[0.5ex]

HPQCD 12 & \cite{Na:2012sp} & 2+1 & \gA & \soso & \soso & \soso & \soso
& \okay & $-$ & $-$ & 191(9) & 228(10)  \\[0.5ex]

HPQCD 12 & \cite{Na:2012sp} & 2+1 & \gA & \soso & \soso & \soso & \soso
& \okay & $-$ & $-$ & 189(4)$^\triangle$ &  $-$  \\[0.5ex]

HPQCD 11A & \cite{McNeile:2011ng} & 2+1 & \gA & \good & \soso &
 \good & \good & \okay & $-$ & $-$ & $-$ & 225(4)$^\nabla$ \\[0.5ex] 

FNAL/MILC 11 & \cite{Bazavov:2011aa} & 2+1 & \gA & \soso & \soso &
     \good & \soso & \okay & 197(9) & $-$ & $-$ & 242(10) &  \\[0.5ex]  

HPQCD 09 & \cite{Gamiz:2009ku} & 2+1 & \gA & \soso & \soso & \soso &
\soso & \okay & $-$ & $-$ & 190(13)$^\bullet$ & 231(15)$^\bullet$  \\[0.5ex] 

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]

Balasubramamian 19net$^\dagger$ & \cite{Balasubramanian:2019net} & 2 & \gA & \good & \good & \good & \soso & \okay & $-$ & $-$ & $-$ & 215(10)(2)({\raisebox{0.5ex}{\tiny$\substack{+2 \\ -5}$}})\\[0.5ex]

ALPHA 14 & \cite{Bernardoni:2014fva} & 2 & \gA & \good & \good &\good 
& \good & \okay &  $-$ & $-$ & 186(13) & 224(14) \\[0.5ex]

ALPHA 13 & \cite{Bernardoni:2013oda} & 2 & \rC  & \good   & \good   &
\good    &\good  & \okay   & $-$ & $-$ & 187(12)(2) &  224(13) &  \\[0.5ex] 

ETM 13B, 13C$^\ddagger$ & \cite{Carrasco:2013zta,Carrasco:2013iba} & 2 & \gA & \good & \soso & \good
& \soso &  \okay &  $-$ & $-$ & 189(8) & 228(8) \\[0.5ex]

ALPHA 12A& \cite{Bernardoni:2012ti} & 2 & \rC  & \good      & \good      &
\good          &\good  & \okay   & $-$ & $-$ & 193(9)(4) &  219(12) &  \\[0.5ex] 

ETM 12B & \cite{Carrasco:2012de} & 2 & \rC & \good & \soso & \good
& \soso &  \okay &  $-$ & $-$ & 197(10) & 234(6) \\[0.5ex]

ALPHA 11& \cite{Blossier:2011dk} & 2 & \rC  & \good      & \soso      &
\good          &\good  & \okay  & $-$ & $-$ & 174(11)(2) &  $-$ &  \\[0.5ex]  

ETM 11A & \cite{Dimopoulos:2011gx} & 2 & \gA & \good & \soso & \good
& \soso &  \okay & $-$ & $-$ & 195(12) & 232(10) \\[0.5ex]

ETM 09D & \cite{Blossier:2009hg} & 2 & \gA & \good & \soso & \soso
& \soso &  \okay & $-$ & $-$ & 194(16) & 235(12) \\[0.5ex]
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
\end{tabular*}
%
%
\begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}lllllllll}
  \multicolumn{10}{l}{\vbox{\begin{flushleft} 
	$^\diamond$Statistical errors only. \\
        $^\triangle$Obtained by combining $f_{B_s}$ from HPQCD 11A with $f_{B_s}/f_B$ calculated in this work.\\
        $^\nabla$This result uses one ensemble per lattice spacing with light to strange sea-quark mass 
        ratio $m_{\ell}/m_s \approx 0.2$. \\
        $^\bullet$This result uses an old determination of  $r_1=0.321(5)$ fm from Ref.~\cite{Gray:2005ur} that 
        has since been superseded. \\
        $^\ddagger$Obtained by combining $f_{D_s}$, updated in this work, with $f_{B_s}/f_{D_s}$,  calculated in this work.\\
        $^\ddagger$Update of ETM 11A and 12B. 
\end{flushleft}}}
\end{tabular*}
\vspace{-0.5cm}
\caption{Decay constants of the $B$, $B^+$, $B^0$ and $B_{s}$ mesons
  (in MeV). Here $f_B$ stands for the mean value of $f_{B^+}$ and
  $f_{B^0}$, extrapolated (or interpolated) in the mass of the light
  valence-quark to the physical value of $m_{ud}$.}
\label{tab:FBssumm}
\end{table}

%
%
%
%
%
%
\begin{table}[!htb]
%\begin{table}[htb]
\begin{center}
\mbox{} \\[3.0cm]
\footnotesize
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}l@{\hspace{1mm}}r@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{5mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l}
Collaboration & Ref. & $\Nf$ & 
\hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{chiral extrapolation}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{finite volume}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{renormalization/matching}\end{rotate}\hspace{-0.15cm}  &
\hspace{0.15cm}\begin{rotate}{60}{heavy-quark treatment}\end{rotate}\hspace{-0.15cm} & 
 $f_{B_s}/f_{B^+}$  & $f_{B_s}/f_{B^0}$  & $f_{B_s}/f_{B}$  \\
 %\rule{0.12cm}{0cm}
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
&&&&&&&&&& \\[-0.1cm]

FNAL/MILC 17  & \cite{Bazavov:2017lyh} & 2+1+1 & \gA & \good & \good
                                                                                   & \good 
& \good &  \okay &  1.2180(49) & 1.2109(41) & $-$ \\[0.5ex]

HPQCD 17A & \cite{Hughes:2017spc} & 2+1+1 & \gA & \soso & \good & \good 
& \soso &  \okay &  $-$ & $-$ & 1.207(7) \\[0.5ex]

ETM 16B & \cite{Bussone:2016iua} & 2+1+1 & \gA & \good & \soso & \soso 
& \soso &  \okay &  $-$ & $-$& 1.184(25) \\[0.5ex]

ETM 13E & \cite{Carrasco:2013naa} & 2+1+1 & \rC & \good & \soso & \soso
& \soso &  \okay &  $-$ & $-$ & 1.201(25) \\[0.5ex]

HPQCD 13 & \cite{Dowdall:2013tga} & 2+1+1 & \gA & \soso & \good & \good & \soso
& \okay & 1.217(8) & 1.194(7) & 1.205(7)  \\[0.5ex]

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]
RBC/UKQCD 18knm & \cite{Boyle:2018knm} & 2+1 & \oP & \good & \good & \good & \good & \okay & $-$ & $-$ & 1.1949(60)({\raisebox{0.5ex}{\tiny$\substack{+95 \\ -175}$}})
\\[0.5ex]

RBC/UKQCD 14 & \cite{Christ:2014uea} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & 1.223(71) & 1.197(50) & $-$ \\[0.5ex]

RBC/UKQCD 14A & \cite{Aoki:2014nga} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ & 1.193(48) \\[0.5ex]


RBC/UKQCD 13A & \cite{Witzel:2013sla} & 2+1 & \rC & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ &  1.20(2)$_{\rm stat}^\diamond$ \\[0.5ex]

HPQCD 12 & \cite{Na:2012sp} & 2+1 & \gA & \soso & \soso & \soso & \soso
& \okay & $-$ & $-$ & 1.188(18) \\[0.5ex]

FNAL/MILC 11 & \cite{Bazavov:2011aa} & 2+1 & \gA & \soso & \soso &
     \good& \soso & \okay & 1.229(26) & $-$ & $-$ \\[0.5ex]  
     
RBC/UKQCD 10C & \cite{Albertus:2010nm} & 2+1 & \gA & \tbr & \tbr & \tbr 
  & \soso & \okay & $-$ & $-$ & 1.15(12) \\[0.5ex]

HPQCD 09 & \cite{Gamiz:2009ku} & 2+1 & \gA & \soso & \soso & \soso &
\soso & \okay & $-$ & $-$ & 1.226(26)  \\[0.5ex] 

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]
ALPHA 14 \al \cite{Bernardoni:2014fva} & 2 & \gA & \good & \good & \good 
& \good &  \okay &  $-$ \al $-$ & 1.203(65)\\[0.5ex]

ALPHA 13 & \cite{Bernardoni:2013oda} & 2 & \rC  & \good  & \good  &
\good   &\good  & \okay   & $-$ & $-$ & 1.195(61)(20)  &  \\[0.5ex] 

ETM 13B, 13C$^\dagger$ & \cite{Carrasco:2013zta,Carrasco:2013iba} & 2 & \gA & \good & \soso & \good
& \soso &  \okay &  $-$ & $-$ & 1.206(24)  \\[0.5ex]

ALPHA 12A & \cite{Bernardoni:2012ti} & 2 & \rC & \good & \good & \good
& \good &  \okay & $-$ & $-$ & 1.13(6)  \\ [0.5ex]

ETM 12B & \cite{Carrasco:2012de} & 2 & \rC & \good & \soso & \good
& \soso &  \okay & $-$ & $-$ & 1.19(5) \\ [0.5ex]

ETM 11A & \cite{Dimopoulos:2011gx} & 2 & \gA & \soso & \soso & \good
& \soso &  \okay & $-$ & $-$ & 1.19(5) \\ [0.5ex]
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
\end{tabular*}
\begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}lllllllll}
  \multicolumn{10}{l}{\vbox{\begin{flushleft}
 	 $^\diamond$Statistical errors only. \\
          $^\dagger$Update of ETM 11A and 12B. 
\end{flushleft}}}
\end{tabular*}
\vspace{-0.5cm}
\caption{Ratios of decay constants of the $B$ and $B_s$ mesons (for details see Tab.~\ref{tab:FBssumm}).}
\label{tab:FBratsumm}
\end{center}
\end{table}
%
\begin{figure}[!htb]
%\begin{figure}[htb]
\centering	
%\hspace{-0.8cm}
\includegraphics[width=0.48\linewidth]{HQ/Figures/fB}
%\hspace{-0.95cm}
\includegraphics[width=0.48\linewidth]{HQ/Figures/fBs}
 \vspace{-2mm}
\caption{Decay constants of the $B$ and $B_s$ mesons. The values are taken from Tab.~\ref{tab:FBssumm} 
(the $f_B$ entry for FNAL/MILC 11 represents $f_{B^+}$). The
significance of the colours is explained in Sec.~\ref{sec:qualcrit}.
%\ref{sec:qualcrit}
The black squares and grey bands indicate
our averages in Eqs.~(\ref{eq:fB2}), (\ref{eq:fB21}),
(\ref{eq:fB211}), (\ref{eq:fBs2}), (\ref{eq:fBs21}) and
(\ref{eq:fBs211}).}
\label{fig:fB}
\end{figure}
%
%
\begin{figure}[!htb]
%\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\linewidth]{HQ/Figures/fBratio}
\vspace{-2mm}
\caption{Ratio of the decay constants of the $B$ and $B_s$ mesons. The
  values are taken from Tab.~\ref{tab:FBratsumm}.  Results labelled
  as FNAL/MILC 17 1 and FNAL/MILC 17 2 correspond to
  those for $f_{B_{s}}/f_{B^{0}}$ and $f_{B_{s}}/f_{B^{+}}$ reported in FNAL/MILC
  17.  The
significance of the colours is explained in
Sec.~\ref{sec:qualcrit}.
%\ref{sec:qualcrit}. 
The black squares and grey bands indicate
our averages in Eqs.~(\ref{eq:fBratio2}), (\ref{eq:fBratio21}) and
(\ref{eq:fBratio211}).}
\label{fig:fBratio}
\end{center}
\end{figure}
%


%%%%%%%% N_f = 2 
One new $N_{f}=2$ calculation of $f_{B_{s}}$ has appeared after the publication of the previous FLAG review~\cite{Aoki:2016frl}. In Tab.~\ref{tab:FBssumm}, this result is labelled Balasubramamian 19net~\cite{Balasubramanian:2019net}.

% beginning of the description of Balasubramamian 19net
In Balasubramamian 19net~\cite{Balasubramanian:2019net}, simulations at three values of the lattice spacing, $a=0.0751$, 0.0653 and 0.0483 fm were performed with nonperturbatively ${\cal O}(a)$-improved Wilson-clover fermions and the Wilson plaquette gauge action. These three lattice
spacings correspond to the bare couplings $\beta=5.20$, 5.30 and
5.50.  The pion masses in this work range from 194 to 439 MeV, and the
lattice sizes are between 2.09 and 4.18 fm.  A key feature of
Balasubramamian 19net~\cite{Balasubramanian:2019net} is the use of a variant of the ratio method~\cite{Blossier:2009hg}, applied for the first time to Wilson-clover fermions. This variant is required because, in contrast to twisted-mass Wilson fermions, there is no simple relationship between the heavy quark pole mass and the bare quark mass. In
the application of this approach to the $B_s$-decay constant,  one
first computes the quantity ${\mathcal{F}}_{hq} \equiv f_{hq}/M_{hq}$,
where $f_{hq}$ and $M_{hq}$ are decay constant and mass of the
pseudoscalar meson composed of valence (relativistic) heavy quark $h$
and light (or strange) quark $q$.   The matching between the lattice
and the continuum heavy-light currents for extracting the above
$f_{hq}$ is straightforward because 
the valence heavy quark is also described by Wilson-clover fermions.  In the second step,
the ratio $z_{q} (M_{hq}, \lambda) \equiv
[{\mathcal{F}}_{hq}C_{A}^{{\mathrm{stat}}}(\bar{\mu}^{(h^{\prime})})M_{hq}^{3/2}]/[{\mathcal{F}}_{h^{\prime}q}C_{A}^{{\mathrm{stat}}}(M_{hq})M_{h^{\prime}q}^{3/2}]$
is calculated, where $C_{A}^{{\mathrm{stat}}}(M_{hq})$ is the matching coefficient for the $(hq)$-meson decay constant in QCD and its counterpart in HQET, and $M_{hq}
= \lambda M_{h^{\prime}q}$, with $\lambda=1.18$ in Balasubramamian 19net~\cite{Balasubramanian:2019net}.  The authors of Balasubramamian 19net~\cite{Balasubramanian:2019net} use the NNLO perturbative result of
$C_{A}^{{\mathrm{stat}}}(M_{hq})$ in their work.  By
starting from a ``triggering'' point with the heavy-meson mass around that
of the $D_s$ meson, one can proceed with the calculations in steps, such that
$M_{hq}$ is increased by a factor of $\lambda$ at each step.
In Balasubramamian 19net~\cite{Balasubramanian:2019net}, the authors simulate up to heavy-quark mass
around 4.5 GeV, but observed significant $(aM_{H_s})^2$ cutoff effects on the two coarsest lattices with lattice spacings $a = 0.0751$ and 0.0653 fm and so simulate up to 3.2 GeV on these lattices. 
In this formulation of the ratio method, the ratio obeys
$z_{q} (M_{hq}, \lambda) \rightarrow 1/\sqrt{\lambda}$ in the limit
$M_{hq} \rightarrow \infty$.   Designing the computations in
such a way that in the last step, $M_{hq}$ is equal to the physical
$B_s$ mass, one obtains
$f_{B_{(s)}}/f_{D_{(s)}}$.  Combining this ratio with results for
$f_{D_{(s)}}$, updated with a third lattice spacing in Balasubramamian 19net~\cite{Balasubramanian:2019net}, the decay constant of the $B_s$ meson can be extracted. The authors estimated the systematic uncertainty associated with their generic fit form, which combines chiral-continuum extrapolation with heavy quark discretization effects, and quote a single systematic uncertainty. The systematic uncertainty associated with scale-setting is estimated from $f_{D_s}$.
% end of the description of Balasubramamian 19net

There have been no new $N_{f}=2$ calculations of $f_B$ or $f_{B_{s}}/f_B$. Therefore, our averages for these two cases stay the same as those in Ref.~\cite{Aoki:2016frl}. We update our average of $f_{B_{s}}$ to include the new calculation of Balasubramamian 19net~\cite{Balasubramanian:2019net}:
%
%FLAGRESULT BEGIN
% TAG      & fB    & fBs & fBofBs &END
% REFS     & \cite{Carrasco:2013zta,Bernardoni:2014fva}& \cite{Carrasco:2013zta,Bernardoni:2014fva,Balasubramanian:2019net}& \cite{Carrasco:2013zta,Bernardoni:2014fva}&END
% UNITS    & '[MeV]' & '[MeV]' & 1 & END
% NUMRESULTS & 2 & 3 & 2&END
% FLAVOURs & 2& 2& 2& END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&\label{eq:fB2}
\Nf=2:&\FLAGAVBEGIN f_{B} &= 188(7) \FLAGAVEND\;{\rm MeV}
&&\Refs~\mbox{\cite{Carrasco:2013zta,Bernardoni:2014fva}},\\
&\label{eq:fBs2}
\Nf=2: &\FLAGAVBEGIN f_{B_{s}} &= 225.3(6.6)\FLAGAVEND \; {\rm MeV} 
&&\Refs~\mbox{\cite{Carrasco:2013zta,Bernardoni:2014fva,Balasubramanian:2019net}}, \\
&\label{eq:fBratio2}
%\label{eq:fbav2}
\Nf=2: &\FLAGAVBEGIN f_{B_{s}}\over{f_B} &= 1.206(0.023)\FLAGAVEND
&&\Refs~\mbox{\cite{Carrasco:2013zta,Bernardoni:2014fva}},
\end{align}
%FLAGRESULTFORMULA END

%%%%%%% N_f = 2+1
One new $N_{f}=2+1$ calculation of $f_{B_{s}}/f_B$ was completed after the publication of the previous FLAG review~\cite{Aoki:2016frl}.  In Tab.~\ref{tab:FBssumm}, this result is labelled RBC/UKQCD 18knm~\cite{Boyle:2018knm}.

% beginning of the description of RBC/UKQCD 18knm
The RBC/UKQCD collaboration presented in RBC/UKQCD 18knm~\cite{Boyle:2018knm} the ratio of
decay constants, $f_{B_{s}}/f_B$, using $N_f=2+1$ dynamical ensembles generated using Domain Wall Fermions (DWF). Three lattice spacings, of $a=0.114$, 0.0835 and 0.0727 fm, were used, with pion masses ranging from 139 to 431 MeV, and lattice sizes between 2.65 and 5.47 fm. Two different Domain Wall discretizations (M\"obius and Shamir) have been used for both valence and sea 
quarks.  These discretizations correspond to two different choices for the DWF kernel. The M\"obius DWF are loosely equivalent to Shamir DWF at twice the 
extension in the fifth dimension~\cite{Blum:2014tka}. The bare parameters for these discretizations were chosen to lie on the same scaling trajectory, to enable a combined continuum extrapolation. Heavy quark masses between the charm and approximately half the bottom quark mass were used, with a linear extrapolation in $1/m_H$ applied to reach the physical $B_s$ mass, where $m_H$ is the mass of the heavy meson used to set the heavy quark mass. For the central fit, the authors set the heavy quark mass through the pseudoscalar heavy-strange meson $H_s$, and estimate systematic uncertainties by comparing these results to those obtained with $H$ a heavy-light mesons or a heavy-heavy mesons.
For the quenched heavy quark M\"obius DWF are always used, with a domain-wall height slightly different from the one adopted
for light valence quarks. The choice helps to keep cutoff effects under control, according to the study in Ref.~\cite{Boyle:2016imm}. The chiral-continuum extrapolations are
performed with a Taylor expansion in $a^2$ and $m_\pi^2-(m_\pi^{\mathrm{phys}})^2$ and the associated systematic error is estimated by varying the fit function to apply cuts in the pion mass. The corresponding systematic error is estimated as approximately $0.5\%$, which is roughly equal to the statistical uncertainty and to the systematic uncertainties associated with extrapolation to the physical $m_{B_s}$ mass and with higher-order corrections to the static limit. These latter corrections take the form ${\cal O}(\Lambda/m_{B_s})$ for some scale $\Lambda$, chosen to be $\Lambda = 500$ MeV. Isospin corrections and heavy-quark discretization effects are estimated to be less than $0.1\%$.
% end of the description of RBC/UKQCD 18knm

At time of writing, RBC/UKQCD 18knm~\cite{Boyle:2018knm} has not been published and therefore is not included in our average. Thus, our averages for these quantities remain the same as in Ref.~\cite{Aoki:2016frl},
%
%FLAGRESULT BEGIN
% TAG      & fB    & fBs & fBofBs &END
% REFS     & \cite{Bazavov:2011aa,McNeile:2011ng,Na:2012sp,Aoki:2014nga,Christ:2014uea}& \cite{Bazavov:2011aa,McNeile:2011ng,Na:2012sp,Aoki:2014nga,Christ:2014uea}& \cite{Bazavov:2011aa,McNeile:2011ng,Na:2012sp,Aoki:2014nga,Christ:2014uea,Boyle:2018knm}&END
% UNITS    & '[MeV]' & '[MeV]' & 1 & END
% NUMRESULTS & 5 & 5 & 5 &END
% FLAVOURs & 2+1& 2+1& 2+1& END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&\label{eq:fB21}
\Nf=2+1:&\FLAGAVBEGIN f_{B} &= 192.0(4.3) \FLAGAVEND\;{\rm MeV}
&&\Refs~\mbox{\cite{Bazavov:2011aa,McNeile:2011ng,Na:2012sp,Aoki:2014nga,Christ:2014uea}},\\
&\label{eq:fBs21}
\Nf=2+1: &\FLAGAVBEGIN f_{B_{s}} &= 228.4(3.7)\FLAGAVEND \; {\rm MeV} 
&&\Refs~\mbox{\cite{Bazavov:2011aa,McNeile:2011ng,Na:2012sp,Aoki:2014nga,Christ:2014uea}}, \\
&\label{eq:fBratio21}
%\label{eq:fbav21}
\Nf=2+1: &\FLAGAVBEGIN f_{B_{s}}\over{f_B} &= 1.201(0.016)\FLAGAVEND
&&\Refs~\mbox{\cite{Bazavov:2011aa,Na:2012sp,Aoki:2014nga,Christ:2014uea,Boyle:2018knm}}.
\end{align}
%FLAGRESULTFORMULA END


%%%%%%% N_f = 2+1+1
No new $N_{f}=2+1+1$ calculations of $f_{B}$, $f_{B_{s}}/f_{B}$ or $f_{B_{(s)}}$ have appeared since the last FLAG review. Therefore, our averages for these quantities remain the same as in Ref.~\cite{Aoki:2016frl},
%
%FLAGRESULT BEGIN
% TAG      & fB    & fBs & fBofBs &END
% REFS     & \cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}& \cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}& \cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}&END
% UNITS    & '[MeV]' & '[MeV]' & 1 & END
% NUMRESULTS & 4 & 4 & 4&END
% FLAVOURs & 2+1+1& 2+1+1& 2+1+1& END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&\label{eq:fB211}
\Nf=2+1+1:&\FLAGAVBEGIN f_{B} &= 190.0(1.3) \FLAGAVEND\;{\rm MeV}
&&\Refs~\mbox{\cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}},\\
&\label{eq:fBs211}
\Nf=2+1+1: &\FLAGAVBEGIN f_{B_{s}} &= 230.3(1.3)  \FLAGAVEND\; {\rm MeV}
&&\Refs~\mbox{\cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}}, \\
&\label{eq:fBratio211}
%\label{eq:fbav211}
\Nf=2+1+1: &\FLAGAVBEGIN f_{B_{s}}\over{f_B} &= 1.209(0.005)\FLAGAVEND
&&\Refs~\mbox{\cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}}.
\end{align}
%FLAGRESULTFORMULA END

The PDG presented averages for the $N_{f}=2+1$ and $N_{f}=2+1+1$ lattice-QCD determinations of the isospin-averaged $f_{B}$, $f_{B_{s}}$ and $f_{B_{s}}/f_{B}$ in 2020~\cite{Zyla:2020zbs}.  The $N_{f}=2+1$ and $N_{f}=2+1+1$ lattice-computation results used in Ref.~\cite{Zyla:2020zbs} are identical to those included in our current work, and the averages quoted in Ref.~\cite{Zyla:2020zbs} are those determined in \cite{Aoki:2016frl}.
%


