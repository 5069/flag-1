\subsection{Form factors for $D\to \pi \ell\nu$ and $D\to K \ell \nu$ semileptonic decays}
 \label{sec:DtoPiK}


% Introduction of terminology and notation

The SM prediction for the differential decay rate of the semileptonic processes $D\to \pi \ell\nu$ and
$D\to K \ell \nu$ can be written as
%
\begin{eqnarray}
	\frac{d\Gamma(D\to P\ell\nu)}{dq^2} = \frac{G_{\rm\scriptscriptstyle F}^2 |V_{cx}|^2}{24 \pi^3}
	\,\frac{(q^2-m_\ell^2)^2\sqrt{E_P^2-m_P^2}}{q^4m_{D}^2} \,
	\bigg[ \left(1+\frac{m_\ell^2}{2q^2}\right)m_{D}^2(E_P^2-m_P^2)|f_+(q^2)|^2 & \nonumber\\
+ \frac{3m_\ell^2}{8q^2}(m_{D}^2-m_P^2)^2|f_0(q^2)|^2 & \!\!\!\! \bigg]\,, \label{eq:DtoPiKFull}
\end{eqnarray}
%
where $x = d, s$ is the daughter light quark, $P= \pi, K$ is the
daughter light-pseudoscalar meson, $q = (p_D - p_P)$ is the
momentum of the outgoing lepton pair, and $E_P$ is the light-pseudoscalar meson energy 
in the rest frame of the decaying $D$.  The vector and scalar form
factors $f_+(q^2)$ and $f_0(q^2)$ parameterize the hadronic matrix
element of the heavy-to-light quark flavour-changing vector current
$V_\mu = \overline{x} \gamma_\mu c$,
%
\begin{equation}
\langle P| V_\mu | D \rangle  = f_+(q^2) \left( {p_D}_\mu+ {p_P}_\mu - \frac{m_D^2 - m_P^2}{q^2}\,q_\mu \right) + f_0(q^2) \frac{m_D^2 - m_P^2}{q^2}\,q_\mu \,,
\end{equation}
%
and satisfy the kinematic constraint $f_+(0) = f_0(0)$.  Because the contribution to the decay width from
the scalar form factor is proportional to $m_\ell^2$, within current precision standards it can be
neglected for $\ell = e, \mu$, and Eq.~(\ref{eq:DtoPiKFull})
simplifies to
%
\begin{equation}
\frac{d\Gamma \!\left(D \to P \ell \nu\right)}{d q^2} = \frac{G_{\rm\scriptscriptstyle F}^2}{24 \pi^3} |\vec{p}_{P}|^3 {|V_{cx}|^2 |f_+ (q^2)|^2} \,. \label{eq:DtoPiK}
\end{equation}
%
In models of new physics, decay rates may also receive contributions from matrix elements of other
parity-even currents. In the case of the scalar density, partial vector current conservation allows one
to write matrix elements of the latter in terms of $f_+$ and $f_0$, while for tensor currents $T_{\mu\nu}=\bar x\sigma_{\mu\nu}c$
a new form factor has to be introduced, viz.,
%
\begin{equation}
\langle P| T_{\mu\nu} | D \rangle  = \frac{2}{m_D+m_P}\left[p_{P\mu}p_{D\nu}-p_{P\nu}p_{D\mu}\right]f_T(q^2)\,.
\end{equation}
%
Recall that, unlike the Noether current $V_\mu$, the operator $T_{\mu\nu}$ requires
a scale-dependent renormalization.


% Connection to experiment


Lattice-QCD computations of $f_{+,0}$ allow for comparisons to experiment
to ascertain whether the SM provides the correct prediction for the $q^2$-dependence of
$d\Gamma(D\to P\ell\nu)/dq^2$;
and, subsequently, to determine the CKM matrix elements $|V_{cd}|$ and $|V_{cs}|$
from Eq.~(\ref{eq:DtoPiKFull}). The inclusion of $f_T$ allows for analyses to
constrain new physics. Currently, state-of-the-art experimental results by
CLEO-c~\cite{Besson:2009uv} and BESIII~\cite{Ablikim:2017oaf,Ablikim:2018frk}
provide data for the differential rates in the whole $q^2$ range available,
with a precision of order 2--3\% for the total branching fractions in both
the electron and muon final channels.


% General comments about difficulties faced

Calculations of the $D\to \pi \ell\nu$ and $D\to
K \ell \nu$ form factors typically use the same light-quark and
charm-quark actions as those of the leptonic decay constants $f_D$ and
$f_{D_s}$. Therefore many of the same issues arise; in particular,
considerations about cutoff effects coming from the large charm-quark mass,
or the normalization of weak currents, apply.
Additional complications arise,
however, due to the necessity of covering a sizeable range of values in $q^2$:
\begin{itemize}

\item Lattice kinematics imposes restrictions on the values
of the hadron momenta.
Because lattice calculations are performed
in a finite spatial volume, the pion or kaon three-momentum can only
take discrete values in units of $2\pi/L$ when periodic boundary
conditions are used.  For typical box sizes in recent lattice $D$- and
$B$-meson form-factor calculations, $L \sim 2.5$--3~fm; thus the
smallest nonzero momentum in most of these analyses lies in the range
$|\vec{p}_P| \sim 400$--$500$~MeV.  The largest momentum in lattice
heavy-light form-factor calculations is typically restricted to
$ |\vec{p}_P| \leq 4\pi/L$. %(2,0,0)$.  
For $D \to \pi \ell \nu$ and $D \to
K \ell \nu$, $q^2=0$ corresponds to $|\vec{p}_\pi| \sim 940$~MeV and $|\vec{p}_K| \sim
1$~GeV, respectively, and the full recoil-momentum region is within
the range of accessible lattice momenta.
This has implications for both the accuracy of the study of the $q^2$-dependence,
and the precision of the computation, since statistical errors and cutoff effects
tend to increase at larger meson momenta.
As a consequence, many recent studies have incorporated the use of
nonperiodic (``twisted'') boundary conditions~\cite{Bedaque:2004kc,Sachrajda:2004mi}
as a means to circumvent these difficulties and 
study other values of momentum including,
perhaps, that for which $q^2=0$~\cite{DiVita:2011py,Koponen:2011ev,Koponen:2012di,Koponen:2013tua,Lubicz:2017syv,Lubicz:2018rfs}.
\item Final-state pions and kaons can have energies
$\gtrsim 1~{\rm GeV}$, given the available kinematical range $0 \lesssim q^2 \leq q_{\rm\scriptscriptstyle max}^2=(m_D-m_P)^2$.
This makes the use of (heavy-meson) chiral perturbation theory to extrapolate to physical
light-quark masses potentially problematic.

\item Accurate comparisons to experiment, including the determination of CKM parameters,
requires good control of systematic uncertainties in the parameterization of the $q^2$-dependence of form factors. While this issue is far more important for semileptonic
$B$ decays, where existing lattice computations cover just a fraction of the kinematic range,
the increase in experimental precision requires accurate work in the charm sector
as well. The parameterization of semileptonic form factors is discussed in detail
in Appendix \ref{sec:zparam}.

\end{itemize}


% Description of N_f=2 results

The most advanced $N_f = 2$ lattice-QCD calculation of the
$D \to \pi \ell \nu$ and $D \to K \ell \nu$ form factors is by the ETM
collaboration~\cite{DiVita:2011py}. This work, for which published results
are still at the preliminary stage, uses
the twisted-mass Wilson action for both the light and charm quarks,
with three lattice spacings down to $a \approx 0.068$~fm and (charged)
pion masses down to $m_\pi \approx 270$~MeV.  The calculation employs
the method of Ref.~\cite{Becirevic:2007cr} to avoid the need to
renormalize the vector current, by introducing double-ratios of lattice three-point correlation functions
in which the vector current renormalization cancels. Discretization
errors in the double ratio are of ${\mathcal O}((am_c)^2)$,
due to the automatic ${\mathcal O}(a)$ improvement at maximal twist.
The vector and scalar form factors $f_+(q^2)$ and
$f_0(q^2)$ are obtained by taking suitable linear combinations of
these double ratios.
Extrapolation to physical light-quark masses is performed
using $SU(2)$ heavy-light meson $\chi$PT.  The ETM collaboration simulates with twisted boundary
conditions for the valence quarks to access arbitrary momentum values
over the full physical $q^2$ range, and interpolate to $q^2=0$ using
the Be{\v{c}}irevi{\'c}-Kaidalov ansatz~\cite{Becirevic:1999kt}.  The
statistical errors in $f_+^{D\pi}(0)$ and $f_+^{DK}(0)$ are 9\% and
7\%, respectively, and lead to rather large systematic uncertainties
in the fits to the light-quark mass and energy dependence (7\% and
5\%, respectively).  Another significant source of uncertainty is from
discretization errors (5\% and 3\%, respectively).  On the finest
lattice spacing used in this analysis $am_c \sim 0.17$, so $\cO((am_c)^2)$ cutoff errors are expected to be about 5\%.  This can be
reduced by including the existing $N_f = 2$ twisted-mass ensembles
with $a \approx 0.051$~fm discussed in Ref.~\cite{Baron:2009wt}.


% Description of N_f=2+1 results

The first published $N_f = 2+1$ lattice-QCD calculation of the $D \to
\pi \ell \nu$ and $D \to K \ell \nu$ form factors came from the
Fermilab Lattice, MILC, and HPQCD
collaborations~\cite{Aubin:2004ej}.\footnote{Because only two of the
  authors of this work are members of HPQCD, and to distinguish it
  from other more recent works on the same topic by HPQCD, we
  hereafter refer to this work as ``FNAL/MILC.''}  This work uses
asqtad-improved staggered sea quarks and light ($u,d,s$) valence
quarks and the Fermilab action for the charm quarks, with a single
lattice spacing of $a \approx 0.12$ fm, and for a minimum RMS pion
mass is $\approx 510$~MeV, dictated by the presence of fairly large
staggered taste splittings. The vector current is normalized using a
mostly nonperturbative approach, such that the perturbative truncation
error is expected to be negligible compared to other
systematics. Results for the form factors are provided over the full
kinematic range, rather than focusing just at $q^2=0$ as was customary
in previous work, and fitted to a Be{\v{c}}irevi{\'c}-Kaidalov ansatz.
In fact, the publication of this result predated the precise
measurements of the $D\to K \ell\nu$ decay width by the
FOCUS~\cite{Link:2004dh} and Belle experiments~\cite{Abe:2005sh}, and
showed good agreement with the experimental determination of the shape
of $f_+^{DK}(q^2)$.  Progress on extending this work was reported
in~\cite{Bailey:2012sa}; efforts are aimed at reducing both the
statistical and systematic errors in $f_+^{D\pi}(q^2)$ and
$f_+^{DK}(q^2)$ by increasing the number of configurations analyzed,
simulating with lighter pions, and adding lattice spacings as fine as
$a \approx 0.045$~fm.


The most precise published calculations of the
$D \to \pi \ell \nu$~\cite{Na:2011mc} and $D \to
K \ell \nu$~\cite{Na:2010uf} form factors in $N_f=2+1$ QCD
are by the HPQCD collaboration. They are also based on $N_f = 2+1$
asqtad-improved staggered MILC configurations, but use two lattice spacings
$a \approx 0.09$ and 0.12~fm, and a HISQ action for the valence
$u,d,s$, and $c$ quarks. In these mixed-action calculations, the HISQ
valence light-quark masses are tuned so that the ratio $m_l/m_s$ is
approximately the same as for the sea quarks; the minimum RMS sea-pion
mass $\approx 390$~MeV. Form factors are determined only at $q^2=0$,
by using a Ward identity to relate matrix elements of vector
currents to matrix elements of the absolutely normalized quantity
$(m_{c} - m_{x} ) \langle P | \bar{x}c | D \rangle$,
and exploiting the kinematic identity $f_+(0) = f_0(0)$
to yield
$f_+(q^2=0) = (m_{c} - m_{x} ) \langle P | \bar{x}c | D \rangle / (m^2_D - m^2_P)$.
A modified $z$-expansion (cf. App.~\ref{sec:zparam})
is employed to simultaneously extrapolate to the physical
light-quark masses and continuum and interpolate to $q^2 = 0$, and
allow the coefficients of the series expansion to vary with the light-
and charm-quark masses.  The form of the light-quark dependence is
inspired by $\chi$PT, and includes logarithms of the form $m_\pi^2
{\rm log} (m_\pi^2)$ as well as polynomials in the valence-, sea-, and
charm-quark masses.  Polynomials in $E_{\pi(K)}$ are also included to
parameterize momentum-dependent discretization errors.
The number of terms is increased until the result for $f_+(0)$
stabilizes, such that the quoted fit error for $f_+(0)$ not only contains
statistical uncertainties, but also reflects relevant systematics.  The
largest quoted uncertainties in these calculations are from statistics and
charm-quark discretization errors. Progress towards extending the computation
to the full $q^2$ range have been reported in~\cite{Koponen:2011ev,Koponen:2012di};
however, the information contained in these conference proceedings
is not enough to establish an updated value of $f_+(0)$ with respect
to the previous journal publications.

The most recent $N_f=2+1$ computation of $D$ semileptonic form factors has
been carried out by the JLQCD collaboration, and so far published in conference
proceedings only; the most recent update is Ref.~\cite{Kaneko:2017xgg}.
They use their own M\"obius domain-wall configurations at three values
of the lattice spacing $a=0.080, 0.055, 0.044~{\rm fm}$, with several
pion masses ranging from 226 to 501~MeV (though there is so far only one
ensemble, with $m_\pi=284~{\rm MeV}$, at the finest lattice spacing).
The vector and scalar form factors are computed at four values of the momentum transfer for each ensemble.
The computed form factors are observed to depend mildly on both the
lattice spacing and the pion mass.
The momentum dependence of the form factors is fitted to a BCL
$z$-parameterization with a Blaschke factor that contains the measured
value of the $D_{(s)}^*$ mass in the vector channel,
and a trivial Blaschke factor in the scalar channel. The systematics
of this latter fit is assessed by a BCL fit with the experimental value
of the scalar resonance mass in the Blaschke factor.
Continuum and chiral extrapolations are carried out through a
linear fit in the squared lattice spacing and the square pion and $\eta_c$ masses.
A global fit that uses hard-pion HM$\chi$PT to model the mass dependence
is furthermore used for a comparison of the form factor shapes with experimental data.\footnote{It is important
to stress the finding in~\cite{Colangelo:2012ew} that
the factorization of chiral logs in hard-pion $\chi$PT breaks down,
implying that it does not fulfill the expected requisites for a proper
effective field theory. Its use to model the mass dependence of form
factors can thus be questioned.}
Since the computation is only published in proceedings so far, it will not
enter our $N_f=2+1$ average.\footnote{The ensemble parameters
quoted in Ref.~\cite{Kaneko:2017xgg} appear to show that the volumes
employed at the lightest pion masses are insufficient to meet our criteria
for finite-volume effects. There is however a typo in the table which result
in a wrong assignment of lattice sizes, whereupon the criteria are indeed met.
We thank T.~Kaneko for correspondence on this issue.}



The first full computation of both the vector and scalar form factors
in $N_f=2+1+1$ QCD has been achieved by the
ETM collaboration~\cite{Lubicz:2017syv}. They have furthermore provided a separate
determination of the tensor form factor, relevant for new physics analyses~\cite{Lubicz:2018rfs}.
Both works use the available $N_f = 2+1+1$ twisted-mass Wilson lattices~\cite{Baron:2010bv},
totaling three lattice spacings down to $a\approx 0.06$~fm,
and a minimal pion mass of 220~MeV.
Matrix elements are extracted from suitable double ratios of correlation functions
that avoid the need of nontrivial current normalizations.
The use of twisted boundary conditions allows both for imposing
several kinematical conditions, and considering arbitrary frames
that include moving initial mesons. 
After interpolation to the physical strange- and charm-quark masses,
the results for form factors are fitted to a modified $z$-expansion
that takes into account both the light-quark mass dependence through
hard-pion $SU(2)$ $\chi$PT~\cite{Bijnens:2010ws}, and the
lattice-spacing dependence. In the case of the latter,
a detailed study of Lorentz-breaking effects due to the breaking of
rotational invariance down to the hypercubic subgroup
is performed, leading to a nontrivial momentum-dependent parameterization
of cutoff effects.
The $z$-parameterization itself includes a single-pole Blaschke factor
(save for the scalar channel in $D\to K$, where the Blaschke factor is trivial),
with pole masses treated as free parameters.
The final quoted uncertainty on the form factors
is about 5--6\% for $D\to\pi$, and 4\% for $D\to K$.
The dominant source of uncertainty is quoted as statistical+fitting procedure+input parameters ---
the latter referring to the values of quark masses, the lattice spacing (i.e., scale setting),
and the LO $SU(2)$ LECs.

The FNAL/MILC collaboration has also reported ongoing work on extending their computation
to $N_f=2+1+1$, using MILC HISQ ensembles at four values
of the lattice spacing down to $a=0.042~{\rm fm}$ and pion masses down to the
physical point. The latest updates on this computation, focusing on the form factors
at $q^2=0$, but without explicit values of the latter yet, can be found in Refs.~\cite{Primer:2015qpz,Primer:2017xzs}.
A similar update of the HPQCD collaboration is ongoing, for which results
for the $D \to K$ vector and scalar form factors
are being determined for the full $q^2$ range based on
MILC $N_f=2+1+1$ ensembles~\cite{Chakraborty:2017pud}.
This supersedes previously reported progress by HPQCD in extending
their $N_f=2+1$ computation to nonvanishing $q^2$, see Refs.~\cite{Koponen:2011ev,Koponen:2012di}. 


% Summary table and discussion of estimates

\begin{table}[h]
\begin{center}
\mbox{} \\[3.0cm]
\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} r l l l l l l l c c}
Collaboration & Ref. & $\Nf$ & 
\hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{chiral extrapolation}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{finite volume}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{renormalization}\end{rotate}\hspace{-0.15cm}  &
\hspace{0.15cm}\begin{rotate}{60}{heavy-quark treatment}\end{rotate}\hspace{-0.15cm}  &
$f_+^{D\pi}(0)$ & $f_+^{DK}(0)$\\
&&&&&&&&& \\[-0.1cm]
\hline
\hline
&&&&&&&&& \\[-0.1cm]
%
ETM 17D, 18 & \cite{Lubicz:2017syv,Lubicz:2018rfs} & 2+1+1 & \gA	 & \good & \soso & \soso & \good & \okay & 0.612(35) & 0.765(31) \\[0.5ex]
&&&&&&&&& \\[-0.1cm]
%
\hline\\[0.5ex]
JLQCD 17B & \cite{Kaneko:2017xgg} & 2+1 & \rC & \good & \good & \soso & \good & \okay & 0.615(31)($^{+17}_{-16}$)($^{+28}_{-7}$)$^*$ & 0.698(29)(18)($^{+32}_{-12}$)$^*$ \\[0.5ex]
Meinel 16 & \cite{Meinel:2016dqj} & 2+1 & \gA & \soso & \good & \good & \soso & \okay & n/a & n/a \\[0.5ex]
HPQCD 11 & \cite{Na:2011mc} & 2+1 & \gA  & \soso & \soso & \soso & \good &  \okay & 0.666(29) &\\[0.5ex]
HPQCD 10B & \cite{Na:2010uf} & 2+1 & \gA  & \soso & \soso & \soso & \good &  \okay & & 0.747(19)  \\[0.5ex]
FNAL/MILC 04 & \cite{Aubin:2004ej} & 2+1 & \gA  & \tbr & \tbr & \soso & \soso & \okay & 0.64(3)(6)& 0.73(3)(7)
\\[0.5ex]
&&&&&&&&& \\[-0.1cm]
%
\hline\\[0.5ex]
ETM 11B & \cite{DiVita:2011py} & 2 & \rC  & \soso & \soso & \good & \good &  \okay & 0.65(6)(6) & 0.76(5)(5)\\[0.5ex]
&&&&&&&&& \\[-0.1cm]
\hline
\hline
\end{tabular*}\\
\begin{minipage}{\linewidth}
{\footnotesize 
\begin{itemize}
   \item[$^*$] The first error is statistical, the second from the $q^2\to 0$ extrapolation, the third from the chiral-continuum extrapolation.
\end{itemize}
}
\end{minipage}
\caption{Summary of computations of charmed-hadrons semileptonic form factors. Note that Meinel~16 addresses only $\Lambda_c\to \Lambda$ transitions (hence the absence of quoted values for $f_+^{D\pi}(0)$ and $f_+^{DK}(0)$), while ETM~18 provides a computation of tensor form factors.}
\label{tab_DtoPiKsumm2}
\end{center}
\end{table}




Table \ref{tab_DtoPiKsumm2} contains our summary of the existing
calculations of the $D \to \pi \ell \nu$ and $D \to K \ell \nu$
semileptonic form factors.  Additional tables in
Appendix~\ref{app:DtoPi_Notes} provide further details on the
simulation parameters and comparisons of the error estimates. Recall
that only calculations without red tags that are published in a
refereed journal are included in the FLAG average. We will quote
no FLAG estimate for $N_f=2$, since the results by ETM have only appeared
in conference proceedings. For $N_f=2+1$, only HPQCD~10B,11 qualify,
which provides our estimate for $f_+(q^2=0)=f_0(q^2=0)$.
For $N_f=2+1+1$, we quote as FLAG estimate the only available
result by ETM 17D: 

%
%FLAGRESULT BEGIN
% TAG      & Dtopi    & DtoK	 &END
% REFS     & \cite{Na:2011mc} &\cite{Na:2010uf} &END
% UNITS    & 1 & 1 &END
% NUMRESULTS & 2 & 2 & 2 &END
% FLAVOURs & 2+1 & 2+1 &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
	&&\FLAGAVBEGIN f_+^{D\pi}(0)&=  0.666(29)\FLAGAVEND&&\Ref~\mbox{\cite{Na:2011mc}},\nonumber\\[-3mm]
&N_f=2+1:&\label{eq:Nf=2p1Dsemi}\\[-3mm]
        &&\FLAGAVBEGIN f_+^{DK}(0)  &= 0.747(19)\FLAGAVEND &&\Ref~\mbox{\cite{Na:2010uf}}.\nonumber
\end{align}
%FLAGRESULTFORMULA END
%

%
%FLAGRESULT BEGIN
% TAG      & Dtopi    & DtoK	 &END
% REFS     & \cite{Lubicz:2017syv} &\cite{Lubicz:2017syv} &END
% UNITS    & 1 & 1 &END
% NUMRESULTS & 2 & 2 & 2 &END
% FLAVOURs & 2+1+1 & 2+1+1 &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
	&&\FLAGAVBEGIN f_+^{D\pi}(0)&=  0.612(35)\FLAGAVEND&&\Ref~\mbox{\cite{Lubicz:2017syv}},\nonumber\\[-3mm]
&N_f=2+1+1:&\label{eq:Nf=2p1p1Dsemi}\\[-3mm]
        &&\FLAGAVBEGIN f_+^{DK}(0)  &= 0.765(31)\FLAGAVEND &&\Ref~\mbox{\cite{Lubicz:2017syv}}.\nonumber
\end{align}
%FLAGRESULTFORMULA END
%

In Fig.~\ref{fig:DtoPiK} we display the existing $N_f =2$, $N_f = 2+1$, and $N_f=2+1+1$
results for $f_+^{D\pi}(0)$ and $f_+^{DK}(0)$; the grey bands show our
estimates of these quantities.  Sec.~\ref{sec:Vcd} discusses the
implications of these results for determinations of the CKM matrix
elements $|V_{cd}|$ and $|V_{cs}|$ and tests of unitarity of the
second row of the CKM matrix.

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.7\linewidth]{HQ/Figures/DtoPiandDtoK}

\vspace{-2mm}
\caption{$D\to\pi \ell\nu$ and $D\to K\ell\nu$ semileptonic form
  factors at $q^2=0$. The HPQCD result for
  $f_+^{D\pi}(0)$ is from HPQCD 11, the one for $f_+^{DK}(0)$
  represents HPQCD 10B (see Tab.~\ref{tab_DtoPiKsumm2}). \label{fig:DtoPiK}}
 \end{center}
\end{figure}




\subsection{Form factors for $\Lambda_c\to\Lambda\ell\nu$ semileptonic decays}

In recent years, Meinel and collaborators have pioneered the computation of form
factors for semileptonic heavy-baryon decays (see also Sec.~\ref{sec:Lambdab}).
In particular, Ref.~\cite{Meinel:2016dqj} deals with $\Lambda_c\to\Lambda\ell\nu$
transitions. The motivation for this study is twofold: apart from allowing for
a new determination of $|V_{cs}|$ in combination with the recent pioneering
experimental measurement of the decay rates in Refs.~\cite{Ablikim:2015prg,Ablikim:2016vqd},
it allows one to test the techniques previously employed for $b$ baryons in the
better-controlled (from the point of view of systematics) charm environment.

The amplitudes of the decays $\Lambda_c\to \Lambda\ell\nu$
receive contributions from both the vector and the axial components of the current
in the matrix element
$\langle \Lambda|\bar s\gamma^\mu(\mathbf{1}-\gamma_5)c|\Lambda_c\rangle$,
and can be parameterized in terms of six different form factors --- see, e.g., Ref.~\cite{Feldmann:2011xf}
for a complete description. They split into three form factors $f_+$, $f_0$, $f_\perp$ in the
parity-even sector, mediated by the vector component of the current, and another three form factors
$g_+,g_0,g_\perp$ in the parity-odd sector, mediated by the axial component. All
of them provide contributions that are parametrically comparable.

The computation in Meinel 16~\cite{Meinel:2016dqj} uses RBC/UKQCD $N_f=2+1$ DWF ensembles,
and treats the $c$ quarks within the Columbia RHQ approach.
Two values of the lattice spacing ($a\sim0.11,~0.085~{\rm fm}$) are considered,
with the absolute scale set from the $\Upsilon(2S)$--$\Upsilon(1S)$ splitting.
In one ensemble the pion mass $m_\pi=139~{\rm MeV}$ is at the physical point,
while for other ensembles they range roughly in the 300--350~MeV interval.
Results for the form factors are obtained from suitable three-point functions,
and fitted to a modified $z$-expansion ansatz that combines the $q^2$-dependence
with the chiral and continuum extrapolations. The paper goes on to quote
the predictions for the total rates in the $e$ and $\mu$ channels
(where errors are statistical and systematic, respectively)
%
\begin{gather}
\begin{split}
\frac{\Gamma(\Lambda_c\to \Lambda e^+\nu_e)}{|V_{cs}|^2} &= 0.2007(71)(74)~{\rm ps}^{-1}\,,\\
\frac{\Gamma(\Lambda_c\to \Lambda\mu^+\nu_\mu)}{|V_{cs}|^2} &= 0.1945(69)(72)~{\rm ps}^{-1}\,.
\end{split}
\end{gather}
%
The combination with the recent experimental determination of the total branching fractions
by BESIII in Refs.~\cite{Ablikim:2015prg,Ablikim:2016vqd} to extract $|V_{cs}|$
is discussed in Sec.~\ref{sec:Vcd} below.








