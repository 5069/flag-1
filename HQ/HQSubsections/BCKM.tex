\subsection{Determination of $|V_{ub}|$}
\label{sec:Vub}

We now use the lattice-determined Standard Model transition amplitudes
for leptonic (Sec.~\ref{sec:fB}) and semileptonic
(Sec.~\ref{sec:BtoPiK}) $B$-meson decays to obtain exclusive
determinations of the CKM matrix element $|V_{ub}|$.
In this section, we describe the aspect of our work
that involves experimental input for the relevant charged-current
exclusive decay processes.
The relevant
formulae are Eqs.~(\ref{eq:B_leptonic_rate})
and~(\ref{eq:B_semileptonic_rate}). Among leptonic channels the only
input comes from $B\to\tau\nu_\tau$, since the rates for decays to $e$
and $\mu$ have not yet been measured.  In the semileptonic case we
only consider $B\to\pi\ell\nu$ transitions (experimentally
measured for $\ell=e,\mu$). As discussed in Secs.~\ref{sec:BtoPiK} and~\ref{sec:Lambdab},
there are now lattice predictions for the rates of the decays $B_s\to K\ell\nu$
and $\Lambda_b\to p\ell\nu$; however, in the former case the process has not been
experimentally measured yet, while in the latter case the only existing lattice computation
does not meet FLAG requirements for controlled systematics.

We first investigate the determination of $|V_{ub}|$ through the
$B\to\tau\nu_\tau$ transition.  This is the only experimentally
measured leptonic decay channel of the charged $B$ meson.
The experimental measurements of the branching fraction of
this channel, $B(B^{-} \to \tau^{-} \bar{\nu})$, have not been
updated since the publication of the previous FLAG report~\cite{Aoki:2016frl}.
In Tab.~\ref{tab:leptonic_B_decay_exp} we summarize the
current status of experimental results for this branching fraction.
%
\begin{table}[h]
\begin{center}
\noindent
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}lll}
Collaboration & Tagging method  & $B(B^{-}\to \tau^{-}\bar{\nu})
                                  \times 10^{4}$\\
&& \\[-2ex]
\hline \hline &&\\[-2ex]
Belle~\cite{Adachi:2012mm} &  Hadronic  & $0.72^{+0.27}_{-0.25}\pm 0.11$ \\
Belle~\cite{Kronenbitter:2015kls} &  Semileptonic & $1.25 \pm 0.28 \pm
                                                    0.27$ \\
&& \\[-2ex]
 \hline
&& \\[-2ex]
BaBar~\cite{Lees:2012ju} & Hadronic & $1.83^{+0.53}_{-0.49}\pm 0.24$\\
BaBar~\cite{Aubert:2009wt} & Semileptonic  & $1.7 \pm 0.8 \pm 0.2$\\
&& \\[-2ex]
\hline \hline && \\[-2ex]
\end{tabular*}
\caption{Experimental measurements for $B(B^{-}\to \tau^{-}\bar{\nu})$.
  The first error on each result is statistical, while the second
  error is systematic.}
\label{tab:leptonic_B_decay_exp}
\end{center}
\end{table}
%

It is obvious that all the measurements listed in Tab.~\ref{tab:leptonic_B_decay_exp} have significance smaller than
$5\sigma$, and the large uncertainties are dominated by statistical errors. These measurements lead to the averages
of experimental measurements for $B(B^{-}\to \tau \bar{\nu})$~\cite{Kronenbitter:2015kls,Lees:2012ju},
%
\begin{eqnarray}
 B(B^{-}\to \tau \bar{\nu} )\times 10^4 &=& 0.91 \pm 0.22 \mbox{ }{\rm from}\mbox{ } {\rm Belle,} \label{eq:leptonic_B_decay_exp_belle}\\
                             &=& 1.79 \pm 0.48 \mbox{ }{\rm from }\mbox{ } {\rm BaBar,} \label{eq:leptonic_B_decay_exp_babar}\\
                             &=& 1.06 \pm 0.33 \mbox{ }{\rm average,}
\label{eq:leptonic_B_decay_exp_ave}
\end{eqnarray}
%
where, following our standard procedure we perform a weighted average and rescale the uncertainty by the square root of the reduced chi-squared. Note that the Particle Data Group~\cite{Rosner:2015wva} did not inflate the uncertainty in the calculation of the averaged branching ratio.

Combining the results in Eqs.~(\ref{eq:leptonic_B_decay_exp_belle}--\ref{eq:leptonic_B_decay_exp_ave}) with
the experimental measurements of the mass of the $\tau$-lepton and the
$B$-meson lifetime and mass we get
%
\begin{eqnarray}
 |V_{ub}| f_{B} &=& 0.72 \pm 0.09 \mbox{ }{\rm MeV}\mbox{ }{\rm from}\mbox{ } {\rm Belle,}\label{eq:Vub_fB_belle}\\
                &=& 1.01 \pm 0.14 \mbox{ }{\rm MeV}\mbox{ }{\rm from }\mbox{ } {\rm BaBar,}\label{eq:Vub_fB_babar} \\
                &=& 0.77 \pm 0.12 \mbox{ }{\rm MeV}\mbox{ } {\rm average,}\label{eq:Vub_fB}
\end{eqnarray}
%
which can be used to extract $|V_{ub}|$, viz.,
%
\begin{align}
&N_f=2    &\mbox{Belle}~B\to\tau\nu_\tau:   && |V_{ub}| &= 3.83(14)(48) \times 10^{-3}  \,,  \\
&N_f=2+1  &\mbox{Belle}~B\to\tau\nu_\tau:   && |V_{ub}| &= 3.75(8)(47) \times 10^{-3}   \,,  \\
&N_f=2+1+1&\mbox{Belle}~B\to\tau\nu_\tau:   && |V_{ub}| &= 3.79(3)(47) \times 10^{-3}   \,;  \\
&         & \nonumber \\
&N_f=2    &\mbox{Babar}~B\to\tau\nu_\tau:   && |V_{ub}| &=  5.37(20)(74) \times 10^{-3} \,,  \\
&N_f=2+1  &\mbox{Babar}~B\to\tau\nu_\tau:   && |V_{ub}| &=  5.26(12)(73) \times 10^{-3} \,,  \\
&N_f=2+1+1&\mbox{Babar}~B\to\tau\nu_\tau:   && |V_{ub}| &= 5.32(4)(74) \times 10^{-3}  \,,  \\
&         & \nonumber \\
&N_f=2    &\mbox{average}~B\to\tau\nu_\tau:   && |V_{ub}| &=  4.10(15)(64) \times 10^{-3} \,,  \\
&N_f=2+1  &\mbox{average}~B\to\tau\nu_\tau:   && |V_{ub}| &=  4.01(9)(63) \times 10^{-3} \,,  \\
&N_f=2+1+1&\mbox{average}~B\to\tau\nu_\tau:   && |V_{ub}| &= 4.05(3)(64) \times 10^{-3}  \,,
\end{align}
where the first error comes from the uncertainty in $f_B$ and the second comes from experiment.


Let us now turn our attention to semileptonic decays. The experimental
value of $|V_{ub}|f_+(q^2)$ can be extracted from the measured
branching fractions for $B^0\to\pi^\pm\ell\nu$ and/or
$B^\pm\to\pi^0\ell\nu$ applying
Eq.~(\ref{eq:B_semileptonic_rate});\footnote{Since $\ell=e,\mu$ the
  contribution from the scalar form factor in
  Eq.~(\ref{eq:B_semileptonic_rate}) is negligible.}  $|V_{ub}|$ can
then be determined by performing fits to the constrained BCL $z$-parameterization of the form factor $f_+(q^2)$ given in
Eq.~(\ref{eq:bcl_c}). This can be done in two ways: one option is to
perform separate fits to lattice and experimental results, and extract
the value of $|V_{ub}|$ from the ratio of the respective $a_0$
coefficients; a second option is to perform a simultaneous fit to
lattice and experimental data, leaving their relative normalization
$|V_{ub}|$ as a free parameter. We adopt the second strategy, because
it combines the lattice and experimental input in a more efficient
way, leading to a smaller uncertainty on $|V_{ub}|$.

The available state-of-the-art experimental input consists of five
data sets: three untagged measurements by BaBar
(6-bin~\cite{delAmoSanchez:2010af} and 12-bin~\cite{Lees:2012vv}) and
Belle~\cite{Ha:2010rf}, all of which assume isospin symmetry and
provide combined $B^0\to\pi^-$ and $B^+\to\pi^0$ data; and the two
tagged Belle measurements of $\bar B^0\to\pi^+$ (13-bin) and
$B^-\to\pi^0$ (7-bin)~\cite{Sibidanov:2013rkk}.  Including all of
them, along with the available information about cross-correlations,
will allow us to obtain a meaningful final error
estimate.\footnote{See, e.g., Sec.~V.D of~~\cite{Lattice:2015tia} for
  a detailed discussion.} The lattice input data set will be the same
discussed in Sec.~\ref{sec:BtoPiK}.


We perform a constrained BCL fit of the vector and scalar form factors (this is necessary in order to take into account the $f_+(q^2=0) = f_0 (q^2=0)$ constraint) together with the combined experimental data sets. We find that the error on $|V_{ub}|$ stabilizes for $(N^+ = N^0 = 3)$. The result of the combined fit is presented in
Tab.~\ref{tab:FFVUBPI}.
%
\begin{table}[t]
\begin{center}
\begin{tabular}{|c|c|cccccc|}
\multicolumn{8}{l}{$B\to \pi\ell\nu \; (N_f=2+1)$} \\[0.2em]\hline
        & Central Values & \multicolumn{6}{|c|}{Correlation Matrix} \\[0.2em]\hline
$V_{ub}^{} \times 10^3$ & 3.73 (14)   &  1 & 0.852 & 0.345 & $-$0.374 & 0.211 & 0.247 \\[0.2em]
$a_0^+$                 & 0.414 (12)  &  0.852 & 1 & 0.154 & $-$0.456 & 0.259 & 0.144 \\[0.2em]
$a_1^+$                 & $-$0.494 (44) &  0.345 & 0.154 & 1 & $-$0.797 & $-$0.0995 & 0.223 \\[0.2em]
$a_2^+$                 & $-$0.31 (16)  & $-$0.374 & $-$0.456 & $-$0.797 & 1 & 0.0160 & $-$0.0994 \\[0.2em]
$a_0^0$                 & 0.499 (19)  &  0.211 & 0.259 & $-$0.0995 & 0.0160 & 1 & $-$0.467  \\[0.2em]
$a_1^0$                 & $-$1.426 (46) &  0.247 & 0.144 & 0.223 & $-$0.0994 & $-$0.467 & 1 \\[0.2em]
\hline
\end{tabular}
\end{center}
\caption{$|V_{ub}|$, coefficients for the $N^+ =N^0=N^T=3$ $z$-expansion of the $B\to \pi$ form factors $f_+$ and $f_0$, and their correlation matrix. \label{tab:FFVUBPI}}
\end{table}
%


%
In Fig.~\ref{fig:Vub_SL_fit} we show both the lattice and experimental data for
$(1-q^2/m_{B^*}^2)f_+(q^2)$ as a function of $z(q^2)$, together with our preferred fit;
experimental data has been rescaled by the resulting value for $|V_{ub}|^2$.
It is worth noting the good consistency between the form factor shapes from
lattice and experimental data. This can be quantified, e.g., by computing the ratio of the
two leading coefficients in the constrained BCL parameterization: the fit to lattice form
factors yields  $a_1^+/a_0^+=-1.67(35)$   (cf.~the results presented in Sec.~\ref{sec:BtoPi}),
while the above lattice+experiment fit yields  $a_1^+/a_0^+=-1.19(13)$.



%
\begin{figure}[tbp]
\begin{center}
\includegraphics[width=0.65\textwidth]{HQ/Figures/plots.form.factors/ff_Bpi_latt+exp.eps}
\caption{
Lattice and experimental data for $(1-q^2/m_{B^*}^2)f_+^{B\to\pi}(q^2)$ and $f_0^{B\to \pi} (q^2)$ versus $z$.
Green symbols denote lattice-QCD points included in the fit, while blue and indigo
points show experimental data divided by the value of $|V_{ub}|$ obtained from the fit. The grey and orange bands display the preferred $N^+ = N^0 = 3$ BCL fit (six parameters) to the lattice-QCD and experimental data with errors.
}
\label{fig:Vub_SL_fit}
\end{center}
\end{figure}
%

We plot the values of $|V_{ub}|$ we have obtained in
Fig.~\ref{fig:Vxbsummary}, where the (GGOU) determination through inclusive decays
by the Heavy Flavour Averaging Group (HFLAV)~\cite{Amhis:2016xyh},
yielding $|V_{ub}| = 4.52 \penalty 0 (15)({}^{+11}_{-14}) \times 10^{-3}$, is also shown for comparison.
In this plot the tension between the BaBar and the Belle measurements of $B(B^{-} \to
\tau^{-} \bar{\nu})$ is manifest. As discussed above, it is for this
reason that we do not extract $|V_{ub}|$ through the average of
results for this branching fraction from these two collaborations. In
fact this means that a reliable determination of $|V_{ub}|$ using
information from leptonic $B$-meson decays is still absent;
the situation will only clearly improve with the more precise experimental
data expected from Belle~II~\cite{Urquijo:2015qsa,Kou:2018nap}.
The value for $|V_{ub}|$ obtained from semileptonic $B$ decays for $N_f=2+1$, on the other hand,
is significantly more precise than both the leptonic and the inclusive determinations,
and exhibits the well-known $\sim 3\sigma$ tension with the latter.

\subsection{Determination of $|V_{cb}|$}
\label{sec:Vcb}

We will now use the lattice-QCD results for the $B \to D^{(*)}\ell\nu$ form factors
in order to obtain determinations of the CKM matrix element $|V_{cb}|$ in the Standard Model.
The relevant formulae are given in~Eq.~(\ref{eq:vxb:BtoDstar}).

Let us summarize the lattice input that satisfies FLAG requirements for the control
of systematic uncertainties, discussed in Sec.~\ref{sec:BtoD}.
In the (experimentally more precise) $B\to D^*\ell\nu$ channel, there is only one
$N_f=2+1$ lattice computation of the relevant form factor $\mathcal{F}^{B\to D^*}$ at zero recoil.
Concerning the $B \to D\ell\nu$ channel, for $N_f=2$ there is one determination
of the relevant form factor $\mathcal{G}^{B\to D}$ at zero recoil;\footnote{The same work
provides $\mathcal{G}^{B_s\to D_s}$, for which there are, however, no experimental data.} while
for $N_f=2+1$ there are two determinations of the $B \to D$ form factor as a function
of the recoil parameter in roughly the lowest third of the kinematically allowed region.
In this latter case, it is possible to replicate the analysis carried out for $|V_{ub}|$
in~Sec.~\ref{sec:Vub}, and perform a joint fit to lattice and experimental data;
in the former, the value of $|V_{cb}|$ has to be extracted by matching to the
experimental value for $\mathcal{F}^{B\to D^*}(1)\eta_{\rm EW}|V_{cb}|$ and
$\mathcal{G}^{B\to D}(1)\eta_{\rm EW}|V_{cb}|$.

The latest experimental average by HFLAV~\cite{Amhis:2016xyh} for the $B\to D^*$ form factor at zero recoil makes use of the CLN~\cite{Caprini:1997mu} parameterization of the $B\to D^*$ form factor and is
%
\begin{gather}
[\mathcal{F}^{B\to D^*}(1)\eta_{\rm EW}|V_{cb}|]_{\rm CLN,HFLAV} = 35.61(43)\times 10^{-3}\,.
\label{eq:BDsHFLAFVCLN}
\end{gather}
%
Recently the Belle collaboration presented an updated measurement of the $B\to D^* \ell\nu$ branching ratio~\cite{Abdesselam:2018nnh} in which, as suggested in Refs.~\cite{Bigi:2017njr, Bernlochner:2017xyx, Grinstein:2017nlq},  the impact of the form factor parameterization has been studied by comparing the CLN~\cite{Caprini:1997mu} and BGL~\cite{Boyd:1994tt, Boyd:1997kz} ans\"atze. The fit results using the two parameterizations are perfectly compatible. In light of the fact that the BGL parameterization imposes much less stringent constraints on the shape of the form factor than the CLN one we choose to focus on the BGL fit:
\begin{align}
[\mathcal{F}^{B\to D^*}(1)\eta_{\rm EW}|V_{cb}|]_{\rm BGL, \; Belle}\ &= 34.93(23)(59)\times 10^{-3}\,,
\label{eq:BDsBelleBCL}
\end{align}
%
where the first error is statistical and the second is systematic. In the following we present determinations of $|V_{cb}|$ obtained from~Eqs.(\ref{eq:BDsHFLAFVCLN}) and (\ref{eq:BDsBelleBCL}).
%
By using $\eta_{\rm EW}=1.00662$~\footnote{Note that this determination does not include the electromagnetic Coulomb correction roughly estimated in Ref.~\cite{Bailey:2014tva}. Currently the numerical impact of this correction is negligible.} and the $N_f = 2 +1$ lattice value for $\mathcal{F}^{B\to D^*}(1)$ in~Eq.~(\ref{eq:BDstarFNAL})~\footnote{In light of our policy not to average $N_f=2+1$ and $N_f = 2+1+1$ calculations and of the controversy over the use of the CLN vs.\ BGL parameterizations, we prefer to simply use only the more precise $N_f=2+1$ determination of $\mathcal{F}^{B\to D^*}(1)$ in Eq.~(\ref{eq:BDstarFNAL}) for the extraction of $V_{cb}$.}, we thus extract the averages
%
\begin{align}
& N_f=2+1 & [B\to D^*\ell\nu]_{\rm CLN ,HFLAV}: && |V_{cb}| &= 39.05(55)(47) \times 10^{-3} \,, \\
& N_f=2+1 & [B\to D^*\ell\nu]_{\rm BGL, Belle}: && |V_{cb}| &= 38.30(53)(69) \times 10^{-3} \,,
\label{eq:vcbdsav}
\end{align}
%
where the first uncertainty comes from the lattice computation and the second from the
experimental input.

For the zero-recoil $B \to D$ form factor, HFLAV~\cite{Amhis:2016xyh} quotes
\begin{gather}
\mbox{HFLAV:} \qquad \mathcal{G}^{B\to D}(1)\eta_{\rm EW}|V_{cb}| =
41.57(45)(89) \times 10^{-3}\,,
\label{eq:BDHFLAV}
\end{gather}
yielding the following average for $N_f=2$:
\begin{align}
&N_f=2&B\to D\ell\nu: && |V_{cb}| &= 40.0(3.7)(1.0) \times 10^{-3} \,,
\end{align}
where the first uncertainty comes from the lattice computation and the second from the experimental input.

Finally, for $N_f=2+1$ we perform, as discussed above, a joint fit to the available
lattice data, discussed in Sec.~\ref{sec:BtoD}, and state-of-the-art experimental determinations.
In this case, we will combine the aforementioned recent Belle measurement~\cite{Glattauer:2015teq},
which provides partial integrated decay rates in 10 bins in the recoil parameter $w$,
with the 2010 BaBar data set in Ref.~\cite{Aubert:2009ac}, which quotes the value of
$\mathcal{G}^{B\to D}(w)\eta_{\rm EW}|V_{cb}|$ for  ten values of $w$.\footnote{We thank Marcello Rotondo for providing the ten bins result of the BaBar analysis.}
The fit is dominated by the more precise Belle data; given this, and the fact that only partial
correlations among systematic uncertainties are to be expected, we will treat both data sets
as uncorrelated.\footnote{ We have checked that results using just one experimental data set
are compatible within $1\sigma$. In the case of BaBar, we have
taken into account the introduction of some EW corrections in the data.}

A constrained $(N^+ = N^0 = 3)$ BCL fit using the same ansatz as for lattice-only data
in Sec.~\ref{sec:BtoD}, yields our average, which we present in Tab.~\ref{tab:FFVCBD}.
The fit is illustrated in Fig.~\ref{fig:Vcb_SL_fit}. In passing, we
note that, if correlations between the FNAL/MILC and HPQCD
calculations are neglected, the $|V_{cb}|$ central value rises to $40.3
\times 10^{-3}$ in nice agreement with the results presented in
Ref.~\cite{Bigi:2016mdz}.
%
\begin{table}[t]
\begin{center}
\begin{tabular}{|c|c|cccccc|}
\multicolumn{8}{l}{$B\to D\ell\nu \; (N_f=2+1)$} \\[0.2em]\hline
        & Central Values & \multicolumn{6}{|c|}{Correlation Matrix} \\[0.2em]\hline
$|V_{cb}^{}| \times 10^3$ & 40.1 (1.0)  &  1 & $-$0.525 & $-$0.431 & $-$0.185 & $-$0.526 & $-$0.497 \\[0.2em]
$a_0^+$                   & 0.8944 (95) &  $-$0.525 & 1 & 0.282 & $-$0.162 & 0.953 & 0.450 \\[0.2em]
$a_1^+$                   & $-$8.08 (22)  &  $-$0.431 & 0.282 & 1 & 0.613 & 0.350 & 0.934 \\[0.2em]
$a_2^+$                   & 49.0 (4.6)  &  $-$0.185 & $-$0.162 & 0.613 & 1 & $-$0.0931 & 0.603 \\[0.2em]
$a_0^0$                   & 0.7802 (75) &  $-$0.526 & 0.953 & 0.350 & $-$0.0931 & 1 & 0.446 \\[0.2em]
$a_1^0$                   & $-$3.42 (22)  &  $-$0.497 & 0.450 & 0.934 & 0.603 & 0.446 & 1 \\[0.2em]
\hline
\end{tabular}
\end{center}
\caption{$|V_{cb}|$, coefficients for the $N^+ =N^0=N^T=3$ $z$-expansion of the $B\to D$ form factors $f_+$ and $f_0$, and their correlation matrix. \label{tab:FFVCBD}}
\end{table}
%

In order to combine the determinations of $|V_{cb}|$
from exclusive $B\to D$ and $B\to D^*$ semileptonic decays, we need to estimate
the correlation between the lattice uncertainties in the two modes.
We assume conservatively that the statistical component of the lattice
error in both determinations are 100\% correlated because they are based
on the same MILC configurations (albeit on different subsets).
Considering separately the BGL and CLN determination of $|V_{cb}|$
from $B\to D^*$ semileptonic decays, we obtain:
%
\begin{align}
|V_{cb}^{}|\times 10^3 &=   39.08 (91) \;\quad {\rm BGL,Belle}\\
|V_{cb}^{}|\times 10^3 &=   39.41 (60) \;\quad {\rm CLN,HFLAV}
\end{align}
%
where we applied a rescaling factor 1.35 to the BGL case.


Our results are summarized in Tab.~\ref{tab:Vcbsummary}, which also
shows the HFLAV inclusive determination of $|V_{cb}|=42.00(65) \times 10^{-3}$~\cite{Gambino:2016jkc} for comparison, and illustrated in Fig.~\ref{fig:Vxbsummary}.


%
\begin{table}[!t]
\begin{center}
\noindent
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}lcc}
 & from  & $|V_{cb}| \times 10^3$\\
&& \\[-2ex]
\hline \hline &&\\[-2ex]
our average for $N_f=2+1$ (BGL) & $B \to D^*\ell\nu$ & $38.30(53)(69)$ \\
our average for $N_f=2+1$ (CLN) & $B \to D^*\ell\nu$ & $39.05(55)(47)$ \\
our average for $N_f=2+1$ & $B \to D\ell\nu$ &  $40.1(1.0)$  \\
our average for $N_f=2+1$ (BGL) & $B \to (D,D^*)\ell\nu$  & $39.08(91)$ \\
our average for $N_f=2+1$ (CLN) & $B \to (D,D^*)\ell\nu$ & $39.41(60)$ \\
&& \\[-2ex]
 \hline
our average for $N_f=2$ & $B \to D\ell\nu$ & $41.0(3.8)(1.5)$ \\
&& \\[-2ex]
 \hline
HFLAV inclusive average & $B \to X_c\ell\nu$ & $42.46(88)$ \\
&& \\[-2ex]
\hline \hline && \\[-2ex]
\end{tabular*}
\caption{Results for $|V_{cb}|$. When two errors are quoted in
our averages, the first one comes from the lattice form factor, and the
second from the experimental measurement. The HFLAV inclusive average
obtained in the kinetic scheme
from Ref.~\cite{Amhis:2016xyh} is shown for comparison.}
\label{tab:Vcbsummary}
\end{center}
\end{table}
%

%
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.65\textwidth]{HQ/Figures/plots.form.factors/ff_BD_latt+exp.eps}
\caption{
Lattice and experimental data for $f_+^{B\to D}(q^2)$ and $f_0^{B\to D}(q^2)$ versus $z$.
Green symbols denote lattice-QCD points included in the fit, while blue and indigo
points show experimental data divided by the value of $|V_{cb}|$ obtained from the fit. The grey and orange bands display the preferred $N^+=N^0=3$ BCL fit (six parameters) to the lattice-QCD and experimental data with errors.
}
\label{fig:Vcb_SL_fit}
\end{center}
\end{figure}
%

%
\begin{figure}[!h]
\begin{center}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=1.0\linewidth]{HQ/Figures/Vub}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=1.0\linewidth]{HQ/Figures/Vcb}
\end{minipage}
\vspace{-2mm}
\caption{Left: Summary of $|V_{ub}|$ determined using: i) the $B$-meson leptonic
decay branching fraction, $B(B^{-} \to \tau^{-} \bar{\nu})$, measured
at the Belle and BaBar experiments, and our averages for $f_{B}$ from
lattice QCD; and ii) the various measurements of the $B\to\pi\ell\nu$
decay rates by Belle and BaBar, and our averages for lattice determinations
of the relevant vector form factor $f_+(q^2)$.
Right: Same for determinations of $|V_{cb}|$ using semileptonic decays.
The HFLAV inclusive results are from Ref.~\cite{Amhis:2016xyh, Gambino:2016jkc}.}
\label{fig:Vxbsummary}
\end{center}
\end{figure}
%

In Fig.~\ref{fig:VubVcb} we present a summary of determinations of $|V_{ub}|$ and $|V_{cb}|$ from $B\to (\pi,D^{(*)})\ell\nu$ and $B\to \tau \nu$. For comparison purposes, we also add the determination of $|V_{ub}/V_{cb}|$ obtained from
$\Lambda_b\to (p,\Lambda_c)\ell\nu$ decays in Refs.~\cite{Detmold:2015aaa,Aaij:2015bfa}---which, as discussed
in the text, does not meet the FLAG criteria to enter our averages---as well as the results from inclusive $B\to X_{u,c} \ell\nu$ decays. Currently, the determinations of $V_{cb}$ from $B\to D^*$ and $B\to D$ decays are quite compatible; however, a sizeable tension involving the extraction of $V_{cb}$ from inclusive dedecays remains. In the determination of the $1\sigma$ and $2\sigma$
contours for our average we have included an estimate of the correlation between $|V_{ub}|$ and $|V_{cb}|$ from semileptonic $B$ decays: the lattice inputs to these quantities are dominated by results from the Fermilab/MILC and HPQCD collaborations which are both based on MILC $N_f=2+1$ ensembles, leading to our conservatively introducing a 100\% correlation between the lattice statistical uncertainties of the three computations involved. The results of the fit are
%
\begin{align}
\begin{cases}
|V_{cb}^{}|\times 10^3 =  39.09 (68) & \\
|V_{ub}^{}| \times 10^3 = 3.73 (14) & {\rm BGL}\\
p{\rm -value} = 0.32 &
\end{cases}
%
\;\;\;\; {\rm and} \;\;\;\;
%
\begin{cases}
|V_{cb}^{}|\times 10^3 =   39.41 (61) & \\
|V_{ub}^{}| \times 10^3 = 3.74 (14) & {\rm CLN}\\
p{\rm -value} = 0.55 &
\end{cases}
\end{align}
%
for the BGL and CLN $B\to D^*$ parameterizations, respectively.
%
\begin{figure}[!h]
\begin{center}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=1\linewidth]{HQ/Figures/vub_vcb_BGL.eps}
\end{minipage}
\begin{minipage}{0.49\textwidth}
\includegraphics[width=1\linewidth]{HQ/Figures/vub_vcb_CLN.eps}
\end{minipage}
\vspace{-2mm}
\caption{Summary of $|V_{ub}|$ and $|V_{cb}|$ determinations. Left and right panels correspond to using the BGL and CLN parameterization for the $B\to D^*$ form factor, respectively. The solid and dashed lines correspond to 68\% and 95\% C.L. contours. As discussed in the text, baryonic modes are not included in our averages. The results of the fit in the two cases are $(|V_{cb}^{}|,|V_{ub}^{}|) \times 10^3 = (39.09\pm 0.68, 3.73 \pm 0.14)$ with a $p$-value of 0.32 and $(|V_{cb}^{}|,|V_{ub}^{}|) \times 10^3 = (39.41\pm 0.61, 3.74 \pm 0.14)$ with a $p$-value of 0.55, for the BGL and CLN $B\to D^*$ parameterizations, respectively.
\label{fig:VubVcb}}
\end{center}
\end{figure}
%

References~\cite{Bigi:2016mdz, Bigi:2017njr, Grinstein:2017nlq} published
in 2016 and 2017 presented evidence that
there can be a considerable difference in the CKM matrix elements when
choosing between the CLN and BGL parameterizations of form factors.
In mid-2018, it appeared that switching to BGL
might resolve the difference between the inclusive and exclusive determinations of $|V_{cb}|$;
however, it did not seem to shed light on $|V_{ub}|$.
In September, 2018, a new analysis of Belle~\cite{Abdesselam:2018nnh}
appeared to find a 10\% difference between CNL and BGL parametrizations for
$\mathcal{F}^{B\to D^*}(1)\eta_{\rm EW}|V_{cb}|$, supporting previous findings.
However, in April, 2019, a new version of that preprint found the two
parametrizations completely compatible.  Further, in March, 2019, a BaBar
preprint~\cite{Dey:2019bgc} presented an angular analysis of the full
dataset from that experiment.  This unbinned fit using the BGL parametrization
of the form factors and the FNAL/MILC result for $\mathcal{F}^{B\to D^*}(1)$
finds $|V_{cb}| = (38.36\pm 0.90)\times 10^{-3},$ quite compatible with previous
exclusive determinations and not indicating a resolution of the difference
from the inclusive value.
A recent paper by Gambino, Jung, and Schacht~\cite{Gambino:2019sif} reviews
the history, presents numerous fits of the Belle tagged and untagged data,
and finds about a $2 \sigma$ difference between exclusive and inclusive
values for $|V_{cb}|$.

It will be interesting to see what happens when both experimental and
theoretical precisions are improved.  At least four groups are working
to improve the form factor calculations: FNAL/MILC, HPQCD, JLQCD,
and LANL/SWME.  It
would also be good to have additional results on the $\Lambda_b$ form factors.
We can expect new measurements from Belle II and LHCb.
