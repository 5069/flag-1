 \subsubsection{Nested averaging}
\label{sec:nested_average}

We have encountered one case
where the correlations between results are more involved,
and a nested averaging scheme is required.
This concerns the $B$-meson bag parameters discussed in Sec.~\ref{sec:BMix}.
In the following, we describe the details of the nested averaging scheme.
This is an updated version of the section added in the web update of the FLAG 16 report.

The issue arises for a quantity $Q$ that is given by a ratio, $Q=Y/Z$.
In most simulations, both $Y$ and $Z$ are calculated, and the error in $Q$ can be
obtained in each simulation in the standard way.
However, in other simulations only $Y$ is calculated,
with $Z$ taken from a global average of some type.
The issue to be addressed is that this average value $\overline{Z}$ has errors
that are correlated with those in $Q$.

In the example that arises in Sec.~\ref{sec:BMix},
$Q=B_B$,  $Y=B_B f_B^2$ and $Z=f_B^2$.
In one of the simulations that contribute to the average, 
$Z$ is replaced by $\overline{Z}$, 
the PDG average for $f_B^2$~\cite{Rosner:2015wva}
(obtained with an averaging procedure similar to that used by FLAG).
This simulation is labeled with $i=1$, so that
\begin{equation}
 Q_1 = \frac{Y_1}{\overline{Z}}.
  \label{eq:FNAL_B_PDG}
\end{equation}
The other simulations have results labeled $Q_j$, with $j\ge 2$.
In this set up, the issue is that $\overline{Z}$ is correlated with the $Q_j$, $j\ge 2$.\footnote{%
%
There is also a small correlation between $Y_1$ and $\overline{Z}$, but we follow the
original Ref.~\cite{Bazavov:2016nty}
 and do not take this into account. Thus, the error in $Q_1$
is obtained by simple error propagation from those in $Y_1$ and $\overline{Z}$.
Ignoring this correlation is conservative, because, as in the
calculation of $B_K$, the correlations between $B_B f_B^2$ and $f_B^2$ tend to
lead to a cancelation of errors. By ignoring this effect we are making a small overestimate
of the error in $Q_1$.}
%

We begin by decomposing the error in $Q_1$ in the same
schematic form as above,
\begin{equation}
 Q_1 %= \frac{Y_1}{\overline{Z}}
  = x_1 
  \pm \frac{\sigma_{Y_1}^{(1)}}{\overline{Z}}
  \pm \frac{\sigma_{Y_1}^{(2)}}{\overline{Z}} \pm\cdots
  \pm \frac{\sigma_{Y_1}^{(E)}}{\overline{Z}}
  \pm \frac{Y_1 \sigma_{\overline{Z}}}{\overline{Z}^2}.
  \label{eq:Q1nested}
\end{equation}
Here the last term represents the error propagating from that in $\overline{Z}$,
while the others arise from errors in $Y_1$.
For the remaining $Q_j$ ($j\ge 2$) the decomposition is as in Eq.~(\ref{eq:resultQi}).
The total error of $Q_1$ then reads 
\begin{equation}
 \sigma_1^2 = 
  \left(\frac{\sigma_{Y_1}^{(1)}}{\overline{Z}}\right)^2
  + \left(\frac{\sigma_{Y_1}^{(2)}}{\overline{Z}}\right)^2 +\cdots
  + \left(\frac{\sigma_{Y_1}^{(E)}}{\overline{Z}}\right)^2
  + \left(\frac{Y_1}{\overline{Z}^2}\right)^2 \sigma_{\overline{Z}}^2,
  \label{eq:sigma1}
\end{equation}
while that for the $Q_j$ ($j\ge 2$) is
\begin{equation}
 \sigma_j^2 = 
  \left(\sigma_j^{(1)}\right)^2
  + \left(\sigma_j^{(2)}\right)^2 +\cdots
  + \left(\sigma_j^{(E)}\right)^2.
  \label{eq:sigmaj}
\end{equation}
Correlations between $Q_j$ and $Q_k$ ($j,k\ge 2$) are taken care of by
Schmelling's prescription, as explained above.
What is new here is how the correlations 
between $Q_1$ and $Q_j$ ($j\ge 2$) are taken into account.

To proceed, we recall from Eq.~(\ref{eq:sigma2av}) that
$\sigma_{\overline{Z}}$ is given by
\begin{equation}
 \sigma_{\overline{Z}}^2 = \sum_{{i'},{j'}=1}^{M'} \omega[Z]_{i'}
  \omega[Z]_{j'} C[Z]_{i'j'}.
\end{equation}
Here the indices
$i'$ and $j'$ run over the $M'$ simulations that contribute to $\overline{Z}$,
which, in general, are different from those contributing to the results for $Q$.
The weights $\omega[Z]$ and correlation matrix $C[Z]$ are given an explicit
argument $Z$ to emphasize that they refer to the calculation of this quantity
and not to that of $Q$.
$C[Z]$ is calculated using the Schmelling prescription
[Eqs.~(\ref{eq:sigmaij})--(\ref{eq:sigma2av})] in terms of the errors, $\sigma[Z]_{i'}^{(\alpha)}$,
taking into account the correlations between the different calculations of $Z$.

We now generalize Schmelling's prescription for $\sigma_{i;j}$, Eq.~(\ref{eq:sigmaij}),
to that for $\sigma_{1;k}$ ($k\ge 2$), i.e., the part of the error in $Q_1$ that
is correlated with $Q_k$. We take
\begin{equation}
 \sigma_{1;k} \,\, = \,\, 
  \sqrt{
  \frac{1}{\overline{Z}^2} \sum^\prime_{(\alpha)\leftrightarrow k}
  \Big[\sigma_{Y_1}^{(\alpha)} \Big]^2 
  + \frac{Y_1^2}{\overline{Z}^4} 
  \sum_{i',j'}^{M'} \omega[Z]_{i'} \omega[Z]_{j'} C[Z]_{i'j'\leftrightarrow k}
  }
  \,\,\, .
\label{eq:sigma1k}
\end{equation}
The first term under the square root sums those sources of error in $Y_1$ that
are correlated with $Q_k$. Here we are using a more explicit notation from that
in Eq.~(\ref{eq:sigmaij}), with $(\alpha) \leftrightarrow k$ indicating that the sum
is restricted to the values of $\alpha$ for which the error $\sigma_{Y_1}^{(\alpha)}$
is correlated with $Q_k$.
The second term accounts for the correlations within $\overline{Z}$ with $Q_k$,
and is the nested part of the present scheme.
The new matrix $C[Z]_{i'j'\leftrightarrow k}$ is a restriction
of the full correlation matrix $C[Z]$, and is defined as follows.
Its diagonal elements are given by
\begin{eqnarray}
C[Z]_{i'i'\leftrightarrow k} \,\,&=& \,\, (\sigma[Z]_{i'\leftrightarrow k})^2 \qquad \qquad (i' = 1, \cdots , M') \,\,\, ,
 \\
 (\sigma[Z]_{i'\leftrightarrow k})^2 & = &
  \sum^\prime_{(\alpha)\leftrightarrow k} (\sigma[Z]_{i'}^{(\alpha)})^2,
\label{eq:sigmaZipk}
\end{eqnarray}
where the summation 
$\sum^\prime_{(\alpha)\leftrightarrow k}$ 
over $(\alpha)$ is restricted to those $\sigma[Z]_{i'}^{(\alpha)}$ that are
correlated with $Q_k$.
The off-diagonal elements are
\begin{eqnarray}
C[Z]_{i'j'\leftrightarrow k} \,\,&=& \,\, \sigma[Z]_{i';j'\leftrightarrow k} \, \sigma[Z]_{j';i'\leftrightarrow k} \qquad \qquad (i' \neq j') \,\,\, ,\\
 \sigma[Z]_{i';j'\leftrightarrow k} & = &
  \sqrt{
  \sum^\prime_{(\alpha)\leftrightarrow j'k} 
  (\sigma[Z]_{i'}^{(\alpha)})^2},
\label{eq:sigmaZipjpk}
\end{eqnarray}
where the summation 
$\sum^\prime_{(\alpha)\leftrightarrow j'k}$ 
over $(\alpha)$ is restricted to $\sigma[Z]_{i'}^{(\alpha)}$ that are
correlated with {\it both} $Z_{j'}$ and $Q_k$.

The last quantity that we need to define is $\sigma_{k;1}$.
\begin{equation}
\sigma_{k;1} \,\, = \,\, \sqrt{\sum^\prime_{(\alpha)\leftrightarrow 1} \Big[ \sigma_k^{(\alpha)} \Big]^2 } \,\,\, ,
\label{eq:sigmak1}
\end{equation}
where the summation $\sum^\prime_{(\alpha)\leftrightarrow 1}$ is
restricted to those $\sigma_k^{(\alpha)}$ that are correlated with one of
the terms in Eq.~(\ref{eq:sigma1}).

In summary, we construct the correlation matrix $C_{ij}$ using
Eq.~(\ref{eq:Ciiij}), as in the generic case, except the expressions
for $\sigma_{1;k}$ and $\sigma_{k;1}$ are now given by
Eqs.~(\ref{eq:sigma1k}) and (\ref{eq:sigmak1}), respectively. All other $\sigma_{i;j}$ are given by
the original Schmelling prescription, Eq.~(\ref{eq:sigmaij}).
In this way we extend the philosophy of Schmelling's approach while accounting
for the more involved correlations.


