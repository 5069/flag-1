% !TEX root = ../FLAG_master.tex
\subsection{Bottom quark mass}
\label{s:bmass}


Now we review the lattice results for the $\overline{\rm MS}$-bottom-quark mass $\overline{m}_b$.  Related heavy quark actions and observables have been discussed 
in previous FLAG reviews \cite{Aoki:2013ldr,Aoki:2016frl,Aoki:2019cca}, and descriptions
can be found in Sec.~\ref{app:HQactions}.  In Tab.~\ref{tab:mb} we collect results for
$\overline{m}_b(\overline{m}_b)$ obtained with $N_f =2+1$ and
$2+1+1$ sea quark flavours.  Available results for the quark mass ratio $m_b / m_c$ are also reported.
After discussing the new results we evaluate the corresponding FLAG averages.

\input{qmass/qmass_tab_mb}

\subsubsection{$N_f=2+1$}

There are no new three flavour results since the last review, and as in previous reviews, we simply take the HPQCD 10 result as our average,
%FLAGRESULT BEGIN
% TAG      &mb&END
% REFS     &\cite{McNeile:2010ji}&END
% UNITS    & '[GeV]' &END
% NUMRESULTS & 1  &END
% FLAVOURs & 2+1  &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&N_f= 2+1 :  &\FLAGAVBEGIN\overline{m}_b(\overline{m}_b)& = 4.164 (23)  \FLAGAVEND ~ \gev&&\Ref ~\mbox{\cite{McNeile:2010ji}}\,.
\end{align}
%FLAGRESULTFORMULA END
Since HPQCD quotes $\overline{m}_b(\overline{m}_b)$ using $N_f
= 5$ running, we used that value in the average. The corresponding 4-flavour RGI
average is
\begin{align}
&N_f= 2+1 :  & M_b^{\rm RGI} & = 6.874(38)_m(54)_\Lambda  ~ \gev = 6.874(66)  ~ \gev &&\Ref ~\mbox{\cite{McNeile:2010ji}}\,.
\end{align}

\subsubsection{$N_f=2+1+1$}

HPQCD 21syc ~\cite{Hatton:2021syc} is an update of HPQCD 14A (and replaces it in our average), including EM corrections for the first time for the $b$ quark mass. Four flavours of HISQ quarks are used on MILC ensembles with lattice spacings from about 0.09 to 0.03 fm. Ensembles with physical and unphysical mass sea quarks are used. Quenched QED is used to obtain the dominant $O(\alpha)$ effect. The ratio of bottom to charm quark masses is computed in a completely non-perturbative formulation, and the $b$ quark mass is extracted using the value of $\overline{m}_c(3~\rm GeV)$ from HPQCD 20qhk. Since EM effects are included, the QED renormalization scale enters the ratio which is quoted for 3 GeV and $n_f=4$. The total error on the new result is more than $2\times$ smaller than for HPQCD 14A, but is only slightly smaller compared to the NRQCD result reported in HPQCD 14B. The inclusion of QED shifts the ratio $m_b/m_c$ up slightly from the pure QCD value by about one standard deviation, and the value of $\overline{m}_b(m_b)$ is consistent, within errors, to the other pure QCD results entering our average. Therefore we quote a single average.

HPQCD 14B employs the NRQCD
action~\cite{Colquhoun:2014ica} to treat the $b$ quark.  The
$b$ quark mass is computed with the moments method, that is, from
Euclidean-time moments of two-point, heavy-heavy-meson correlation
functions (see also Sec.~\ref{s:curr} for a description of the method).

In HPQCD 14B the $b$ quark mass is computed from ratios of the moments
$R_n$ of heavy current-current correlation functions, namely, 
%
\be
\left[\frac{R_n r_{n-2}}{R_{n-2}r_n}\right]^{1/2} \frac{\bar{M}_{\rm
    kin}}{2 m_b} = \frac{\bar{M}_{\Upsilon,\eta_b}}{2 \bar m_b(\mu)} ~
,
      \label{eq:moments}
\ee 
%
where $r_n$ are the perturbative moments calculated at N$^3$LO,
$\bar{M}_{\rm kin}$ is the spin-averaged kinetic mass of the
heavy-heavy vector and pseudoscalar mesons and
$\bar{M}_{\Upsilon,\eta_b}$ is the experimental spin average of the
$\Upsilon$ and $\eta_b$ masses.  The average kinetic mass $\bar{M}_{\rm kin}$
is chosen since in the lattice calculation the splitting of the
$\Upsilon$ and $\eta_b$ states is inverted.  In Eq.~(\ref{eq:moments}),
the bare mass $m_b$ appearing on the left-hand side is tuned so that
the spin-averaged mass agrees with experiment, while the mass
$\overline{m}_b$ at the fixed scale $\mu = 4.18$ GeV is extrapolated
to the continuum limit using three HISQ (MILC) ensembles with $a
\approx$ 0.15, 0.12 and 0.09 fm and two pion masses, one of which is
the physical one. Their final result is
$\overline{m}_b(\mu = 4.18\, \gev) = 4.207(26)$ GeV, where the error is
from adding systematic uncertainties in quadrature only (statistical
errors are smaller than $0.1 \%$ and ignored). The errors arise from
renormalization, perturbation theory, lattice spacing, and NRQCD
systematics. The finite-volume uncertainty is not estimated, but at
the lowest pion mass they have $ m_\pi L \simeq 4$, which leads to the
tag \good\ .

The next four-flavour result~\cite{Bussone:2016iua} is from the ETM collaboration and updates their preliminary result
appearing in a conference proceedings~\cite{Bussone:2014cha}. The calculation is performed on a set of configurations
generated with twisted-Wilson fermions with three lattice spacings in
the range 0.06 to 0.09 fm and with pion masses in the range 210 to 440
MeV. The $b$ quark mass is determined from a ratio of heavy-light
pseudoscalar meson masses designed to yield the quark pole mass in the
static limit. The pole mass is related to the $\overline{\rm MS}$ mass
through perturbation theory at N$^3$LO. The key idea is that by taking
ratios of ratios, the $b$ quark mass is accessible through fits to
heavy-light(strange)-meson correlation functions computed on the
lattice in the range $\sim 1$--$2\times m_c$ and the static limit, the
latter being exactly 1. By simulating below $\overline{m}_b$, taking
the continuum limit is easier. They find
$\overline{m}_b(\overline{m}_b) = 4.26(3)(10)$ GeV, where the first
error is statistical and the second systematic. The dominant errors
come from setting the lattice scale and fit systematics.

Gambino, {\it et al.}~\cite{Gambino:2017vkx} use twisted-mass-fermion ensembles from the ETM collaboration and the ETM ratio method as in ETM 16. Three values of the lattice spacing are used, ranging from 0.062 to 0.089 fm. Several volumes are also used. The light quark masses produce pions with masses from 210 to 450 MeV. The main difference with ETM 16 is that the authors use the kinetic mass defined in the heavy quark expansion (HQE)  to extract the $b$ quark mass instead of the pole mass.

The final $b$ quark mass result is FNAL/MILC/TUM 18~\cite{Bazavov:2018omf}. The mass is extracted from the same fit and analysis done for the charm quark mass. Note that relativistic HISQ quarks are used (almost) all the way up to the $b$ quark mass (0.9 $am_b$) on the finest two lattices, $a=0.03$ and 0.042 fm. The authors investigated the effect of leaving out the heaviest points from the fit, and the result did not noticeably change.

All of the above results enter our average. We note that here the ETM 16 result is consistent with the average and a stretching factor on the error is not used. The average and error is dominated by the very precise FNAL/MILC/TUM 18 value.
%FLAGRESULT BEGIN
% TAG      &mb &END
% REFS     &\cite{Hatton:2021syc,Colquhoun:2014ica,Bussone:2016iua,Gambino:2017vkx,Bazavov:2018omf}&END
% UNITS    & '[GeV]' &   &END
% NUMRESULTS & 5  &END
% FLAVOURs & 2+1+1  &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&N_f= 2+1+1 :&\FLAGAVBEGIN\overline{m}_b(\overline{m}_b)& = 4.203 (11)  \FLAGAVEND ~ \gev&&\Refs~\mbox{\cite{Hatton:2021syc,Colquhoun:2014ica,Bussone:2016iua,Gambino:2017vkx,Bazavov:2018omf}}\,.
\end{align}
%FLAGRESULTFORMULA END
We have included a 100\%
correlation on the statistical errors of ETM 16 and Gambino 17 since
the same ensembles are used in both. While FNAL/MILC/TUM 18 and HPQCD 21syc also use the same MILC HISQ ensembles, the statistical error in the HPQCD 21syc analysis is negligible, so we do not include a correlation between them. The average has $\chi^2/{\rm dof}=0.02$.

The above translates to the RGI average
\begin{align}
&N_f= 2+1+1 :& M_b^{\rm RGI} & = 6.936(20)_m(54)_\Lambda  ~ \gev = 6.936(57) ~ \gev&&\Refs~\mbox{\cite{Hatton:2021syc,Chakraborty:2014aca,Colquhoun:2014ica,Bussone:2016iua,Gambino:2017vkx,Bazavov:2018omf}}\,.
\end{align}

All the results for $\overline{m}_b(\overline{m}_b)$ discussed above
are shown in Fig.~\ref{fig:mb} together with the FLAG averages
corresponding to $N_f=2+1$ and $2+1+1$ quark flavours.
\begin{figure}[!htb]
\begin{center}
%\psfig{file=qmass/Figures/mb,width=11cm}
\includegraphics[width=11cm]{qmass/Figures/mb}
\end{center}
\vspace{-1cm}
\caption{ \label{fig:mb} The $b$ quark mass, $N_f =2+1$ and $2+1+1$. The updated PDG value from
  Ref.~\cite{Tanabashi:2018oca} is reported for comparison.}
\end{figure}
