#
use POSIX;
use strict;
#use Math::Matrix;

my %mass;
my $tag;
my $CORR;
my $corr;
#2+1+1
# mu=mc
if(1){
    $CORR="YES";
    $mass{"HPQCD14"}{"mass"}=1.2715;
    $mass{"HPQCD14"}{"err"}=0.00494; 
    $mass{"HPQCD14"}{"sys err"}=0.0081;
    # A and B switched in last script.
    # this is updated by ETM 21
    #$mass{"ETM14"}{"mass"}=1.348;
    #$mass{"ETM14"}{"err"}=0.036;
    #$mass{"ETM14"}{"sys err"}=0.028;
    # keep this
    $mass{"ETM14A"}{"mass"}=1.3478;
    $mass{"ETM14A"}{"err"}=0.0027;
    $mass{"ETM14A"}{"sys err"}=0.0195;
    $mass{"ETM21"}{"mass"}=1.335;
    $mass{"ETM21"}{"err"}=0.022;
    $mass{"ETM21"}{"sys err"}=0.019;
    $mass{"FMT18"}{"mass"}=1.273;
    $mass{"FMT18"}{"err"}=0.004;
    $mass{"FMT18"}{"sys err"}=0.010;
    $mass{"HPQCD20"}{"mass"}=1.2719;
    $mass{"HPQCD20"}{"err"}=0.0078;
    $mass{"HPQCD20"}{"sys err"}=0.0;

}
# mu=3 GeV
if(1){
    $CORR="YES";
    $mass{"HPQCD20"}{"mass"}=0.9841;
    $mass{"HPQCD20"}{"err"}=0.0051;
    $mass{"HPQCD20"}{"sys err"}=0.0;
    $mass{"FMT18"}{"mass"}=0.9837;
    $mass{"FMT18"}{"err"}=0.0043;
    $mass{"FMT18"}{"sys err"}=0.0036;
    $mass{"HPQCD14"}{"mass"}=0.9851;
    $mass{"HPQCD14"}{"err"}=0.0022;
    $mass{"HPQCD14"}{"sys err"}=0.0059;
    $mass{"ETM21"}{"mass"}=1.036;
    $mass{"ETM21"}{"err"}=0.017; 
    $mass{"ETM21"}{"sys err"}=0.015; 
    $mass{"ETM14A"}{"mass"}=1.0557;
    $mass{"ETM14A"}{"err"}=0.0022;
    $mass{"ETM14A"}{"sys err"}=0.0153;
}
# 2+1
# mu=mc
if(0){
$mass{"HPQCD10"}{"mass"}=1.273;
$mass{"HPQCD10"}{"err"}=0.006; 
$mass{"HPQCD10"}{"sys err"}=0.0;
$mass{"XQCD14"}{"mass"}=1.304;
$mass{"XQCD14"}{"err"}=0.005;
$mass{"XQCD14"}{"sys err"}=0.020;
$mass{"JLQCD16"}{"mass"}=1.273;
$mass{"JLQCD16"}{"err"}=0.010;
$mass{"JLQCD16"}{"sys err"}=0.0;
}
# mu=3 GeV
if(0){
$mass{"HPQCD10"}{"mass"}=0.986;
$mass{"HPQCD10"}{"err"}=0.006; 
$mass{"HPQCD10"}{"sys err"}=0.0;
$mass{"XQCD14"}{"mass"}=1.006;
$mass{"XQCD14"}{"err"}=0.005;
$mass{"XQCD14"}{"sys err"}=0.022;
$mass{"JLQCD16"}{"mass"}=1.0033;
$mass{"JLQCD16"}{"err"}=0.0096;
$mass{"JLQCD16"}{"sys err"}=0.0;
}
my $avg;
my $errsq;
my %weight;
my $normw;
my $dof;

foreach $tag (sort(keys(%mass))){
    $weight{$tag} = 1/($mass{$tag}{"err"}**2+$mass{$tag}{"sys err"}**2);
    $normw += $weight{$tag};
}

foreach $tag (sort(keys(%mass))){

    $avg += $weight{$tag}/$normw * $mass{$tag}{"mass"};
    $errsq += $weight{$tag};
    printf "$tag %e stat %e sys %e\n", $mass{$tag}{"mass"}, $mass{$tag}{"err"}, $mass{$tag}{"sys err"};
}

printf "charm quark avg mass %e %e\n", $avg, 1/$errsq**0.5;

# include 100% correlations for stat errors (or others if needed)
if($CORR=="YES"){
    my $c12 = sqrt($mass{"ETM14A"}{"err"}**2) * sqrt($mass{"ETM21"}{"err"}**2);
    $corr = 2*($c12)*$weight{"ETM14A"}*$weight{"ETM21"}/$normw**2;
    my $c34 = sqrt($mass{"HPQCD14"}{"err"}**2) * sqrt($mass{"FMT18"}{"err"}**2);
    $corr += 2*($c34)*$weight{"HPQCD14"}*$weight{"FMT18"}/$normw**2;
    my $c56 = sqrt($mass{"HPQCD14"}{"err"}**2) * sqrt($mass{"HPQCD20"}{"err"}**2);
    $corr += 2*($c56)*$weight{"HPQCD14"}*$weight{"HPQCD20"}/$normw**2;
    my $c78 = sqrt($mass{"HPQCD20"}{"err"}**2) * sqrt($mass{"FMT18"}{"err"}**2);
    $corr += 2*($c78)*$weight{"HPQCD20"}*$weight{"FMT18"}/$normw**2;
}else{
    $corr=0.;
}
$errsq = (1/$errsq+$corr);
printf "c quark avg mass %e corr err %e\n", $avg, $errsq**0.5;

# calc chisq 
my $chisq;
my $sum;
foreach $tag (sort(keys(%mass))){
   $chisq = $mass{$tag}{"mass"} - $avg;
   $chisq *= $chisq;
   $chisq /= ($mass{$tag}{"err"}**2+$mass{$tag}{"sys err"}**2);
   $sum+= $chisq;
   $dof++;
}
$dof--;
printf "chisq = %e dof=%g chisq/dof = %e\n", $sum, $dof, $sum/$dof;
printf "c quark avg mass %e corr err %e stretch factor %e \n", $avg, sqrt($sum/$dof)*$errsq**0.5, sqrt($sum/$dof);
