#
use POSIX;
use strict;
#use Math::Matrix;

my %mass;
my $tag;
my $CORR;
my $corr;
#2+1+1
if(1){
    $CORR="YES";
    $mass{"HPQCD14A"}{"mass"}=11.652;
    $mass{"HPQCD14A"}{"err"}=0.035; 
    $mass{"HPQCD14A"}{"sys err"}=0.055;
    $mass{"ETM21"}{"mass"}=11.48;
    $mass{"ETM21"}{"err"}=0.12;
    $mass{"ETM21"}{"sys err"}=0.25;
    $mass{"FMT18"}{"mass"}=11.784;
    $mass{"FMT18"}{"err"}=0.011;
    $mass{"FMT18"}{"sys err"}=0.019;
}
# 2+1
if(0){
    $mass{"HPQCD10"}{"mass"}=0.986;
    $mass{"HPQCD10"}{"err"}=0.006; 
    $mass{"HPQCD10"}{"sys err"}=0.0;
    $mass{"XQCD14"}{"mass"}=1.006;
    $mass{"XQCD14"}{"err"}=0.005;
    $mass{"XQCD14"}{"sys err"}=0.022;
    $mass{"JLQCD16"}{"mass"}=1.0033;
    $mass{"JLQCD16"}{"err"}=0.0096;
    $mass{"JLQCD16"}{"sys err"}=0.0;
}
my $avg;
my $errsq;
my %weight;
my $normw;
my $snormw;
my $dof;

foreach $tag (sort(keys(%mass))){
    $weight{$tag} = 1/($mass{$tag}{"err"}**2+$mass{$tag}{"sys err"}**2);
    $normw += $weight{$tag};
}

foreach $tag (sort(keys(%mass))){

    $avg += $weight{$tag}/$normw * $mass{$tag}{"mass"};
    $errsq += $weight{$tag};
    printf "$tag %e stat %e sys %e\n", $mass{$tag}{"mass"}, $mass{$tag}{"err"}, $mass{$tag}{"sys err"};
}

printf "charm quark avg mass %e %e\n", $avg, 1/$errsq**0.5;

# include 100% correlations for stat errors (or others if needed)
if($CORR){
    my $c1 = sqrt($mass{"HPQCD14A"}{"err"}**2);
    my $c2 = sqrt($mass{"FMT18"}{"err"}**2);
    $corr = 2*($c1*$c2)*$weight{"HPQCD14A"}*$weight{"FMT18"}/$normw**2;
}else{
    $corr=0.;
}
$errsq = (1/$errsq+$corr);
printf "c quark avg mass %e corr err %e\n", $avg, $errsq**0.5;

# calc chisq 
my $chisq;
my $sum;
foreach $tag (sort(keys(%mass))){
   $chisq = $mass{$tag}{"mass"} - $avg;
   $chisq *= $chisq;
   $chisq /= ($mass{$tag}{"err"}**2+$mass{$tag}{"sys err"}**2);
   $sum+= $chisq;
   $dof++;
}
$dof--;
printf "chisq = %e dof=%g\n", $sum, $dof;
printf "c quark avg mass %e corr err %e stretch factor %e \n", $avg, sqrt($sum/$dof)*$errsq**0.5, sqrt($sum/$dof);
