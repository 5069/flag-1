#
use POSIX;
use strict;
#use Math::Matrix;

my %mass;
my $tag;
my $CORR;
my $corr;
#2+1+1
# mu=mb
if(1){
    $CORR="yes";
    $mass{"HPQCD21"}{"mass"}=4.209;
    $mass{"HPQCD21"}{"err"}=0.021; 
    $mass{"HPQCD21"}{"sys err"}=0.0;
    $mass{"HPQCD14B"}{"mass"}=4.196;
    $mass{"HPQCD14B"}{"err"}=0.0; 
    $mass{"HPQCD14B"}{"sys err"}=0.023;
    #$mass{"ETM14B"}{"mass"}=4.26;
    #$mass{"ETM14B"}{"err"}=0.07; 
    #$mass{"ETM14B"}{"sys err"}=0.14;
    $mass{"ETM16"}{"mass"}=4.26;
    $mass{"ETM16"}{"err"}=0.03; 
    $mass{"ETM16"}{"sys err"}=0.10;
    $mass{"FMT18"}{"mass"}=4.201;
    $mass{"FMT18"}{"err"}=0.012;
    $mass{"FMT18"}{"sys err"}=0.0081;
    $mass{"GAM17"}{"mass"}=4.26;
    $mass{"GAM17"}{"err"}=0.108; 
    $mass{"GAM17"}{"sys err"}=0.14;
}
# 2+1
# mu=mc
if(0){
}
my $avg;
my $errsq;
my %weight;
my $normw;

foreach $tag (sort(keys(%mass))){
    $weight{$tag} = 1/($mass{$tag}{"err"}**2+$mass{$tag}{"sys err"}**2);
    $normw += $weight{$tag};
}

foreach $tag (sort(keys(%mass))){

    $avg += $weight{$tag}/$normw * $mass{$tag}{"mass"};
    $errsq += $weight{$tag};
    printf "$tag %e stat %e sys %e\n", $mass{$tag}{"mass"}, $mass{$tag}{"err"}, $mass{$tag}{"sys err"};
}

printf "b quark avg mass %e %e\n", $avg, 1/$errsq**0.5;

# include 100% correlations for stat errors (or others if needed)
if($CORR){
    my $c12 = sqrt($mass{"ETM16"}{"err"}**2) * sqrt($mass{"GAM17"}{"err"}**2);
    $corr = 2*$c12*$weight{"ETM16"}*$weight{"GAM17"}/$normw**2;
    my $c34 = sqrt($mass{"HPQCD21"}{"err"}**2) * sqrt($mass{"HPQCD21"}{"err"}**2);
    $corr += 2*$c34*$weight{"FMT18"}*$weight{"FMT18"}/$normw**2;
}else{
    $corr=0.;
}
$errsq = (1/$errsq+$corr);
printf "b quark avg mass %e corr err %e\n", $avg, $errsq**0.5;

# calc chisq 
my $chisq;
my $sum;
my $dof;
foreach $tag (sort(keys(%mass))){
   $chisq = $mass{$tag}{"mass"} - $avg;
   $chisq *= $chisq;
   $chisq /= ($mass{$tag}{"err"}**2+$mass{$tag}{"sys err"}**2);
   $sum+= $chisq;
   $dof++;
}
$dof--;
printf "chisq = %e dof = %g chisq/dof = %e\n", $sum, $dof, $chisq/$dof;
printf "b quark avg mass %e corr err %e stretch factor %e \n", $avg, sqrt($sum/2)*$errsq**0.5, sqrt($sum/2);
