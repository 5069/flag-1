# FLAG plot for ms/mud
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm m_s/m_{ud}$"			# plot title
plotnamestring	= "ratio_msmud"			# filename for plot
plotxticks	= ([[22., 24., 26., 28., 30., 32., 34.]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [+60.,21.5,"$\\rm N_f=2+1+1$"],
		   [+19.,12.0,"$\\rm N_f=2+1$"],
		   [+19.,-1.0,"pheno."],
		    ]
xlimits		= [21.,46.]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 35.					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
 [27.35, 0.112, 0.086, 0.05, 0.05,  "FNAL/MILC 14A",['s','g','g',0,tpos]],
[26.66, 0.32,  0.32,  0.32, 0.32,  "ETM 14"	 ,['s','l','g',0,tpos]],
[27.178, 0.047,  0.047,  0.085, 0.054,  "MILC 17"	 ,['s','g','g',0,tpos]],
[27.17, 0.32, 0.38,0.56, 0.38,  "ETM 21gqw"	 ,['s','g','g',0,tpos]],
] 
dat2p1=[
[27.4, 0.42, 0.42, 0.1, 0.1,            "MILC 04, HPQCD/MILC/UKQCD 04",['s','w','r',0,tpos]],
[28.8, 1.65, 1.65, 0.4, 0.4,            "RBC/UKQCD 08"	              ,['s','w','r',0,tpos]],
[28.8, 0, 0, 0.4, 0.4,                  "PACS-CS 08"    	      ,['s','w','r',0,tpos]],
[27.2, 0.316, 0.316, 0.1, 0.1,          "MILC 09"      	              ,['s','w','g',0,tpos]],
[27.41, 0.229, 0.229, 0.05, 0.05,       "MILC 09A"                    ,['s','g','g',0,tpos]],
[31.2, 0, 0, 2.7, 2.7,                  "PACS-CS 09"    	      ,['s','w','r',0,tpos]],
[28.31, 1.79, 1.79, 0.29, 0.29,         "Blum 10"	              ,['s','w','r',0,tpos]],
[26.8, 1.36, 1.36, 0.8, 0.8,            "RBC/UKQCD 10A"	              ,['s','l','g',0,tpos]],
[27.53, 0.215, 0.215, 0.2, 0.2,         "BMW 10A, 10B"     	              ,['s','g','g',0,tpos]],
[28.4, 1.39, 1.39, 0.5, 0.5,            "Laiho 11"	              ,['s','l','g',0,tpos]],
[26.8, 0, 0, 2., 2.,                    "PACS-CS 12"	              ,['s','w','r',0,tpos]],
[27.36, 0.39, 0.39, 0.380131, 0.380131, "RBC/UKQCD 12"                ,['s','l','g',0,tpos]],
[27.34, 0.21, 0.21, 0.0, 0.0,           "RBC/UKQCD 14B"                ,['s','g','g',0,tpos]],
[27.00, 1.0, 1.0, 0.4, 0.4,           "ALPHA 19xed"                ,['s','g','g',0,tpos]],
]
datapheno=[
[25.9, 0,0,0,0,      "Weinberg 77" ,['o','b','b',0,tpos]],
[24.4, 1.5,1.5,0,0,  "Leutwyler 96",['o','b','b',0,tpos]],
[26.6, 1.6,1.6,0,0,  "Kaiser 98"   ,['o','b','b',0,tpos]],
[23.4, 5.8,5.8,0,0,  "Narison 06"  ,['o','b','b',0,tpos]],
[23.5, 1.5,1.5,0,0,  "Oller 07"    ,['o','b','b',0,tpos]],
#[27.5, 0.3,0.3,0,0,  "PDG"         ,['^','k','k',0,tpos]],
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan,1.,1.,0.,0., "",['^','k','k',0,tpos],1],
#[0,0,0,0.,0., "dummy",['s','k','k',0,0],0],
[27.42, 0.12, 0.12, 0.0, 0.0, "FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],1],
[27.250, 0.064, 0.064, 0.064, 0.064, "FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1]]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datapheno,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#execfile('FLAGplot.py')
exec(compile(open('FLAGplot.py').read(), 'FLAGplot.py', 'exec'))

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

