# Using the magic encoding
# -*- coding: utf-8 -*-
# FLAG plot for mud
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm m_{ud}$"			# plot title
plotnamestring	= "mud"			# filename for plot
plotxticks	= ([[3., 3.5, 4., 4.5]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [+2.0,25.0, "$\\rm N_f=2+1+1$"],
		   [+2.0,16.0,"$\\rm N_f=2+1$"],
                   [+2.0,-1.0,"pheno."]
		    ]

LABEL=1		    
xaxisstrings	= [				# x,y-location for x-strings
		   [+5.6,-3.3,"MeV"] 	 		    
                   ]	
                   	
xlimits		= [2.4, 6.1]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 4.6					# x-position for the data labels
	
SIZE=[20.32,19.24]
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[3.70, 0.17,  0.17,  0.0,  0.0 ,"ETM 14"      ,['s','l','g',0,tpos]],
[3.404,0.025,0.025,  0.014, 0.014 ,"FNAL/MILC/TUMQCD 18"      ,['s','g','g',0,tpos]],
[3.636, 0.066, 0.066,0.057,0.060,  "ETM 21gqw"      ,['s','g','g',0,tpos]],] 
dat2p1=[
[2.8,  0.32,  0.32,  0.0,  0.0, "MILC 04, HPQCD/MILC/UKQCD 04" 	,['D','w','r',0,tpos]],
[3.2,  0.28,  0.28,  0.0,  0.0, "HPQCD 05"       	,['D','l','g',0,tpos]],
[3.55, 0.28,  0.59,  0.19, 0.19, "CP-PACS/JLQCD 07",['D','w','r',0,tpos]],
[3.72, 0.41,  0.41,  0.16, 0.16, "RBC/UKQCD 08"	,['s','w','r',0,tpos]],
[2.527,0.047, 0.047, 0.0,  0.0, "PACS-CS 08"	,['D','w','r',0,tpos]],
[3.2,  0.224, 0.224,0.0, 0.0, "MILC 09"   	,['D','l','g',0,tpos]],
[3.25, 0.175, 0.175, 0.01, 0.01, "MILC 09A"    	,['D','l','g',0,tpos]],
[3.4,  0.07,  0.07,  0.0,  0.0,  "HPQCD 09A"         	,['s','l','g',0,tpos]],
[2.97, 0.28,  0.28,  0.28, 0.28, "PACS-CS 09"     ,['s','w','r',0,tpos]],
[3.44, 0.2506,0.2506,0.12, 0.12, "Blum 10"	,['s','w','r',0,tpos]],
[3.59, 0.207, 0.207, 0.13, 0.13, "RBC/UKQCD 10A",['s','l','g',0,tpos]],
[3.39, 0.06,  0.06,  0.0,  0.0,  "HPQCD 10"	,['s','g','g',0,tpos]],
[3.19, 0.172, 0.172, 0.04, 0.04, "MILC 10A"  	,['s','g','g',0,tpos]],
[2.78, 0.27,  0.27,  0.0,  0.0,  "PACS-CS 10"   ,['s','w','r',0,tpos]],
[3.469,0.067, 0.067, 0.047,0.047,"BMW 10A, 10B" ,['s','g','g',0,tpos]],
[3.31, 0.27,  0.27,  0.07, 0.07 ,"Laiho 11"	,['D','l','g',0,tpos]],
[3.12, 0.25,  0.25,  0.24, 0.24 ,"PACS-CS 12"	,['s','w','r',0,tpos]],
[3.37, 0.09,  0.09,  0.07, 0.07 ,"RBC/UKQCD 12"	,['s','l','g',0,tpos]],
[3.31, 0.04,  0.04,  0.04, 0.04 ,"RBC/UKQCD 14B"	,['s','g','g',0,tpos]],
[3.54, 0.12, 0.12,  0.09,  0.09, "ALPHA 19xed", ['s','g','g',0,tpos]],
]
datapheno=[
[3.9, 0.6,0.6,0,0, "Maltman 01",['o','b','b',0,tpos]],
[3.95, 0.3,0.3,0,0, "Narison 06",['o','b','b',0,tpos]],
[4.1, 0.2,0.2,0,0, "Dominguez 09",['o','b','b',0,tpos]],
#[3.40, 0.25, 0.25, 0., 0., "PDG",['^','k','k',0,tpos]],
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
#[3.6, 0.22, 0.22, 0.22, 0.22, "our estimate for $N_f=2$",['s','w','k',0,tpos],1],
#[3.9, 0.9, 0.9, 0.9, 0.9, "PDG 10",['s','w','k',0,tpos],1],
#[3.75, 1.25, 1.25, 1.25, 1.25, "PDG @ CD 09",['s','w','k',0,tpos],1],
#[0,0,0,0.,0., "dummy",['s','k','k',0,0],0],
[np.nan, 0., 0., 0., 0., "PDG",['^','k','k',0,tpos],1],
[3.38083913,4.022e-02,4.022e-02, 0., 0., "FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],1],
[3.42168386,6.156e-02,6.156e-02,  0., 0., "FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datapheno,dat2p1,dat2p1p1]


################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#execfile('FLAGplot.py')
exec(compile(open('FLAGplot.py').read(), 'FLAGplot.py', 'exec'))

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

