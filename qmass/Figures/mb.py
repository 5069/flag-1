# Using the magic encoding
# -*- coding: utf-8 -*-
# FLAG plot for ms
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm \\overline{m}_b(\\overline{m}_b)$"			# plot title
plotnamestring	= "mb"			# filename for plot
plotxticks	= ([[4.1, 4.3, 4.5, 4.7]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [+3.95,11.,"$\\rm N_f = 2+1+1$"],
		   [+3.95,4.0,"$\\rm N_f=2+1$"],
		   #[+3.95,3.7,"$\\rm N_f=2$"]
		   ]
		   
LABEL=1
xaxisstrings	= [				# x,y-location for x-strings
		   [+5.15,-2.32,"GeV"] 	 		    
                   ]		   
		   
xlimits		= [4.0,5.3]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 4.8					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[4.162, 0.048, 0.048, 0., 0., "HPQCD 14A "        ,['s','g','g',0,tpos]],
[4.26, 0.104, 0.104, 0., 0., "ETM 16"        ,['s','g','g',0,tpos]],
#[4.26, 0.07, 0.07, 0.157, 0.157, "ETM 14B"        ,['s','l','g',0,tpos]],
[4.196, 0.023, 0.023, 0., 0., "HPQCD 14B "        ,['s','g','g',0,tpos]],
#[4., 0., 0., 0., 0., "ETM 16B"        ,['s','g','g',1,tpos]],
[4.26, 0.18, 0.18, 0., 0., "Gambino 17"        ,['s','g','g',0,tpos]],
[4.201, 0.0144, 0.0144, 0., 0., "FNAL/MILC/TUMQCD 18"        ,['s','g','g',0,tpos]],
[4.209, 0.021, 0.021, 0., 0., "HPQCD 21syc"        ,['s','g','g',0,tpos]],
] 
dat2p1=[
#[4.4, 0.3, 0.3, 0., 0., "HPQCD/UKQCD 05"        ,['s','l','g',0,tpos]],
[4.164, 0.023, 0.023, 0., 0., "HPQCD 10"        ,['s','g','g',0,tpos]],
[4.166, 0.043, 0.043, 0., 0., "HPQCD 13B "        ,['s','w','r',0,tpos]],
[4.184, 0.089, 0.089, 0., 0., "Maezawa 16 "        ,['s','w','r',0,tpos]],
]
dat2=[
[4.29, 0.14, 0.14, 0., 0., "ETM 11"        ,['s','l','g',0,tpos]],
[4.21, 0.11, 0.11, 0., 0., "ALPHA 13C"        ,['s','g','g',0,tpos]],
[4.31, 0.09, 0.09, 0.12, 0.12, "ETM 13B"        ,['s','g','g',0,tpos]],
]
datapheno=[
[4.18, 0.02, 0.03, 0, 0, "PDG",['^','k','k',0,tpos]],
#[4.185, 0.023, 0.023, 0, 0, "Our avg",['^','k','k',0,tpos]],
]
# datapheno=[
# [95, 5, 5, 5, 5, "PDG",['s','w','k',0,tpos],1]
# ]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan, 0.081, 0.081, 0, 0, "our estimate",['s','k','k',0,tpos],1],
#[4.255, 0.081, 0.081, 0, 0, "FLAG average for $\\rm N_f = 2$",['s','k','k',0,tpos],1],
[4.164, 0.023, 0.023, 0, 0, "FLAG average for $\\rm N_f = 2+1$",['s','k','k',0,tpos],1],
[4.203, 0.011, 0.011, 0, 0, "FLAG average for $\\rm N_f = 2+1+1$",['s','k','k',0,tpos],1],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datapheno,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#execfile('FLAGplot.py')
exec(compile(open('FLAGplot.py').read(), 'FLAGplot.py', 'exec'))

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

