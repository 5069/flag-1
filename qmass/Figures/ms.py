# Using the magic encoding
# -*- coding: utf-8 -*-
# FLAG plot for ms
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print ("numpy library not found. If you want the FLAG-logo to be added to the")
 print ("plot, please install this library")
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm m_s$"			# plot title
plotnamestring	= "ms"			# filename for plot
plotxticks	= ([[80., 90., 100.]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [+70.,20.5,"$\\rm N_f=2+1+1$"],
		   [+70.,13.5,"$\\rm N_f=2+1$"],
                   [+70.,-1.0,"pheno."]
		   ]
		   
LABEL=1
xaxisstrings	= [				# x,y-location for x-strings
		   [+121.,-3.3,"MeV"] 	 		    
                   ]		   
		   
xlimits		= [80.,140.]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 120.					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 2		neg. stat. error
# 3		pos. stat. error
# 4		neg. tot. error
# 5		pos. tot. error
# 6		Collaboration string
# 7		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
#               4       position of label
dat2p1p1=[
[99.6, 4.3,4.3,3.6, 3.6, "ETM 14"		,['s','l','g',0,tpos]],
[93.7, 0.8, 0.8, 0., 0., "HPQCD 14A "		,['s','g','g',0,tpos]],
[92.52, 0.69,0.69,0.,0., "FNAL/MILC/TUMQCD 18"		,['s','g','g',0,tpos]],
[94.49, 0.96,0.96,0.,0., "HPQCD 18  "		,['s','g','g',0,tpos]],
[98.7, 2.4, 2.4,3.2,4.0, "ETM 21gqw"		,['s','g','g',0,tpos]],
] 
dat2p1=[
# [76, 7.6, 7.6, 0.0, 0.0, "MILC 04, HPQCD/MILC/UKQCD 04" 	,['D','w','r',0,tpos]],
# [87, 5.7, 5.7, 0.0, 0.0, "HPQCD 05"   	    	,['D','w','g',0,tpos]],
# [90.1, 6.1, 17.2, 4.3, 4.3, "CP-PACS/JLQCD 07"  ,['D','w','r',0,tpos]],
# [107.3, 11.7, 11.7, 4.4, 4.4, "RBC/UKQCD 08"	,['s','w','r',0,tpos]],
# [72.72, 0.78, 0.78, 0.0, 0.0, "PACS-CS 08"	,['D','w','r',0,tpos]],
# [88, 5.0, 5.0, 0.0, 0.0, "MILC 09"   	    	,['D','w','g',0,tpos]],
[89, 4.78, 4.78, 0.2, 0.2, "MILC 09A"  	    	,['D','g','g',0,tpos]],
[92.4, 1.5, 1.5, 0.0, 0.0, "HPQCD 09A" 		,['s','l','g',0,tpos]],
[92.75, 1.11, 1.11, 0.58, 0.58, "PACS-CS 09"	,['s','w','r',0,tpos]],
[97.6, 6.22, 6.22, 2.9, 2.9, "Blum 10"		,['s','w','r',0,tpos]],
[96.2, 2.6, 2.6, 1.6, 1.6, "RBC/UKQCD 10A"	,['s','l','g',0,tpos]],
[92.2, 1.3, 1.3, 0.0, 0.0, "HPQCD 10"		,['s','g','g',0,tpos]],
[86.7, 2.3, 2.3, 0.0, 0.0, "PACS-CS 10"		,['s','w','r',0,tpos]],
[95.5, 1.86, 1.86, 1.1, 1.1, "BMW 10A, 10B"	,['s','g','g',0,tpos]],
#[94.2, 5.86, 5.86, 1.4, 1.4, "Laiho 11"		,['D','w','g',0,tpos]],
[83.60, 2.30, 2.30, 0.58, 0.58, "PACS-CS 12"	,['s','w','r',0,tpos]],
[92.31, 1.88, 1.88, 1.26, 1.26,  "RBC/UKQCD 12"	,['s','l','g',0,tpos]],
[90.3, 1.35, 1.35, 0.9, 0.9,  "RBC/UKQCD 14B"	,['s','g','g',0,tpos]],
[92.0, 1.7, 1.7, 0.0, 0.0,  "Maezawa 16"	,['s','w','r',0,tpos]],
[95.7, 2.5, 2.5,2.4,2.4, "ALPHA 19xed"           ,['s','g','g',0,tpos]],]
datapheno=[
[102, 0,0,0,0, "Vainshtein 78",['o','b','b',0,tpos]],
[104, 15,15,0,0, "Narison 06",['o','b','b',0,tpos]],
[92, 9,9,0,0, "Jamin 06",['o','b','b',0,tpos]],
[105, 9.2,9.2,0,0, "Chetyrkin 06",['o','b','b',0,tpos]],
[103, 8,8,0,0, "Dominguez 09",['o','b','b',0,tpos]],
#[93.5, 2.5, 2.5, 0, 0, "PDG",['^','k','k',0,tpos]],
#[95, 5, 5, 0, 0, "PDG",['^','k','k',0,tpos]],
]
# datapheno=[
# [95, 5, 5, 5, 5, "PDG",['s','w','k',0,tpos],1]
# ]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
#[95, 6.32, 6.32, 2, 2, "our estimate for $N_f=2^x$",['s','w','k',0,tpos],1],
#[95, 5, 5, 0, 0, "",['^','k','k',0,tpos],1],
[np.nan, 2.5, 2.5, 0, 0, "",['^','k','k',0,tpos],1],
#[0,0,0,0.,0., "dummy",['s','k','k',0,0],0],
 [92.2,1.0,1.0,0.,0., "FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],1],
[93.42,0.64,0.64,0.,0., "FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],1]]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datapheno,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#execfile('FLAGplot.py')
exec(compile(open('FLAGplot.py').read(), 'FLAGplot.py', 'exec'))

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

