% !TEX root = ../FLAG_master.tex
\section{Quark masses}
\label{sec:qmass}
Authors: T.~Blum, A.~Portelli, A.~Ramos\\

Quark masses are fundamental parameters of the Standard Model. An
accurate determination of these parameters is important for both
phenomenological and theoretical applications. The bottom- and charm-quark 
masses, for instance, are important sources of parametric
uncertainties in several Higgs decay modes. The up-,
down- and strange-quark masses govern the amount of explicit chiral
symmetry breaking in QCD. From a theoretical point of view, the values
of quark masses provide information about the flavour structure of
physics beyond the Standard Model. The Review of Particle Physics of
the Particle Data Group contains a review of quark masses
\cite{Manohar_and_Sachrajda}, which covers light as well as heavy
flavours. Here we also consider light- and heavy-quark masses, but
focus on lattice results and discuss them in more detail. We do not
discuss the top quark, however, because it decays weakly before it can
hadronize, and the nonperturbative QCD dynamics described by present
day lattice simulations is not relevant. The lattice determination of
light- (up, down, strange), charm- and bottom-quark masses is
considered below in Secs.~\ref{sec:lqm}, \ref{s:cmass},
and \ref{s:bmass}, respectively.

Quark masses cannot be measured directly in experiment because
quarks cannot be isolated, as they are confined inside hadrons. From a
theoretical point of view, in QCD with $N_f$ flavours, a precise
definition of quark 
masses requires one to choose a particular renormalization
scheme. This renormalization procedure introduces a
renormalization scale $\mu$, and quark masses depend on this
renormalization scale according to the Renormalization Group (RG)
equations. In mass-independent renormalization schemes the RG equations
reads
\begin{equation}
  \label{eq:qmass_tau}
  \mu \frac{{\rm d} \bar m_i(\mu)}{{\rm d}{\mu}} = \bar m_i(\mu) \tau(\bar g)\,,
\end{equation}
where the function $\tau(\bar g)$ is the anomalous
dimension, which
depends only on the value of the strong coupling $\alpha_s=\bar
g^2/(4\pi)$. Note that in QCD $\tau(\bar g)$ is the same for all quark
flavours.  The anomalous 
dimension is scheme dependent, but its 
perturbative expansion  
\begin{equation}
  \label{eq:tau_asymp}
  \tau(\bar g) \raisebox{-.1ex}{
            $\stackrel{\small{\bar g \to 0}}{\sim}$} -\bar g^2\left(
    d_0 + d_1\bar g^2 + \dots
  \right) 
\end{equation}
has a leading coefficient $d_0 = 8/(4\pi)^2$,  
which is scheme independent.\footnote{We follow the conventions of
  Gasser and Leutwyler~\cite{Gasser:1982ap}.}
   Equation~(\ref{eq:qmass_tau}), being a
first order differential equation, can be solved exactly by using
Eq.~(\ref{eq:tau_asymp}) as boundary condition. The formal
solution of the RG equation reads
\begin{equation}
  \label{eq:qmass_rgi}
  M_i = \bar m_i(\mu)[2b_0\bar g^2(\mu)]^{-d_0/(2b_0)}
  \exp\left\{
    - \int_0^{\bar g(\mu)}{\rm d} x\, \left[
      \frac{\tau(x)}{\beta(x)} - \frac{d_0}{b_0x}
    \right]
  \right\}\,,
\end{equation}
where $b_0 = (11-2N_f/3) / (4\pi)^2$ is the universal
leading perturbative coefficient in the expansion of the
$\beta$-function $\beta(\bar g)$. The renormalization group invariant
(RGI) quark masses $M_i$ are formally integration constants of the
RG Eq.~(\ref{eq:qmass_tau}). They are scale independent, and due
to the universality of the coefficient $d_0$, they are also scheme
independent. Moreover, they are nonperturbatively defined by
Eq.~(\ref{eq:qmass_rgi}). They only depend on the number
of flavours $N_f$, making them a natural candidate to quote quark
masses and compare determinations from different lattice
collaborations. Nevertheless, it is customary in the phenomenology
community to use the $\overline{\rm MS}$ scheme at a scale $\mu = 2$
GeV to compare different results for light-quark masses, and use a
scale equal to its own mass for the charm and bottom
quarks. In this review, we will quote the final averages of both
quantities.

Results for quark masses are always quoted in the four-flavour
theory. $N_{\rm f}=2+1$ results have to be converted to the four
flavour theory. Fortunately, the charm quark is heavy $(\Lambda_{\rm
  QCD}/m_c)^2<1$, and this conversion can be performed in perturbation
theory with negligible ($\sim 0.2\%$) perturbative
uncertainties. Nonperturbative corrections in this matching are more
difficult to estimate. Since these effects are suppressed by a factor of 
$1/N_{\rm c}$, and a factor of the strong coupling at the scale of the
charm mass, naive power counting arguments would suggest that the 
effects are $\sim 1\%$. In practice, numerical nonperturbative
studies~\cite{Athenodorou:2018wpk,Bruno:2014ufa} 
have found this power counting argument to be an overestimate by one
order of magnitude in the determination of simple hadronic
quantities or the $\Lambda$-parameter. Moreover, lattice 
determinations do not show any significant deviation between the
$N_{\rm f}=2+1$ and $N_{\rm f}=2+1+1$ simulations. For example, the
difference in the final averages for the mass of the strange quark
$m_s$ between $N_f=2+1$ and $N_f=2+1+1$ determinations is about a
0.8\%, and negligible from a statistical point of view. 

We quote all final averages at $2$ GeV in the $\overline{\rm
  MS}$ scheme and also the RGI values (in the four flavour theory). We
use the exact RG
 Eq.~(\ref{eq:qmass_rgi}). Note that to use this equation we
need the value of the strong coupling in the $\overline{\rm MS}$
scheme at a scale $\mu = 2$ GeV. All our results are obtained from the
RG equation in the $\overline{\rm MS}$ scheme and the 5-loop beta
function together with the 
value of the $\Lambda$-parameter in the four-flavour theory
$\Lambda^{(4)}_{\overline{\rm MS}} = 294(12)\, {\rm MeV}$ obtained in
this review (see Sec.~\ref{sec:alpha_s}). In the uncertainties of the RGI
massses we separate the contributions from the determination of the
quark masses and the propagation of the uncertainty of
$\Lambda^{(4)}_{\overline{\rm MS}}$. These are identified with the
subscripts $m$ and $\Lambda$, respectively. 

Conceptually, all lattice determinations of quark masses contain three
basic ingredients:
\begin{enumerate}
\item Tuning the lattice bare-quark masses to match the experimental
  values of some quantities.  Pseudo-scalar meson masses provide 
  the most common choice, since they have a strong dependence on the
  values of quark masses. In pure
  QCD with $N_f$ quark flavours these values are not
  known, since the electromagnetic interactions affect the
  experimental values of meson masses. Therefore, pure QCD
  determinations use model/lattice information to determine the
  location of the physical point. This is discussed at length in Sec.~\ref{sec:physical point and isospin}.

\item Renormalization of the bare-quark masses. Bare-quark masses
  determined with
  the above-mentioned criteria have to be renormalized. Many of the
  latest determinations use some nonperturbatively defined
  scheme. One can also use perturbation theory to connect directly the
  values of the bare-quark masses to the values in the $\overline{\rm
    MS}$ scheme at $2$ GeV. Experience shows that
  1-loop calculations are unreliable for the renormalization of
  quark masses: usually at least two loops are required to have
  trustworthy results.
   
\item If quark masses have been nonperturbatively renormalized, for
  example, to some MOM/SF scheme, the values in this scheme must be
  converted to the phenomenologically useful values in the
  $\overline{\rm MS}$ scheme (or to the scheme/scale independent RGI
  masses). Either option 
  requires the use of perturbation theory. The larger the
  energy scale of this matching with perturbation theory, the better,
  and many recent computations in MOM schemes do a nonperturbative
  running up to $3--4$ GeV. Computations in the SF scheme allow us to
  perform this running nonperturbatively over large energy scales and
  match with perturbation theory directly at the electro-weak scale $\sim 100$
  GeV. 
\end{enumerate}
Note that quark masses are different from other 
quantities determined on the lattice since perturbation theory is 
unavoidable when matching to schemes in the continuum.

We mention that lattice-QCD calculations of the $b$-quark mass have an
additional complication which is not present in the case of the charm
and light quarks. 
At the lattice spacings currently used in numerical simulations the
direct treatment of the $b$ quark with the fermionic actions commonly
used for light quarks is very challenging. Only two determinations of
the $b$-quark mass uses this approach, reaching the physical $b$-quark
mass region at two lattice spacings with $aM\sim 1$. 
There are a few widely used approaches to treat the $b$ quark on the
lattice, which have been already discussed in the FLAG 13 review (see
Sec.~8 of Ref.~\cite{Aoki:2013ldr}). 
Those relevant for the determination of the $b$-quark mass will be
briefly described in Sec.~\ref{s:bmass}. 



\medskip

%\newpage

\input{qmass/qmass_mudms}

%\newpage

\input{qmass/qmass_mc}

\newpage

\input{qmass/qmass_mb}

