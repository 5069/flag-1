# FLAG plot for BK
# Goes with final version 7/5/13
# run with "python BK7.py"
# SS: filled in green circle for Nf=2 ETMC result
# Goes with final final version 11/30/13
# run with "python BK8.py"
# SS: added SWME 13 and updated average
# UW: This is now called BK.py and produces BK.eps, BK.pdf etc when run 
# with "python BK.py", the version number should not be attached to the file name.
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\sigma_{\pi N}$"			# plot title
plotnamestring	= "SIGMA"			# filename for plot
plotxticks	= ([[10,20,30,40,50,60,70]])# locations for x-ticks
nminorticks	= 4			# number of intermediate minor ticks
					# 0 if none
xaxisstrings    = [[95.,-3.5,"MeV"]]    
                   
yaxisstrings	= [				# x,y-location for y-strings
		   [+0.44,37.0,"$\\rm N_f=2+1+1$"],
		   [+0.44,28.0,"$\\rm N_f=2+1$"],
		   [+0.44,19.5,"$\\rm N_f=2$"],
                   [+0.44,1.5,"$\pi N$ scatt."],[+0.44,14,"FH+mixed latt."]
		    ]
xlimits		= [0,145]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 95					# x-position for the data labels
FS = 10
FSY = 12
LABEL=1
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[64.9,13.28495389529071719163,13.28495389529071719163,1.5,1.5,"ETM 14A",['<','g','g',0,tpos],1],
]
dat2p1=[
[75,15,15,15,15,"PACS-CS 09",['<','w','r',0,tpos],1],
[59,17.11724276862368976315,17.11724276862368976315,2,2,"Martin Camalich 10",['<','w','r',0,tpos],1],
[39,8.06225774829854965236,18.43908891458577462000,4,4,"BMW 11A",['<','g','g',0,tpos],1],
[31,5.00000000000000000000,5.00000000000000000000,3,3,"QCDSF 11",['<','w','r',0,tpos],1],
 [45,6,6,6,6,"Shanahan 12",['<','w','r',0,tpos],1],
[51,7.00000000000000000000,7.00000000000000000000,7,7,"Shanahan 12",['<','w','r',0,tpos],1],
[38,4.24264068711928514640,4.24264068711928514640,3,3,"BMW 15",['<','g','g',0,tpos],1],
[45.9,7.91201617794099306275,7.91201617794099306275,7.4,7.4,"$\chi$QCD 15A",['s','g','g',0,tpos],1],
[26,6.16441400296897645024,6.16441400296897645024,3,3,"JLQCD 18",['s','w','r',0,tpos],1],
]
dat2=[
[53,7.28010988928051827109,21.09502310972898649979,2,2,"JLQCD 08B",['<','w','r',0,tpos],1],
[37,10.00000000000000000000,10.00000000000000000000,8,8,"QCDSF 12",['<','g','g',0,tpos],1],
[37.2,3.89486841883008931000,5.37121960079831403647,2.6,2.6,"ETM 16A",['s','w','r',0,tpos],1],
[35,6.00000000000000000000,6.00000000000000000000,6,6,"RQCD 16",['s','w','r',0,tpos],1],
]
otherlattice=[
[47.6,12.51598977308626598895,10.50951949424901157312,4.7,4.7,"Procura 06",['p','b','b',0,tpos],1],
[50,9.48683298050513799599,9.48683298050513799599,9,9,"Young 09",['p','b','b',0,tpos],1],
[43,6.08276253029821968899,6.08276253029821968899,1,1,"Ren 12",['p','b','b',0,tpos],1],
[52,8.54400374531753116787,8.54400374531753116787,3,3,"Alvarez-Ruso 13",['p','b','b',0,tpos],1],
[41,6.40312423743284868648,6.40312423743284868648,5,5,"Alvarez-Ruso 13",['p','b','b',0,tpos],1],
[39,1,2.00000000000000000000,1,2,"Lutz 14",['p','b','b',0,tpos],1],
[55,4.12310562561766054982,4.12310562561766054982,1,1,"Ren 14",['p','b','b',0,tpos],1],
[57,6.00000000000000000000,6.00000000000000000000,6,6,"Ren 16",['p','b','b',0,tpos],1],
[64.9,13.28495389529071719164,13.28495389529071719164,1.5,1.5,"ETM 17A",['p','b','b',0,tpos],1],
[50.2,2.33238075793812018834,2.33238075793812018834,1.2,1.2,"Ren 17",['p','b','b',0,tpos],1],
[48.4,.80000000000000000000,.40000000000000000000,0.8,0.4,"Lutz 18",['p','b','b',0,tpos],1],
]
phenom=[
[59,7,7,0.0,0.0,"Alarcon 11",['o','b','b',0,tpos],1],
[45,6,6,0.0,0.0,"Chen 12",['o','b','b',0,tpos],1],
[52,7,7,0.0,0.0,"Chen 12",['o','b','b',0,tpos],1],
[59.1,3.5,3.5,0.0,0.0,"Hoferichter 15",['o','b','b',0,tpos],1],
[58,5,5,0.0,0.0,"Ruiz de Elvira 17",['o','b','b',0,tpos],1],
]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan,0.0,0.0,0.0,0.0,"",['s','k','k',0,tpos],0],[np.nan,0.0,0.0,0.0,0.0,"",['s','k','k',0,tpos],0],
[37,10,10,10,10,"FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
[39.7,3.6,3.6,3.6,3.6,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[64.9,13.3,13.3,13.3,13.3,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[phenom,otherlattice,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot2.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker
