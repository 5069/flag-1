# FLAG plot for BK
# Goes with final version 7/5/13
# run with "python BK7.py"
# SS: filled in green circle for Nf=2 ETMC result
# Goes with final final version 11/30/13
# run with "python BK8.py"
# SS: added SWME 13 and updated average
# UW: This is now called BK.py and produces BK.eps, BK.pdf etc when run 
# with "python BK.py", the version number should not be attached to the file name.
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$g_A^{u-d}$"			# plot title
plotnamestring	= "GA"			# filename for plot
plotxticks	= ([[0.9,1.0,1.1,1.2,1.3,1.4]])# locations for x-ticks
nminorticks	= 4			# number of intermediate minor ticks
					# 0 if none
    
                   
yaxisstrings	= [				# x,y-location for y-strings
		   [+0.44,28.0,"$\\rm N_f=2+1+1$"],
		   [+0.44,18.0,"$\\rm N_f=2+1$"],
		   [+0.44,7.0,"$\\rm N_f=2$"],
                   [+0.44,-.30,"Expt"]
		    ]
xlimits		= [0.8,1.9]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.45					# x-position for the data labels
FS = 12
FSY = 16
LABEL=0
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[1.195,.03858756276314947569,.03858756276314947569,0.033,0.033,"PNDME 16",['s','l','g',0,tpos]],
[1.278,.03342154993413680687,.03342154993413680687,0.021,0.021,"CalLat 17",['s','l','g',0,tpos]],
[1.271,0.013,0.013,0.013,0.013,"CalLat 18",['s','g','g',0,tpos]],
[1.218,.03905124837953327197,.03905124837953327197,0.025,0.025,"PNDME 18",['s','g','g',0,tpos]],
]
dat2p1=[
[1.226,0.084,0.084,0.084,0.084,"LHPC 05",['s','w','r',0,tpos]],
[1.20,.07211102550927978586,.07211102550927978586,0.06,0.06,"RBC/UKQCD 08B",['s','w','r',0,tpos]],
[1.19,.07211102550927978586,.07211102550927978586,0.06,0.06,"RBC/UKQCD 09B",['s','w','r',0,tpos]],
[1.21,0.17,0.17,0.17,0.17,"LHPC 10",['s','w','r',0,tpos]],
[0.97,0.08,0.08,0.08,0.08,"LHPC 12A",['s','w','r',0,tpos]],
[1.123,.09861541461658009978,.09861541461658009978,0.028,0.028,"JLQCD 18",['s','w','r',0,tpos]],
[1.254,.03400000000000000000,.03400000000000000000,0.016,0.016,"$\chi$QCD 18",['s','g','g',0,tpos]],
[1.163,.07629547824084989638,.07629547824084989638,0.075,0.075,"PACS 18",['s','w','r',0,tpos]],
[1.251,0.024,0.024,0.024,0.024,"Mainz 18",['s','l','g',0,tpos]],
]
dat2=[
[1.31,.11401754250991379791,.11401754250991379791,0.09,0.09,"QCDSF 06",['s','w','r',0,tpos]],
[1.23,0.12,0.12,0.12,0.12,"RBC 08",['s','w','r',0,tpos]],
[1.233,.08700000000000000000,.07206941098690900221,0.063,0.063,"Mainz 12",['s','l','g',0,tpos]],
[1.29,.05830951894845300470,.05830951894845300470,0.05,0.05,"QCDSF 13",['s','w','r',0,tpos]],
[1.280,.06365532185135819419,.06365532185135819419,0.044,0.044,"RQCD 14",['s','w','r',0,tpos]],
[1.242,0.057,0.057,0.057,0.057,"ETM 15D",['s','w','r',0,tpos]],
[1.212,.03966106403010388222,.03966106403010388222,0.033,0.033,"ETM 17B",['s','w','r',0,tpos]],
[1.278,.11042191811411355844,.06800000000000000000,0.068,0.068,"Mainz 17",['s','g','g',0,tpos]],
]
expt=[
[1.2724,0.0023,0.0023,0,0,"PDG",['^','k','k',0,tpos]],
]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan,0.0,0.0,0.0,0.0,"",['s','k','k',0,tpos],0],
[1.278,0.086,0.086,0.086,0.086,"FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
[1.254,0.034,0.034,0.034,0.034,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[1.251,0.033,0.033,0.033,0.033,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[expt,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker
