# FLAG plot for BK
# Goes with final version 7/5/13
# run with "python BK7.py"
# SS: filled in green circle for Nf=2 ETMC result
# Goes with final final version 11/30/13
# run with "python BK8.py"
# SS: added SWME 13 and updated average
# UW: This is now called BK.py and produces BK.eps, BK.pdf etc when run 
# with "python BK.py", the version number should not be attached to the file name.
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$g_S^{u-d}$"			# plot title
plotnamestring	= "GS"			# filename for plot
plotxticks	= ([[0.4,0.8,1.2,1.6,2.0,2.4]])# locations for x-ticks
nminorticks	= 4			# number of intermediate minor ticks
					# 0 if none
    
                   
yaxisstrings	= [				# x,y-location for y-strings
		   [+0.44,12,"$\\rm N_f=2+1+1$"],
		   [+0.44,6.8,"$\\rm N_f=2+1$"],
		   [+0.44,2.7,"$\\rm N_f=2$"],
                   [+0.44,-0.2,"CVC"]
		    ]
xlimits		= [0.2,4.2]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 2.6					# x-position for the data labels
FS = 12
FSY = 16
LABEL=0
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.72,0.32,0.32,0.32,0.32,"PNDME 13",['s','w','r',0,tpos]],
[0.97,.13416407864998738178,.13416407864998738178,0.12,0.12,"PNDME 16",['s','l','g',0,tpos]],
[1.022,.10000000000000000000,.10000000000000000000,0.080,0.080,"PNDME 18",['s','g','g',0,tpos]],
]
dat2p1=[
[1.08,.32249030993194198609,.32249030993194198609,0.28,0.28,"LHPC 12",['s','w','r',0,tpos]],
[0.88,.11045361017187260769,.11045361017187260769,0.08,0.08,"JLQCD 18",['s','w','r',0,tpos]],
[1.22,0.11,0.11,0.11,0.11,"Mainz 18",['s','l','g',0,tpos]],
]
dat2=[
[1.02,.34985711369071802825,.34985711369071802825,0.18,0.18,"RQCD 14",['s','w','r',0,tpos]],
[0.930,.32775600680994391195,.32775600680994391195,0.252,0.252,"ETM 17",['s','w','r',0,tpos]],
]
pheno=[
[1.02,0.11,0.11,0.0,0.0,"Gonzalez-Alonso 13",['o','b','b',0,tpos]],
]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan,0.0,0.0,0.0,0.0,"",['s','k','k',0,tpos],0],
[np.nan,0.03,0.03,0.03,0.03,"FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
[np.nan,0.03,0.03,0.03,0.03,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[1.022,0.10,0.1,0.1,0.1,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[pheno,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker
