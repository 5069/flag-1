# FLAG plot for BK
# Goes with final version 7/5/13
# run with "python BK7.py"
# SS: filled in green circle for Nf=2 ETMC result
# Goes with final final version 11/30/13
# run with "python BK8.py"
# SS: added SWME 13 and updated average
# UW: This is now called BK.py and produces BK.eps, BK.pdf etc when run 
# with "python BK.py", the version number should not be attached to the file name.
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\sigma_s$"			# plot title
plotnamestring	= "SIGMAS"			# filename for plot
plotxticks	= ([[-20,0,20,40,60,80,100,120,140,160,180]])# locations for x-ticks
nminorticks	= 4			# number of intermediate minor ticks
					# 0 if none
xaxisstrings    = [[200.,-2.9,"MeV"]]    
                   
yaxisstrings	= [				# x,y-location for y-strings
		   [+0.44,28.0,"$\\rm N_f=2+1+1$"],
		   [+0.44,18.0,"$\\rm N_f=2+1$"],
		   [+0.44,6.6,"$\\rm N_f=2$"],
                   [+0.44,3.5,"FH+mixed latt."]
		    ]
xlimits		= [-40,360]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 200					# x-position for the data labels
FS = 12
FSY = 12
FSXTICS=11
LABEL=1
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[40.9728,8.79016775266547458128,8.79016775266547458128,7.4496,7.4496,"MILC 12C",['o','g','g',0,tpos],1],
]
dat2p1=[
[59.0,10.00000000000000000000,10.00000000000000000000,6.0,6.0,"MILC 09D",['o','l','g',0,tpos],1],
[-4.0,33.97057550292605802322,33.97057550292605802322,23.0,23.0,"Martin Camalich 10",['<','w','r',0,tpos],1],
[67.0,54.20332093147061040136,61.26989472816156226563,27.0,27.0,"BMW 11A",['<','g','g',0,tpos],1],
[43.16042,10.32097000000000000000,10.32097000000000000000,10.32097,10.32097,"Engelhardt 12",['s','w','r',0,tpos],1],
[71.0,68.09552114493287289037,68.09552114493287289037,34.0,34.0,"QCDSF 11",['<','w','r',0,tpos],1],
[8.44443,20.57786760538856200134,20.57786760538856200134,14.07405,14.07405,"JLQCD 12A",['s','w','r',0,tpos],1],
[21.58021,37.82287288748040078792,37.82287288748040078792,27.20983,27.20983,"JLQCD 12A",['<','w','r',0,tpos],1],
[21.0,6.00000000000000000000,6.00000000000000000000,6.0,6.0,"Shanahan 12",['<','w','r',0,tpos],1],
[59.0,6.00000000000000000000,6.00000000000000000000,6.0,6.0,"Shanahan 12",['<','w','r',0,tpos],1],
[48.0,18.02775637731994646558,18.02775637731994646558,10.0,10.0,"Junnarkar 13",['<','g','g',0,tpos],1],
[33.30,6.20000000000000000000,6.20000000000000000000,6.20,6.20,"$\chi$QCD 13A",['s','w','r',0,tpos],1],
[58.62311,8.50374180490564988029,8.50374180490564988029,5.06165,5.06165,"MILC 12C",['o','g','g',0,tpos],1],
[105.0,55.22680508593630387104,55.22680508593630387104,41.0,41.0,"BMW 15",['<','g','g',0,tpos],1],
[40.20,12.21228889275061327557,12.21228889275061327557,11.70,11.70,"$\chi$QCD 15A",['s','g','g',0,tpos],1],
[17.0,20.12461179749810726767,20.12461179749810726767,18.0,18.0,"JLQCD 18",['s','w','r',0,tpos],1],
]
dat2=[
[35.0,12.00000000000000000000,12.00000000000000000000,12.0,12.0,"RQCD 16",['s','w','r',0,tpos],1],
[41.10,10.04390362359177558430,11.31724348063608641206,8.20,8.20,"ETM 16A",['s','w','r',0,tpos],1],
]
otherlattice=[
[33,16.76305461424021012844,16.76305461424021012844,16,16,"Young 09",['p','b','b',0,tpos],1],
[126,59.09314681077662833047,59.09314681077662833047,24,24,"Ren 12",['p','b','b',0,tpos],1],
[84,4.00000000000000000000,28.00000000000000000000,4,28,"Lutz 14",['p','b','b',0,tpos],1],
[27,27.29468812791236125575,27.29468812791236125575,27,27,"Ren 14",['p','b','b',0,tpos],1],
[42.59,19.68000000000000000000,9.86000000000000000000,19.68,9.86,"Lutz 18",['p','b','b',0,tpos],1],
]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan,0.0,0.0,0.0,0.0,"",['s','k','k',0,tpos],0],
[np.nan,0.03,0.03,0.03,0.03,"FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
[52.9,7.0,7.0,7.0,7.0,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[41.0,8.8,8.8,8.8,8.8,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[otherlattice,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot2.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker
