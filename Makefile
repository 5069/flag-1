quick:
	pdflatex FLAG_master.tex

pdflatex:
	pdflatex FLAG_master.tex
	bibtex FLAG_master
	pdflatex FLAG_master.tex
	pdflatex FLAG_master.tex

M	:
	latex FLAG_master.tex
	bibtex FLAG_master
	latex FLAG_master.tex
	latex FLAG_master.tex
	dvips -Ppdf -GO FLAG_master.dvi -o FLAG_master.ps
	ps2pdf FLAG_master.ps FLAG.pdf

M0	:
	latex FLAG_master.tex
	dvips -Ppdf -GO FLAG_master.dvi -o FLAG_master.ps
	ps2pdf FLAG_master.ps FLAG.pdf

S	:
	latex Scalesetting_master.tex
	bibtex Scalesetting_master
	latex Scalesetting_master.tex
	latex Scalesetting_master.tex
	dvips -Ppdf -GO Scalesetting_master.dvi -o Scalesetting_master.ps
	ps2pdf Scalesetting_master.ps FLAG.pdf

S0	:
	latex Scalesetting_master.tex
	dvips -Ppdf -GO Scalesetting_master.dvi -o Scalesetting_master.ps
	ps2pdf Scalesetting_master.ps FLAG.pdf


SECTIONS =  FLAG_introduction FLAG_criteria FLAG_qmass FLAG_VudVus FLAG_LECs FLAG_BK FLAG_HQD FLAG_HQB FLAG_Alpha_s FLAG_glossary FLAG_notes FLAG_NME

#
#FLAG_glossary FLAG_notes

FLAGsections: %: ${SECTIONS} 


${SECTIONS}: %: Sections/%.tex 
	latex $<
	bibtex $@
	latex $<
	grep sec FLAG_master.aux >> $@.aux
	grep eq FLAG_master.aux >> $@.aux

	latex $<
	dvips -Ppdf -GO $@.dvi -o$@.ps
	ps2pdf $@.ps
	if [ ! -d Sections_pdf ]; then mkdir Sections_pdf; fi	
	mv  $@.pdf Sections_pdf/	


# make sure first entry is a single-scatter plot (MULTIPLE=0)!!!
qmassFigures = ms mud ratio_msmud ratio_mumd
VudusFigures = fp fKfpi Vusud fKandfpi VusVersusVud
LECFigures = su2_Fpi_F su2_Sigma su2_l3bar su2_l4bar su2_l6bar su3_2L6-L4 su3_2L8-L5 su3_L4 su3_L5 su3_L6 su3_L8
BKFigures = BK
HQFigures = FBratio fBandfBs fBsqrtBB2  BB2 xiandRBB BtoPi BtoDStar Vub Vcb fDandfDs FDratio DtoPiandDtoK VcdandVcs BtoD    
Alpha_sFigures = alphasMSbarZ r0LamMSbar
NMEFigures = 
Plots: qmassPlots VudusPlots LECPlots BKPlots HQPlots Alpha_sPlots NMEPlots

qmassPlots: ${qmassFigures}
	rm plots/FLAG_Logo.*
	mv plots/*.eps qmass/Figures/
	mv plots/*.pdf qmass/Figures/
	mv plots/*.png qmass/Figures/

${qmassFigures}: %: qmass/Figures/%.py
	python $<

VudusPlots: ${VudusFigures}
	rm plots/FLAG_Logo.*
	mv plots/*.pdf Vudus/Figures
	mv plots/*.eps Vudus/Figures
	mv plots/*.png Vudus/Figures

${VudusFigures}: %: Vudus/Figures/%.py
	python $<

LECPlots: ${LECFigures}
	rm plots/FLAG_Logo.*
	mv plots/*.eps LEC/Figures/
	mv plots/*.pdf LEC/Figures/
	mv plots/*.png LEC/Figures/

${LECFigures}: %: LEC/Figures/%.py
	python $<

BKPlots: ${BKFigures}
	mv plots/*.eps BK/Figures/
	mv plots/*.pdf BK/Figures/
	mv plots/*.png BK/Figures/

${BKFigures}: %: BK/Figures/%.py
	python $<

HQPlots: ${HQFigures}
	rm plots/FLAG_Logo.*
	mv plots/*.pdf HQ/Figures/
	mv plots/*.eps HQ/Figures/
	mv plots/*.png HQ/Figures/


${HQFigures}: %: HQ/Figures/%.py
	python $<

Alpha_sPlots: ${Alpha_sFigures}
	rm plots/FLAG_Logo.*
	mv plots/*.eps Alpha_s/Figures/
	mv plots/*.pdf Alpha_s/Figures/
	mv plots/*.png Alpha_s/Figures/

${Alpha_sFigures}: %: Alpha_s/Figures/%.py
	python $<

NMEPlots: ${NMEFigures}
	rm plots/FLAG_Logo.*
	mv plots/*.eps NME/Figures/
	mv plots/*.pdf NME/Figures/
	mv plots/*.png NME/Figures/

${NMEFigures}: %: NME/Figures/%.py
	python $<


clean   :
	rm *.aux *.bbl *.blg *.dvi *.log *.out *.ps FLAG.pdf


