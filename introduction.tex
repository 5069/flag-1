\section{Introduction}
\label{sec:introduction}

Flavour physics provides an important opportunity for exploring the
limits of the Standard Model of particle physics and for constraining
possible extensions that go beyond it. As the LHC explores 
a new energy frontier and as experiments continue to extend the precision 
frontier, the importance of flavour physics will grow,
both in terms of searches for signatures of new physics through
precision measurements and in terms of attempts to construct the
theoretical framework behind direct discoveries of new particles. 
Crucial to such searches for new physics is the ability to quantify
strong-interaction effects.
Large-scale numerical
simulations of lattice QCD allow for the computation of these effects
from first principles. 
The scope of the Flavour Lattice Averaging
Group (FLAG) is to review the current status of lattice results for a
variety of physical quantities that are important for flavour physics. Set up in
November 2007, it
comprises experts in Lattice Field Theory, Chiral Perturbation
Theory and Standard Model phenomenology. 
Our aim is to provide an answer to the frequently posed
question ``What is currently the best lattice value for a particular
quantity?" in a way that is readily accessible to those who are not
expert in lattice methods.
This is generally not an easy question to answer;
different collaborations use different lattice actions
(discretizations of QCD) with a variety of lattice spacings and
volumes, and with a range of masses for the $u$- and $d$-quarks. Not
only are the systematic errors different, but also the methodology
used to estimate these uncertainties varies between collaborations. In
the present work, we summarize the main features of each of the
calculations and provide a framework for judging and combining the
different results. Sometimes it is a single result that provides the
``best" value; more often it is a combination of results from
different collaborations. Indeed, the consistency of values obtained
using different formulations adds significantly to our confidence in
the results.

The first three editions of the FLAG review were made public in
2010~\cite{Colangelo:2010et}, 2013~\cite{Aoki:2013ldr},
and 2016~\cite{Aoki:2016frl} 
(and will be referred to as FLAG 10, FLAG 13 and FLAG 16, respectively).
The third edition reviewed results related to both light 
($u$-, $d$- and $s$-), and heavy ($c$- and $b$-) flavours.
The quantities related to pion and kaon physics  were 
light-quark masses, the form factor $f_+(0)$
arising in semileptonic $K \rightarrow \pi$ transitions 
(evaluated at zero momentum transfer), 
the decay constants $f_K$ and $f_\pi$, 
the $B_K$ parameter from neutral kaon mixing,
and the kaon mixing matrix elements of new operators that arise in 
theories of physics beyond the Standard Model.
Their implications for
the CKM matrix elements $V_{us}$ and $V_{ud}$ were also discussed.
Furthermore, results
were reported for some of the low-energy constants of $SU(2)_L \times
SU(2)_R$ and $SU(3)_L \times SU(3)_R$ Chiral Perturbation Theory.
The quantities related to $D$- and $B$-meson physics that were
reviewed were the masses of the charm and bottom quarks
together with the decay constants, form factors, and mixing parameters
of $B$- and $D$-mesons.
These are the heavy-light quantities most relevant
to the determination of CKM matrix elements and the global
CKM unitarity-triangle fit. Last but not least, the current status of 
lattice results on the QCD coupling  $\alpha_s$ was reviewed.

In the present paper we provide updated results for all the above-mentioned
quantities, but also extend the scope of the review by adding a section on
nucleon matrix elements. This presents results for matrix elements of flavor
nonsinglet and singlet bilinear operators, including the nucleon axial charge
$g_A$ and the nucleon sigma terms.
These results are relevant for constraining $V_{ud}$, for searches for new physics
in neutron decays and other processes, and for dark matter searches.
In addition, the section on
up and down quark masses has been largely rewritten, 
replacing previous estimates for $m_u$, $m_d$, and the mass ratios $R$ and $Q$ that were largely phenomenological with those from lattice QED+QCD calculations. 
We have also updated the discussion of the phenomenology of isospin-breaking 
effects in the light meson sector, and their relation to quark masses, 
with a lattice-centric discussion. 
A short review of QED in lattice-QCD simulations is also provided,
including a discussion of ambiguities arising when attempting 
to define ``physical'' quantities in pure QCD.

Our main results are collected in Tabs.~\ref{tab:summary1}, \ref{tab:summary2} and \ref{tab:summary3}.
As is clear from the tables, for most quantities there are results from ensembles with
different values for $N_f$.  In most cases, there is reasonable agreement among
results with $N_f=2$, $2+1$, and $2+1+1$.  As precision increases, we may
some day be able to distinguish among the different values of $N_f$, in
which case, presumably $2+1+1$ would be the most realistic.  (If isospin
violation is critical, then $1+1+1$ or $1+1+1+1$ might be desired.)
At present, for some quantities the errors in the $N_f=2+1$ results are smaller
than those with $N_f=2+1+1$ (e.g., for $m_c$), while for others
the relative size of the errors is reversed.
Our suggestion to those using the averages is to take whichever of the
$N_f=2+1$ or $N_f=2+1+1$ results has the smaller error.
We do not recommend using the $N_f=2$ results, except for studies of
the $N_f$-dependence of condensates and $\alpha_s$, as these have an uncontrolled
systematic error coming from quenching the strange quark.

Our plan is to continue providing FLAG updates, in the form of a peer
reviewed paper, roughly on a triennial basis. This effort is
supplemented by our more frequently updated
website \href{http://flag.unibe.ch}{{\tt
http://flag.unibe.ch}} \cite{FLAG:webpage}, where figures as well as pdf-files for
the individual sections can be downloaded. The papers reviewed in the
present edition have appeared before the closing date {\bf 30 September 2018}.\footnote{%
  %
  Working groups were given the option of including papers submitted to {\tt arxiv.org}
  before the closing date but published after this date. This flexibility allows this review to
  be up-to-date at the time of submission.
  Three papers of this type were included: 
  Ref.~\cite{Bazavov:2017lyh} in Secs.~\ref{sec:DDecays} and \ref{sec:BDecays},
  and Refs.~\cite{Liang:2018pis} and \cite{Gupta:2018lvp} in Sec.~\ref{sec:NME}.}

\input{summarytable.tex}
%\clearpage

This review is organized as follows.  In the remainder of
Sec.~\ref{sec:introduction} we summarize the composition and rules of
FLAG and discuss general issues that arise in modern lattice
calculations.  In Sec.~\ref{sec:qualcrit}, we explain our general
methodology for evaluating the robustness of lattice results.  We also
describe the procedures followed for combining results from different
collaborations in a single average or estimate (see
Sec.~\ref{sec:averages} for our definition of these terms). The rest
of the paper consists of sections, each dedicated to a set of
closely connected physical quantities. Each of these
sections is accompanied by an Appendix with explicatory notes.\footnote{%
%
In some cases, in order to keep the length of this review within reasonable bounds,
we have dropped these notes for older data, since they can be found in 
previous FLAG reviews~\cite{Colangelo:2010et,Aoki:2013ldr,Aoki:2016frl} .}
%
Finally, in Appendix \ref{comm} we provide a glossary in which we introduce
some standard lattice terminology (e.g., concerning the gauge,
light-quark and heavy-quark actions), and in addition we summarize and
describe the most commonly used lattice techniques and methodologies
(e.g., related to renormalization, chiral extrapolations, scale
setting).

\subsection{FLAG composition, guidelines and rules}

FLAG strives to be representative of the lattice community, both in
terms of the geographical location of its members and the lattice
collaborations to which they belong. We aspire to provide the nuclear- and
particle-physics communities with a single source of reliable
information on lattice results.

In order to work reliably and efficiently, we have adopted a formal
structure and a set of rules by which all FLAG members abide.  The
collaboration presently consists of an Advisory Board (AB), an
Editorial Board (EB), and eight Working Groups (WG). The r\^{o}le of
the Advisory Board is to provide oversight of the content, procedures, schedule
and membership of FLAG, to help resolve disputes, to serve as a source
of advice to the EB and to FLAG as a whole, and to provide a
critical assessment of drafts.
They also give their approval of the final version of the preprint before
it is rendered public. The Editorial Board coordinates the activities
of FLAG, sets priorities and intermediate deadlines, organizes votes on
FLAG procedures, writes the introductory sections, and takes care of
the editorial work needed to amalgamate the sections written by the
individual working groups into a uniform and coherent review. The
working groups concentrate on writing the review of the physical
quantities for which they are responsible, which is subsequently
circulated to the whole collaboration for critical evaluation.

The current list of FLAG members and their Working Group assignments is:
\begin{itemize}
\item
Advisory Board (AB):\hfill
S.~Aoki, M.~Golterman, R.~Van De Water, and A.~Vladikas
\item
Editorial Board (EB):\hfill
G.~Colangelo, A.~J\"uttner, S.~Hashimoto, S.R.~Sharpe, \\
\hbox{} \hfill and U.~Wenger
\item
Working Groups (coordinator listed first):
\begin{itemize}
\item Quark masses \hfill T.~Blum, A.~Portelli, and A.~Ramos
\item $V_{us},V_{ud}$ \hfill S.~Simula, T.~Kaneko, and J.~N.~Simone
\item LEC \hfill S.~D\"urr, H.~Fukaya, and U.M.~Heller
\item $B_K$ \hfill P.~Dimopoulos, G.~Herdoiza, and R.~Mawhinney
\item $f_{B_{(s)}}$, $f_{D_{(s)}}$, $B_B$ \hfill D.~Lin, Y.~Aoki, and  M.~Della Morte
\item $B_{(s)}$, $D$ semileptonic and radiative decays \hfill E.~Lunghi, D.~Becirevic, S.~Gottlieb, \\
\hbox{} \hfill and C.~Pena
\item $\alpha_s$ \hfill R.~Sommer, R.~Horsley, and T.~Onogi
\item NME \hfill R.~Gupta, S.~Collins, A.~Nicholson, and
 H.~Wittig
\end{itemize}
\end{itemize}

The most important FLAG guidelines and rules are the following:
\begin{itemize}
\item
the composition of the AB reflects the main geographical areas in
which lattice collaborations are active, with members from
America, Asia/Oceania, and Europe;
\item
the mandate of regular members is not limited in time, but we expect that a
certain turnover will occur naturally;
\item
whenever a replacement becomes necessary this has to keep, and
possibly improve, the balance in FLAG, so that different collaborations, from
different geographical areas are represented;
\item
in all working groups the three members must belong to three different
lattice collaborations;\footnote{The WG on semileptonic $D$ and $B$
decays currently has four members, but only three of them belong to
lattice collaborations.}$^{,}$\footnote{The NME WG, new in this addition of the
FLAG review, has been formed with four members (all members of lattice
collaborations) rather than three. This reflects the large amount of work needed to
create a section for which some of the systematic errors are substantially different
from those described in other sections,
and to provide a better representation of relevant collaborations.}
\item
a paper is in general not reviewed (nor colour-coded, as described in
the next section) by any of its authors;
\item
lattice collaborations % not represented in FLAG 
will be consulted on the colour coding
of their calculation;
\item
there are also internal rules regulating our work, such as voting procedures.
\end{itemize}
 
For this edition of the FLAG review, we sought the advice of external reviewers
once a complete draft of the review was available. For each review section, we
have asked one lattice expert (who could be a FLAG alumnus/alumna) and
one nonlattice phenomenologist for a critical assessment. This is similar
to the procedure followed by the Particle Data Group in the creation of the
Review of Particle Physics.  The reviewers provide comments and feedback on
scientific and stylistic matters. They are not anonymous, and enter into a discussion with
the authors of the WG. Our aim with this additional step is to make sure that a wider
array of viewpoints enter into the discussions, 
so as to make this review more useful for its intended audience.

\begin{comment}
{\bf Not sure if we should list them here or simply in the acknowledgements.}
The external reviewers were 
\begin{itemize}
\item Quark masses: Aneesh Manohar and Nazario Tantalo. 
\item $V_{us}$, $V_{ud}$: Elvira Gamiz and Emilie Passemar.
\item LEC: Marc Knecht and Tilo Wettig.
\item $B_K$: J\'er\^ome Charles and Laurent Lellouch.
\item $f_{B_{(s)}}$, $f_{D_{(s)}}$, $B_B$: Claude Bernard and Zoltan Ligeti.
\item $B_{(s)}$, $D$ semileptonic and radiative decays: Paolo Gambino and Vittorio Lubicz.
\item $\alpha_s$: Peter Petreczsky and Kim Maltman.
\item NME: Bira van Kolck and Takeshi Yamazaki.
\end{itemize}
\end{comment}

\subsection{Citation policy}
We draw attention to this particularly important point.  As stated
above, our aim is to make lattice-QCD results easily accessible to
those without lattice expertise,
and we are well aware that it is likely that some
readers will only consult the present paper and not the original
lattice literature. It is very important that this paper not be the
only one cited when our results are quoted. We strongly suggest that
readers also cite the original sources. In order to facilitate this,
in Tabs.~\ref{tab:summary1}, \ref{tab:summary2}, and \ref{tab:summary3}, besides
summarizing the main results of the present review, we also cite the
original references from which they have been obtained. In addition,
for each figure we make a bibtex file available on our webpage
\cite{FLAG:webpage} which contains the bibtex entries of all the
calculations contributing to the FLAG average or estimate. The
bibliography at the end of this paper should also make it easy to cite
additional papers. Indeed, we hope that the bibliography will be one of
the most widely used elements of the whole paper.




\subsection{General issues}

Several general issues concerning the present review are thoroughly
discussed in Sec.~1.1 of our initial 2010 paper~\cite{Colangelo:2010et},
and we encourage the reader to consult the relevant pages. In the
remainder of the present subsection, we focus on a few important
points. Though the discussion has been duly updated, it is similar
to that of Sec.~1.2  in the previous two reviews~\cite{Aoki:2013ldr,Aoki:2016frl},
with the addition of comments on the contributions from excited states
that are particularly relevant for the new section on NMEs.

The present review aims to achieve two distinct goals:
first, to provide a {\bf description} of the relevant work done on the lattice;
and, second,
to draw {\bf conclusions} on the basis of that work,  summarizing
the results obtained for the various quantities of physical interest.

The core of the information about the work done on the lattice is
presented in the form of tables, which not only list the various
results, but also describe the quality of the data that underlie
them. We consider it important that this part of the review represents
a generally accepted description of the work done. For this reason, we
explicitly specify the quality requirements
%
used and provide sufficient details in appendices so that the reader
can verify the information given in the tables.\footnote{%
%
We also use terms
like ``quality criteria", ``rating", ``colour coding", etc., when referring to
the classification of results, as described in Sec.~\ref{sec:qualcrit}.}

On the other hand, the conclusions drawn 
on the basis of the available lattice results
are the responsibility of FLAG alone. Preferring to
err on the side of caution, in several cases we draw
conclusions that are more conservative than those resulting from
a plain weighted average of the available lattice results. This cautious
approach is usually adopted when the average is
dominated by a single lattice result, or when
only one lattice result is available for a given quantity. In such
cases, one does not have the same degree of confidence in results and
errors as when there is agreement among several different
calculations using different approaches. The reader should keep
in mind that the degree of confidence cannot be quantified, and
it is not reflected in the quoted errors. 

Each discretization has its merits, but also its shortcomings. For most
topics covered in this review we
have an increasingly broad database, and for most quantities
lattice calculations based on totally different discretizations are
now available. This is illustrated by the dense population of the
tables and figures in most parts of this review. Those
calculations that do satisfy our quality criteria indeed lead, in almost all cases, to
consistent results, confirming universality within the accuracy
reached. In our opinion, the consistency between independent lattice
results, obtained with different discretizations, methods, and
simulation parameters, is an important test of lattice QCD, and
observing such consistency also provides further evidence that
systematic errors are fully under control.

In the sections dealing with heavy quarks and with $\alpha_s$, the
situation is not the same. Since the $b$-quark mass can barely be resolved
with current lattice spacings, most lattice methods for treating $b$
quarks use effective field theory at some level. This introduces
additional complications not present in the light-quark sector.  An
overview of the issues specific to heavy-quark quantities is given in
the introduction of Sec.~\ref{sec:BDecays}. For $B$- and $D$-meson
leptonic decay constants, there already exists a good number of
different independent calculations that use different heavy-quark
methods, but there are only one or two independent calculations of
semileptonic $B$ and $D$ meson form factors and $B$ meson mixing
parameters. For $\alpha_s$, most lattice methods involve a range of
scales that need to be resolved and controlling the systematic error
over a large range of scales is more demanding. The issues specific to
determinations of the strong coupling are summarized in Sec.~\ref{sec:alpha_s}.
%
\smallskip
\\{\it Number of sea quarks in lattice simulations:}\\
\noindent
Lattice-QCD simulations currently involve two, three or four flavours of
dynamical quarks. Most simulations set
the masses of the two lightest quarks to be equal, while the
strange and charm quarks, if present, are heavier
(and tuned to lie close to their respective physical values). 
Our notation for these simulations indicates which quarks
are nondegenerate, e.g., 
$\Nf=2+1$ if $m_u=m_d < m_s$ and $\Nf =2+1+1$ if $m_u=m_d < m_s < m_c$. 
Calculations with $\Nf =2$, i.e., two degenerate dynamical
flavours, often include strange valence quarks interacting with gluons,
so that bound states with the quantum numbers of the kaons can be
studied, albeit neglecting strange sea-quark fluctuations.  The
quenched approximation ($N_f=0$), in which all sea-quark contributions 
are omitted, has uncontrolled systematic errors and
is no longer used in modern lattice simulations with relevance to phenomenology.
Accordingly, we will review results obtained with $N_f=2$, $N_f=2+1$,
and $N_f = 2+1+1$, but omit earlier results with $N_f=0$. 
The only exception concerns the QCD coupling constant $\alpha_s$.
Since this observable does not require valence light quarks,
it is theoretically well defined also in the $N_f=0$ theory,
which is simply pure gluodynamics.
The $N_f$-dependence of $\alpha_s$, 
or more precisely of the related quantity $r_0 \Lambda_\msbar$, 
is a theoretical issue of considerable interest; here $r_0$ is a quantity
with the dimension of length that sets the physical scale, as discussed in
Appendix~\ref{sec_scale}.
We stress, however, that only results with $N_f \ge 3$ 
are used to determine the physical value of $\alpha_s$ at a high scale.
%
\smallskip
\\{\it Lattice actions, simulation parameters, and scale setting:}\\
\noindent
The remarkable progress in the precision of lattice
calculations is due to improved algorithms, better computing resources,
and, last but not least, conceptual developments.
Examples of the latter are improved
actions that reduce lattice artifacts and actions that preserve
chiral symmetry to very good approximation.
A concise characterization of
the various discretizations that underlie the results reported in the
present review is given in Appendix~\ref{sec_lattice_actions}.

Physical quantities are computed in lattice simulations in units of the
lattice spacing so that they are dimensionless.
For example, the pion decay constant that is obtained from a simulation
is $f_\pi a$, where $a$ is the spacing between two neighboring lattice sites.
(All simulations with results quoted in this review use hypercubic lattices,
i.e., with the same spacing in all four Euclidean directions.)
To convert these results to physical units requires knowledge
of the lattice spacing $a$ at the fixed values of the bare QCD parameters
(quark masses and gauge coupling) used in the simulation.
This is achieved by requiring agreement between
the lattice calculation and experimental measurement of a known
quantity, which thus ``sets the scale" of a given simulation. A few details
on this procedure are provided in Appendix~\ref{sec_scale}.
%
\smallskip
\\{\it Renormalization and scheme dependence:}\\
\noindent
Several of the results covered by this review, such as quark masses,
the gauge coupling, and $B$-parameters, are for quantities defined in a
given renormalization scheme and at a specific renormalization scale. 
The schemes employed (e.g., regularization-independent MOM schemes) are often
chosen because of their specific merits when combined with the lattice
regularization. For a brief discussion of their properties, see
Appendix~\ref{sec_match}. The conversion of the results obtained in
these so-called intermediate schemes to more familiar regularization
schemes, such as the $\msbar$-scheme, is done with the aid of
perturbation theory. It must be stressed that the renormalization
scales accessible in simulations are limited, because of the presence
of an ultraviolet (UV) cutoff of $\sim \pi/a$.
To safely match to $\msbar$, a scheme defined in perturbation theory,
Renormalization Group (RG) running to higher scales is performed,
either perturbatively or nonperturbatively (the latter using
finite-size scaling techniques).
%
\smallskip
\\{\it Extrapolations:}\\
\noindent
Because of limited computing resources, lattice simulations are often
performed at unphysically heavy pion masses, although results at the
physical point have become increasingly common. Further, numerical
simulations must be done at nonzero lattice spacing, and in a finite
(four-dimensional) volume.  In order to obtain physical results,
lattice data are obtained at a sequence of pion masses and a sequence
of lattice spacings, and then extrapolated to the physical pion mass
and to the continuum limit.  In principle, an extrapolation to
infinite volume is also required. However, for most quantities
discussed in this review, finite-volume effects are exponentially
small in the linear extent of the lattice in units of the pion mass,
and, in practice, one often verifies volume independence by comparing
results obtained on a few different physical volumes, holding other
parameters fixed. To control the associated systematic uncertainties,
these extrapolations are guided by effective theories.  For
light-quark actions, the lattice-spacing dependence is described by
Symanzik's effective theory~\cite{Symanzik:1983dc,Symanzik:1983gh};
for heavy quarks, this can be extended and/or supplemented by other
effective theories such as Heavy-Quark Effective Theory (HQET).  The
pion-mass dependence can be parameterized with Chiral Perturbation
Theory ($\chi$PT), which takes into account the Nambu-Goldstone nature
of the lowest excitations that occur in the presence of light
quarks. Similarly, one can use Heavy-Light Meson Chiral Perturbation
Theory (HM$\chi$PT) to extrapolate quantities involving mesons
composed of one heavy ($b$ or $c$) and one light quark.  One can
combine Symanzik's effective theory with $\chi$PT to simultaneously
extrapolate to the physical pion mass and the continuum; in this case,
the form of the effective theory depends on the discretization.  See
Appendix~\ref{sec_ChiPT} for a brief description of the different
variants in use and some useful references.  Finally, $\chi$PT can
also be used to estimate the size of finite-volume effects measured in
units of the inverse pion mass, thus providing information on the
systematic error due to finite-volume effects in addition to that
obtained by comparing simulations at different volumes.
%
\smallskip
\\{\it Excited-state contamination:}\\
\noindent
%
In all the hadronic matrix elements discussed in this review, the hadron in question
is the lightest state with the chosen quantum numbers. This implies that it dominates the
required correlation functions as their extent in Euclidean time is increased. Excited-state
contributions are suppressed by $e^{-\Delta E \Delta \tau}$, 
where $\Delta E$ is the gap between
the ground and excited states, and $\Delta \tau$ the relevant separation in Euclidean time. 
The size of $\Delta E$ depends on the hadron in question, and in general
is a multiple of the pion mass. In practice, as discussed at length in Sec.~\ref{sec:NME},
the contamination of signals due to  excited-state contributions is a much more
challenging problem for baryons than for the other particles discussed here.
This is in part due to the fact that the signal-to-noise ratio drops exponentially for
baryons, which reduces the values of $\Delta \tau$ that can be used.
\smallskip
\\{\it Critical slowing down:}\\
\noindent
The lattice spacings reached in recent simulations go down to 0.05 fm
or even smaller. In this regime, long autocorrelation times slow down
the sampling of the
configurations~\cite{Antonio:2008zz,Bazavov:2010xr,Schaefer:2010hu,Luscher:2010we,Schaefer:2010qh,Chowdhury:2013mea,Brower:2014bqa,Fukaya:2015ara,DelDebbio:2002xa,Bernard:2003gq}.
Many groups check for autocorrelations in a number of observables,
including the topological charge, for which a rapid growth of the
autocorrelation time is observed with decreasing lattice spacing.
This is often referred to as topological freezing. A solution to the
problem consists in using open boundary conditions in time~\cite{Luscher:2011kk}, 
instead of the more common antiperiodic ones. More recently
two other approaches have been proposed, one based on a multiscale
thermalization algorithm \cite{Endres:2015yca,Detmold:2018zgk} and another based on
defining QCD on a nonorientable manifold \cite{Mages:2015scv}.  The
problem is also touched upon in Sec.~\ref{s:crit}, where it is
stressed that attention must be paid to this issue. While large scale
simulations with open boundary conditions are already far advanced
\cite{Bruno:2014jqa}, only one result reviewed here
has been obtained with any of the above methods
(results for $\alpha_s$ from Ref.~\cite{Bruno:2017gxd} which use open boundary
conditions).
It is usually {\it  assumed} that the continuum limit can be reached by extrapolation
from the existing simulations, and that potential systematic errors due
to the long autocorrelation times have been adequately controlled.
Partially or completely frozen topology would produce a mixture of different $\theta$ vacua, and 
the difference from the desired $\theta=0$ result
may be estimated in some cases using  
 chiral perturbation theory, which gives predictions for the $\theta$-dependence of the 
physical quantity of interest \cite{Brower:2003yx,Aoki:2007ka}. These ideas have been systematically and successfully tested in various models in \cite{Bautista:2015yza,Bietenholz:2016ymo}, and a numerical test on MILC ensembles indicates that the topology dependence 
for some of the physical quantities reviewed here is small, consistent with theoretical 
expectations~\cite{Bernard:2017npd}.
%
\smallskip
\\ {\it Simulation algorithms and numerical errors:}\\
\noindent
Most of the modern lattice-QCD simulations use exact algorithms such 
as those of Refs.~\cite{Duane:1987de,Clark:2006wp}, which do not produce any systematic errors when exact 
arithmetic is available. In reality, one uses numerical calculations at 
double (or in some cases even single) precision, and some errors are 
unavoidable. More importantly, the inversion of the Dirac operator is 
carried out iteratively and it is truncated once some accuracy is 
reached, which is another source of potential systematic error. In most 
cases, these errors have been confirmed to be much less than the 
statistical errors. In the following we assume that this source of error 
is negligible. 
Some of the most recent simulations use an inexact algorithm in order to 
speed up the computation, though it may produce systematic effects. 
Currently available tests indicate that errors from the use of inexact
algorithms are under control~\cite{Bazavov:2012xda}.
