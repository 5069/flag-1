# FLAG plot for su3_L6
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
flagyear='2020'
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm f_+(0)$"			# plot title
plotnamestring	= "f+0"			# filename for plot
plotxticks	= ([[.95,.97,.99,1.01]])	# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

yaxisstrings	= [				# x,y-location for y-strings
		   [+0.922,+26.5,"$\\rm N_f=2+1+1$"],
		   [+0.922,+16.0,"$\\rm N_f=2+1$"],
		   [+0.922,+7.5,"$\\rm N_f=2$"],
		   [+0.922,+3.8,"non-lattice"]
		    ]
xlimits		= [0.94,1.05]			# plot's xlimits
logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
	
PRELIM = 0

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column		item
#		0	 	marker style (see bottom of this file for a full list)
#		1		marker face color r=red, b=blue, k=black, w=white
#						  g=green
#		2		marker color (errorbar and frame), color coding as 
#				for face color
#		3		color intensity 0=full, 1=sosos, 2=bleak
#		4		x-position for collaboration text

dat2=[
#[0.967 ,0.006  ,0.006   ,0.006  ,0.006   ,"JLQCD 05"	,['s','w','r',0,1.01]],
#[0.952 ,0.006  ,0.006   ,0.006  ,0.006   ,"JLQCD 05"	,['s','w','r',0,1.01]],
#[0.968 ,0.01082,0.01082 ,0.009  ,0.009   ,"RBC 06"	,['s','w','r',0,1.01]],
#[0.9647,0.0015 ,0.0015  ,0.0015 ,0.0015  ,"QCDSF 07 (stat. err. only)"	,['s','w','r',0,1.01]],
[0.9560,0.0084 ,0.0084  ,0.0057 ,0.0057  ,"ETM 09A"	,['s','g','g',0,1.01]],
[0.9544,0.0068 ,0.0068  ,0.0068 ,0.0068  ,"ETM 10D (stat. err. only)"	,['s','l','g',0,1.01]]
]

dat2p1=[
[0.9644,0.0049 ,0.0049  ,0.0033,0.0033  ,"RBC/UKQCD 07"	,['s','w','r',0,1.01]], 
[0.9599,0.00597 ,0.00481,0.0034,0.0034  ,"RBC/UKQCD 10"	,['s','w','r',0,1.01]],
[0.964 ,0.006  ,0.006   ,0.006 ,0.006   ,"JLQCD 11"	,['s','w','r',0,1.01]], 
[0.959 ,0.006  ,0.006   ,0.0078 ,0.0078 ,"JLQCD 12"	,['s','w','r',0,1.01]], 
[0.9667,0.00402,0.00402 ,0.0023,0.0023  ,"FNAL/MILC 12I"	,['s','g','g',0,1.01]], 
[0.9670,0.0050,0.0027 ,0.0020,0.0020  ,"RBC/UKQCD 13"	,['s','l','g',0,1.01]], 
[0.9685,0.0037,0.0037 ,0.0034,0.0034  ,"RBC/UKQCD 15A",['s','g','g',0,1.01]],  
[0.9636,0.0050 ,0.0067  ,0.0036 ,0.0036 ,"JLQCD 17"	,['s','w','r',0,1.01]],
[0.9603,0.0047 ,0.0049  ,0.0016 ,0.0016 ,"PACS 19"	,['s','w','r',0,1.01]],
]

dat2p1p1=[
[0.9704,0.004 ,0.004 ,0.0024,0.0024  ,"FNAL/MILC 13C"	,['s','l','g',0,1.01]],
[0.9704,0.0033,0.0033,0.0024,0.0024  ,"FNAL/MILC 13E"	,['s','l','g',0,1.01]], 
[0.9709,0.0046,0.0046,0.0045,0.0045  ,"ETM 16"		,['s','g','g',0,1.01]],
[0.9696,0.0019 ,0.0019 ,0.0015,0.0015  ,"FNAL/MILC 18"	,['s','g','g',0,1.01]],
]

datother=[
[0.961,0.008,0.008,0.008,0.008,"Leutwyler 84"			,['o','b','b',0,1.01]],
[0.978,0.01 ,0.01 ,0.01 ,0.01 ,"Bijnens 03"		,['o','b','b',0,1.01]],
[0.974,0.011,0.011,0.011,0.011,"Jamin 04"		,['o','b','b',0,1.01]],
[0.984,0.012,0.012,0.012,0.012,"Cirigliano 05"		,['o','b','b',0,1.01]],
[0.986,0.009,0.009,0.007,0.007,"Kastner 08"		,['o','b','b',0,1.01]]
]
# The color coding for the FLAG-average (below)
# is as for the data itself; the additional 
# list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more than one)
# is significant, the last item will be
# plotted last and will therefore be plotted on top of all others.

FLAGaverage=[# there should be as many entries for average as there are data sets
             # in the case where no average is provided for a given data set
             # just replace the central value by "NaN"
             # 
             # with respect to the above there is another column at the end specifying
             # the type of FLAG-band
             # 0 -> grey area bounded by solid black lines
             # 1 -> dashed lines marking the edge of the interval
 [np.nan,0.0000,0.0000,0.0000,0.0000,"place holder no data      "  ,['s','w','k',0,1.01],0],
 #[0.9604,0.0075,0.0075,0.0000,0.0000,"our estimate for $\\rm N_f=2$"  ,['s','w','k',0,1.01],1],
 #[0.9599,0.0038,0.0038,0.0000,0.0000,"our estimate for $\\rm N_f=2+1$",['s','k','k',0,1.01],0]
 [0.9560,0.0084,0.0084,0.0057,0.0057,"FLAG average for $\\rm N_f=2$"  ,['s','k','k',0,1.01],1],
 [0.9677,0.0027,0.0027,0.0000,0.0000,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,1.01],0],
 [0.9698,0.0017,0.0017,0.0000,0.0000,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,1.01],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datother,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current working
# directory. That's where all plots will be stored 
execfile('FLAGplot.py')





# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

