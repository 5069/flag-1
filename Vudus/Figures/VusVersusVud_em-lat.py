# -*- coding: utf-8 -*-
# This file generates FLAG-scatter plots
import matplotlib.pyplot as plt
import matplotlib.image as implt
import matplotlib.patches as mpatches
import matplotlib.patches as polygon
from numpy import *
import os.path
import os.path

PRELIM=0
flagyear='2020'
try: 
 import numpy
 blogo=1
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
 blogo=0
try: 
 from PIL import Image
 blogo=1
except ImportError:
 print "Image library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
 blogo=0

#from FLAG import *
blogo=0
plt.rc('font', family='sans-serif')

################################################################################
xlims = 0.955,0.980
ylims = 0.217,.229
xVud = arange(xlims[0],xlims[1],0.00001)
n=len(xVud)-1
Vub=0.00394;
dVub=0.00036;

fplus_2p1p1        = 0.9698;
dfplus_2p1p1_stat  = 0.0000;
dfplus_2p1p1_syst  = 0.0017;
dfplus_2p1p1_comb  = sqrt(dfplus_2p1p1_stat**2+dfplus_2p1p1_syst**2);
fplus_2p1          = 0.9677;
dfplus_2p1_stat    = 0.0000;
dfplus_2p1_syst    = 0.0027;
dfplus_2p1_comb    = sqrt(dfplus_2p1_stat**2+dfplus_2p1_syst**2);
#fplus_2            = 0.9560;
#dfplus_2_stat      = 0.0057;
#dfplus_2_syst      = 0.0062;
#dfplus_2_comb    = sqrt(dfplus_2_stat**2+dfplus_2_syst**2);

fKfpi_2p1p1       = 1.1932;
dfKfpi_2p1p1_stat = 0.000;
dfKfpi_2p1p1_syst = 0.0021;
dfKfpi_2p1p1_comb = sqrt(dfKfpi_2p1p1_stat**2+dfKfpi_2p1p1_syst**2);
fKfpi_2p1       = 1.1917;
dfKfpi_2p1_stat = 0.0000;
dfKfpi_2p1_syst = 0.0037;
dfKfpi_2p1_comb = sqrt(dfKfpi_2p1_stat**2+dfKfpi_2p1_syst**2);
#fKfpi_2         = 1.205;
#dfKfpi_2_stat   = 0.006;
#dfKfpi_2_syst   = 0.017;
#dfKfpi_2_comb = sqrt(dfKfpi_2_stat**2+dfKfpi_2_syst**2);

# Vud w/ RC by Marciano-Sirlin
# Vud             = 0.97420;
# dVud            = 0.00021;
# w/ RC by Seng et al.
# Vud             = 0.97370;
# dVud            = 0.00014;
# w/ RC by Czarnecki-Marciano-Sirlin
Vud             = 0.97370;
dVud            = 0.00014

Vusfplus        = 0.2165;
dVusfplus       = 0.0004;
R2        = 0.27683;
dR2       = 0.00035;
################################################################################
fig=plt.figure()
ax=fig.add_subplot(111)

def VusVudellipse(Vud,Vusfp,dVusfp,R2ofKfpi,dR2ofKfpi):
 #print (dR2ofKfpi**2*dVusfp**2*Vud**2* 
 #      (dVusfp**2 - Vusfp**2 + 2*R2ofKfpi*Vusfp*Vud + (dR2ofKfpi**2 - 
 #          R2ofKfpi**2)*Vud**2))
 a = (dVusfp**2*R2ofKfpi*Vud + 
   dR2ofKfpi**2*Vusfp*Vud**2 - sqrt(dR2ofKfpi**2*dVusfp**2*Vud**2* 
       (dVusfp**2 - Vusfp**2 + 2*R2ofKfpi*Vusfp*Vud + (dR2ofKfpi**2 - 
           R2ofKfpi**2)*Vud**2)))/(dVusfp**2 + dR2ofKfpi**2*Vud**2)
 b = (dVusfp**2*R2ofKfpi*Vud + 
   dR2ofKfpi**2*Vusfp*Vud**2 + sqrt(dR2ofKfpi**2*dVusfp**2*Vud**2*
       (dVusfp**2 - Vusfp**2 + 2*R2ofKfpi*Vusfp*Vud + (dR2ofKfpi**2 - 
           R2ofKfpi**2)*Vud**2)))/(dVusfp**2 + dR2ofKfpi**2*Vud**2)
 return a,b


def printellipse(fKfpi,dfKfpi,fplus,dfplus,Vusfplus,dVusfplus,R2,dR2,face,edge,a,zo):
 Vus,dVus = Vus_from_fplus(Vusfplus,dVusfplus,fplus,dfplus)
 xVud = arange(xlims[0],xlims[1],0.00001)
 R2ofKfpi = R2/fKfpi
 dR2ofKfpi=sqrt((dR2/fKfpi)**2+(R2/fKfpi**2*dfKfpi)**2)
 
 # multiply the errors by 1.5 to get ellipses corresponding to 68% probability
 dVus = 1.5 * dVus
 dR2ofKfpi = 1.5 * dR2ofKfpi
 
 el = VusVudellipse(xVud,Vus,dVus,R2ofKfpi,dR2ofKfpi)
 i1=0
 i2=0
 e1=(len(el[0])-(sum(isnan(el[0]))))*[0.]
 e2=(len(el[1])-(sum(isnan(el[1]))))*[0.]
 xVud1=(len(el[0])-(sum(isnan(el[0]))))*[0.]
 xVud2=(len(el[1])-(sum(isnan(el[1]))))*[0.]
 for i in range(n):
  if isnan(el[0][i])==0:
   e1[i1]=el[0][i]
   xVud1[i1]=xVud[i]
   i1+=1
  if isnan(el[1][i])==0:
   e2[i2]=el[1][i]
   xVud2[i2]=xVud[i]
   i2+=1
 n1=len(e1)
 n2=len(e2)
 x=append(xVud1[:n1-1],xVud2[i2-1:0:-1])
 x=append(x,xVud1[0])
 y=append(e1[:n1-1],e2[i2-1:0:-1])
 y=append(y,e1[0])
 verts=concatenate((array([x]).T,array([y]).T),1)
 poly = plt.Polygon(verts,alpha=a, facecolor=face, edgecolor=edge,zorder=zo)
 pl1=ax.add_patch(poly)
 return pl1

def fKfpiband(xVud,fKfpi,dfKfpi,R2,dR2,face,edge,a):
 y  = R2/fKfpi*xVud;
 dy = sqrt((dR2/fKfpi)**2+(R2/fKfpi**2*dfKfpi)**2)*xVud
 verts = [(xlims[0],y[0]-dy[0]),
          (xlims[1],y[n]-dy[n]),
 	  (xlims[1],y[n]+dy[n]),
 	  (xlims[0],y[0]+dy[0])]
 poly = plt.Polygon(verts,alpha=a, facecolor=face, edgecolor=edge)
 p=ax.add_patch(poly)
 return p

def fKfpilines(xVud,fKfpi,dfKfpi,R2,dR2,face,edge,a):
 y  = R2/fKfpi*xVud;
 dy = sqrt((dR2/fKfpi)**2+(R2/fKfpi**2*dfKfpi)**2)*xVud
 verts = [(xlims[0],y[0]-dy[0]),
          (xlims[1],y[n]-dy[n]),
 	  (xlims[1],y[n]+dy[n]),
 	  (xlims[0],y[0]+dy[0])]
 p = plt.plot([xlims[0],xlims[1]],[y[0]-dy[0],y[n]-dy[n]],'--',c=face,alpha=a)
 p = plt.plot([xlims[0],xlims[1]],[y[0]+dy[0],y[n]+dy[n]],'--',c=face,alpha=a)
 return p
 
def Vus_from_fplus(Vusfplus,dVusfplus,fplus,dfplus):
 Vus = Vusfplus/fplus
 dVus = sqrt((dVusfplus/fplus)**2+(Vusfplus/fplus**2*dfplus)**2)   
 return Vus,dVus

def Vus_from_fKfpi(RfKfpi,dRfKfpi,Vud,dVud,fKfpi,dfKfpi):
 Vus = RfKfpi*Vud/fKofpi
 dVus = sqrt( (Vus/RfKfpi*dRfKfpi)**2
            + (Vus/fKfpi*dfKfpi)**2
            + (Vus/Vud*dVud)**2)
 return Vus,dVus

def RfKfpi_over_fKfpi(RfKfpi,dRfKfpi,fKfpi,dfKfpi):
 R = RfKfpi/fKfpi
 dR = sqrt((dRfKfpi/fKfpi)**2+(RfKfpi/fKfpi**2*dfKfpi)**2)
 return R,dR

def fplusband(Vusfplus,dVusfplus,fplus,dfplus,face,edge,a):
 Vus,dVus = Vus_from_fplus(Vusfplus,dVusfplus,fplus,dfplus)
 x=xlims[0]
 y=Vus-dVus
 height=2*dVus
 width=xlims[1]-xlims[0]
 p1=mpatches.Rectangle((x,y),width,height,facecolor=face,edgecolor=edge,alpha=a)
 p=plt.gca().add_patch(p1)
 return p


def fpluslines(Vusfplus,dVusfplus,fplus,dfplus,face,edge,a):
 Vus,dVus = Vus_from_fplus(Vusfplus,dVusfplus,fplus,dfplus)
 x=xlims[0]
 y=Vus-dVus
 height=2*dVus
 width=xlims[1]-xlims[0]
 p=plt.plot([xlims[0],xlims[1]],[y,y],'--',color=face,alpha=a)
 p=plt.plot([xlims[0],xlims[1]],[y+height,y+height],'--',color=face,alpha=a)
 return p




# 2-flavour ellipse
#ell2=printellipse(fKfpi_2,dfKfpi_2_comb,fplus_2,dfplus_2_comb,Vusfplus,dVusfplus,R2,dR2,(1,1,1),(0,0,.6),1,10)
# 2+1-flavour ellipse
ell2p1=printellipse(fKfpi_2p1,dfKfpi_2p1_comb,fplus_2p1,dfplus_2p1_comb,Vusfplus,dVusfplus,R2,dR2,(1,1,1),(0,.6,0),1,10)
# 2+1+1-flavour ellipse
ell2p1p1=printellipse(fKfpi_2p1p1,dfKfpi_2p1p1_comb,fplus_2p1p1,dfplus_2p1p1_comb,Vusfplus,dVusfplus,R2,dR2,(1,1,1),(.6,0,0),1,10)

# 2 flavour results from fplus
#bandfplus2=fplusband(Vusfplus,dVusfplus,fplus_2,dfplus_2_comb,(.7,.7,1),(.3,.3,.8),.6)
# 2+1 flavour results from fplus
bandfplus2p1=fplusband(Vusfplus,dVusfplus,fplus_2p1,dfplus_2p1_comb,(.7,1,.7),(.3,.8,.3),.8)
# 2+1+1 flavour results from fplus
bandfplus2p1p1=fplusband(Vusfplus,dVusfplus,fplus_2p1p1,dfplus_2p1p1_comb,(1,.7,.7),(.8,.3,.3),1.0)

# 2 flavour results from fplus
#fpluslines(Vusfplus,dVusfplus,fplus_2,dfplus_2_comb,(.7,.7,1),(.3,.3,.8),.6)
# 2+1 flavour results from fplus
fpluslines(Vusfplus,dVusfplus,fplus_2p1,dfplus_2p1_comb,(.7,1,.7),(.3,.8,.3),.8)
# 2+1+1 flavour results from fplus
fpluslines(Vusfplus,dVusfplus,fplus_2p1p1,dfplus_2p1p1_comb,(1,.7,.7),(.8,.3,.3),1.0)

# 2 flavour result from fKfpi
#bandfKfpi2=fKfpiband(xVud,fKfpi_2,dfKfpi_2_comb,R2,dR2,(.3,.3,1),(.3,.3,.8),.6)
# 2+1 flavour result from fKfpi
bandfKfpi2p1=fKfpiband(xVud,fKfpi_2p1,dfKfpi_2p1_comb,R2,dR2,(0,1,0),(.3,.8,.3),.8)
# 2+1+1 flavour result from fKfpi
bandfKfpi2p1p1=fKfpiband(xVud,fKfpi_2p1p1,dfKfpi_2p1p1_comb,R2,dR2,(1,.3,.3),(.8,.3,.3),1.0)

# 2 flavour result from fKfpi
#fKfpilines(xVud,fKfpi_2,dfKfpi_2_comb,R2,dR2,(.3,.3,1),(.3,.3,.8),.6)
# 2+1 flavour result from fKfpi
fKfpilines(xVud,fKfpi_2p1,dfKfpi_2p1_comb,R2,dR2,(0,1,0),(.3,.8,.3),.8)
# 2+1+1 flavour result from fKfpi
fKfpilines(xVud,fKfpi_2p1p1,dfKfpi_2p1p1_comb,R2,dR2,(1,.3,.3),(.8,.3,.3),1.0)

# Vudband
x=Vud-dVud
y=ylims[0]
height=ylims[1]-ylims[0]
width=2*dVud
p2=mpatches.Rectangle((x,y),width,height,facecolor=(.6,1,1),edgecolor=(.6,1,1))
bandVud=plt.gca().add_patch(p2)
plt.draw()

# unitarity:
unitarity=plt.plot(xVud,sqrt(1-Vub**2-xVud**2),'k:')
unitarity=plt.plot(xVud,sqrt(1-Vub**2-xVud**2)-.5/sqrt(1-Vub**2-xVud**2)*2*Vub*dVub,'k:')
unitarity=plt.plot(xVud,sqrt(1-Vub**2-xVud**2)+.5/sqrt(1-Vub**2-xVud**2)*2*Vub*dVub,'k:')

leg=plt.legend([
            bandfplus2p1p1,
	    bandfKfpi2p1p1,
	    bandfplus2p1,
	    bandfKfpi2p1,
#	    bandfplus2,
#	    bandfKfpi2,
	    ell2p1p1,
	    ell2p1,
#	    ell2,
	    bandVud], 
	['lattice results for $f_+(0)$, $N_f=2+1+1$',
	 'lattice results for $f_{K^\pm}/f_{\pi^\pm}$, $N_f=2+1+1$',
	 'lattice results for $f_+(0)$, $N_f=2+1$',
	 'lattice results for $f_{K^\pm}/f_{\pi^\pm}$, $N_f=2+1$',
#	 'lattice results for $f_+(0)$, $N_f=2$',
#	 'lattice results for $f_{K^\pm}/f_{\pi^\pm}$, $N_f=2$',
	 'lattice results for $N_f=2+1+1$ combined',
	 'lattice results for $N_f=2+1$ combined',
#	 'lattice results for $N_f=2$, combined',
	 'nuclear $\\beta$ decay'],
	 loc='lower right', labelspacing=.1)

for t in leg.get_texts():
    t.set_fontsize(10)    # the legend text fontsize
for l in leg.get_lines():
    l.set_linewidth(0.6)  # the legend line width

#logo="logo"
#if os.path.isfile("./plots/FLAG_Logo.png"):
# if len(logo)>0 and blogo==1:
#  plotbox=ax.bbox.extents
#  im = Image.open('./plots/FLAG_Logo.png')
#  height=1792/20
#  width=1026/20
#  im2 = im.resize((height,width),Image.ANTIALIAS)
#  im2 = numpy.array(im2).astype(numpy.float) / 255
#  fig.figimage(im2,plotbox[0],plotbox[3]-height-15,zorder=10)
#else: 
# print "File ./plots/FLAG_Logog.png is missing. Logo not added to plots"


plt.xlim(xlims)
plt.ylim(ylims)
plt.xlabel("$V_{ud}$",fontsize=20)
plt.ylabel("$V_{us}$",fontsize=20)
# insert logo if specified #####################################################
logo = 'upper left'
if 2==2:
 if (logo)>0:
  if not os.path.isfile("./plots/FLAG_Logo.png"):
   print "FLAG_Logo.png is missing! Use mkFLAGlogo in FLAGplot.py to generate"
   print "it"
   print ""
  plotbox=ax.bbox.extents
  ax = plt.axis()
  if logo=='upper right':
   xpos=0.777
   ypos=0.855
   Pxpos=.0
   Pypos=+250
  elif logo=='upper left':
   xpos=0.12
   ypos=0.855
   Pxpos=45.10
   Pypos=+250
  elif logo=='lower left':
   xpos=0.12
   ypos=0.015
   Pxpos=45.10
   Pypos=-50
  elif logo=='lower right':
   xpos=0.777
   ypos=0.015
   Pxpos=.0
   Pypos=-50
  else:
   print "FLAGplot.py: There seems to be sth. wrong with the key word <<logo>>"
   exit()
  # draw the logo
  axicon = fig.add_axes([xpos,ypos,.13,.13],frameon=False,xticks=[],yticks=[])
  im  = Image.open('../../scripts/FLAGplot/Logos/FLAG_Logo_'+flagyear+'.png')
  im2 = numpy.array(im).astype(numpy.float) / 255
  axicon.imshow(im2)

if PRELIM == 1:
 plt.annotate('PRELIMINARY',rotation=30,fontsize=45,color=[.2,.2,.2],
		 xy=(1.25,-4.75), horizontalalignment='center',
		 verticalalignment='center',
		 xycoords='axes fraction',alpha=.5,zorder=0)

if PRELIM==2:
  plt.text(Pxpos,Pypos,'PRELIMINARY',rotation=0,fontsize=10,color=[.2,.2,.2])


plt.savefig("plots/VusVersusVud_em-lat.eps",format="eps",dpi=600)
plt.savefig("plots/VusVersusVud_em-lat.pdf",format="pdf",dpi=600)
plt.savefig("plots/VusVersusVud_em-lat.png",format="png",dpi=400)
plt.show()
