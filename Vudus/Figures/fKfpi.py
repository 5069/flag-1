# FLAG plot for RFKpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################
flagyear='2020'
# layout specifications
titlestring	= "$\\rmf_{K^\pm}/f_{\pi^\pm}$"			# plot title
plotnamestring	= "RfKfpi"			                # filename for plot
plotxticks	= ([[1.14, 1.18,  1.22,  1.26]])# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

yaxisstrings	= [				# x,y-location for y-strings
		   [+1.108,33.0,"$\\rm N_f=2+1+1$"],
		   [+1.108,18.0,"$\\rm N_f=2+1$"],
		   [+1.108,3.0,"$\\rm N_f=2$"]
		    ]
xlimits		= [1.13,1.36]			# plot's xlimits
logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
PRELIM = 0
tpos = 1.28					# x-position for the data labels
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

dat2p1p1=[
[1.2201 ,0.013     ,0.013     ,0.013     ,0.013     ," ETM 10E (stat. err. only)",['s','l','g',0,tpos]],
[1.1872 ,0.0042    ,0.0042    ,0.0042    ,0.0042    ," MILC 11 (stat. err. only)",['s','l','g',0,tpos]],
[1.1947 ,0.00452   ,0.00452   ,0.0026    ,0.0026    ," MILC 13A        ",['s','l','g',0,tpos]],
[1.1916 ,0.00219   ,0.00219   ,0.0015    ,0.0015    ," HPQCD 13A       ",['s','g','g',0,tpos]],
[1.183  ,0.0172    ,0.0172    ,0.010     ,0.010     ," ETM 13F         ",['s','l','g',0,tpos]],
[1.1956 ,0.00206   ,0.00279   ,0.0010    ,0.0010    ," FNAL/MILC 14A   ",['s','l','g',0,tpos]],
[1.184  ,0.0163    ,0.0163    ,0.012     ,0.012     ," ETM 14E         ",['s','g','g',0,tpos]],
[1.1950 ,0.00222   ,0.00139   ,0.0013    ,0.0013     ," FNAL/MILC 17   ",['s','g','g',0,tpos]],
[1.180  ,0.016     ,0.016     ,0.016     ,0.0016     ," ETM 20 (stat. err. only)",['s','l','g',0,tpos]],
[1.1942 ,0.00446   ,0.00446    ,0.0032    ,0.0032     ," Miller 20   ",['s','g','g',0,tpos]],
]                                         
dat2p1=[                                  
[1.210  ,0.0136    ,0.0136    ,0.0040    ,0.0040    ," MILC 04        ",['s','l','g',0,tpos]],
#[1.21504,0.02417   ,0.01136   ,0.002     ,0.002     ," NPLQCD 06      ",['s','w','r',0,tpos]] ,
[1.1866 ,0.00755   ,0.00755   ,0.002     ,0.002     ," HPQCD/UKQCD 07 ",['s','g','g',0,tpos]] ,
[1.20229,0.06459   ,0.06459   ,0.018     ,0.018     ," RBC/UKQCD 08   ",['s','w','r',0,tpos]] ,
#[1.1866 ,0.0201    ,0.0201    ,0.        ,0.        ," PACS-CS 08, 08A",['s','w','r',0,tpos]] ,
[1.191  ,0.02334   ,0.02334   ,0.0160    ,0.0160    ," Aubin 08       ",['s','l','g',0,tpos]] ,
[1.197  ,0.01334   ,0.00671   ,0.0030    ,0.0030    ," MILC 09        ",['s','l','g',0,tpos]] ,
[1.198  ,0.00825   ,0.00632   ,0.0020    ,0.0020    ," MILC 09A       ",['s','l','g',0,tpos]] ,
#[1.2072 ,0.012     ,0.012     ,0.012     ,0.012     ," JLQCD/TWQCD 09A (stat. err. only)",['s','w','r',0,tpos]] ,
[1.18954,0.009434  ,0.009434  ,0.007     ,0.007     ," BMW 10         ",['s','g','g',0,tpos]] ,
#[1.32762,0.07203   ,0.07203   ,0.        ,0.        ," PACS-CS 09     ",['s','w','r',0,tpos]] ,
[1.20131,0.02603   ,0.02603   ,0.007     ,0.007     ," RBC/UKQCD 10A  ",['s','l','g',0,tpos]] ,
[1.22681,0.0191    ,0.0191    ,0.        ,0.        ," JLQCD/TWQCD 10 ",['s','w','r',0,tpos]] ,
[1.197  ,0.00728   ,0.00361   ,0.0020    ,0.0020    ," MILC 10        ",['s','g','g',0,tpos]] ,
[1.202  ,0.0152    ,0.0152    ,0.011     ,0.011     ," Laiho 11       ",['s','l','g',0,tpos]] ,
[1.19641,0.01855   ,0.01855   ,0.012     ,0.012     ," RBC/UKQCD 12   ",['s','l','g',0,tpos]] ,
[1.19193,0.00520   ,0.00520   ,0.        ,0.        ," RBC/UKQCD 14B  ",['s','g','g',0,tpos]] ,
[1.178  ,0.028     ,0.028     ,0.010     ,0.010     ," BMW 16         ",['s','g','g',0,tpos]] ,
[1.190  ,0.0164    ,0.0164    ,0.010     ,0.010     ," QCDSF/UKQCD 16 ",['s','g','g',0,tpos]] ,
]                                     
dat2=[                                    
[1.2072 ,0.03      ,0.03      ,0.        ,0.        ," QCDSF/UKQCD 07 ",['s','l','g',0,tpos]],
[1.2053 ,0.01849   ,0.01849   ,0.006     ,0.006     ," ETM 09         ",['s','g','g',0,tpos]] ,
[1.1854 ,0.008     ,0.008     ,0.008     ,0.008     ," ETM 10D (stat. err. only)",['s','l','g',0,tpos]] ,
#[1.2121 ,0.045     ,0.045     ,0.        ,0.        ," BGR 11         ",['s','w','r',0,tpos]],
[1.18503,0.006840  ,0.006840  ,0.0057    ,0.0057    ," ALPHA 13A       ",['s','l','g',0,tpos]],
[1.1991 ,0.005     ,0.005     ,0.        ,0.        ," ETM 14D (stat. err. only)",['s','w','r',0,tpos]] 
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
 [1.2053,0.01849 ,0.01849 ,0.006,0.006," FLAG average for $\\rm N_f=2$"  ,['s','k','k',0,tpos],1],
 [1.1917,0.0037  ,0.0037  ,0.0  ,0.0  ," FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
 [1.1932,0.0021  ,0.0021  ,0.000,0.000," FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

