# FLAG plot for Vus and Vud
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################
flagyear='2020'
FS=6
MS=7
PRELIM=0
# layout specifications
titlestring	= "$\\rm |V_{us}|$"			# plot title
plotnamestring	= "Vusud"			# filename for plot
plotxticks	= ([[.22, .23]])# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

yaxisstrings	= [				# x,y-location for y-strings
		   [+.208,43.0,"$\\rm N_f=2+1+1$"],
		   [+.208,24.5,"$\\rm N_f=2+1$"],
		   [+.208,8.7,"$\\rm N_f=2$"]
		    ]
xlimits		= [.21,.24]			# plot's xlimits
logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
REV=1
tpos   		= 0.235				# x-position for the data labels 
tpos2		= 500				# x-position for the data labels
					        # move data labels of l.h.s side
						# plot to nirvana
REV    		= 1				# reverse order of data points
PRELIM 		= 0				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
REMOVEWHITESPACE= 0						# one figure
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
# THE DATA #####################################################################


datVus2p1p1=[
[0.22517 ,0.00085593 ,0.00085593 ,0,0,"Miller 20",['s','g','g',0,tpos]],
[0.22328 ,0.00059471 ,0.00059471 ,0,0,"FNAL/MILC 18",['s','g','g',0,tpos]],
[0.2251  ,0.00045    ,0.00045    ,0,0,"FNAL/MILC 17",['s','g','g',0,tpos]],
[0.2230  ,0.001118   ,0.001118   ,0,0,"ETM 16    ",['^','g','g',0,tpos]],
[0.227000,0.002973   ,0.002973   ,0,0,"ETM 14E      ",['s','g','g',0,tpos]],
[0.224900,0.000566   ,0.000566   ,0,0,"FNAL/MILC 14A",['s','l','g',0,tpos]],
[0.223100,0.000860   ,0.000860   ,0,0,"FNAL/MILC 13E",['^','g','g',0,tpos]],
[0.222898,0.0010534  ,0.0010534  ,0,0,"FNAL/MILC 13C  ",['^','l','g',0,tpos]],
[0.227045,0.003156   ,0.003156   ,0,0,"ETM 13F        ",['s','l','g',0,tpos]],
[0.225600,0.000500   ,0.000500   ,0,0,"HPQCD 13A      ",['s','g','g',0,tpos]],
[0.224935,0.000896274,0.000896274,0,0,"MILC 13A       ",['s','l','g',0,tpos]],
[0.226713,0.000951708,0.000951708,0,0,"MILC 11 (stat. err. only)       ",['s','l','g',0,tpos]],
[0.22034 ,0.00227933 ,0.00227933 ,0,0,"ETM 10E (stat. err. only)       ",['s','l','g',0,tpos]]]

datVus2p1=[
# [0.2244  ,0.0014     ,0.0014     ,0,0,"JLQCD 17       ",['^','w','r',0,tpos]],
[0.2259  ,0.0029     ,0.0029     ,0,0,"QCDSF/UKQCD 16 ",['s','g','g',0,tpos]],
[0.2281  ,0.0052     ,0.0052     ,0,0,"BMW 16      ",['s','g','g',0,tpos]],
[0.223500,0.000949   ,0.000949   ,0,0,"RBC/UKQCD 15A",['^','g','g',0,tpos]],
[0.225600,0.000949   ,0.000949   ,0,0,"RBC/UKQCD 14B",['s','g','g',0,tpos]],
[0.223681,0.000809161,0.00127027 ,0,0,"RBC/UKQCD 13   ",['^','l','g',0,tpos]],
[0.224900,0.00333    ,0.00333    ,0,0,"RBC/UKQCD 12   ",['s','l','g',0,tpos]],
[0.224000,0.00106    ,0.00106    ,0,0,"FNAL/MILC 12I   ",['^','g','g',0,tpos]],
# [0.225547,0.00128635 ,0.00128635 ,0,0,"JLQCD 12       ",['^','w','r',0,tpos]],
[0.223637,0.00271382 ,0.00271382 ,0,0,"Laiho 11       ",['s','l','g',0,tpos]],
# [0.224378,0.00148975 ,0.00148975 ,0,0,"JLQCD 11       ",['^','w','r',0,tpos]],
[0.225000,0.00103    ,0.00103    ,0,0,"MILC 10        ",['s','g','g',0,tpos]],
# [0.219335,0.00326018 ,0.00326018 ,0,0,"JLQCD/TWQCD 10 ",['s','w','r',0,tpos]],
[0.223759,0.00461451 ,0.00461451 ,0,0,"RBC/UKQCD 10A  ",['s','l','g',0,tpos]],
# [0.225336,0.00124336 ,0.00149455 ,0,0,"RBC/UKQCD 10   ",['^','w','r',0,tpos]],
[0.225900,0.00170    ,0.00170    ,0,0,"BMW 10         ",['s','g','g',0,tpos]],
# [0.203396,0.0105398  ,0.0105398  ,0,0,"PACS-CS 09     ",['s','w','r',0,tpos]],
# [0.222722,0.00216215 ,0.00216215 ,0,0,"JLQCD/TWQCD 09A (stat. err. only)",['s','w','r',0,tpos]],
[0.224347,0.00118924 ,0.00151653 ,0,0,"MILC 09A       ",['s','l','g',0,tpos]],
[0.224525,0.00125581 ,0.0024076  ,0,0,"MILC 09        ",['s','l','g',0,tpos]],
[0.225598,0.00421489 ,0.00421489 ,0,0,"Aubin 08       ",['s','l','g',0,tpos]],
# [0.226393,0.00365766 ,0.00365766 ,0,0,"PACS-CS 08     ",['s','w','r',0,tpos]],
# [0.223586,0.011394   ,0.011394   ,0,0,"RBC/UKQCD 08   ",['s','w','r',0,tpos]],
# [0.224285,0.00126057 ,0.00126057 ,0,0,"RBC/UKQCD 07   ",['^','w','r',0,tpos]],
[0.226500,0.00143    ,0.00143 ,0,0,"HPQCD/UKQCD 07 ",['s','g','g',0,tpos]],
#[0.221355,0.00199591 ,0.00419271 ,0,0,"NPLQCD 06      ",['s','w','r',0,tpos]],
[0.223059,0.00241463 ,0.00241463 ,0,0,"MILC 04        ",['s','l','g',0,tpos]]]
datVus2=[
# [0.223992,0.00113162 ,0.00113162 ,0,0,"ETM 14D (stat. err. only)",['s','w','r',0,tpos]],
[0.227088,0.00123433 ,0.00123433 ,0,0,"ALPHA 13A     ",['s','l','g',0,tpos]],
# [0.222186,0.0071382  ,0.0071382  ,0,0,"BGR 11         ",['s','w','r',0,tpos]],
[0.226617,0.00149888 ,0.00149888 ,0,0,"ETM 10D (stat. err. only)",['s','l','g',0,tpos]],
[0.226635,0.00169761 ,0.00169761 ,0,0,"ETM 10D (stat. err. only)",['^','l','g',0,tpos]],
[0.226500,0.00205    ,0.00205    ,0,0,"ETM 09A        ",['^','g','g',0,tpos]],
[0.223300,0.00320    ,0.00320    ,0,0,"ETM 09         ",['s','g','g',0,tpos]],
[0.223059,0.00526981 ,0.00526981 ,0,0,"QCDSF/UKQCD 07 ",['s','l','g',0,tpos]],
# [0.224215,0.000624638,0.000624638,0,0,"QCDSF 07 (stat. err. only)",['^','w','r',0,tpos]],
# [0.223450,0.00254975 ,0.00254975 ,0,0,"RBC 06         ",['^','w','r',0,tpos]],
# [0.223681,0.00148108 ,0.00148108 ,0,0,"JLQCD 05       ",['^','w','r',0,tpos]]
]



datVusother1=[
[0.2186,0.0021,0.0021,0,0,"HFLAV 18 $\\tau$ decay",['o','b','b',0,tpos]],
[0.2231,0.0027,0.0027,0,0,"Hudspith 17 $\\tau$ decay",['o','b','b',0,tpos]],
# [0.2208,0.0023,0.0023,0,0,"Hudspith 17 $\\tau$ decay and $e^+e^-$",['o','b','b',0,tpos]]
# [0.2208,0.0039,0.0039,0,0,"Maltman 09 $\\tau$ decay and $e^+e^-$",['o','b','b',0,tpos]],
# [0.2165,0.00264764,0.00264764,0,0,"Gamiz 08 $\\tau$ decay",['o','b','b',0,tpos]]
]

datVusother2=[
[0.225652,0.000907,0.000907,0,0,"PDG 20 nuclear $\\beta$ decay",['o','b','b',0,tpos]]
]
###
tpos2=300
datVud2p1p1=[
[0.974309,0.000197823,0.000197823,0,0,"Miller 20xhy",['s','g','g',0,tpos2]],
[0.974744,0.000136240,0.000136240,0,0,"FNAL/MILC 18",['s','g','g',0,tpos2]],
[0.974320,0.000103   ,0.000103   ,0,0,"FNAL/MILC 17",['s','g','g',0,tpos2]],
[0.974810,0.000250   ,0.0002550  ,0,0,"ETM 16       ",['^','g','g',0,tpos2]],
[0.973880,0.000694   ,0.000694   ,0,0,"ETM 14E      ",['s','g','g',0,tpos2]],
[0.974380,0.000120   ,0.000120   ,0,0,"FNAL/MILC 14A",['s','l','g',0,tpos2]],
[0.974790,0.000200   ,0.000200   ,0,0,"FNAL/MILC 13E",['^','g','g',0,tpos2]],
[0.974833,0.000240872,0.000240872,0,0,"FNAL/MILC 13C  ",['^','l','g',0,tpos2]],
[0.973875,0.000735779,0.000735779,0,0,"ETM 13F        ",['s','l','g',0,tpos2]],
[0.974200,0.000122   ,0.000122   ,0,0,"HPQCD 13A      ",['s','g','g',0,tpos2]],
[0.974365,0.000206917,0.000206917,0,0,"MILC 13A       ",['s','l','g',0,tpos2]],
[0.973953,0.000221544,0.000221544,0,0,"MILC 11        ",['s','l','g',0,tpos2]],
[0.975414,0.000514891,0.000514891,0,0,"ETM 10E        ",['s','l','g',0,tpos2]]]
datVud2p1=[                                                                             
# [0.974490,0.00031    ,0.00031    ,0,0,"JLQCD 17       ",['^','w','r',0,tpos2]],
[0.974130,0.00068    ,0.00068    ,0,0,"QCDSF/UKQCD 16 ",['s','g','g',0,tpos2]],
[0.973630,0.00120    ,0.00120    ,0,0,"BMW 16         ",['s','g','g',0,tpos2]],
[0.974690,0.000212   ,0.000212   ,0,0,"RBC/UKQCD 15A  ",['^','g','g',0,tpos2]],
[0.974210,0.000231   ,0.000231   ,0,0,"RBC/UKQCD 14B  ",['s','g','g',0,tpos2]],
[0.974653,0.000291532,0.000185713,0,0,"RBC/UKQCD 13   ",['^','l','g',0,tpos2]],
[0.974380,0.000766   ,0.000766   ,0,0,"RBC/UKQCD 12   ",['s','l','g',0,tpos2]],
[0.974590,0.000241   ,0.000241   ,0,0,"FNAL/MILC 12I   ",['^','g','g',0,tpos2]],
# [0.974223,0.000297817,0.000297817,0,0,"JLQCD 12       ",['^','w','r',0,tpos2]],
[0.974664,0.000622692,0.000622692,0,0,"Laiho 11       ",['s','l','g',0,tpos2]],
# [0.974493,0.000343021,0.000343021,0,0,"JLQCD 11       ",['^','w','r',0,tpos2]],
[0.974340,0.000237   ,0.000237   ,0,0,"MILC 10        ",['s','g','g',0,tpos2]],
# [0.975641,0.000732928,0.000732928,0,0,"JLQCD/TWQCD 10 ",['s','w','r',0,tpos2]],
[0.974636,0.00105941 ,0.00105941 ,0,0,"RBC/UKQCD 10A  ",['s','l','g',0,tpos2]],
# [0.974272,0.000345677,0.000287581,0,0,"RBC/UKQCD 10   ",['^','w','r',0,tpos2]],
[0.974130,0.000391   ,0.000391   ,0,0,"BMW 10         ",['s','g','g',0,tpos2]],
# [0.979088,0.00218955 ,0.00218955 ,0,0,"PACS-CS 09     ",['s','w','r',0,tpos2]],
# [0.974873,0.000493975,0.000493975,0,0,"JLQCD/TWQCD 09A",['s','w','r',0,tpos2]],
[0.974501,0.000349138,0.000273791,0,0,"MILC 09A       ",['s','l','g',0,tpos2]],
[0.97446 ,0.000554738,0.000289357,0,0,"MILC 09        ",['s','l','g',0,tpos2]],
[0.974212,0.000976044,0.000976044,0,0,"Aubin 08       ",['s','l','g',0,tpos2]],
# [0.974027,0.00085015 ,0.00085015 ,0,0,"PACS-CS 08     ",['s','w','r',0,tpos2]],
# [0.974675,0.00261373 ,0.00261373 ,0,0,"RBC/UKQCD 08   ",['s','w','r',0,tpos2]],
# [0.974515,0.000290127,0.000290127,0,0,"RBC/UKQCD 07   ",['^','w','r',0,tpos2]],
[0.974010,0.000322   ,0.000322   ,0,0,"HPQCD/UKQCD 07 ",['s','g','g',0,tpos2]],
#[0.975184,0.000951696,0.000453051,0,0,"NPLQCD 06      ",['s','w','r',0,tpos2]],
[0.974796,0.000552534,0.000552534,0,0,"MILC 04        ",['s','l','g',0,tpos2]]]
datVud2=[                                                                             
# [0.974582,0.000260076,0.000260076,0,0,"ETM 14D      ",['s','w','r',0,tpos2]],
[0.973866,0.000287831,0.000287831,0,0,"ALPHA 13A    ",['s','l','g',0,tpos2]],
# [0.974995,0.00162669 ,0.00162669 ,0,0,"BGR 11         ",['s','w','r',0,tpos2]],
[0.973975,0.000348754,0.000348754,0,0,"ETM 10D        ",['s','l','g',0,tpos2]],
[0.973971,0.000395024,0.000395024,0,0,"ETM 10D        ",['^','l','g',0,tpos2]],
[0.974010,0.000474   ,0.000474   ,0,0,"ETM 09A        ",['^','g','g',0,tpos2]],
[0.974750,0.000734   ,0.000734   ,0,0,"ETM 09         ",['s','g','g',0,tpos2]],
[0.974796,0.00120587 ,0.00120587 ,0,0,"QCDSF/UKQCD 07 ",['s','l','g',0,tpos2]],
# [0.974531,0.000143728,0.000143728,0,0,"QCDSF 07       ",['^','w','r',0,tpos2]],
# [0.974706,0.000584532,0.000584532,0,0,"RBC 06         ",['^','w','r',0,tpos2]],
# [0.974653,0.000339911,0.000339911,0,0,"JLQCD 05       ",['^','w','r',0,tpos2]]
]



datVudother1=[
[0.97370 ,0.00014 ,0.00014  ,0,0,"Seng 18 nuclear $\\beta$ decay"               ,['o','b','b',0,tpos2]]
]

datVudother2=[
[0.975807,0.00046,0.00046,0,0,"HFAG 18 $\\tau$ decay",['o','b','b',0,tpos2]],
[0.974788,0.000602,0.000602,0,0,"Hudspith 17 $\\tau$ decay",['o','b','b',0,tpos2]],
# [0.975311,0.000483,0.000483,0,0,"Hudspith 17 $\\tau$ decay and $e^+e^-$",['o','b','b',0,tpos2]]
#[0.975311,0.000883,0.000883,0,0,"M 09 Maltman 09 $\\tau$ decay and $e^+e^-$",['o','b','b',0,tpos2]],
#[0.9763  ,0.0006  ,0.0006   ,0,0,"G 08 Gamiz 08 $\\tau$ decay"		     ,['o','b','b',0,tpos2]]
]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
              # in the case where no average is provided for a given data set
              # just replace the central value by "NaN"
              # 
              # with respect to the above there is another column at the end specifying
              # the type of FLAG-band
              # 0 -> grey area bounded by solid black lines
              # 1 -> dashed lines marking the edge of the interval
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
 [0.2256 ,0.0019 ,0.0019 ,0.000,0.000, "FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
 [0.2249 ,0.0005 ,0.0005 ,0.000,0.000, "FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
 [0.2248 ,0.0007 ,0.0007 ,0.000,0.000, "FLAG average for $\\rm N_f=2+1+1$", ['s','k','k',0,tpos],0]
]



# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datVusother2,datVusother1,datVus2,datVus2p1,datVus2p1p1]
################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
#MULTIPLE=1
import matplotlib.pyplot as plt
fig=plt.figure( )
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
execfile('FLAGplot.py')
plotnamestring	= "Vusud"			# filename for plot

titlestring	= "$\\rm |V_{ud}|$"			# plot title
xlimits		= [.971,.978]			# plot's xlimits
plotxticks	= ([[.973,.975]])# locations for x-ticks

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval

 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos2],0], 
 [np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos2],0], 
 [0.97423,0.00044 ,0.00044 ,0.000,0.000, "FLAG average for  $\\rm N_f=2$",['s','k','k',0,tpos2],0],
 [0.97438,0.00012 ,0.00012 ,0.000,0.000, "FLAG average for $N_f=2+1$",['s','k','k',0,tpos2],0], 
 [0.97440,0.00017 ,0.00017 ,0.000,0.000, "FLAg average for $\\rm N_f=2+1+1$", ['s','k','k',0,tpos2],0]
]
yaxisstrings	= [				# x,y-location for y-strings
		   [+0,26.0,"$\\rm N_f=2+1+1$"],
		   [+0,14.0,"$\\rm N_f=2+1$"],
		   [+0,2.0,"$\\rm N_f=2$"]
		    ]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datVudother1,datVudother2,datVud2,datVud2p1,datVud2p1p1]

#ax.spines["right"].set_color('none')

ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
logo		= 'none'			# either of 'upper left'
PRELIM=0
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

