# FLAG plot for fK and fpi
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= " $\\rm f_{\pi^\pm}$"			# plot title
plotnamestring	= "fKandfPi"			# filename for plot
plotxticks	= ([[120,125,130]])# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none

yaxisstrings	= [				# x,y-location for y-strings
		   [+112,29.0,"$\\rm N_f=2+1+1$"],
		   [+112,15.0,"$\\rm N_f=2+1$"],
		   [+112,1.5,"$\\rm N_f=2$"]
		    ]
		    
LABEL=1		    	    
xaxisstrings	= [				# x,y-location for x-strings
#		   [+165,-3.1,"MeV"] 	 		    
                   ]		
		    
xlimits		= [115,140]			# plot's xlimits
logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos   		= 545				# x-position for the data labels 
					        # move data labels of l.h.s side
						# plot to nirvana
tpos2		= 130				# x-position for the data labels
						# here a reasonable choice
REV    		= 1				# reverse order of data points
PRELIM 		= 0				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
REMOVEWHITESPACE=0						# one figure
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

################################################################################
# data for l.h.s. scatter plot
################################################################################
#-- Nf=4 --
datfpiNf4=[
[0.0,0.0,0.0,0.0,0.0               ,"ETM 14E",       ['s','l','g',0,tpos]],
[0.0,0.0,0.0,0.0,0.0               ,"FNAL/MILC 14A", ['s','l','g',0,tpos]],
[0.0,0.0,0.0,0.0,0.0               ,"HPQCD 13A",       ['s','l','g',0,tpos]],
[0.0,0.0,0.0,0.0,0.0               ,"MILC 13A",        ['s','l','g',0,tpos]],
[0.0,0.0,0.0,0.0,0.0               ,"ETM 10E",         ['s','l','g',0,tpos]]]

#-- Nf=3 --
datfpiNf3=[
[125.7 ,0.  ,0.  ,7.4    ,7.4    ," JLQCD 15C    ",['s','l','g',0,tpos]],
[130.19,0.   ,0.  ,0.89  ,0.89   ," RBC/UKQCD 14B  ",['s','g','g',0,tpos]],
[127   ,3.   ,3.  ,4.2426,4.2426 ," RBC/UKQCD 12A  ",['s','l','g',0,tpos]],       
[130.53,0.87,0.87,2.27308,2.27308," Laiho 11       ",['s','l','g',0,tpos]],       
[129.2 ,0.4 ,0.4 ,1.45602,1.45602," MILC 10        ",['s','g','g',0,tpos]],                     
[0.    ,0.  ,0.  ,0.     ,0.     ," MILC 10        ",['s','l','g',0,tpos]],  
[118.5 ,0.  ,0.  ,3.6    ,3.6    ," JLQCD/TWQCD 10 ",['s','w','r',0,tpos]],              
[124   ,2.  ,2.  ,5.38516,5.38516," RBC/UKQCD 10A  ",['s','l','g',0,tpos]],            
#[124.1 ,8.5 ,8.5 ,8.5376 ,8.5376 ," PACS-CS 09     ",['s','w','r',0,tpos]],             
#[0.    ,0.  ,0.  ,0.     ,0.     ," JLQCD/TWQCD 09A",['s','w','r',0,tpos]],        
[128.  ,0.3 ,0.3 ,2.91548,2.91548," MILC 09A       ",['s','l','g',0,tpos]],        
[0.    ,0.  ,0.  ,0.     ,0.     ," MILC 09A       ",['s','l','g',0,tpos]],        
[128.3 ,0.5 ,0.5 ,3.43657,2.45153," MILC 09        ",['s','l','g',0,tpos]],  
[0.    ,0.  ,0.  ,0.     ,0.     ," MILC 09        ",['s','l','g',0,tpos]],        
[129.1 ,1.9 ,1.9 ,4.42832,4.42832," Aubin 08       ",['s','l','g',0,tpos]],        
#[134.  ,0.  ,0.  ,4.2    ,4.2    ," PACS-CS 08,08A ",['s','w','r',0,tpos]],  
[124.1 ,3.6 ,3.6 ,7.78267,7.78267," RBC/UKQCD 08   ",['s','w','r',0,tpos]],        
[132   ,0.  ,0.  ,2.     ,2.     ," HPQCD/UKQCD 07 ",['s','g','g',0,tpos]],                 
[129.5 ,0.9 ,0.9 ,3.61386,3.61386," MILC 04        ",['s','l','g',0,tpos]]]
#-- Nf=2 --                      
datfpiNf2=[                      
[0.    ,0.  ,0.  ,0.     ,0.     ," ETM 14D      ",['s','l','g',0,tpos]],
#[127.3 ,1.7 ,1.7 ,2.62488,2.62488," TWQCD 11       ",['s','w','r',0,tpos]],                    
[0.    ,0.  ,0.  ,0.     ,0.     ," ETM 09         ",['s','l','g',0,tpos]]]  
#[119.6 ,3.  ,3.  ,3.16228,7.15891," JLQCD/TWQCD 08A",['s','w','r',0,tpos]]]
#datfpiother=[
#[130.41,0.03,0.03,0.20224,0.20224," PDG"                 ,['^','k','k',0,tpos]]]   
 
################################################################################
# data for r.h.s. scatter plot
################################################################################
#-- Nf=4 --
datfKNf4=[
[154.4  ,1.5     ,1.5     ,1.98    ,1.98    ," ETM 14E",      ['s','g','g',0,tpos2]],
[155.92 ,0.13    ,0.13    ,0.2642  ,0.3640  ," FNAL/MILC 14A",['s','g','g',0,tpos2]],
[155.37 ,0.20    ,0.20    ,0.34    ,0.34    ," HPQCD 13A",      ['s','g','g',0,tpos2]],
[155.80 ,0.34    ,0.34    ,0.6381  ,0.6381  ," MILC 13A",       ['s','l','g',0,tpos2]],
[159.6  ,2.0     ,2.0     ,0.0     ,0.0     ," ETM 10E",        ['s','l','g',0,tpos2]]]

#-- Nf=3 --
datfKNf3=[
[0.     ,0.      ,0.      ,0.      ,0.      ," JLQCD 15C    ",['s','l','g',0,tpos2]],
[155.18 ,0.      ,0.      ,0.89    ,0.89    ," RBC/UKQCD 14B  ",['s','g','g',0,tpos2]],
[151.74 ,3.0     ,3.0     ,3.61    ,3.61    ," RBC/UKQCD 12A  ",['s','l','g',0,tpos2]],       
[156.8  ,1.0     ,1.0     ,1.97    ,1.97    ," Laiho 11       ",['s','l','g',0,tpos2]],
[0.     ,0.      ,0.      ,0.      ,0.      ," MILC 10        ",['s','l','g',0,tpos2]],              
[156.1  ,0.40    ,0.40    ,0.985   ,0.721   ," MILC 10        ",['s','g','g',0,tpos2]],  
[145.65 ,2.7     ,2.7     ,2.7     ,2.7     ," JLQCD/TWQCD 10 ",['s','w','r',0,tpos2]],              
[148.80 ,2.      ,2.      ,3.60555 ,3.60555 ," RBC/UKQCD 10A  ",['s','l','g',0,tpos2]],            
#[165.0  ,3.4     ,3.4     ,3.5735  ,3.5735  ," PACS-CS 09     ",['s','w','r',0,tpos2]],             
#[156.9  ,5.5     ,0.5     ,5.5     ,5.5     ," JLQCD/TWQCD 09A",['s','w','r',0,tpos2]],        
[153.8  ,0.30    ,0.30    ,3.91    ,3.91    ," MILC 09A       ",['s','l','g',0,tpos2]],        
[156.2  ,0.30    ,0.30    ,1.14    ,1.14    ," MILC 09A       ",['s','l','g',0,tpos2]],        
[154.3  ,0.40    ,0.40    ,3.423   ,2.138   ," MILC 09        ",['s','l','g',0,tpos2]],  
[156.5  ,0.40    ,0.40    ,2.729   ,1.078   ," MILC 09        ",['s','l','g',0,tpos2]],  
[153.9  ,1.7     ,1.7     ,4.71699 ,4.71699 ," Aubin 08       ",['s','l','g',0,tpos2]],        
#[159.0  ,3.1     ,3.1     ,3.1     ,3.1     ," PACS-CS 08,08A ",['s','w','r',0,tpos2]],  
[149.4  ,3.6     ,3.6     ,7.25603 ,7.25603 ," RBC/UKQCD 08   ",['s','w','r',0,tpos2]],        
[156.7  ,0.      ,0.      ,2.      ,2.      ," HPQCD/UKQCD 07 ",['s','g','g',0,tpos2]], 
[156.6  ,1.0     ,1.0     ,3.736   ,3.736   ," MILC 04        ",['s','l','g',0,tpos2]]]
#-- Nf=2 --
datfKNf2=[
[153.3,0.0,0.0,7.5,7.5   	," ETM 14D"            ,['s','w','r',0,tpos2]],
#[0.0,0.0,0.0,0.0,0.0            ," TWQCD 11"             ,['s','w','r',0,tpos2]],                    
[157.5,0.8,0.8,2.41,2.41 	," ETM 09"               ,['s','g','g',0,tpos2]]] 
#[0.0,0.0,0.0,0.0,0.0            ," JLQCD/TWQCD 08A"      ,['s','w','r',0,tpos2]]]

#datfKother=[
#[156.2,0.2,0.2,0.7   ,0.7       ," PDG"                  ,['^','k','k',0,tpos2]]],   

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.



# first define FLAGaverage for l.h.s. plot, the one for the r.h.s. one follows below
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
 #[np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0], 
 [0.0    ,0.000,0.000,0.000,0.000,  " FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
 [130.2  ,0.8  ,0.8  ,0.000,0.000,  " FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0], 
 [0.0    ,0.000,0.000,0.000,0.000,  " FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
#datasets=[datfpiother,datfpiNf2,datfpiNf3,datfpiNf4]
datasets=[datfpiNf2,datfpiNf3,datfpiNf4]

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 


# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
execfile('FLAGplot.py')


# now some definitions for the r.h.s. plot
plotnamestring	= "fKandfPi"			# filename for plot

titlestring	= "         $\\rm f_{K^\pm}$"	# plot title
xlimits		= [135,165]			# plot's xlimits
plotxticks	= ([[150,155,160]])# locations for x-ticks

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
 #[np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$" ,['s','k','k',0,tpos2],0], 
 [157.5,2.4,2.4,0.000,0.000," FLAG average for $\\rm N_f=2$"    ,['s','k','k',0,tpos2],0],
 [155.7,0.7,0.7,0.000,0.000," FLAG average for $\\rm N_f=2+1$"  ,['s','k','k',0,tpos2],0], 
 [155.7,0.3,0.3,0.000,0.000," FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos2],0] 
]
yaxisstrings	= [				# x,y-location for y-strings
		   [+0,25.0,"$\\rm N_f=2+1+1$"],
		   [+0,14.0,"$\\rm N_f=2+1$"],
		   [+0,2.0,"$\\rm N_f=2$"]
		    ]


xaxisstrings	= [				# x,y-location for x-strings
		   [+165.,-3.1,"MeV"] 	 		    
                   ]		   

# The follwing list should contain the list names of the previous DATA-blocks
#datasets=[datfKother,datfKNf2,datfKNf3,datfKNf4]
datasets=[datfKNf2,datfKNf3,datfKNf4]

# do the r.h.s. plot
ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
logo		= 'none'			# either of 'upper left'
execfile('FLAGplot.py')

################################################################################
# We are done
################################################################################


# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

